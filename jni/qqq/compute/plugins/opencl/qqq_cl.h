/*
 * qqq_cl.h
 *
 *  Created on: Jun 25, 2014
 *      Author: miel
 */

#ifndef QQQ_CL_H_
#define QQQ_CL_H_

#include <qqq_platform.h>

#ifdef __APPLE__
#include "OpenCL/opencl.h"
#else
#include <CL/cl.h>
#include <CL/cl_ext.h>
#include <CL/cl_platform.h>
#endif

#define APISTRING_OCL	"OPENCL"

typedef struct {
  cl_context ctx;
  cl_command_queue queue;
  cl_platform_id* platform_id;
  cl_device_id*   device_id;
  cl_uint num_platforms;
  cl_uint num_devices;
} qqq_cl;



#endif /* QQQ_CL_H_ */
