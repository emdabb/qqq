/**
 * @file cl_image_format.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 24, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef CL_IMAGE_FORMAT_H_
#define CL_IMAGE_FORMAT_H_

#include "qqq_cl.h"

namespace qqq {
  struct ocl_image_format {
    static const cl_image_format RGB8  = { CL_RGB,  CL_UNORM_INT8 };
    static const cl_image_format RGBA8 = { CL_RGBA, CL_UNORM_INT8 };
    static const cl_image_format BGRA8 = { CL_BGRA, CL_UNORM_INT8 };

  };
}



#endif /* CL_IMAGE_FORMAT_H_ */
