/*
 * compute_interop_buffer.h
 *
 *  Created on: Jun 28, 2014
 *      Author: miel
 */

#ifndef COMPUTE_INTEROP_BUFFER_H_
#define COMPUTE_INTEROP_BUFFER_H_

#include <core/lockable.h>

namespace qqq {

struct compute_interop_buffer_impl : public ILockable {
  compute_interop_buffer_impl() {

  }
};

class compute_interop_buffer {
public:
};

}



#endif /* COMPUTE_INTEROP_BUFFER_H_ */
