/*
 * ocl_gl_context.cpp
 *
 *  Created on: Jun 25, 2014
 *      Author: miel
 */

#include <qqq.h>
#include <compute/platform/opencl/qqq_cl.h>
#include <compute/compute_context.h>
#include <CL/cl_gl.h>
#include <CL/cl_gl_ext.h>

namespace qqq {
namespace impl {

class ocl_gl_context : public IComputeContext {
public:
  virtual bool create(const int) {
    return false;
  }

  virtual const char* api() const {
    return APISTRING_OCL;
  }
};

}

QQQ_C_API void new_compute_context(IComputeContext** ppContext) {
  *ppContext = new impl::ocl_gl_context;
}

}


