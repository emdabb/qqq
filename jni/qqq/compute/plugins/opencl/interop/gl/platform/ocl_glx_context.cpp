/*
 * ocl_glx_context.cpp
 *
 *  Created on: Jun 28, 2014
 *      Author: miel
 */

#include <qqq.h>
#include <compute/platform/opencl/qqq_cl.h>
#include <gfx/graphics_context.h>
#include <core/debug_log.h>
#include <compute/platform/opencl/plugins/interop/gl/qqq_cl_gl.h>
#include <GL/glx.h>

namespace qqq {

class ocl_glx_context {
  qqq_cl cl;
  IGraphicsContext* ctx;
public:
  ocl_glx_context() : ctx(NULL) {

  }

  virtual ~ocl_glx_context() {

  }

  void create() {
    DEBUG_METHOD();
    cl_int err = 0;
    clGetPlatformIDs(0, NULL, &cl.num_platforms);
    cl.platform_id = (cl_platform_id*)malloc(cl.num_platforms * sizeof(cl_platform_id));
    clGetPlatformIDs(cl.num_platforms, cl.platform_id, NULL);

    clGetDeviceIDs(cl.platform_id[0], CL_DEVICE_TYPE_GPU, 0, NULL, &cl.num_devices);
    cl.device_id = (cl_device_id*)malloc(cl.num_devices * sizeof(cl_device_id));
    clGetDeviceIDs(cl.platform_id[0], CL_DEVICE_TYPE_GPU, cl.num_devices, cl.device_id, NULL);


    synchronized(ctx) {
      cl_context_properties props[] = {
        CL_CONTEXT_PLATFORM, (cl_context_properties)cl.platform_id[0],
        CL_GL_CONTEXT_KHR,  (cl_context_properties)glXGetCurrentContext(),
        CL_GLX_DISPLAY_KHR, (cl_context_properties)glXGetCurrentDisplay(),
        0
      };
      cl.ctx = clCreateContext(props, 1, &cl.device_id[0], NULL, NULL, &err);
    }
  }
};

QQQ_C_API void new_ocl_glx_context(ocl_glx_context** ppcontext) {
  *ppcontext = new ocl_glx_context;
}

}


