/*
 * qqq_cl_gl.h
 *
 *  Created on: Jun 28, 2014
 *      Author: miel
 */

#ifndef QQQ_CL_GL_H_
#define QQQ_CL_GL_H_

#include <compute/plugins/opencl/qqq_cl.h>

#include <CL/cl_gl.h>
#include <CL/cl_gl_ext.h>

#include <GL/gl.h>
#include <GL/glext.h>



#endif /* QQQ_CL_GL_H_ */
