/*
 * ocl_gl_interop_texture2d.cpp
 *
 *  Created on: Jun 28, 2014
 *      Author: miel
 */

#include "qqq_cl_gl.h"

namespace qqq {
class ocl_gl_interop_texture2d {
public:
  void create(cl_context ctx) {
    cl_int err = CL_SUCCESS;
    GLuint texid;
    glGetIntegerv(GL_TEXTURE_2D_BINDING_EXT, (GLint*)&texid);
    clCreateFromGLTexture2D(ctx, CL_MEM_WRITE_ONLY, GL_TEXTURE_2D, 0, texid, &err);
  }
};
}
