/*
 * ocl_gl_interop_buffer.cpp
 *
 *  Created on: Jun 28, 2014
 *      Author: miel
 */

#include <compute/plugins/opencl/qqq_cl.h>
#include <compute/plugins/opencl/interop/compute_interop_buffer.h>
#include "qqq_cl_gl.h"

namespace qqq {
namespace impl {
class ocl_gl_interop_buffer : public compute_interop_buffer_impl {
  //buffer_object* obj;
  cl_mem mem;
public:

  virtual void create() {
    //clCreateFromGLTexture2D(context->handle, CL_MEM_WRITE_ONLY, GL_TEXTURE_2D, 0, in->texid, &err);
  }
  virtual bool lock() {
    //obj->get_impl().lock();

    cl_int err = CL_SUCCESS;
    //CLint err = clEnqueueAcquireGLObjects(cl.queue, 1,  &pbo.mem, 0, 0, NULL);
    return CL_SUCCESS == err;
  }
};

}
}

