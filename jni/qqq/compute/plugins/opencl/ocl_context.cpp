/**
 * @file ocl_context.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 24, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include "qqq_cl.h"
#include <compute/compute_context.h>
#include <compute/compute_program.h>
#include <compute/compute_kernel.h>
#include <core/debug_log.h>
#include <core/factory.h>
#include <stdexcept>

#define CL_OK	CL_SUCCESS

#define DEBUG_CL_DEVICE_STRING(dev, x) \
{\
  char x##_VALUE[2048];\
  clGetDeviceInfo(dev, x, sizeof(x##_VALUE), x##_VALUE, NULL);\
  DEBUG_VALUE_AND_TYPE_OF(x##_VALUE);\
}

namespace qqq {

struct unknown_device_type : public std::runtime_error {
  unknown_device_type() : std::runtime_error("Unknown device type.") {}
  virtual ~unknown_device_type() throw() {}
};

QQQ_C_API compute_program* new_compute_program();
QQQ_C_API compute_kernel*  new_compute_kernel();

namespace impl {

class ocl_context : public IComputeContext {
  cl_platform_id*  platformID;
  cl_device_id* 	 deviceID;
  cl_context 		 ctx;
  cl_uint 		 num_platforms;
  cl_uint 		 num_devices;
  cl_command_queue queue;

public:
  ocl_context() : platformID(NULL), deviceID(NULL), ctx(NULL), num_platforms(0), num_devices(0), queue(NULL) {

  }

  virtual ~ocl_context() {

  }

  void register_factories() {
    Factory<compute_program>::instance().register_class(APISTRING_OCL, &new_compute_program);
    Factory<compute_kernel>::instance().register_class(APISTRING_OCL, &new_compute_kernel);
  }

  void init_platforms() {
    DEBUG_METHOD();
    clGetPlatformIDs(0, NULL, &num_platforms);
    platformID = new cl_platform_id[num_platforms];
    clGetPlatformIDs(num_platforms, platformID, NULL);
    DEBUG_VALUE_AND_TYPE_OF(num_platforms);
  }

  void init_devices() {
    DEBUG_METHOD();
    cl_device_type cltype = CL_DEVICE_TYPE_ALL;
    clGetDeviceIDs(platformID[0], cltype, 0, NULL, &num_devices);
    deviceID = new cl_device_id[num_devices];
    clGetDeviceIDs(platformID[0], cltype, num_devices, deviceID, NULL);
    DEBUG_VALUE_AND_TYPE_OF(num_devices);
  }

  void init_context() {
    DEBUG_METHOD();
    cl_int err = CL_OK;
    cl_context_properties props[] = {
      CL_CONTEXT_PLATFORM, (cl_context_properties)platformID[0],
      0
    };
    ctx = clCreateContext(props, 1, &deviceID[0], NULL, NULL, &err);
  }

  virtual bool create(const int type) {
    DEBUG_METHOD();
    init_platforms();
    init_devices();
    init_context();
    register_factories();
    DEBUG_CL_DEVICE_STRING(deviceID[0], CL_DEVICE_VENDOR);
    DEBUG_CL_DEVICE_STRING(deviceID[0], CL_DEVICE_EXTENSIONS);
    return true;
  }

  const char* api() const {
    return APISTRING_OCL;
  }
};
}
#if 0
QQQ_C_API void new_compute_context(IComputeContext** ppContext) {
  *ppContext = new impl::ocl_context;
}
#endif
}
