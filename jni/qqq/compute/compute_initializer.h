/*
 * compute_initializer.h
 *
 *  Created on: Jun 25, 2014
 *      Author: miel
 */

#ifndef COMPUTE_INITIALIZER_H_
#define COMPUTE_INITIALIZER_H_

namespace qqq {
struct compute_initializer {
  bool has_interop;
  int  device_type;
  IGraphicsContext* with;
};
}



#endif /* COMPUTE_INITIALIZER_H_ */
