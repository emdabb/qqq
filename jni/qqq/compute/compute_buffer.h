/**
 * @file compute_buffer.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 25, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef COMPUTE_BUFFER_H_
#define COMPUTE_BUFFER_H_

namespace qqq {

template <typename T, typename TRAITS = std::char_traits<T> >
class compute_buffer : public std::basic_streambuf<T, TRAITS> {
public:
  compute_buffer() {

  }
  virtual ~compute_buffer() {

  }
};

template <typename T, typename TRAITS = std::char_traits<T> >
class compute_stream : public std::basic_iostream<T, TRAITS> {
public:
  compute_stream() {

  }

  virtual ~compute_stream() {

  }
};


}



#endif /* COMPUTE_BUFFER_H_ */
