/**
 * @file compute_context.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 24, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef COMPUTE_CONTEXT_H_
#define COMPUTE_CONTEXT_H_

#include <qqq.h>

namespace qqq {

struct device_type {
  enum {
    E_DEVICETYPE_CPU,
    E_DEVICETYPE_GPU,
    E_DEVICETYPE_ACCEL,
    E_DEVICETYPE_DONTCARE,
    E_DEVICETYPE_UNKNOWN
  };
};

struct IComputeContext {
  virtual ~IComputeContext() {}
  virtual bool create(const int) = 0;
  virtual const char* api() const = 0;
};

typedef void(QQQ_CALL *PFNCREATECOMPUTECONTEXT)(IComputeContext**);
QQQ_C_API void QQQ_CALL new_compute_context(IComputeContext**);

}


#endif /* COMPUTE_CONTEXT_H_ */
