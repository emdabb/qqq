#ifndef _QQQ_CAPI_H
#define _QQQ_CAPI_H

#include <qqq_platform.h>

#ifdef __cplusplus
#define QQQ_C_API extern "C"
#else
#define QQQ_C_API extern
#endif

#ifndef QQQ_CALL
#   if defined(__WIN32__)
#       define QQQ_CALL __cdecl
#   else
#       define QQQ_CALL
#   endif
#endif

#endif
