/*
 * fixed.h
 *
 *  Created on: Jun 3, 2014
 *      Author: miel
 */

#ifndef FIXED_H_
#define FIXED_H_

#include <stdint.h>
#include <limits>
#include <qqq_types.h>

namespace qqq {

template<typename T, uint8_t I, uint8_t F = std::numeric_limits<T>::digits - I>
struct fixed {
  template<unsigned long long P, typename Q = void>
  struct pow2 {
    static const long long value = 2 * pow2<P - 1, Q>::value;
  };

  template<typename Q>
  struct pow2<0, Q> {
    static const long long value = 1;
  };

  template<unsigned long N, typename P = void>
  struct factorial {
    static const unsigned long value = N * factorial<N - 1, P>::value;
  };

  template<typename P>
  struct factorial<1, P> {
    static const unsigned long value = 1;
  };



// no 128-bit int types?
//		template <typename U>
//		struct promote_type<signed long, U> { typedef signed long long type; };
//
//		template <typename U>
//		struct promote_type<unsigned long, U> { typedef unsigned long long type; };

  T _packed_data;
  static const uint8_t INTEGER_BITS = I;
  static const uint8_t FRACTIONAL_BITS = F;
  static const fixed ONE;
  static const fixed PI;
  static const fixed TWO_PI;
  static const fixed PI_OVER_4;
  typedef typename promote_type<T>::type promoted_t;

  fixed() {
  }
  template<typename B>
  fixed(B val) :
      _packed_data((T) val << F) {
  }
  fixed(bool val) :
      _packed_data((T) val * pow2<F>::value) {
  }
  fixed(float val) :
      _packed_data((T) (val * pow2<F>::value + (val >= 0.f ? .5f : -.5f))) {
  }
  fixed(double val) :
      _packed_data((T) (val * pow2<F>::value + (val >= 0.0 ? .5 : -0.5))) {
  }
  fixed(const fixed& val) :
      _packed_data(val._packed_data) {
  }
  ~fixed() {
  }

  template<char I2, char F2>
  fixed(const fixed<T, I2, F2>& val) {
    char d0 = I - I2;
    char d1 = I2 - I;
    if (d0 > 0) {
      _packed_data >>= d0;
    }
    if (d1 > 0) {
      _packed_data <<= d1;
    }
  }

  void swap(fixed& val) {
    T c = _packed_data;
    _packed_data = val._packed_data;
    val._packed_data = c;
  }

  fixed& operator =(const fixed& val) {
    fixed copy(val);
    swap(copy);
    return *this;
  }

  bool operator >(const fixed& val) {
    return _packed_data > val._packed_data;
  }

  bool operator <(const fixed& val) {
    return _packed_data < val._packed_data;
  }

  bool operator !() const {
    return _packed_data == 0;
  }

  fixed operator -() const {
    fixed res;
    res._packed_data = -_packed_data;
    return res;
  }

  fixed& operator ++() {
    _packed_data += ONE;
    return *this;
  }

  fixed& operator --() {
    _packed_data -= ONE;
    return *this;
  }

  fixed operator *(const fixed& val) const {
    fixed copy(*this);
    copy *= val;
    return copy;
  }

  fixed& operator *=(const fixed& val) {
    promoted_t v0 = static_cast<promoted_t>(_packed_data);
    _packed_data = (v0 * val._packed_data) >> FRACTIONAL_BITS;
    return *this;
  }

  fixed operator /(const fixed& val) const {
    fixed copy(*this);
    copy /= val;
    return copy;
  }

  fixed& operator /=(const fixed& val) {
    promoted_t v0 = static_cast<promoted_t>(_packed_data)
        << FRACTIONAL_BITS;
    _packed_data = v0 / val._packed_data;
    return *this;
  }

  fixed& operator +=(const fixed& val) {
    _packed_data += val._packed_data;
    return *this;
  }

  fixed operator +(const fixed& val) const {
    fixed copy(*this);
    copy += val;
    return copy;
  }

  fixed& operator -=(const fixed& val) {
    _packed_data -= val._packed_data;
    return *this;
  }

  fixed operator -(const fixed& val) const {
    fixed copy(*this);
    copy -= val;
    return copy;
  }

  operator char() const {
    return (char) (_packed_data >> F);
  }
  operator signed char() const {
    return (signed char) (_packed_data >> F);
  }
  operator unsigned char() const {
    return (unsigned char) (_packed_data >> F);
  }
  operator short() const {
    return (short) (_packed_data >> F);
  }
  operator unsigned short() const {
    return (unsigned short) (_packed_data >> F);
  }
  operator int() const {
    return (int) (_packed_data >> F);
  }
  operator unsigned int() const {
    return (unsigned int) (_packed_data >> F);
  }
  operator long() const {
    return (long) (_packed_data >> F);
  }
  operator unsigned long() const {
    return (unsigned long) (_packed_data >> F);
  }
  operator long long() const {
    return (long long) (_packed_data >> F);
  }
  operator bool() const {
    return (bool) _packed_data;
  }
  operator float() const {
    return (float) _packed_data / pow2<F>::value;
  }
  operator double() const {
    return (double) _packed_data / pow2<F>::value;
  }
  operator long double() const {
    return (long double) _packed_data / pow2<F>::value;
  }

  static fixed sqrt(const fixed& x) {
    if (x._packed_data < 0) {
      //errno = EDOM;
      return 0;
    }
    promoted_t op = static_cast<promoted_t>(x._packed_data) << (I - 1);

    promoted_t res = 0;
    promoted_t one = (promoted_t) 1
        << (std::numeric_limits<promoted_t>::digits - 1);

    while (one > op)
      one >>= 2;

    while (one != 0) {
      if (op >= res + one) {
        op = op - (res + one);
        res = res + (one << 1);
      }
      res >>= 1;
      one >>= 2;
    }

    fixed root;
    root._packed_data = static_cast<T>(res);
    return root;
  }
  /**
   * Calculates the absolute value.
   *
   * The fabs function computes the absolute value of its argument.
   * @param x
   * @return
   */
  inline static fixed fabs(fixed const& x) {
    return x < 0 ? -x : x;
  }
  /**
   *  Calculates the ceiling value.
   *
   *  The ceil function computes the smallest integral value not less than
   *  its argument.
   *
   * @param x
   * @return The smallest integral value not less than the argument.
   */
  inline static fixed ceil(fixed const& x) {
    fixed result;
    result = x & ~(pow2<F>::value - 1);
    return result + (x & (pow2<F>::value - 1) ? 1 : 0);
  }
  /**
   *  Calculates the floor.
   *
   *  The floor function computes the largest integral value not greater than
   *  its argument.
   *
   *  /return The largest integral value not greater than the argument.
   * @param x
   * @return
   */
  inline static fixed floor(fixed const& x) {
    fixed result;
    result = x & ~(pow2<F>::value - 1);
    return result;
  }
  /**
   *  Calculates the remainder.
   *
   *  The fmod function computes the fixed point remainder of x/y.
   *
   *  /return The fixed point remainder of x/y.
   * @param x
   * @param y
   * @return
   */
  inline static fixed fmod(fixed const& x, fixed const& y) {
    fixed result;
    result = x % y;
    return result;
  }
  /**
   *  Split in integer and fraction parts.
   *
   *  The modf function breaks the argument into integer and fraction parts,
   *  each of which has the same sign as the argument. It stores the integer
   *  part in the object pointed to by ptr.
   *
   *  /return The signed fractional part of x/y.
   * @param x
   * @param ptr
   * @return
   */
  inline static fixed modf(fixed const& x, fixed * ptr) {
    fixed integer;
    integer = x & ~(pow2<F>::value - 1);
    *ptr = x < fixed(0) ? integer + fixed(1) : integer;

    fixed fraction;
    fraction = x & (pow2<F>::value - 1);

    return x < fixed(0) ? -fraction : fraction;
  }
  /**
   *  Calculates the exponential.
   *
   *  The function computes the exponential function of x. The algorithm uses
   *  the identity e^(a+b) = e^a * e^b.
   *
   *  /return The exponential of the argument.
   * @param x
   * @return
   */
  inline static fixed exp(
  /// The argument to the exp function.
      fixed x) {
    // a[x] = exp( (1/2) ^ x ), x: [0 ... 31]
    static const fixed a[] =
        { 1.64872127070012814684865078781,
            1.28402541668774148407342056806,
            1.13314845306682631682900722781,
            1.06449445891785942956339059464,
            1.03174340749910267093874781528,
            1.01574770858668574745853507208,
            1.00784309720644797769345355976,
            1.00391388933834757344360960390,
            1.00195503359100281204651889805,
            1.00097703949241653524284529261,
            1.00048840047869447312617362381,
            1.00024417042974785493700523392,
            1.00012207776338377107650351967,
            1.00006103701893304542177912060,
            1.00003051804379102429545128481,
            1.00001525890547841394814004262,
            1.00000762942363515447174318433,
            1.00000381470454159186605078771,
            1.00000190735045180306002872525,
            1.00000095367477115374544678825,
            1.00000047683727188998079165439,
            1.00000023841860752327418915867,
            1.00000011920929665620888994533,
            1.00000005960464655174749969329,
            1.00000002980232283178452676169,
            1.00000001490116130486995926397,
            1.00000000745058062467940380956,
            1.00000000372529030540080797502,
            1.00000000186264515096568050830,
            1.00000000093132257504915938475,
            1.00000000046566128741615947508 };

    static const fixed e(2.718281828459045);

    fixed y = ONE;    //(1);
    for (int i = F - 1; i >= 0; --i) {
      if (!(x & 1 << i))
        y *= a[F - i - 1];
    }

    int x_int = (int) (floor(x));
    if (x_int < 0) {
      for (int i = 1; i <= -x_int; ++i)
        y /= e;
    } else {
      for (int i = 1; i <= x_int; ++i)
        y *= e;
    }

    return y;
  }

  inline static fixed cos(fixed const& x) {
    fixed x_ = fixed::fmod(x, TWO_PI);
    if (x_ > PI)
      x_ -= TWO_PI;

    fixed xx = FX_MUL(x_, x_);

    static const fixed s0 = ONE / factorial<6>::value;
    static const fixed s1 = ONE / factorial<4>::value;
    static const fixed s2 = ONE / factorial<2>::value;

    return -xx * s0 + s1 * xx - s2 * xx + ONE;
  }

  inline static fixed sin(fixed const& x) {
    fixed x_ = fmod(x, TWO_PI);
    //        if (x_ > PI)
    //            x_ -= TWO_PI;
    x_ -= (x_ > PI) & TWO_PI;

    fixed xx = FX_MUL(x_, x_);

    static const fixed s0 = ONE / factorial<7>::value;
    static const fixed s1 = ONE / factorial<5>::value;
    static const fixed s2 = ONE / factorial<3>::value;

    return -xx * s0 + s1 * xx - s2 * xx + ONE * x_;
  }

  inline static fixed atan2(fixed const& y, fixed const& x) {
    fixed absy;
    absy = (y ^ (y >> 31)) - (y >> 31) + 1; // Add an epsilon to avoid div zero

    static const fixed mn0 = .1963;
    static const fixed mn1 = .9817;

    fixed angle;
    if (x >= 0) {
      fixed r = (x - absy) / (x + absy);
      fixed r3 = r * r * r;
      angle = mn0 * r3 - mn1 * r + PI_OVER_4;
    } else {
      fixed r = (x + absy) / (y - absy);
      fixed r3 = r * r * r;
      angle = mn0 * r3 - mn1 * r + PI - PI_OVER_4;

    }
    return y < 0 ? -angle : angle;
  }
};
} // qqq

template<typename S, typename B, uint8_t I, uint8_t F>
S & operator>>(S & s, qqq::fixed<B, I, F> & v) {
  double value = 0.;
  s >> value;
  if (s)
    v = value;
  return s;
}

//The input stream S is a template parameter. This allows you to use just about any stream, be it a file or string stream, be it an ANSI or wide character stream.
//Collapse | Copy Code

template<typename S, typename B, uint8_t I, uint8_t F>
S & operator<<(S & s, qqq::fixed<B, I, F> const& v) {
  double value = v;
  s << value;
  return s;
}


#include <limits>

namespace std {

template<typename T, unsigned char I, unsigned char F>
class numeric_limits<qqq::fixed<T, I, F> > {
public:
//	typedef fixed<T, I, F> fp_type;
//
//	/// Tests whether a type allows denormalized values.
//	//!
//	//! An enumeration value of type const float_denorm_style, indicating
//	//! whether the type allows denormalized values. The fixed_point class does
//	//! not have denormalized values.
//	//!
//	//! The member is always set to denorm_absent.
//	static const float_denorm_style has_denorm = denorm_absent;
//
//	/// Tests whether loss of accuracy is detected as a denormalization loss
//	/// rather than as an inexact result.
//	//!
//	//! The fixed_point class does not have denormalized values.
//	//!
//	//! The member is always set to false.
//	static const bool has_denorm_loss = false;
//
//	/// Tests whether a type has a representation for positive infinity.
//	//!
//	//! The fixed_point class does not have a representation for positive
//	//! infinity.
//	//!
//	//! The member is always set to false.
//	static const bool has_infinity = false;
//
//	/// Tests whether a type has a representation for a quiet not a number
//	/// (NAN), which is nonsignaling.
//	//!
//	//! The fixed_point class does not have a quiet NAN.
//	//!
//	//! The member is always set to false.
//	static const bool has_quiet_NaN = false;
//
//	/// Tests whether a type has a representation for signaling not a number
//	//! (NAN).
//	//!
//	//! The fixed_point class does not have a signaling NAN.
//	//!
//	//! The member is always set to false.
//	static const bool has_signaling_NaN = false;
//
//	/// Tests if the set of values that a type may represent is finite.
//	//!
//	//! The fixed_point type has a bounded set of representable values.
//	//!
//	//! The member is always set to true.
//	static const bool is_bounded = true;
//
//	/// Tests if the calculations done on a type are free of rounding errors.
//	//!
//	//! The fixed_point type is considered exact.
//	//!
//	//! The member is always set to true.
//	static const bool is_exact = true;
//
//	/// Tests if a type conforms to IEC 559 standards.
//	//!
//	//! The fixed_point type does not conform to IEC 559 standards.
//	//!
//	//! The member is always set to false.
//	static const bool is_iec559 = false;
//
//	/// Tests if a type has an integer representation.
//	//!
//	//! The fixed_point type behaves like a real number and thus has not
//	//! integer representation.
//	//!
//	//! The member is always set to false.
//	static const bool is_integer = false;
//
//	/// Tests if a type has a modulo representation.
//	//!
//	//! A modulo representation is a representation where all results are
//	//! reduced modulo some value. The fixed_point class does not have a
//	//! modulo representation.
//	//!
//	//! The member is always set to false.
//	static const bool is_modulo = false;
//
//	/// Tests if a type has a signed representation.
//	//!
//	//! The member stores true for a type that has a signed representation,
//	//! which is the case for all fixed_point types with a signed base type.
//	//! Otherwise it stores false.
//	static const bool is_signed = std::numeric_limits<
//			typename fp_type::base_type>::is_signed;
//
//	/// Tests if a type has an explicit specialization defined in the template
//	/// class numeric_limits.
//	//!
//	//! The fixed_point class has an explicit specialization.
//	//!
//	//! The member is always set to true.
//	static const bool is_specialized = true;
//
//	/// Tests whether a type can determine that a value is too small to
//	/// represent as a normalized value before rounding it.
//	//!
//	//! Types that can detect tinyness were included as an option with IEC 559
//	//! floating-point representations and its implementation can affect some
//	//! results.
//	//!
//	//! The member is always set to false.
//	static const bool tinyness_before = false;
//
//	/// Tests whether trapping that reports on arithmetic exceptions is
//	//! implemented for a type.
//	//!
//	//! The member is always set to false.
//	static const bool traps = false;
//
//	/// Returns a value that describes the various methods that an
//	/// implementation can choose for rounding a real value to an integer
//	/// value.
//	//!
//	//! The member is always set to round_toward_zero.
//	static const float_round_style round_style = round_toward_zero;
//
//	/// Returns the number of radix digits that the type can represent without
//	/// loss of precision.
//	//!
//	//! The member stores the number of radix digits that the type can represent
//	//! without change.
//	//!
//	//! The member is set to the template parameter I (number of integer bits).
//	static const int digits = I;
//
//	/// Returns the number of decimal digits that the type can represent without
//	/// loss of precision.
//	//!
//	//! The member is set to the number of decimal digits that the type can
//	//! represent.
//	static const int digits10 = (int) (digits * 301. / 1000. + .5);
//
//	static const int max_exponent = 0;
//	static const int max_exponent10 = 0;
//	static const int min_exponent = 0;
//	static const int min_exponent10 = 0;
//	static const int radix = 0;
//
//	/// The minimum value of this type.
//	//!
//	//! /return The minimum value representable with this type.
//	static fp_type (min)() {
//		fp_type minimum;
//		minimum._packed_data =
//				(std::numeric_limits<typename fp_type::base_type>::min)();
//		return minimum;
//	}
//
//	/// The maximum value of this type.
//	//!
//	//! /return The maximum value representable with this type.
//	static fp_type (max)() {
//		fp_type maximum;
//		maximum._packed_data =
//				(std::numeric_limits<typename fp_type::base_type>::max)();
//		return maximum;
//	}
//
//	/// The function returns the difference between 1 and the smallest value
//	/// greater than 1 that is representable for the data type.
//	//!
//	//! /return The smallest effective increment from 1.0.
//	static fp_type epsilon() {
//		fp_type epsilon;
//		epsilon.value_ = 1;
//		return epsilon;
//	}
//
//	/// Returns the maximum rounding error for the type.
//	//!
//	//! The maximum rounding error for the fixed_point type is 0.5.
//	//!
//	//! /return Always returns 0.5.
//	static fp_type round_error() {
//		return (fp_type) (0.5);
//	}
//
//	static fp_type denorm_min() {
//		return (fp_type) (0);
//	}
//
//	static fp_type infinity() {
//		return (fp_type) (0);
//	}
//
//	static fp_type quiet_NaN() {
//		return (fp_type) (0);
//	}
//
//	static fp_type signaling_NaN() {
//		return (fp_type) (0);
//	}
};/* numeric_limits */
} /* std */

#endif /* FIXED_H_ */
