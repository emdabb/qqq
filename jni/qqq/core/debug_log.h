/**
 * @file debug_log.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 6, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */


#ifndef DEBUG_LOG_H_
#define DEBUG_LOG_H_

#include <qqq.h>
#include <core/io/stream.h>

#if defined(__LINUX__)

#define RESET   "\033[0m"
#define BLACK   "\033[30m"      /* Black */
#define RED     "\033[31m"      /* Red */
#define GREEN   "\033[32m"      /* Green */
#define YELLOW  "\033[33m"      /* Yellow */
#define BLUE    "\033[34m"      /* Blue */
#define MAGENTA "\033[35m"      /* Magenta */
#define CYAN    "\033[36m"      /* Cyan */
#define WHITE   "\033[37m"      /* White */
#define BOLDBLACK   "\033[1m\033[30m"      /* Bold Black */
#define BOLDRED     "\033[1m\033[31m"      /* Bold Red */
#define BOLDGREEN   "\033[1m\033[32m"      /* Bold Green */
#define BOLDYELLOW  "\033[1m\033[33m"      /* Bold Yellow */
#define BOLDBLUE    "\033[1m\033[34m"      /* Bold Blue */
#define BOLDMAGENTA "\033[1m\033[35m"      /* Bold Magenta */
#define BOLDCYAN    "\033[1m\033[36m"      /* Bold Cyan */
#define BOLDWHITE   "\033[1m\033[37m"      /* Bold White */
#else
#define RESET   ""
#define BLACK   ""      /* Black */
#define RED     ""      /* Red */
#define GREEN   ""      /* Green */
#define YELLOW  ""      /* Yellow */
#define BLUE    ""      /* Blue */
#define MAGENTA ""      /* Magenta */
#define CYAN    ""      /* Cyan */
#define WHITE   ""      /* White */
#define BOLDBLACK   ""      /* Bold Black */
#define BOLDRED     ""      /* Bold Red */
#define BOLDGREEN   ""      /* Bold Green */
#define BOLDYELLOW  ""      /* Bold Yellow */
#define BOLDBLUE    ""      /* Bold Blue */
#define BOLDMAGENTA ""      /* Bold Magenta */
#define BOLDCYAN    ""      /* Bold Cyan */
#define BOLDWHITE   ""      /* Bold White */
#endif

#ifdef QQQ_HAVE_TYPEINFO
#include <typeinfo>
#endif

#if defined(__WINDOWS_OS__)
#include "gettimeofday.h"
#endif

#if !defined(__ANDROID__)
#include <iomanip>
#include <stdio.h>
#include <stdarg.h>
#include <string>


#define FNNAME	__FUNCTION__


#define DEBUG_SET_STREAM(x)			debug_log::set_stream(x)
#define DEBUG_METHOD()				debug_log _debug_log_(FNNAME)
#define DEBUG_MESSAGE(...)			_debug_log_.message(__VA_ARGS__)
#define DEBUG_VALUE_OF(x)			_debug_log_.value_of(#x, x, false)
#define DEBUG_VALUE_AND_TYPE_OF(x)	_debug_log_.value_of(#x, x, true)
#else
#include <android/log.h>

#define LOG_TAG					"QQQ::"
#define DEBUG_SET_STREAM(x)			debug_log::set_stream(x)
#define DEBUG_METHOD()				debug_log _debug_log_(__FUNCTION__)
#define DEBUG_MESSAGE(...)			_debug_log_.message(__VA_ARGS__)
#define DEBUG_VALUE_OF(x)			_debug_log_.value_of(#x, x, false)
#define DEBUG_VALUE_AND_TYPE_OF(x)	_debug_log_.value_of(#x, x, true)

#endif


#include "gettimeofday.h"
#include <string>
#include <core/io/stream.h>
#include <core/mutex.h>

namespace qqq {

class debug_log {
  static std::ostream* stream;
  //static IMutex* lock;
  static int indent;
  std::string context;
  struct timeval start, end;
protected:
  void write_indentation() {
    for(int i=0; i < indent; i++) {
      *stream << ".";
    }
  }
public:
  explicit debug_log(const std::string& c);

  ~debug_log();
  void message(const char* format, ...);
  template <typename T>
  void value_of(const std::string& name, const T& val, bool outputType) {
    write_indentation();
    *stream << context << ": " << name;
#ifdef QQQ_HAVE_TYPEINFO
    if(outputType) {
      *stream << "(" << BOLDRED << typeid(val).name() << RESET << ")";
    }
#endif
    *stream << "= [" << BOLDRED << val << RESET << "]" << std::endl;
    stream->flush();
  }


  static void set_stream(std::ostream* s) {
    debug_log::stream = s;
  }
};

}



#endif /* DEBUG_LOG_H_ */
