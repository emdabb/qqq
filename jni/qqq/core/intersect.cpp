/**
 * @file intersect.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jul 5, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include "intersect.h"
#include "sphere.h"
#include "ray.h"
#include "plane.h"
#include "aabb.h"
#include "math.h"
#include <algorithm>

using namespace qqq;

bool intersect::ray_sphere(const ray& r, const sphere& s, real* dist) {
#if 0
  Vector3 L = s.c - r.orig;

  real a = Vector3::dot(r.dir, r.dir);
  real b = (real)2. * Vector3::dot(r.dir, L);
  real c = Vector3::dot(L, L) - s.r * s.r;

  real t0, t1;

  if(!math::solve_quadratic(a, b, c, &t0, &t1)) {
    return false;
  }

  return false;
#else
  Vector3 L = r.orig - s.c;
  real tca = Vector3::dot(L, r.dir);
  if(tca < (real)0.) {
    return false;
  }
  real r2 = s.r * s.r;
  real d2 = Vector3::dot(L, L) - tca * tca;
  if(d2 > r2) {
    return false;
  }
  real thc = real_sqrt(r2 - d2);
  real t0 = tca - thc;
  real t1 = tca + thc;
#endif

  *dist = t0;
  return true;
}

bool intersect::ray_aabb(const ray& r, const AABB& box, real* dist, const Matrix& world) {
  Vector3 fDir;
  fDir.X = (real)1. / r.dir.X;
  fDir.Y = (real)1. / r.dir.Y;
  fDir.Z = (real)1. / r.dir.Z;

  Matrix worldI = Matrix::inverted(world);

  Vector3 ro, rd;
  Vector3::transform(r.orig, worldI, &ro);
  Vector3::rotate(r.dir, worldI, &rd);
  Vector3::normalize(&rd);

  Vector3 t1 = (box.min - ro) * fDir;
  Vector3 t2 = (box.max - ro) * fDir;

  real tmin = std::max(std::max(std::min(t1.X, t2.X), std::min(t1.Y, t2.Y)), std::min(t1.Z, t2.Z));
  real tmax = std::min(std::min(std::max(t1.X, t2.X), std::max(t1.Y, t2.Y)), std::max(t1.Z, t2.Z));

  if(tmax < (real)0.) {
    *dist = tmax;
    return false;
  }

  if(tmax < tmin) {
    *dist = tmax;
    return false;
  }
  *dist =tmin;
  return true;
}

bool intersect::ray_plane(const ray&, const Plane&, real* d) {
  return false;
}
bool intersect::ray_frustum(const ray&, const frustum&, real* d) {
  return false;
}
bool intersect::ray_triangle(const ray&, const Vector3[], real* d) {
  return false;
}
bool intersect::ray_cone(const ray&, const cone&, real* d) {
  return false;
}
bool intersect::ray_ray(const ray&, const ray&, real* d) {
  return false;
}
bool intersect::sphere_sphere(const sphere&, const sphere&, real* d) {
  return false;
}
bool intersect::sphere_aabb(const sphere&, const AABB&, real* d) {
  return false;
}
bool intersect::sphere_plane(const sphere&, const Plane&, real* d) {
  return false;
}
bool intersect::sphere_frustum(const sphere&, const frustum&, real* d) {
  return false;
}
bool intersect::sphere_triangle(const sphere&, const Vector3[], real* d) {
  return false;
}
bool intersect::sphere_cone(const sphere&, const cone&, real* d) {
  return false;
}
bool intersect::aabb_aabb(const AABB&, const AABB&, real* d) {
  return false;
}
bool intersect::aabb_plane(const AABB&, const Plane&, real* d) {
  return false;
}
bool intersect::aabb_frustum(const AABB&, const frustum&, real* d) {
  return false;
}
bool intersect::aabb_triangle(const AABB&, const Vector3[], real* d) {
  return false;
}
bool intersect::aabb_cone(const AABB&, const cone&, real* d) {
  return false;
}
bool intersect::plane_plane(const Plane&, const Plane&, real* d) {
  return false;
}
bool intersect::plane_frustum(const Plane&, const frustum&, real* d) {
  return false;
}
bool intersect::plane_triangle(const Plane&, const Vector3[], real* d) {
  return false;
}
bool intersect::plane_cone(const Plane&, const cone&, real* d) {
  return false;
}
