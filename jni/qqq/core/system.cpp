/**
 * @file system.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 9, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include <qqq_platform.h>
#include <core/debug_log.h>
#include "system.h"
#include <qqq_except.h>
#include <string.h>

#if defined(__LINUX__) || defined(__ANDROID__)
#include <unistd.h>
#include <dlfcn.h>
#define LIB_PREFIX	"lib"
#define LIB_POSTFIX	".so"
#elif defined(__WIN32__) || defined(__WP8__)
#include <windows.h>
#define LIB_PREFIX ""
#define LIB_POSTFIX ".dll"
#elif defined(__MACOSX__) || defined (__IOS__)
#include <dlfcn.h>
#define LIB_PREFIX "lib"
#define LIB_POSTFIX ".dylib"
#endif


using namespace qqq;

int system::get_processor_count() {
#if defined(__LINUX__)
  return sysconf(_SC_NPROCESSORS_CONF);
#elif defined(__WIN32__) || defined(__WP8__)
  SYSTEM_INFO sysinfo;
  ::GetNativeSystemInfo(&sysinfo);
  return sysinfo.dwNumberOfProcessors;
#endif
}

lib_t system::load_library(const char* fn) {

  char name[32];
  lib_t res;
#if 1
  ::strcpy(name, LIB_PREFIX);
  ::strcat(name, fn);
  ::strcat(name, LIB_POSTFIX);
#endif
#if defined(__LINUX__) || defined(__ANDROID__)
  if(!(res = dlopen(name, RTLD_NOW))) {
    throw UnsatisfiedLinkerException(dlerror());
  }
#elif defined(__WIN32__) || defined(__WP8__)
  PCWSTR wname = PCWSTR(name);
  ::LoadPackagedLibrary(wname, 0);
#endif
  return res;

}

void* system::get_proc_address(lib_t lib, const char* name) {
  void* res = NULL;
#if defined(__LINUX__) || defined(__ANDROID__)
  if(!(res = dlsym(lib, name))) {
    throw UnsatisfiedLinkerException(dlerror());
  }
#elif defined(__WIN32__) || defined(__WP8__)
  res = (void*)::GetProcAddress(lib, name);
#endif
  return res;
}

