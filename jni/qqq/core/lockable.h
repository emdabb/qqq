/**
 * @file lockable.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 6, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef LOCKABLE_H_
#define LOCKABLE_H_

struct ILockable {
  virtual ~ILockable () {}
  virtual int lock() = 0;
  virtual int unlock() = 0;
};

struct lock_t {

  ILockable* m_lock;
  bool is_locked;

  lock_t(ILockable* l) : m_lock(l), is_locked(true) {
    is_locked = m_lock->lock() != 0;
  }

  ~lock_t() {
    is_locked = m_lock->unlock() != 0;
  }

  operator bool () const {
    return is_locked != false;
  }

  inline void set_unlock() {
    is_locked = false;
  }
};

#define synchronized(M) for(lock_t M##_lock((M)); M##_lock; M##_lock.set_unlock())



#endif /* LOCKABLE_H_ */
