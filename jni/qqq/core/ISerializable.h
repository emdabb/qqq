/**
 * @file ISerializable.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date May 11, 2015
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef QQQ_CORE_ISERIALIZABLE_H_
#define QQQ_CORE_ISERIALIZABLE_H_

namespace qqq {

struct ISerializable {
    virtual ~ISerializable() {}
    virtual void serialize(std::ostream*) = 0;
    virtual void deserialize(std::istream*) = 0;
};

}

#endif /* QQQ_CORE_ISERIALIZABLE_H_ */
