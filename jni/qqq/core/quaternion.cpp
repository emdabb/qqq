/*
 * quaternion.cpp
 *
 *  Created on: Jun 4, 2014
 *      Author: miel
 */


#include "quaternion.h"

using namespace qqq;

const Quaternion Quaternion::Identity = { 0., 0., 0., 1. };

