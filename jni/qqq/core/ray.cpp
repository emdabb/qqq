/**
 * @file ray.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jul 5, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */


#include "ray.h"
#include "plane.h"

using namespace qqq;


void ray::from_planes(const Plane& a, const Plane& b, ray* res) {
  Vector3 d = Vector3::cross(a.n, b.n);
  real num = Vector3::dot(a.n, b.n);
  Vector3 r1 = b.n * -a.d;
  Vector3 r2 = a.n *  b.d;
  Vector3 o = Vector3::cross(r1 + r2, d) / num;

  res->orig.X = o.X;
  res->orig.Y = o.Y;
  res->orig.Z = o.Z;

  res->dir.X = d.X;
  res->dir.Y = d.Y;
  res->dir.Z = d.Z;
}

