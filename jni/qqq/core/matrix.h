/**
 * @file matrix.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 5, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef MATRIX_H_
#define MATRIX_H_

#include <qqq.h>

namespace qqq {

struct Vector3;
struct Quaternion;

__declare_aligned(struct, 16) Matrix {
  real m11, m12, m13, m14;
  real m21, m22, m23, m24;
  real m31, m32, m33, m34;
  real m41, m42, m43, m44;

  typedef const Matrix& __restrict const_ref;
  typedef 	 	Matrix* __restrict ptr;

  static const Matrix Identity;

  static Matrix mul(const_ref a, const_ref b);
  static void mul(const_ref a, const_ref b, ptr out);
  static Matrix inverted(const_ref in);
  static void invert(ptr inout);
  static void invert(const_ref in, ptr out);
  static void transpose(const_ref in, ptr out);
  static void createLookat(Vector3 const& pos, Vector3 const& at, Vector3 const& up, ptr out);
  static void determinant(const_ref a, real*);
  static void createPerspectiveFov(real const& fov, real const& ratio, real const& tnear, real const& tfar, ptr out);
  static void createOrthoOffcentre(real const& left, real const& right, real const& bottom, real const& top, real const& zNearPlane, real const& zFarPlane, ptr out);
  static void createRotation(real const& rx, real const& ry, real const& rz, ptr out);
  static void createRotationX(real const& a, ptr out);
  static void createRotationY(real const& a, ptr out);
  static void createRotationZ(real const& a, ptr out);
  static void createWorld(Vector3 const&, Vector3 const&, Vector3 const&, ptr);
  static void createScale(Vector3 const&, ptr);
  static void createScale(real const&, real const&, real const&, ptr);
  static void createTranslation(Vector3 const&, ptr);
  static void createTranslation(real const& x, real const& y, real const& z, ptr out);
  static void createRotation(Quaternion const&, ptr);

  bool operator != (const_ref other) const;

};
}

#endif /* MATRIX_H_ */
