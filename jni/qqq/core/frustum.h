/**
 * @file frustum.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jul 5, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef FRUSTUM_H_
#define FRUSTUM_H_

#include "plane.h"

namespace qqq {

struct frustum {
  enum {
    E_PLANE_NEAR,
    E_PLANE_FAR,
    E_PLANE_LEFT,
    E_PLANE_RIGHT,
    E_PLANE_TOP,
    E_PLANE_BOTTOM,
    E_PLANE_MAX
  };
  Plane planes[E_PLANE_MAX];

  static void get_corners(const frustum&, Vector3[]);
  static void from_view_projection(const Matrix& view, const Matrix& proj, frustum* out) {
    Matrix m = Matrix::mul(view, proj);

    out->planes[E_PLANE_LEFT]   = {-m.m14 - m.m11, -m.m24 - m.m21, -m.m34 - m.m31, -m.m44 - m.m41 };
    out->planes[E_PLANE_RIGHT]  = { m.m11 - m.m14,  m.m21 - m.m24,  m.m31 - m.m34,  m.m41 - m.m44 };
    out->planes[E_PLANE_TOP]    = { m.m12 - m.m14,  m.m22 - m.m24,  m.m32 - m.m34,  m.m42 - m.m44 };
    out->planes[E_PLANE_BOTTOM] = {-m.m14 - m.m12, -m.m24 - m.m22, -m.m34 - m.m32, -m.m44 - m.m42 };
    out->planes[E_PLANE_FAR]    = { m.m13 - m.m14,  m.m23 - m.m24,  m.m33 - m.m34,  m.m43 - m.m44 };
    out->planes[E_PLANE_NEAR]   = {-m.m13, -m.m23, -m.m33, -m.m43 };
  }
};

}



#endif /* FRUSTUM_H_ */
