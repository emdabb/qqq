/**
 * @file plane.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jul 5, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef PLANE_H_
#define PLANE_H_

#include "vector3.h"

namespace qqq {

struct Plane {
  typedef const Plane& const_ref;
  typedef Plane* ptr;

  Vector3 n;
  float d;

  static void  normalize(ptr inout) {
    real f = (real)1. / Vector3::length(inout->n);
    inout->n.X = inout->n.X * f;
    inout->n.Y = inout->n.Y * f;
    inout->n.Z = inout->n.Z * f;
  }
  static void  normalized(const_ref in, ptr out) {
    real f = (real)1. / Vector3::length(in.n);
    out->n.X = in.n.X * f;
    out->n.Y = in.n.Y * f;
    out->n.Z = in.n.Z * f;
  }
  static Plane normalized(const_ref in) {
    Plane out;
    normalized(in, &out);
    return out;
  }
};

}



#endif /* PLANE_H_ */
