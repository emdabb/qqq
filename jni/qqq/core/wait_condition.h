/**
 * @file wait_condition.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jul 29, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef WAIT_CONDITION_H_
#define WAIT_CONDITION_H_

#include <qqq.h>
#include "lockable.h"

namespace qqq {
struct IWaitCondition : public ILockable {
  virtual ~IWaitCondition() {}
  virtual bool set() = 0;
  virtual void reset() = 0;
  virtual bool wait(int msec = -1) = 0;
};

typedef void(*PFNQQQCREATEWAITCONDITION)(void);
QQQ_C_API void QQQ_CALL qqqCreateWaitCondition(IWaitCondition**);

}


#endif /* WAIT_CONDITION_H_ */
