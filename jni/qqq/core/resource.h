/*
 * resource.h
 *
 *  Created on: Jul 15, 2014
 *      Author: miel
 */

#ifndef RESOURCE_H_
#define RESOURCE_H_

#include <core/event.h>

namespace qqq {

struct resource;

struct resource_destroyed_event {
  resource* res;
};

struct resource {
  virtual ~resource() {}
  virtual void destroy() = 0;
  virtual const char* get_name() const = 0;
  virtual void set_name(const char*) = 0;
  EventHandler<resource_destroyed_event> on_destroyed;
};
}


#endif /* RESOURCE_H_ */
