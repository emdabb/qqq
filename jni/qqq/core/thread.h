/**
 * @file thread.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 5, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef THREAD_H_
#define THREAD_H_

#include <qqq.h>
#include <core/runnable.h>

#if defined(__WIN32__)
#include <Windows.h>
#elif defined(__LINUX__) || defined(__ANDROID__)
#include <pthread.h>
#endif

namespace qqq {
#if defined(__WIN32__)
typedef	HANDLE				thread_handle;
typedef	DWORD				thread_id;
//typedef	CRITICAL_SECTION	crtitical_section;
//typedef	HANDLE				event;
#elif defined(__LINUX__) || defined(__ANDROID__)
typedef pthread_t 			thread_handle;
typedef pthread_t 			thread_id;
#endif

struct IThread : public IRunnable {
  virtual ~IThread() {}
  virtual void start() = 0;
  virtual void suspend() = 0;
  virtual void yield() = 0;
};

typedef void(QQQ_CALL *PFNQQQCREATETHREAD)(IThread**, IRunnable*);
QQQ_C_API void QQQ_CALL qqqCreateThread(IThread**, IRunnable*);

}

#endif /* THREAD_H_ */
