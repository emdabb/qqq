/**
 * @file factory.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 25, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef FACTORY_H_
#define FACTORY_H_

#include <qqq.h>
#include <map>
#include <string>
#include <typeinfo>
#include <core/singleton.h>

namespace qqq {
template<typename T, typename CLASSID = std::string>
class Factory : public Singleton<Factory<T, CLASSID> >{
  typedef T product_t;
  typedef T*(*create_fun)();
  typedef CLASSID classid_t;
  typedef std::map<classid_t, create_fun> registry_t;
  registry_t reg;
public:
  virtual ~Factory() {
  }

  bool registerClass(const classid_t& id, create_fun fun) {
    typename registry_t::iterator it = reg.find(id);
    if (it == reg.end()) {
      reg[id] = fun;
      return true;
    }
    return false;
  }

  T* create(const classid_t& id) {
    typename registry_t::iterator it = reg.find(id);
    if (it != reg.end()) {
      T* result = (*it).second();
      return result;
    }
    return 0;
  }

  T* create() {
    return new T;
  }
};

template<typename B, typename T>
static B* Factory_CreateInstane() {
  return new T;
}

template<typename BASE, typename TYPE, typename CLASSID>
static void Factory_registerFactory(const CLASSID& id) {
  Factory<BASE, CLASSID>::getInstance().register_class(id, Factory_CreateInstane<BASE, TYPE>);
}

#define REGISTER_FACTORY(B, T) Factory_registerFactory<B, T, std::string>

template<typename BASE, typename TYPE, typename CLASSID = std::string>
class RegisterFactory {
  typedef CLASSID classid_t;
  typedef BASE base_t;
  typedef TYPE specialized_t;
  typedef Factory<base_t, classid_t> factory_t;

  static base_t* createInstance() {
    return new specialized_t;
  }
public:

  RegisterFactory(const CLASSID& id) {
    factory_t::getInstance().register_class(id, this->createInstance);
  }

  RegisterFactory() {
    std::string id = typeid(TYPE).name();
    Factory<BASE, classid_t>::getInstance().registerClass(id, createInstance);
  }
};

}



#endif /* FACTORY_H_ */
