/**
 * @file IResource.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date May 27, 2015
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef JNI_QQQ_CORE_IRESOURCE_H_
#define JNI_QQQ_CORE_IRESOURCE_H_

#include <core/event.h>

namespace qqq {

struct IResource;

struct ResourceEventArgs {
    IResource* Res;
};

struct IResource {
    virtual ~IResource() {}

    virtual void destroy() = 0;

    EventHandler<ResourceEventArgs> OnDestroyed;
};
} // qqq


#endif /* JNI_QQQ_CORE_IRESOURCE_H_ */
