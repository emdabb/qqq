#ifndef _WIN32_THREAD_H_
#define _WIN32_THREAD_H_

#include <qqq.h>

#if defined(__WIN32__)

#include <Windows.h>

namespace qqq {
  class win32_thread {
    HANDLE handle;

  public:
    win32_thread();

    void yield();

    void start();
  };
}
#endif /* __WIN32__ */
#endif
