#ifndef _WIN32_EVENT_H
#define _WIN32_EVENT_H

#include <qqq.h>
#if defined(__WIN32__)
#include <Windows.h>
namespace qqq {
class win32_mutex;
class win32_event {
  HANDLE hEvent;
public:
  win32_event();
  virtual ~win32_event();
  void set();
  void wait(win32_mutex&, int = -1);
};
}
#endif  /** __WIN32__ */
#endif
