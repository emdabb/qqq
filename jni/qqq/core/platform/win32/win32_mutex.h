#ifndef _WIN32_MUTEX_H
#define _WIN32_MUTEX_H

#include <qqq_platform.h>

#if defined(__WIN32__)

#include <core/lockable.h>
#include <windows.h>

namespace qqq {
  class win32_mutex : public lockable  {
    CRITICAL_SECTION handle;
  public:
    win32_mutex();
    virtual ~win32_mutex();
    virtual bool lock();
    virtual bool unlock();
  };
}

#endif

#endif
