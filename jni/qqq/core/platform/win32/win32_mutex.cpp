#include "win32_mutex.h"

using namespace qqq;

#if defined(__WIN32__)

win32_mutex::win32_mutex() {
  InitializeCriticalSectionEx(&handle, 0, 0);
}

win32_mutex::~win32_mutex(){
  DeleteCriticalSection(&handle);
}

bool win32_mutex::lock() {
  EnterCriticalSection(&handle);
  return true;
}
bool win32_mutex::unlock() {
  LeaveCriticalSection(&handle);
  return true;
}

#endif
