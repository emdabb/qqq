#include "win32_thread.h"

using namespace qqq;

#if defined(__WIN32__)

static DWORD WINAPI proc(LPVOID param) {
  return -1;
}
win32_thread::win32_thread() {
  DWORD id;
  ::CreateThread(NULL, 0, &proc, this, CREATE_SUSPENDED, &id);
}

void win32_thread::yield() {
}

void win32_thread::start() {
}

#endif
