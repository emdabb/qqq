#include "win32_event.h"
#include "win32_mutex.h"

#if defined(__WIN32__)

using namespace qqq;

#define UNIQUE_ID_STR(x)	x##__COUNTER__

win32_event::win32_event() {
  hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);//UNIQUE_ID_STR("qqq.core.event.win32::"));
}

win32_event::~win32_event() {
}

void win32_event::set() {
  ::SetEvent(hEvent);
}

void win32_event::wait(win32_mutex& lock, int time) {
  ::WaitForSingleObject(hEvent, time);
}

#endif
