/**
 * @file posix_plugin.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 5, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef POSIX_PLUGIN_H_
#define POSIX_PLUGIN_H_

#include <types.h>
#include <dlfcn.h>

typedef struct _posix_plugin {

  void* handle;

  _posix_plugin() {
    handle = NULL;
  }

  ~_posix_plugin() {
    if(handle) {
      dlclose(handle);
      handle = NULL;
    }
  }

  bool open(const char* fn) {
    handle = dlopen(fn, RTLD_NOW);

    if(!handle) {
      fprintf(stderr, "%s\n", dlerror());
    }
    return handle != NULL;
  }

  void* get_proc_address(const char* name) {
    void* res = dlsym(handle, name);
    if(!res) {
      fprintf(stderr, "%s\n", dlerror());
    }
    return res;
  }



} posix_plugin;



#endif /* POSIX_PLUGIN_H_ */
