#include <core/thread.h>
#include <core/mutex.h>
#include <core/wait_condition.h>

namespace qqq {

QQQ_C_API void new_posix_mutex(IMutex**);

struct PosixThread: public IThread {
	IRunnable* object;
	pthread_mutex_t lock;
	pthread_cond_t event;
	bool is_suspended;
	pthread_t handle;

	static void* thread_callback(void* data) {
		PosixThread* t = (PosixThread*) data;

		pthread_mutex_lock(&t->lock);
		while (t->is_suspended) {
			pthread_cond_wait(&t->event, &t->lock);
		}
		pthread_mutex_unlock(&t->lock);

		t->run();
		pthread_exit(NULL);
		return data;
	}

	explicit PosixThread(IRunnable* r = NULL) :
			object(r), is_suspended(true) {
		pthread_mutex_init(&lock, NULL);
		pthread_cond_init(&event, NULL);

		pthread_attr_t attr;
		pthread_attr_init(&attr);

		pthread_create(&handle, &attr, &PosixThread::thread_callback, this);
		pthread_attr_destroy(&attr);

	}
	virtual ~PosixThread() {

	}

	void start() {
		pthread_mutex_lock(&lock);
		is_suspended = false;
		pthread_cond_signal(&event);
		pthread_mutex_unlock(&lock);
	}

	int run() {
		if (object) {
			object->run();
		}
		return 0;
	}

	void yield() {
		sched_yield();
	}

	void suspend() {

	}

};

QQQ_C_API void QQQ_CALL qqqCreateThread(IThread** ppthread, IRunnable* prun) {
	*ppthread = new PosixThread(prun);
}

}

