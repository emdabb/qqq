#include <qqq.h>
#include <core/mutex.h>


#include <pthread.h>

namespace qqq {

struct posix_mutex : public IMutex {
  pthread_mutex_t handle;

  posix_mutex() {
    pthread_mutexattr_t attr;
    pthread_mutexattr_init(&attr);
    pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_DEFAULT);
    pthread_mutex_init(&handle, &attr);
    pthread_mutexattr_destroy(&attr);
  }

  ~posix_mutex() {
    pthread_mutex_destroy(&handle);
  }

  int lock() {
    return pthread_mutex_lock(&handle) == 0;
  }
  int unlock() {
    return pthread_mutex_unlock(&handle) == 0;
  }
};

QQQ_C_API void new_posix_mutex(IMutex** ppmutex) {
  *ppmutex = new posix_mutex;
}

QQQ_C_API void QQQ_CALL qqqCreateMutex(IMutex** ppmutex) {
  *ppmutex = new posix_mutex;
}

}
