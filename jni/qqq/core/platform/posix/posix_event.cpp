#include <core/gettimeofday.h>
#include <core/wait_condition.h>
#include <time.h>
#include <pthread.h>

#if (!defined __timespec_defined) && defined(__LINUX__)
struct timespec {
  __time_t tv_sec;		/* Seconds.  */
  __syscall_slong_t tv_nsec;	/* Nanoseconds.  */
};
#endif

namespace qqq {
struct PosixWaitCondition : public IWaitCondition {
  pthread_cond_t cond;
  pthread_mutex_t mLock;
  bool is_set;

  PosixWaitCondition() {
    pthread_condattr_t attr;
    pthread_condattr_init(&attr);
    pthread_cond_init(&cond, &attr);
    pthread_condattr_destroy(&attr);

    pthread_mutexattr_t mattr;
    pthread_mutexattr_init(&mattr);
    pthread_mutexattr_settype(&mattr, PTHREAD_MUTEX_DEFAULT);
    pthread_mutex_init(&mLock, &mattr);
    pthread_mutexattr_destroy(&mattr);

    is_set = false;
  }

  virtual ~PosixWaitCondition() {
    pthread_cond_destroy(&cond);
    pthread_mutex_destroy(&mLock);
  }

  virtual int lock() {
      return pthread_mutex_lock(&mLock) == 0;
  }

  virtual int unlock() {
      return pthread_mutex_unlock(&mLock) == 0;
  }

  virtual bool set() {

    is_set = true;
    bool res = pthread_cond_signal(&cond);

    return res;
  }

  virtual void reset() {
    pthread_mutex_lock(&mLock);
    is_set = false;
    pthread_mutex_unlock(&mLock);
  }

  bool wait(int time) {
    bool res = false;
    //pthread_mutex_lock(&mLock);
    struct timespec ts;
    ts.tv_sec = time / 1000;
    ts.tv_nsec = (time % 1000) * 1000;

    while(!is_set) {
      if(time < 0) {
        res = pthread_cond_wait(&cond, &mLock);
      } else {
        res = pthread_cond_timedwait(&cond, &mLock, &ts) == 0;
      }
    }
   // pthread_mutex_unlock(&mLock);
    return res;
  }
};
QQQ_C_API void QQQ_CALL qqqCreateWaitCondition(IWaitCondition** ppcond) {
  *ppcond = new PosixWaitCondition;
}
}


