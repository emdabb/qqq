/**
 * @file timer.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 9, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */


#include "timer.h"
#include <qqq.h>
#include "gettimeofday.h"

timer::timer() {
    reset();
}

void timer::reset() {
    gettimeofday(&start, NULL);
}

long timer::elapsed() const {

  struct timeval now;
  gettimeofday(&now, NULL);

  long usec = now.tv_usec - start.tv_usec;
  long sec = now.tv_sec - start.tv_sec;

  return sec * 1000 + usec / 1000;

}

