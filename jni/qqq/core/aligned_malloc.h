/*
 * aligned_malloc.h
 *
 *  Created on: May 30, 2014
 *      Author: miel
 */

#ifndef ALIGNED_MALLOC_H_
#define ALIGNED_MALLOC_H_

#include <qqq_platform.h>
#if defined(__MACOSX__) || defined(__IOS__)
# include <malloc/malloc.h>
#else
# include <malloc.h>
#endif


bool is_pow2(size_t x);
void* aligned_malloc(size_t sz, size_t a);
void aligned_free(void* mem);


#endif /* ALIGNED_MALLOC_H_ */
