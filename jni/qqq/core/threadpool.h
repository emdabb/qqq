/**
 * @file threadpool.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 6, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef THREADPOOL_H_
#define THREADPOOL_H_

#include <core/thread.h>
#include <core/mutex.h>
#include <core/wait_condition.h>
#include <core/runnable.h>
#include <core/debug_log.h>
#include <core/singleton.h>
#include <queue>

namespace qqq {

class Threadpool : public Singleton<Threadpool> {
  struct internal_thread;
public:
  Threadpool();
  ~Threadpool();
  void add_runnable(IRunnable* ptr);
  IRunnable* next_runnable();
  const IThread* get_threads(size_t* num) const {
    *num = num_threads;
    return (IThread*)*threads;
  }

  bool has_work();

  IMutex* lock;
  IWaitCondition* event;

  std::queue<IRunnable*> queue;
  IThread** threads;
  size_t num_threads;

};
}


#endif /* THREADPOOL_H_ */
