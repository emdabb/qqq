/*
 * vector2.cpp
 *
 *  Created on: Jun 27, 2014
 *      Author: miel
 */

#include <core/vector2.h>
#include <core/random.h>

using namespace qqq;

const Vector2 Vector2::One = { (real) 1., (real) 1. };
const Vector2 Vector2::Zero = { (real) 0., (real) 0. };
const Vector2 Vector2::Right = { (real) 1., (real) 0. };
const Vector2 Vector2::Up = { (real) 0., (real) 1. };

Vector2 Vector2::create_random(int seed) {
	Vector2 out;
	create_random(seed, &out);
	return out;
}
void Vector2::create_random(int seed, ptr out) {
	random rnd(seed);
	out->X = rnd.next() * (real) 2. - (real) 1.;
	out->Y = rnd.next() * (real) 2. - (real) 1.;
}
real Vector2::to_angle(const_ref in) {
	real out;
	to_angle(in, &out);
	return out;
}
void Vector2::to_angle(const_ref in, real* out) {
	*out = real_atan2(in.Y, in.X);
}

void Vector2::normalized(const_ref a, Vector2* out) {
	real fLen = (real) 1. / real_sqrt(a.X * a.X + a.Y * a.Y);
	out->X = a.X * fLen;
	out->Y = a.Y * fLen;
}

Vector2 Vector2::normalized(const_ref a) {
	Vector2 res;
	normalized(a, &res);
	return res;
}
