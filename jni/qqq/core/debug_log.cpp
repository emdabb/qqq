/**
 * @file debug_log.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 6, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include "debug_log.h"
#include "mutex.h"

using namespace qqq;

#if defined(__ANDROID__)
template <typename T, typename TRAITS = std::char_traits<T> >
class basic_androidbuf: public std::basic_streambuf<T, TRAITS> {
public:
  typedef TRAITS traits_type;
  enum { bufsize = 128 }; // ... or some other suitable buffer size
  basic_androidbuf() { this->setp(buffer, buffer + bufsize - 1); }
private:
  int overflow(int c) {
    if (c == traits_type::eof()) {
      *this->pptr() = traits_type::to_char_type(c);
      this->sbumpc();
    }
    return this->sync( )? traits_type::eof(): traits_type::not_eof(c);
  }
  int sync() {
    int rc = 0;
    if (this->pbase() != this->pptr()) {
      __android_log_print(ANDROID_LOG_INFO,
      LOG_TAG,
      "q: %s",
      std::string(this->pbase(),
      this->pptr() - this->pbase()).c_str());
      rc = 0;
      this->setp(buffer, buffer + bufsize - 1);
    }
    return rc;
  }
  char buffer[bufsize];
};

typedef basic_androidbuf<char> androidbuf;

std::ostream* debug_log::stream = new std::ostream(new androidbuf);
#else
std::ostream* debug_log::stream = &std::cout;
#endif
//IMutex* debug_log::lock = NULL;

int debug_log::indent = 0;

debug_log::debug_log(const std::string& c) : context(c){
  //if(!lock) {
  //  qqqCreateMutex(&debug_log::lock);
  //}
  //synchronized(lock) {
    write_indentation();
    *stream << "+" << BOLDWHITE << context << RESET << std::endl;
    stream->flush();
    indent++;
  //}
  ::gettimeofday(&start, NULL);
}

debug_log::~debug_log() {
  ::gettimeofday(&end, NULL);

  //synchronized(lock) {

    long mtime = (end.tv_sec - start.tv_sec) * 1000 + (end.tv_usec - start.tv_usec) / 1000;
    indent--;
    write_indentation();
    *stream << "-" << context << RESET;
    *stream << " (took "<<mtime<<"ms)";
    *stream << std::endl;
    stream->flush();

  //}
}

void debug_log::message(const char* format, ...) {
  //synchronized(lock) {
    write_indentation();
    char buf[8192];
    va_list args;
    va_start(args, format);
    vsprintf(buf, format, args);
    va_end(args);

    *stream << BOLDWHITE << context << ": " << WHITE << buf << RESET << std::endl;
    stream->flush();
  //}
}
