/**
 * @file vector3.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 5, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include "vector3.h"
#include "matrix.h"
#include "quaternion.h"

using namespace qqq;

const Vector3 Vector3::One		= {  1.,  1.,  1. };
const Vector3 Vector3::Zero		= {  0.,  0.,  0. };
const Vector3 Vector3::Up		= {  0.,  1.,  0. };
const Vector3 Vector3::Right 	= {  1.,  0.,  0. };
const Vector3 Vector3::Forward	= {  0.,  0., -1. };

Vector3 Vector3::transform(const_ref v, const Quaternion& q) {
    Vector3 r;
    transform(v, q, &r);
    return r;
}
void Vector3::transform(const_ref v, const Quaternion& q, ptr c) {
    real X = +q.Y * v.Z - q.Z * v.Y + q.W * v.X;
    real Y = -q.X * v.Z + q.Z * v.X + q.W * v.Y;
    real Z = +q.X * v.Y - q.Y * v.X + q.W * v.Z;
    real W = -q.X * v.X - q.Y * v.Y - q.Z * v.Z;

    c->X = +X * +q.W + Y * -q.Z - Z * -q.Y + W * -q.X;
    c->Y = -X * -q.Z + Y * +q.W + Z * -q.X + W * -q.Y;
    c->Z = +X * -q.Y - Y * -q.X + Z * +q.W + W * -q.Z;
}

Vector3 Vector3::transform(const_ref a, const Matrix& b) {
    Vector3 out;
    transform(a, b, &out);
    return out;
}

void Vector3::transform(const_ref a, const Matrix& b, ptr c) {
  c->X = (a.X * b.m11) + (a.Y * b.m21) + (a.Z * b.m31) + b.m41;
  c->Y = (a.X * b.m12) + (a.Y * b.m22) + (a.Z * b.m32) + b.m42;
  c->Z = (a.X * b.m13) + (a.Y * b.m23) + (a.Z * b.m33) + b.m43;
}

void Vector3::rotate(const_ref a, const Matrix& b, ptr c) {
  c->X = (a.X * b.m11) + (a.Y * b.m21) + (a.Z * b.m31);
  c->Y = (a.X * b.m12) + (a.Y * b.m22) + (a.Z * b.m32);
  c->Z = (a.X * b.m13) + (a.Y * b.m23) + (a.Z * b.m33);
}

