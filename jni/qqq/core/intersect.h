/**
 * @file intersect.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jul 5, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef INTERSECT_H_
#define INTERSECT_H_

#include "matrix.h"

namespace qqq {

struct containment_type {
  enum {
    E_CONTAINS,
    E_INTERSECTS,
    E_DISJOINT
  };
};

struct plane_intersect {
  enum {
    E_FRONT,
    E_INTERSECT,
    E_BEHIND
  };
};

struct ray;
struct sphere;
struct AABB;
struct Plane;
struct frustum;
struct cone;

struct intersect {
  static bool ray_sphere(const ray&, const sphere&, real*);
  static bool ray_aabb(const ray&, const AABB&, real*, const Matrix& world = Matrix::Identity);
  static bool ray_plane(const ray&, const Plane&, real*);
  static bool ray_frustum(const ray&, const frustum&, real*);
  static bool ray_triangle(const ray&, const Vector3[], real*);
  static bool ray_cone(const ray&, const cone&, real*);
  static bool ray_ray(const ray&, const ray&, real*);

  static bool sphere_sphere(const sphere&, const sphere&, real*);
  static bool sphere_aabb(const sphere&, const AABB&, real*);
  static bool sphere_plane(const sphere&, const Plane&, real*);
  static bool sphere_frustum(const sphere&, const frustum&, real*);
  static bool sphere_triangle(const sphere&, const Vector3[], real*);
  static bool sphere_cone(const sphere&, const cone&, real*);

  static bool aabb_aabb(const AABB&, const AABB&, real*);
  static bool aabb_plane(const AABB&, const Plane&, real*);
  static bool aabb_frustum(const AABB&, const frustum&, real*);
  static bool aabb_triangle(const AABB&, const Vector3[], real*);
  static bool aabb_cone(const AABB&, const cone&, real*);

  static bool plane_plane(const Plane&, const Plane&, real*);
  static bool plane_frustum(const Plane&, const frustum&, real*);
  static bool plane_triangle(const Plane&, const Vector3[], real*);
  static bool plane_cone(const Plane&, const cone&, real*);
};

}



#endif /* INTERSECT_H_ */
