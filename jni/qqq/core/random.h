/*
 * RandomNumber.h
 *
 *  Created on: Mar 31, 2012
 *      Author: miel
 */

#ifndef RANDOM_H_
#define RANDOM_H_
//****************************************************************************
// Mersenne twister from: http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/SFMT
//****************************************************************************
namespace qqq
{

class random
{
  static const unsigned long N = 624;
  static const unsigned long M = 397;
  static const unsigned long MATRIX_A = 0x9908b0dfUL;
  static const unsigned long UPPER_MASK = 0x80000000UL;
  static const unsigned long LOWER_MASK = 0x7fffffffUL;
  static const unsigned long MAX = 0xffffffffUL;
  unsigned long x[N];
  unsigned int mNext;
public:
  random(unsigned long seed = 5489UL);
  ~random();
  double next();
  double next(double hi);
  double next(double lo, double hi);
  ///
  /// @param-mu
  /// @param-sigma
  ///
  double next_gaussian(double = 0.0, double = 1.0);
  double operator ()();
  double operator ()(double);
  double operator ()(double, double);
private:
  void generate_seed(unsigned long seed);
  unsigned long generate_random_number();
};
}


#endif /* RANDOM_H_ */
