/**
 * @file aabb.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Sep 1, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */



#include "aabb.h"
#include <cassert>
#include <algorithm>

using namespace qqq;


void AABB::transform(const_ref in, const Matrix& m, ptr out) {
  assert(out);
  Vector3::transform(in.max, m, &out->max);
  Vector3::transform(in.min, m, &out->min);
}

void AABB::add(const Vector3& in) {
  min.X = std::min(min.X, in.X);
  min.Y = std::min(min.Y, in.Y);
  min.Z = std::min(min.Z, in.Z);
  max.X = std::max(max.X, in.X);
  max.Y = std::max(max.Y, in.Y);
  max.Z = std::max(max.Z, in.Z);
}
