/**
 * @file ray.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jul 5, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef RAY_H_
#define RAY_H_

#include "vector3.h"

namespace qqq {

struct Plane;

__declare_aligned(struct, 16) ray {
  typedef const ray& const_ref;
  typedef ray* ptr;

  Vector3 orig;
  Vector3 dir;

  static void from_planes(const Plane& a, const Plane& b, ray* res);

//	static ray  normalized(const_ref in);
//	static void normalized(const_ref in, ptr out);
//	static void normalize(ptr inout);

};
}



#endif /* RAY_H_ */
