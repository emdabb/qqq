/**
 * @file string_util.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jul 27, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef STRING_UTIL_H_
#define STRING_UTIL_H_

#include <string>
#include <assert.h>

namespace qqq {
struct string_util {
  static void split(const std::string& src, const std::string& at, std::vector<std::string>* dst) {
    assert(dst);
    dst->clear();
    std::string str = src;
    size_t split = 0;
    while ((split = str.find_first_of(at)) != str.npos) {
      std::string token = str.substr(0, split);
      dst->push_back(token);
      str = str.substr(split + 1, str.npos);
    }
    dst->push_back(str); // append filename

  }
};
}


#endif /* STRING_UTIL_H_ */
