/**
 * @file vector3.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 5, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef VECTOR3_H_
#define VECTOR3_H_

#include <qqq.h>
#include <core/math_helper.h>

namespace qqq {

struct Matrix;
struct Quaternion;
__declare_aligned(struct, 16) Vector3 {
  real X, Y, Z;

  static const Vector3 One;
  static const Vector3 Zero;
  static const Vector3 Up;
  static const Vector3 Right;
  static const Vector3 Forward;

  typedef const Vector3& __restrict const_ref;
  typedef 		Vector3* __restrict ptr;

  inline static void dot(const_ref a, const_ref b, real_ptr c) {
    *c = a.X * b.X + a.Y * b.Y + a.Z * b.Z;
  }

  inline static real dot(const_ref a, const_ref b) {
    real res;
    dot(a, b, &res);
    return res;
  }

  inline static void cross(const_ref a, const_ref b, ptr c) {
    c->X = a.Y * b.Z - b.Y * a.Z;
    c->Y = b.X * a.Z - a.X * b.Z;
    c->Z = a.X * b.Y - b.X * a.Y;
  }

  inline static Vector3 cross(const_ref a, const_ref b) {
    Vector3 res;
    cross(a, b, &res);
    return res;
  }

  inline static void length(const_ref a, real_ptr b) {
    *b = real_sqrt(a.X * a.X + a.Y * a.Y + a.Z * a.Z);
  }

  inline static real length(const_ref a) {
    real res;
    length(a, &res);
    return res;
  }

  inline static void lengthSquared(const_ref a, real_ptr b) {
      *b = a.X * a.X + a.Y * a.Y + a.Z * a.Z;
  }

  inline static real lengthSquared(const_ref a) {
      real b;
      lengthSquared(a, &b);
      return b;
  }

  inline static void normalized(const_ref a, ptr b) {
    real len;
    length(a, &len);
    len = (real)1. / len;
    b->X = a.X * len;
    b->Y = a.Y * len;
    b->Z = a.Z * len;
  }

  inline static Vector3 normalized(const_ref a) {
    Vector3 out;
    normalized(a, &out);
    return out;
  }

  inline static void normalize(ptr a) {
    real len;
    length(*a, &len);
    len = (real)1. / len;
    a->X *= len;
    a->Y *= len;
    a->Z *= len;
  }

  inline static void limit(const_ref vec, real const& l, ptr out) {
      Vector3 copy(vec);
      real len = dot(vec, vec);
      real limit2 = (l * l);
      if (len > limit2)
      {
          Vector3::normalized(vec, &copy);
          copy *= l;
      }
      *out = copy;
  }

  static Vector3 	transform(const_ref a, const Quaternion& b);
  static void		transform(const_ref a, const Quaternion& b, ptr c);
  static Vector3 	transform(const_ref a, const Matrix& b);
  static void 		transform(const_ref a, const Matrix& b, ptr c);
  static void rotate(const_ref a, const Matrix& b, ptr c);

  __forceinline static void mul(const_ref a, const_ref b, ptr c) {
      c->X = a.X * b.Y;
      c->Y = a.Y * b.Y;
      c->Z = a.Z * b.Z;
  }

  __forceinline static void mul(ptr a, const_ref b) {
        a->X *= b.Y;
        a->Y *= b.Y;
        a->Z *= b.Z;
    }

  __forceinline static void add(const_ref a, const_ref b, ptr c) {
      c->X = a.X + b.X;
      c->Y = a.Y + b.Y;
      c->Z = a.Z + b.Z;
  }

  __forceinline static void add(ptr a, const_ref b) {
      a->X += b.X;
      a->Y += b.Y;
      a->Z += b.Z;
  }
  __forceinline static void sub(const_ref a, const_ref b, ptr c) {
      c->X = a.X - b.X;
      c->Y = a.Y - b.Y;
      c->Z = a.Z - b.Z;
  }

  inline static void sub(ptr a, const_ref b) {
      a->X -= b.X;
      a->Y -= b.Y;
      a->Z -= b.Z;
  }
  __forceinline static void scale(const_ref a, real const& b, ptr c) {
      c->X = a.X * b;
      c->Y = a.Y * b;
      c->Z = a.Z * b;
  }

  __forceinline static void scale(ptr a, real const& c) {
      a->X *= c;
      a->Y *= c;
      a->Z *= c;
  }

  __forceinline static void lerp(const_ref a, const_ref b, real const& d, ptr c) {
      c->X = a.X + (b.X - a.X) * d;
      c->Y = a.Y + (b.Y - a.Y) * d;
      c->Z = a.Z + (b.Z - a.Z) * d;
  }


  inline Vector3 operator - () const {
    Vector3 copy(*this);
    copy.X = -copy.X;
    copy.Y = -copy.Y;
    copy.Z = -copy.Z;
    return copy;
  }

  inline Vector3 operator + (const_ref b) const {
    Vector3 copy(*this);
    copy += b;
    return copy;
  }
  inline Vector3 operator - (const_ref b) const {
    Vector3 copy(*this);
    copy -= b;
    return copy;
  }
  inline Vector3 operator * (const_ref b) const {
    Vector3 copy(*this);
    copy *= b;
    return copy;
  }
  inline Vector3 operator / (const_ref b) const {
    Vector3 copy(*this);
    copy /= b;
    return copy;
  }
  inline Vector3& operator += (const_ref b) {
    X += b.X;
    Y += b.Y;
    Z += b.Z;
    return *this;
  }
  inline Vector3& operator -= (const_ref b) {
    X -= b.X;
    Y -= b.Y;
    Z -= b.Z;
    return *this;
  }
  inline Vector3& operator *= (const_ref b) {
    X *= b.X;
    Y *= b.Y;
    Z *= b.Z;
    return *this;
  }
  inline Vector3& operator /= (const_ref b) {
    X /= b.X;
    Y /= b.Y;
    Z /= b.Z;
    return *this;
  }

  inline Vector3 operator * (real const& a) const {
    Vector3 copy(*this);
    copy *= a;
    return copy;
  }

  inline Vector3& operator *= (real const& a) {
    X *= a;
    Y *= a;
    Z *= a;
    return *this;
  }

  inline Vector3 operator / (real const& a) const {
    Vector3 copy(*this);
    copy /= a;
    return copy;
  }

  inline Vector3& operator /= (real const& a) {
    // TODO: reciprocal?
    X /= a;
    Y /= a;
    Z /= a;
    return *this;
  }
};

} // qqq


template<typename S>
S & operator>>(S & s, qqq::Vector3 & v) {
  double valuex = 0.;
  double valuey = 0.;
  double valuez = 0.;
  s >> valuex;
  s >> valuey;
  s >> valuez;
  if (s) {
    v.X = valuex;
    v.Y = valuey;
    v.Z = valuez;
  }
  return s;
}

//The input stream S is a template parameter. This allows you to use just about any stream, be it a file or string stream, be it an ANSI or wide character stream.

template<typename S>
S & operator<<(S & s, qqq::Vector3 const& v) {
  s << "[X:";
  s << v.X;
  s << ", Y:";
  s << v.Y;
  s << ", Z:";
  s << v.Z;
  s << "]";

  return s;
}




#endif /* VECTOR3_H_ */
