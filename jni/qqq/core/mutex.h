/**
 * @file mutex.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jul 29, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef MUTEX_H_
#define MUTEX_H_

#include <qqq.h>
#include "lockable.h"

namespace qqq {

struct IMutex : public ILockable {
  virtual ~IMutex() {}
};

typedef void (QQQ_CALL *PFNQQQCREATEMUTEX)(void);
QQQ_C_API void QQQ_CALL qqqCreateMutex(IMutex**);

}



#endif /* MUTEX_H_ */
