#include "gettimeofday.h"

#if defined(_WIN32)

QQQ_C_API int QQQ_CALL gettimeofday(struct timeval *tv, struct timezone *tz)
{
  FILETIME ft;
  unsigned __int64 tmpres = 0;
  static int tzflag;

  if (NULL != tv)
  {
    GetSystemTimeAsFileTime(&ft);

    tmpres |= ft.dwHighDateTime;
    tmpres <<= 32;
    tmpres |= ft.dwLowDateTime;

    /*converting file time to unix epoch*/
    tmpres -= DELTA_EPOCH_IN_MICROSECS;
    tmpres /= 10;  /*convert into microseconds*/
    tv->tv_sec = (long)(tmpres / 1000000UL);
    tv->tv_usec = (long)(tmpres % 1000000UL);
  }

  if (NULL != tz)
  {
    if (!tzflag)
    {
      _tzset();
      tzflag++;
    }
    tz->tz_minuteswest;
    _get_timezone(&tz->tz_minuteswest);
    tz->tz_minuteswest = tz->tz_minuteswest / 60;

    tz->tz_dsttime;
    _get_daylight(&tz->tz_dsttime);
  }

  return 0;
}
#endif
