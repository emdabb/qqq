/**
 * @file runnable.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 5, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef RUNNABLE_H_
#define RUNNABLE_H_

#include <vector>
#include "event.h"

namespace qqq {

struct IRunnable {
  virtual ~IRunnable() {}
  virtual int run() = 0;
};

struct runnable_group : public IRunnable {
  std::vector<IRunnable*> items;

  void add(IRunnable* r) {
    items.push_back(r);
  }

  int run() {
    for(size_t i=0; i < items.size(); i++) {
      items[i]->run();
      this->on_any_done(EventArgs::Empty);
    }
    this->on_all_done(EventArgs::Empty);
    return 0;
  }

  EventHandler<EventArgs> on_any_done;
  EventHandler<EventArgs> on_all_done;
};

} // qqq

#endif /* RUNNABLE_H_ */
