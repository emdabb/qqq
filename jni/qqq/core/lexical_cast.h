/**
 * @file lexical_cast.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 18, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef LEXICAL_CAST_H_
#define LEXICAL_CAST_H_

#include <iomanip>
#include <sstream>
#include <qqq_types.h>

namespace qqq {
template <typename S, typename T>
inline S lexical_cast(const T& in) {
  std::stringstream sstr;
  S out;
  sstr << std::setprecision(type_length<T>::value) << in;
  sstr >> std::setprecision(type_length<S>::value) >> out;
  return out;
}

template <>
inline std::string lexical_cast<std::string, std::string>(const std::string& in) {
  return in;
}

}



#endif /* LEXICAL_CAST_H_ */
