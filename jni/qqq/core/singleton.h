/**
 * @file singleton.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jul 28, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef SINGLETON_H_
#define SINGLETON_H_

#include <qqq.h>
#include <core/debug_log.h>

namespace qqq {

template <typename T>
class Singleton {
  static T* inst;
public:
    Singleton() {
        intptr_t off = (intptr_t)(T*)1 - (intptr_t)(Singleton<T>*)(T*)1;
        inst = (T*)((intptr_t)this + off);
    }

    virtual ~Singleton() {
        if(inst) {
            delete inst;
            inst = NULL;
        }
    }

  static inline T& instance() {
    return *instance_ptr();
  }
  static inline T* instance_ptr() {
    if(!inst) {
      inst = new T;
    }

    return inst;
  }
};

template <typename T>
T* Singleton<T>::inst = NULL;

}



#endif /* SINGLETON_H_ */
