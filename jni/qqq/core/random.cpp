/*
 * RandomNumber.cpp
 *
 *  Created on: Mar 31, 2012
 *      Author: miel
 */

#include "random.h"
#include "math_helper.h"

using namespace qqq;

random::random(unsigned long seed) :
        mNext(0)
{
    generate_seed(seed);
}

random::~random()
{
}

double random::next()
{
    unsigned long t = generate_random_number();
    static double s = 1.0 / ((double) MAX + 1.0);
    return t * s;
}

double random::next(double hi)
{
    return next() * hi;
}

double random::next(double lo, double hi)
{
    double t = next(hi - lo);
    return lo + t;
}

void random::generate_seed(unsigned long seed)
{
    x[0] = seed & MAX;

    for (unsigned int i = 1; i < N; i++)
    {
        x[i] = (1812433253UL * (x[i - 1] ^ (x[i - 1] >> 30)) + i);
        x[i] &= MAX;
    }
}

unsigned long random::generate_random_number()
{
    unsigned long rnd = 0x0UL;

    // Refill the pool when exhausted
    if (mNext == N)
    {
        unsigned long a;

        for (unsigned int i = 0; i < N - 1; i++)
        {
            rnd = (x[i] & UPPER_MASK) | (x[i + 1] & LOWER_MASK);
            a = (rnd & 0x1UL) ? MATRIX_A : 0x0UL;
            x[i] = x[(i + M) % N] ^ (rnd >> 1) ^ a;
        }

        rnd = (x[N - 1] & UPPER_MASK) | (x[0] & LOWER_MASK);
        a = (rnd & 0x1UL) ? MATRIX_A : 0x0UL;
        x[N - 1] = x[M - 1] ^ (rnd >> 1) ^ a;

        mNext = 0; // Rewind index
    }

    rnd = x[mNext++]; // Grab the next number

    // Voodoo to improve distribution
    rnd ^= (rnd >> 11);
    rnd ^= (rnd << 7) & 0x9d2c5680UL;
    rnd ^= (rnd << 15) & 0xefc60000UL;
    rnd ^= (rnd >> 18);

    return rnd;
}

double random::next_gaussian(double mean, double stddev)
{
    static double n2 = 0.0;
    static int n2_cached = 0;
    if (!n2_cached)
    {
        double x, y, r;
        do
        {
            x = 2.0 * next() - 1;
            y = 2.0 * next() - 1;

            r = x * x + y * y;
        } while (r == 0.0 || r > 1.0);

        {
            double d = sqrt(-2.0 * log(r) / r);
            double n1 = x * d;
            n2 = y * d;

            double result = n1 * stddev + mean;

            n2_cached = 1;
            return result;
        }
    }
    else
    {
        n2_cached = 0;
        return n2 * stddev + mean;
    }
}

double random::operator ()()
{
    return next();
}

double random::operator ()(double hi)
{
    return next(hi);
}

double random::operator ()(double lo, double hi)
{
    return next(lo, hi);
}
