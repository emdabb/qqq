/**
 * @file byte4.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jul 12, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef BYTE4_H_
#define BYTE4_H_

#include <qqq_types.h>

namespace qqq {
struct byte4 {
  union {
    struct { byte_t x, y, z, w; };
    uint32_t packed_value;
  };
};
}



#endif /* BYTE4_H_ */
