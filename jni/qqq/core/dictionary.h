/*
 * dictionary.h
 *
 *  Created on: Jul 11, 2014
 *      Author: miel
 */

#ifndef DICTIONARY_H_
#define DICTIONARY_H_

#include <map>
#include <string>

namespace qqq {
template <typename T = std::string>
struct Dictionary {
  typedef std::map<std::string, T> Type;
  typedef std::pair<std::string, T> Pair;
};
}



#endif /* DICTIONARY_H_ */
