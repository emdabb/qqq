#include "aligned_malloc.h"
#include <stdint.h>
#include <qqq.h>

bool is_pow2(size_t x) {
  return (x != 0) && ((x & (x - 1)) == 0);
}

void* aligned_malloc(size_t sz, size_t a) {
  if (!is_pow2(a)) //ensure alignment is pow 2
    return NULL;
  void* p1, *p2;
  p1 = malloc(sz + a - 1 + sizeof(uintptr_t)); //allocate max memory needed for worst case alignment
  if (p1 == NULL)
    return NULL;
  uintptr_t addr = (uintptr_t) p1 + a - 1 + sizeof(uintptr_t);
  p2 = (void*) (addr - (addr % a)); //take max address and subtract the remainder to get aligned pointer
  *((uintptr_t*) p2 - 1) = (uintptr_t) p1; // store start pointer 1 uintptr_t before aligned pointer so we can free the correct pointer later
  return p2;
}

void aligned_free(void* mem) {
  if (!mem)
    return;
  void* pTrue = (void*) *((uintptr_t*) mem - 1); // get start pointer from 1 uintptr_t before aligned pointer
  free(pTrue);
}
