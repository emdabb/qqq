/*
 * vector4.h
 *
 *  Created on: Jun 27, 2014
 *      Author: miel
 */

#ifndef VECTOR4_H_
#define VECTOR4_H_

#include <qqq.h>

namespace qqq {
__declare_aligned(struct, 16) Vector4 {
  real X, Y, Z, W;
};
}



#endif /* VECTOR4_H_ */
