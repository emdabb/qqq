/*
 * static_array.h
 *
 *  Created on: Jun 27, 2014
 *      Author: miel
 */

#ifndef STATIC_ARRAY_H_
#define STATIC_ARRAY_H_

namespace qqq {

template <typename T, size_t N>
class static_array {
  T data[N];
public:
  const T& operator [] (size_t i) const {
      return data[i];
  }

};

}



#endif /* STATIC_ARRAY_H_ */
