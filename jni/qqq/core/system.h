/**
 * @file system.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 9, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#include <qqq_platform.h>

namespace qqq {

#if defined(__LINUX__) || defined(__ANDROID__) || defined(__IOS__) || defined(__MACOSX__)
typedef void* lib_t;
#elif defined(__WIN32__) || defined(__WP8__)
  typedef HMODULE lib_t;
#endif

struct system {
  static int get_processor_count();
  static lib_t load_library(const char*);
  static void* get_proc_address(lib_t, const char*);
};
}


#endif /* SYSTEM_H_ */
