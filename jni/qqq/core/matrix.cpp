/**
 * @file matrix.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 5, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include "matrix.h"
#include "math.h"
#include "vector3.h"
#include "quaternion.h"
#include <cstring>

#if defined(QQQ_HAVE_SSE) && (QQQ_PRECISION == QQQ_PRECISION_SINGLE)
#include <xmmintrin.h>
#endif

using namespace qqq;

const Matrix Matrix::Identity = {
    1., 0., 0., 0.,
    0., 1., 0., 0.,
    0., 0., 1., 0.,
    0., 0., 0., 1.
};
#if defined(QQQ_HAVE_SSE) && (QQQ_PRECISION == QQQ_PRECISION_SINGLE)
void matmul4_sse(const float m0[16], const float m1[16], float d[16]) {
  __m128 curCol = _mm_set_ps1(0.0f);
  __m128 m21 = _mm_loadu_ps(&m1[ 0]);
  __m128 m22 = _mm_loadu_ps(&m1[ 4]);
  __m128 m23 = _mm_loadu_ps(&m1[ 8]);
  __m128 m24 = _mm_loadu_ps(&m1[12]);

  curCol = _mm_mul_ps(		   m21, _mm_set_ps1(m0[ 0]));
  curCol = _mm_add_ps(_mm_mul_ps(m22, _mm_set_ps1(m0[ 1])), curCol);
  curCol = _mm_add_ps(_mm_mul_ps(m23, _mm_set_ps1(m0[ 2])), curCol);
  curCol = _mm_add_ps(_mm_mul_ps(m24, _mm_set_ps1(m0[ 3])), curCol);
  _mm_storeu_ps(&d[0], curCol);

  curCol = _mm_mul_ps(		   m21, _mm_set_ps1(m0[ 4]));
  curCol = _mm_add_ps(_mm_mul_ps(m22, _mm_set_ps1(m0[ 5])), curCol);
  curCol = _mm_add_ps(_mm_mul_ps(m23, _mm_set_ps1(m0[ 6])), curCol);
  curCol = _mm_add_ps(_mm_mul_ps(m24, _mm_set_ps1(m0[ 7])), curCol);
  _mm_storeu_ps(&d[4], curCol);

  curCol = _mm_mul_ps(		   m21, _mm_set_ps1(m0[ 8]));
  curCol = _mm_add_ps(_mm_mul_ps(m22, _mm_set_ps1(m0[ 9])), curCol);
  curCol = _mm_add_ps(_mm_mul_ps(m23, _mm_set_ps1(m0[10])), curCol);
  curCol = _mm_add_ps(_mm_mul_ps(m24, _mm_set_ps1(m0[11])), curCol);
  _mm_storeu_ps(&d[8], curCol);

  curCol = _mm_mul_ps(		   m21, _mm_set_ps1(m0[12]));
  curCol = _mm_add_ps(_mm_mul_ps(m22, _mm_set_ps1(m0[13])), curCol);
  curCol = _mm_add_ps(_mm_mul_ps(m23, _mm_set_ps1(m0[14])), curCol);
  curCol = _mm_add_ps(_mm_mul_ps(m24, _mm_set_ps1(m0[15])), curCol);
  _mm_storeu_ps(&d[12], curCol);
}
#endif

void matmul4_c(const real* m0, const real* m1, real* d) {
  d[ 0] = (m0[ 0] * m1[0]) + (m0[ 1] * m1[4]) + (m0[ 2] * m1[ 8]) + (m0[ 3] * m1[12]);
  d[ 1] = (m0[ 0] * m1[1]) + (m0[ 1] * m1[5]) + (m0[ 2] * m1[ 9]) + (m0[ 3] * m1[13]);
  d[ 2] = (m0[ 0] * m1[2]) + (m0[ 1] * m1[6]) + (m0[ 2] * m1[10]) + (m0[ 3] * m1[14]);
  d[ 3] = (m0[ 0] * m1[3]) + (m0[ 1] * m1[7]) + (m0[ 2] * m1[11]) + (m0[ 3] * m1[15]);

  d[ 4] = (m0[ 4] * m1[0]) + (m0[ 5] * m1[4]) + (m0[ 6] * m1[ 8]) + (m0[ 7] * m1[12]);
  d[ 5] = (m0[ 4] * m1[1]) + (m0[ 5] * m1[5]) + (m0[ 6] * m1[ 9]) + (m0[ 7] * m1[13]);
  d[ 6] = (m0[ 4] * m1[2]) + (m0[ 5] * m1[6]) + (m0[ 6] * m1[10]) + (m0[ 7] * m1[14]);
  d[ 7] = (m0[ 4] * m1[3]) + (m0[ 5] * m1[7]) + (m0[ 6] * m1[11]) + (m0[ 7] * m1[15]);

  d[ 8] = (m0[ 8] * m1[0]) + (m0[ 9] * m1[4]) + (m0[10] * m1[ 8]) + (m0[11] * m1[12]);
  d[ 9] = (m0[ 8] * m1[1]) + (m0[ 9] * m1[5]) + (m0[10] * m1[ 9]) + (m0[11] * m1[13]);
  d[10] = (m0[ 8] * m1[2]) + (m0[ 9] * m1[6]) + (m0[10] * m1[10]) + (m0[11] * m1[14]);
  d[11] = (m0[ 8] * m1[3]) + (m0[ 9] * m1[7]) + (m0[10] * m1[11]) + (m0[11] * m1[15]);

  d[12] = (m0[12] * m1[0]) + (m0[13] * m1[4]) + (m0[14] * m1[ 8]) + (m0[15] * m1[12]);
  d[13] = (m0[12] * m1[1]) + (m0[13] * m1[5]) + (m0[14] * m1[ 9]) + (m0[15] * m1[13]);
  d[14] = (m0[12] * m1[2]) + (m0[13] * m1[6]) + (m0[14] * m1[10]) + (m0[15] * m1[14]);
  d[15] = (m0[12] * m1[3]) + (m0[13] * m1[7]) + (m0[14] * m1[11]) + (m0[15] * m1[15]);
}

void Matrix::mul(Matrix::const_ref a, Matrix::const_ref b, Matrix::ptr out) {
#if defined(QQQ_HAVE_SSE) && (QQQ_PRECISION == QQQ_PRECISION_SINGLE)
  matmul4_sse(&a.m11, &b.m11, &out->m11);
#else
  matmul4_c(&a.m11, &b.m11, &out->m11);
#endif
}

Matrix Matrix::mul(const_ref a, const_ref b) {
  Matrix out;
  mul(a, b, &out);
  return out;
}

Matrix Matrix::inverted(const_ref in) {
  Matrix res;
  invert(in, &res);
  return res;
}
void Matrix::invert(ptr inout) {
  invert(*inout, inout);
}

void Matrix::invert(const_ref in, ptr out) {
  real num23 = (in.m33 * in.m44) - (in.m34 * in.m43);
  real num22 = (in.m32 * in.m44) - (in.m34 * in.m42);
  real num21 = (in.m32 * in.m43) - (in.m33 * in.m42);
  real num20 = (in.m31 * in.m44) - (in.m34 * in.m41);
  real num19 = (in.m31 * in.m43) - (in.m33 * in.m41);
  real num18 = (in.m31 * in.m42) - (in.m32 * in.m41);
  real num39 = ((in.m22 * num23) - (in.m23 * num22)) + (in.m24 * num21);
  real num38 = -(((in.m21 * num23) - (in.m23 * num20)) + (in.m24 * num19));
  real num37 = ((in.m21 * num22) - (in.m22 * num20)) + (in.m24 * num18);
  real num36 = -(((in.m21 * num21) - (in.m22 * num19)) + (in.m23 * num18));
  real num = (real) 1 / ((((in.m11 * num39) + (in.m12 * num38)) + (in.m13 * num37)) + (in.m14 * num36));
  out->m11 = num39 * num;
  out->m21 = num38 * num;
  out->m31 = num37 * num;
  out->m41 = num36 * num;
  out->m12 = -(((in.m12 * num23) - (in.m13 * num22)) + (in.m14 * num21)) * num;
  out->m22 = (((in.m11 * num23) - (in.m13 * num20)) + (in.m14 * num19)) * num;
  out->m32 = -(((in.m11 * num22) - (in.m12 * num20)) + (in.m14 * num18)) * num;
  out->m42 = (((in.m11 * num21) - (in.m12 * num19)) + (in.m13 * num18)) * num;
  real num35 = (in.m23 * in.m44) - (in.m24 * in.m43);
  real num34 = (in.m22 * in.m44) - (in.m24 * in.m42);
  real num33 = (in.m22 * in.m43) - (in.m23 * in.m42);
  real num32 = (in.m21 * in.m44) - (in.m24 * in.m41);
  real num31 = (in.m21 * in.m43) - (in.m23 * in.m41);
  real num30 = (in.m21 * in.m42) - (in.m22 * in.m41);
  out->m13 = (((in.m12 * num35) - (in.m13 * num34)) + (in.m14 * num33)) * num;
  out->m23 = -(((in.m11 * num35) - (in.m13 * num32)) + (in.m14 * num31)) * num;
  out->m33 = (((in.m11 * num34) - (in.m12 * num32)) + (in.m14 * num30)) * num;
  out->m43 = -(((in.m11 * num33) - (in.m12 * num31)) + (in.m13 * num30)) * num;
  real num29 = (in.m23 * in.m34) - (in.m24 * in.m33);
  real num28 = (in.m22 * in.m34) - (in.m24 * in.m32);
  real num27 = (in.m22 * in.m33) - (in.m23 * in.m32);
  real num26 = (in.m21 * in.m34) - (in.m24 * in.m31);
  real num25 = (in.m21 * in.m33) - (in.m23 * in.m31);
  real num24 = (in.m21 * in.m32) - (in.m22 * in.m31);
  out->m14 = -(((in.m12 * num29) - (in.m13 * num28)) + (in.m14 * num27)) * num;
  out->m24 = (((in.m11 * num29) - (in.m13 * num26)) + (in.m14 * num25)) * num;
  out->m34 = -(((in.m11 * num28) - (in.m12 * num26)) + (in.m14 * num24)) * num;
  out->m44 = (((in.m11 * num27) - (in.m12 * num25)) + (in.m13 * num24)) * num;
}

void Matrix::transpose(const_ref in, ptr out) {
  out->m11 = in.m11;
  out->m12 = in.m21;
  out->m13 = in.m31;
  out->m14 = in.m41;

  out->m21 = in.m12;
  out->m22 = in.m22;
  out->m23 = in.m32;
  out->m24 = in.m42;

  out->m31 = in.m13;
  out->m32 = in.m23;
  out->m33 = in.m33;
  out->m34 = in.m43;

  out->m41 = in.m14;
  out->m42 = in.m24;
  out->m43 = in.m34;
  out->m44 = in.m44;
}


void Matrix::createLookat(Vector3 const& eye, Vector3 const& at, Vector3 const& up, ptr out) {
  Vector3 vz = Vector3::normalized(eye - at);

  Vector3 vx = Vector3::cross(up, vz);

  if (Vector3::dot(vx, vx) < math::EPSILON)
    vx = Vector3::Right;
  else
    vx = Vector3::normalized(vx);

  Vector3 vy = Vector3::normalized(Vector3::cross(vz, vx));


  out->m11 = vx.X;	out->m12 = vy.X;	out->m13 = vz.X; out->m14 = (real)0.;
  out->m21 = vx.Y;	out->m22 = vy.Y;	out->m23 = vz.Y; out->m24 = (real)0.;
  out->m31 = vx.Z;	out->m32 = vy.Z;	out->m33 = vz.Z; out->m34 = (real)0.;

  out->m41 = Vector3::dot(vx, -eye);
  out->m42 = Vector3::dot(vy, -eye);
  out->m43 = Vector3::dot(vz, -eye);

  out->m44 = (real)1.;
}

void Matrix::determinant(const_ref a, real* out) {
  real num22 = a.m11;
  real num21 = a.m12;
  real num20 = a.m13;
  real num19 = a.m14;
  real num12 = a.m21;
  real num11 = a.m22;
  real num10 = a.m23;
  real num9  = a.m24;
  real num8  = a.m31;
  real num7  = a.m32;
  real num6  = a.m33;
  real num5  = a.m34;
  real num4  = a.m41;
  real num3  = a.m42;
  real num2  = a.m43;
  real num   = a.m44;
  real num18 = (num6 * num) - (num5 * num2);
  real num17 = (num7 * num) - (num5 * num3);
  real num16 = (num7 * num2) - (num6 * num3);
  real num15 = (num8 * num) - (num5 * num4);
  real num14 = (num8 * num2) - (num6 * num4);
  real num13 = (num8 * num3) - (num7 * num4);
  *out =    ((num22 * ((((num11 * num18)) - ((num10 * num17))) + ((num9 * num16))))
      - ((num21 * ((((num12 * num18)) - ((num10 * num15))) + ((num9 * num14)))))
      + ((num20 * ((((num12 * num17)) - ((num11 * num15))) + ((num9 * num13)))))
      - ((num19 * ((((num12 * num16)) - ((num11 * num14))) + ((num10 * num13))))));
}

void Matrix::createPerspectiveFov(real const& fov, real const& ratio, real const& tnear, real const& tfar, ptr out) {
  real num = 1 / real_tan(fov * 0.5f);
  real num9 = num / ratio;
  out->m11 = num9;
  out->m12 = (real)0.;
  out->m13 = (real)0.;
  out->m14 = (real)0.;

  out->m21 = (real)0.;
  out->m22 = num;
  out->m23 = (real)0.;
  out->m24 = (real)0.;

  out->m31 = (real)0.;
  out->m32 = (real)0.;
  out->m33 = tfar / (tnear - tfar);
  out->m34 = -1.0f;

  out->m41 = (real)0.;
  out->m42 = (real)0.;
  out->m43 = (tnear * tfar) / (tnear - tfar);
  out->m44 = (real)0.;
}

void Matrix::createOrthoOffcentre(real const& lt, real const& rt, real const& bt, real const& tp, real const& tnear, real const& tfar, ptr out) {
  out->m11 = (real)2. / (rt - lt);
  out->m12 = (real)0.;
  out->m13 = (real)0.;
  out->m14 = (real)0.;
  out->m21 = (real)0.;
  out->m22 = (real)2. / (tp - bt);
  out->m23 = (real)0.;
  out->m24 = (real)0.;
  out->m31 = (real)0.;
  out->m32 = (real)0.;
  out->m33 = (real)1.f / (tnear - tfar);
  out->m34 = (real)0.;
  out->m41 = (lt + rt) / (lt - rt);
  out->m42 = (bt + tp) / (bt - tp);
  out->m43 = tnear / (tnear - tfar);
  out->m44 = (real)1.;
}

void Matrix::createRotation(real const& rx, real const& ry, real const& rz, ptr out) {
  Matrix mrx, mry, mrz;
  Matrix::createRotationX(rx, &mrx);
  Matrix::createRotationY(ry, &mry);
  Matrix::createRotationZ(rz, &mrz);
  Matrix mrw;
  Matrix::mul(mrx, mry, &mrw);
  Matrix::mul(mrw, mrz,  out);
}

void Matrix::createRotationX(real const& a, ptr out) {
    *out = Matrix::Identity;
  real ct = real_cos(a);
  real st = real_sin(a);
  out->m22 =  ct;
  out->m23 = -st;
  out->m32 =  st;
  out->m33 =  ct;
}

void Matrix::createRotationY(real const& a, ptr out) {
    *out = Matrix::Identity;
  real ct = real_cos(a);
  real st = real_sin(a);
  out->m11 = ct;
  out->m13 = st;
  out->m31 = -st;
  out->m33 = ct;

}

void Matrix::createRotationZ(real const& a, ptr out) {
    *out = Matrix::Identity;
  real ct = real_cos(a);
  real st = real_sin(a);
  out->m11 =  ct;
  out->m12 = -st;
  out->m21 =  st;
  out->m22 =  ct;
}

void Matrix::createWorld(Vector3 const& pos, Vector3 const& dir, Vector3 const& up, ptr out){
  Vector3 x, y, z;
  z = Vector3::normalized(dir);
  x = Vector3::normalized(Vector3::cross(dir, up));
  y = Vector3::normalized(Vector3::cross(x, dir));

  out->m11 =  x.X;
  out->m12 =  x.Y;
  out->m13 =  x.Z;
  out->m14 =  (real)0.;
  out->m21 =  y.X;
  out->m22 =  y.Y;
  out->m23 =  y.Z;
  out->m24 = (real)0.;
  out->m31 = -z.X;
  out->m32 = -z.Y;
  out->m33 = -z.Z;
  out->m34 = (real)0.;
  out->m41 =  pos.X;
  out->m42 =  pos.Y;
  out->m43 =  pos.Z;
  out->m44 =  (real)1.;
}

void Matrix::createScale(Vector3 const& in, ptr out) {
  *out = Matrix::Identity;
  out->m11 = in.X;
  out->m22 = in.Y;
  out->m33 = in.Z;
}

void Matrix::createScale(real const& x, real const& y, real const& z, ptr out) {
    *out = Matrix::Identity;
    out->m11 = x;
    out->m22 = y;
    out->m33 = z;
}

void Matrix::createTranslation(Vector3 const& in, ptr out) {
  *out = Matrix::Identity;
  out->m41 = in.X;
  out->m42 = in.Y;
  out->m43 = in.Z;
}

void Matrix::createTranslation(real const& x, real const& y, real const& z, ptr out) {
    *out = Matrix::Identity;
    out->m41 = x;
    out->m42 = y;
    out->m43 = z;
}

void Matrix::createRotation(Quaternion const& in, ptr out) {
  *out = Matrix::Identity;
  real xx = in.X * in.X;
  real yy = in.Y * in.Y;
  real zz = in.Z * in.Z;
  real xy = in.X * in.Y;
  real zw = in.Z * in.W;
  real zx = in.Z * in.X;
  real yw = in.Y * in.W;
  real yz = in.Y * in.Z;
  real xw = in.X * in.W;

  out->m11 = math::ONE - (math::TWO * (yy + zz));
  out->m12 = math::TWO * (xy + zw);
  out->m13 = math::TWO * (zx - yw);
  out->m14 = math::ZERO;
  out->m21 = math::TWO * (xy - zw);
  out->m22 = math::ONE - (math::TWO * (zz + xx));
  out->m23 = math::TWO * (yz + xw);
  out->m24 = math::ZERO;
  out->m31 = math::TWO * (zx + yw);
  out->m32 = math::TWO * (yz - xw);
  out->m33 = math::ONE - (math::TWO * (yy + xx));
  out->m34 = math::ZERO;
  out->m41 = math::ZERO;
  out->m42 = math::ZERO;
  out->m43 = math::ZERO;
  out->m44 = math::ONE;
}

bool Matrix::operator != (const_ref other) const {
    return !memcmp(this, &other, sizeof(Matrix));
}
