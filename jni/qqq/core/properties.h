/**
 * @file properties.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 18, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef PROPERTIES_H_
#define PROPERTIES_H_

#include <map>
#include <string>
#include <vector>
#include <core/lexical_cast.h>
#include <stdexcept>

namespace qqq {

class property_not_found : std::runtime_error {
public:
  explicit property_not_found(const std::string& which) : std::runtime_error("property not found: " + which) {}
  virtual ~property_not_found() throw() {}
};

class properties {
  typedef std::string key_t;
  typedef std::string val_t;
  typedef std::multimap<key_t, val_t> dictionary_t;
  dictionary_t _data;
public:
  properties();
  virtual ~properties();
  void insert(const key_t& key, const val_t& val);
  const bool has(const key_t&) const;

  template <typename T>
  T get(const key_t& key) const{
    dictionary_t::const_iterator it = _data.find(key);
    if(it == _data.end() || _data.size() == 0) {
      throw property_not_found(key);
    }
    return lexical_cast<T>((*it).second);
  }

  template <typename T>
  T get(const key_t& key, T def) const {
    try {
      return get<T>(key);
    } catch(const property_not_found& err) {
      return def;
    }
  }

  template <typename T>
  void get(const key_t& key, std::vector<T>* values, bool append = false) const {
    if(!append) {
      values->clear();
    }
    dictionary_t::const_iterator it = _data.find(key);
    if(it == _data.end()) {
      throw property_not_found(key);
    }
    dictionary_t::const_iterator end = _data.upper_bound(key);
    for(; it != end; it++) {
      values->push_back(lexical_cast<T>((*it).second));
    }
  }
};
}


#endif /* PROPERTIES_H_ */
