/**
 * @file rectangle.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 18, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef RECTANGLE_H_
#define RECTANGLE_H_

#include <qqq.h>

namespace qqq {

__declare_aligned(struct, 16) Rectangle {
    int32_t X, Y;
    int32_t W, H;


    static const bool contains(const Rectangle& a, const Rectangle& rect) {
        if (rect.X >= a.X) {
            if (rect.Y >= a.Y) {
                if (rect.W <= a.W) {
                    if (rect.H <= a.H) {
                        return true;
                    }
                    return false;
                }
                return false;
            }
            return false;
        }
        return false;
    }

    __forceinline static int32_t getRight(const Rectangle& rc) {
        return rc.X + rc.W;
    }

    __forceinline static int32_t getBottom(const Rectangle& rc) {
        return rc.Y + rc.H;
    }

    static const bool contains(const Rectangle& rc, int x, int y) {
        if (x >= rc.X) {
            if (y >= rc.Y) {
                if (x <= Rectangle::getRight(rc)) {
                    if (y <= Rectangle::getBottom(rc)) {
                        return true;
                    }
                    return false;
                }
                return false;
            }
            return false;
        }
        return false;
    }

    __forceinline static real getArea(const Rectangle& rc) {
        return (real)rc.W * (real)rc.H;
    }
};

}

#endif /* RECTANGLE_H_ */
