#ifndef __GETTIMEOFDAY_H
#define __GETTIMEOFDAY_H

#include <qqq_platform.h>
#include <qqq_capi.h>

#if defined(__WP8__)
typedef struct timeval {
  long tv_sec;
  long tv_usec;
} timeval;
#endif

#if defined(_WIN32)
#include < time.h >
#include <Windows.h>

#if defined(_MSC_VER) || defined(_MSC_EXTENSIONS)
#define DELTA_EPOCH_IN_MICROSECS  11644473600000000Ui64
#else
#define DELTA_EPOCH_IN_MICROSECS  11644473600000000ULL
#endif

struct timezone
{
  long  tz_minuteswest; /* minutes W of Greenwich */
  int  tz_dsttime;     /* type of dst correction */
};



QQQ_C_API int QQQ_CALL gettimeofday(struct timeval *tv, struct timezone *tz);

#elif defined(__LINUX__) || defined(__ANDROID__) || defined(__IOS__) || defined(__MACOSX__)
#include <sys/time.h>
#endif

#endif
