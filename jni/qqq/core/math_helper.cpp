/*
 * math.cpp
 *
 *  Created on: Jun 3, 2014
 *      Author: miel
 */

#include <core/math_helper.h>
#include <limits>

using namespace qqq;
const real math::ZERO= (real)0.;
const real math::ONE= (real)1.;
const real math::TWO= (real)2.;
const real math::EPSILON= std::numeric_limits<double>::epsilon(); 	/* e */
const real math::LOG2E 	= 1.4426950408889634074;		 	/* log_2 e */
const real math::LOG10E = 0.4342944819032518277;		 	/* log_10 e */
const real math::LOGE2 	= 0.6931471805599453094;		 	/* log_e 2 */
const real math::LOGE10 = 2.3025850929940456840;		 	/* log_e 10 */
const real math::PI 	= 3.1415926535897932385;	 		/* pi */
const real math::TWO_PI = 6.2831853071795864769; 			/* 2 pi */
const real math::PI_OVER_2	= 1.57079632679489661923;		/* pi/2 */
const real math::PI_OVER_4	= 0.78539816339744830962; 		/* pi/4 */
const real math::PI_OVER_180= 0.01745329251994329576;		/* pi/180 */
const real math::ONE_OVER_PI= 0.31830988618379067154; 		/* 1/pi */
const real math::TWO_OVER_PI= 0.63661977236758134308; 		/* 2/pi */
const real math::SQRT_2 	= 1.41421356237309504880;		/* sqrt(2) */
const real math::TWO_OVER_SQRT_PI 	= 1.12837916709551257390; /* 2/sqrt(pi) */
const real math::ONE_OVER_SQRT_2 	= 0.70710678118654752440; /* 1/sqrt(2) */
const real math::PHI = (real)1. + real_sqrt(5. / 2.);

#if QQQ_PRECISION == QQQ_PRECISION_FIXED

template<typename T, uint8_t I, uint8_t F>
const fixed<T, I, F> fixed<T, I, F>::PI = fixed<T, I, F>(math::PI);

template<typename T, uint8_t I, uint8_t F>
const fixed<T, I, F> fixed<T, I, F>::TWO_PI= fixed<T, I, F>(math::TWO_PI);

template<typename T, uint8_t I, uint8_t F>
const fixed<T, I, F> fixed<T, I, F>::ONE = static_cast<T>(1.);//fixed<T, I, F>::pow2<F>::value);

#endif

