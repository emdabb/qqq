/**
 * @file aabb.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jul 5, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef AABB_H_
#define AABB_H_

#include "vector3.h"

namespace qqq  {

struct Matrix;

struct AABB {
  typedef const AABB& const_ref;
  typedef AABB* ptr;

  Vector3 min;
  Vector3 max;

  void add(const Vector3&);
  static void transform(const_ref, const Matrix&, ptr);
};

}



#endif /* AABB_H_ */
