/**
 * @file threadpool.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 6, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include "threadpool.h"
#include "aligned_malloc.h"
#include "system.h"
#include <core/debug_log.h>

using namespace qqq;

struct Threadpool::internal_thread: public IRunnable {
    Threadpool* pool;

    internal_thread(Threadpool* p) :
            pool(p) {
    }

    virtual ~internal_thread() {
    }
    virtual int run() {

        while (1) {
            try {
                IRunnable* obj = pool->next_runnable();
                if (obj) {
                    obj->run();
                }

            } catch (...) {
                DEBUG_METHOD();
                DEBUG_MESSAGE("thread caught exception");
            }
        }
        return 0;
    }
};

//#define MAX_RUNNABLES	1024
//#define MAX_THREADS		4

Threadpool::Threadpool() {
    DEBUG_METHOD();

    //lock = new mutex;
    qqqCreateMutex(&lock);
    qqqCreateWaitCondition(&event);

    num_threads = system::get_processor_count() * 2 - 1;// -1 for the main thread.
    threads = (IThread**)aligned_malloc(num_threads * sizeof(IThread*), 4);


    DEBUG_VALUE_AND_TYPE_OF(num_threads);

    for(unsigned int i=0; i < num_threads; i++) {
        qqqCreateThread(&threads[i], new internal_thread(this));
    }
    for(unsigned int i=0; i < num_threads; i++) {
        threads[i]->start();
    }

    qqqCreateWaitCondition(&event);
}

Threadpool::~Threadpool() {
    delete lock;
}

void Threadpool::add_runnable(IRunnable* ptr) {
    synchronized(lock)
    {
        queue.push(ptr);
        //event->set();
    }
}

bool Threadpool::has_work() {
    int workItems = 0;
    synchronized(lock)
    {
        workItems = queue.size();
    }
    return workItems > 0;
}

IRunnable* Threadpool::next_runnable() {
    IRunnable* ret = NULL;
    synchronized(lock)
    {
        //while(queue.empty() ) {
        //  event->wait();
        //}
        if (queue.size() > 0) {
            ret = queue.front();
            queue.pop();
        }
    }
    return ret;

}
