/**
 * @file event.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 9, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef EVENT_H_
#define EVENT_H_

#include <vector>
#include <algorithm>

namespace qqq {

struct EventArgs {
    static EventArgs Empty;
};

template<typename ARGS>
class handler_function_base {
    typedef ARGS args_type;
    virtual void call(const args_type&) = 0;
public:

    virtual ~handler_function_base() {
    }

    void operator ()(const args_type& args) {
        call(args);
    }
};

template<typename ARGS>
class handler_function: public handler_function_base<ARGS> {
    typedef ARGS args_type;
    typedef void (*function_t)(const args_type&);
    function_t function_ptr;
    virtual void call(const args_type& args) {
        function_ptr(args);
    }
public:
    handler_function(function_t fun) :
            function_ptr(fun) {
    }
    virtual ~handler_function() {
    }
};

template<typename CLASS, typename ARGS>
class handler_method: public handler_function_base<ARGS> {
    typedef ARGS args_type;
    typedef CLASS class_type;
    typedef void (class_type::*function_t)(const args_type&);
    class_type* obj;
    function_t function_ptr;
    virtual void call(const args_type& args) {
        ((*obj).*function_ptr)(args);
    }
public:
    handler_method(class_type* object, function_t fun) :
            obj(object), function_ptr(fun) {
    }
    virtual ~handler_method() {
    }
};

template<typename _ClassPtr, typename ARGS>
handler_function_base<ARGS>* event(_ClassPtr* object,
        void (_ClassPtr::*fun)(const ARGS&)) {
    return new handler_method<_ClassPtr, ARGS>(object, fun);
}

template<typename _ClassPtr, typename ARGS>
handler_function_base<ARGS>* event(const _ClassPtr* object,
        void (_ClassPtr::*fun)(const ARGS&)) {
    return new handler_method<const _ClassPtr, ARGS>(object, fun);
}

template<typename ARGS>
handler_function_base<ARGS>* event(void (*fun)(const ARGS&)) {
    return new handler_function<ARGS>(fun);
}

template<typename ARGS>
class EventHandler {
    typedef ARGS args_type;
    typedef handler_function_base<args_type> handler_t;
    typedef std::vector<handler_t*> observer_list_t;
    observer_list_t handlers;
public:
    virtual ~EventHandler() {
        handlers.clear();
    }
    void operator +=(handler_function_base<args_type>* callback) {
        handlers.push_back(callback);
    }
    void operator -=(handler_function_base<args_type>* callback) {
        typename observer_list_t::iterator it = std::find(handlers.begin(),
                handlers.end(), callback);
        if (it != handlers.end()) {
            handlers.erase(it);
        }
    }
    void operator ()(const args_type& args) {
        size_t len = handlers.size();
        for (size_t i = 0; i < len; ++i) {
            handlers[i]->operator()(args); //doCall(args);
        }
    }
};
} // qqq

#endif /* EVENT_H_ */
