/**
 * @file properties.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 18, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */


#include "properties.h"

using namespace qqq;

properties::properties() {

}

properties::~properties() {

}


void properties::insert(const key_t& key, const val_t& val) {
  _data.insert(std::pair<std::string, std::string>(key, val));
}

const bool properties::has(const key_t& key) const {
  return _data.find(key) != _data.end();
}
