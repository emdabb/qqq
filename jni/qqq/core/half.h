/*
 * half.h
 *
 *  Created on: Jun 3, 2014
 *      Author: miel
 */

#ifndef HALF_H_
#define HALF_H_

#include <stdint.h>
#include <stddef.h>
/**
 * A half precision float point number that complies with IEEE754 standards.
 *
 * @param f		input IEEE754 floating point number
 * @return		converted IEEE754 conformant half precision floating point number.
 * @note This data type is native to some OpenCL implementations. Specifically ARM and PowerVR GPUs.
 * This means a theoretical 2x speedup when the bottleneck of you application is the PCI transfer time (this
 * is actually true in most cases).
 */

struct HalfSingle {
  uint16_t PackedData;

  explicit HalfSingle(float f) {
    sp2hp(&f, &PackedData, 1);
  }

  virtual ~HalfSingle() {

  }

  inline HalfSingle& operator = (float f) {
    sp2hp(&f, &PackedData, 1);
    return *this;
  }

  static int sp2hp(void* dst, const void* src, size_t count);
  static int hp2sp(void* dst, const void* src, size_t count);

  inline operator float () const {
    float dst;
    hp2sp(&dst, &PackedData, 1);
    return dst;
  }
};




#endif /* HALF_H_ */
