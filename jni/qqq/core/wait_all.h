/**
 * @file wait_all.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 9, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef WAIT_ALL_H_
#define WAIT_ALL_H_

#include <types.h>

struct wait_all {
  wait_all(const thread_t* threads, size_t num) {
    for(size_t i=0; i < num; i++) {
      pthread_join(threads[i].handle, NULL);
    }
  }

  ~wait_all() {

  }
};



#endif /* WAIT_ALL_H_ */
