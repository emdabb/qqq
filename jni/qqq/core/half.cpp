/*
 * half.cpp
 *
 *  Created on: Jun 3, 2014
 *      Author: miel
 */

#include "half.h"

int HalfSingle::sp2hp(void* dst, const void* src, size_t count) {
  if (src == NULL || dst == NULL) { // Nothing to convert (e.g., imag part of pure real)
    return 0;
  }

  uint16_t *hp = (uint16_t *) dst; // Type pun output as an unsigned 16-bit int
  uint32_t *xp = (uint32_t *) src; // Type pun input as an unsigned 32-bit int
  uint16_t hs, he, hm;
  uint32_t x, xs, xe, xm;
  int hes;
  static int next;  // Little Endian adjustment
  static int checkieee = 1; // Flag to check for IEEE754, Endian, and word size
  double one = 1.0; // Used for checking IEEE754 floating point format
  uint32_t *ip; // Used for checking IEEE754 floating point format

  if (checkieee) { // 1st call, so check for IEEE754, Endian, and word size
    ip = (uint32_t *) &one;
    if (*ip) { // If Big Endian, then no adjustment
      next = 0;
    } else { // If Little Endian, then adjustment will be necessary
      next = 1;
      ip++;
    }
    if (*ip != 0x3FF00000u) { // Check for exact IEEE 754 bit pattern of 1.0
      return 1;  // Floating point bit pattern is not IEEE 754
    }
    if (sizeof(int16_t) != 2 || sizeof(int32_t) != 4) {
      return 1;  // short is not 16-bits, or long is not 32-bits.
    }
    checkieee = 0; // Everything checks out OK
  }
  size_t numel = count;
  while (numel--) {
    x = *xp++;
    if ((x & 0x7FFFFFFFu) == 0) {  // Signed zero
      *hp++ = (uint16_t) (x >> 16);  // Return the signed zero
    } else { // Not zero
      xs = x & 0x80000000u;  // Pick off sign bit
      xe = x & 0x7F800000u;  // Pick off exponent bits
      xm = x & 0x007FFFFFu;  // Pick off mantissa bits
      if (xe == 0) {  // Denormal will underflow, return a signed zero
        *hp++ = (uint16_t) (xs >> 16);
      } else if (xe == 0x7F800000u) { // Inf or NaN (all the exponent bits are set)
        if (xm == 0) { // If mantissa is zero ...
          *hp++ = (uint16_t) ((xs >> 16) | 0x7C00u); // Signed Inf
        } else {
          *hp++ = (uint16_t) 0xFE00u; // NaN, only 1st mantissa bit set
        }
      } else { // Normalized number
        hs = (uint16_t) (xs >> 16); // Sign bit
        hes = ((int) (xe >> 23)) - 127 + 15; // Exponent unbias the single, then bias the halfp
        if (hes >= 0x1F) {  // Overflow
          *hp++ = (uint16_t) ((xs >> 16) | 0x7C00u); // Signed Inf
        } else if (hes <= 0) {  // Underflow
          if ((14 - hes) > 24) { // Mantissa shifted all the way off & no rounding possibility
            hm = (uint16_t) 0u;  // Set mantissa to zero
          } else {
            xm |= 0x00800000u;  // Add the hidden leading bit
            hm = (uint16_t) (xm >> (14 - hes)); // Mantissa
            if ((xm >> (13 - hes)) & 0x00000001u) // Check for rounding
              hm += (uint16_t) 1u; // Round, might overflow into exp bit, but this is OK
          }
          *hp++ = (hs | hm); // Combine sign bit and mantissa bits, biased exponent is zero
        } else {
          he = (uint16_t) (hes << 10); // Exponent
          hm = (uint16_t) (xm >> 13); // Mantissa
          if (xm & 0x00001000u) // Check for rounding
            *hp++ = (hs | he | hm) + (uint16_t) 1u; // Round, might overflow to inf, this is OK
          else
            *hp++ = (hs | he | hm);  // No rounding
        }
      }
    }
  }
  return 0;
}

int HalfSingle::hp2sp(void* dst, const void* src, size_t count) {
  if( src == NULL || dst == NULL ) // Nothing to convert (e.g., imag part of pure real)
      return 0;

  uint16_t *hp = (uint16_t *) src; // Type pun input as an unsigned 16-bit int
  uint32_t *xp = (uint32_t *) dst; // Type pun output as an unsigned 32-bit int
  uint16_t h, hs, he, hm;
  uint32_t xs, xe, xm;
  int32_t xes;
  int e;
  static int next;  // Little Endian adjustment
  static int checkieee = 1;  // Flag to check for IEEE754, Endian, and word size
  double one = 1.0; // Used for checking IEEE754 floating point format
  uint32_t *ip; // Used for checking IEEE754 floating point format

  if( checkieee ) { // 1st call, so check for IEEE754, Endian, and word size
    ip = (uint32_t *) &one;
    if( *ip ) { // If Big Endian, then no adjustment
      next = 0;
    } else { // If Little Endian, then adjustment will be necessary
      next = 1;
      ip++;
    }
    if( *ip != 0x3FF00000u ) { // Check for exact IEEE 754 bit pattern of 1.0
      return 1;  // Floating point bit pattern is not IEEE 754
    }
    if( sizeof(int16_t) != 2 || sizeof(int32_t) != 4 ) {
      return 1;  // short is not 16-bits, or long is not 32-bits.
    }
    checkieee = 0; // Everything checks out OK
  }

  size_t numel = count;

  while( numel-- ) {
    h = *hp++;
    if( (h & 0x7FFFu) == 0 ) {  // Signed zero
      *xp++ = ((uint32_t) h) << 16;  // Return the signed zero
    } else { // Not zero
      hs = h & 0x8000u;  // Pick off sign bit
      he = h & 0x7C00u;  // Pick off exponent bits
      hm = h & 0x03FFu;  // Pick off mantissa bits
      if( he == 0 ) {  // Denormal will convert to normalized
        e = -1; // The following loop figures out how much extra to adjust the exponent
        do {
          e++;
          hm <<= 1;
        } while( (hm & 0x0400u) == 0 ); // Shift until leading bit overflows into exponent bit
        xs = ((uint32_t) hs) << 16; // Sign bit
        xes = ((int32_t) (he >> 10)) - 15 + 127 - e; // Exponent unbias the halfp, then bias the single
        xe = (uint32_t) (xes << 23); // Exponent
        xm = ((uint32_t) (hm & 0x03FFu)) << 13; // Mantissa
        *xp++ = (xs | xe | xm); // Combine sign bit, exponent bits, and mantissa bits
      } else if( he == 0x7C00u ) {  // Inf or NaN (all the exponent bits are set)
        if( hm == 0 ) { // If mantissa is zero ...
          *xp++ = (((uint32_t) hs) << 16) | ((uint32_t) 0x7F800000u); // Signed Inf
        } else {
          *xp++ = (uint32_t) 0xFFC00000u; // NaN, only 1st mantissa bit set
        }
      } else { // Normalized number
        xs = ((uint32_t) hs) << 16; // Sign bit
        xes = ((int32_t) (he >> 10)) - 15 + 127; // Exponent unbias the halfp, then bias the single
        xe = (uint32_t) (xes << 23); // Exponent
        xm = ((uint32_t) hm) << 13; // Mantissa
        *xp++ = (xs | xe | xm); // Combine sign bit, exponent bits, and mantissa bits
      }
    }
  }
  return 0;
}

//uint16_t half::to_half(float f) {
//	uif ui;
//	ui.f = f;
//	int s = (ui.i >> 16) & 0x00008000;
//	int e = ((ui.i >> 23) & 0x000000ff) - (127 - 15);
//	int m = ui.i & 0x007fffff;
//	if (e <= 0) {
//		if (e < -10) {
//			return (uint16_t) s;
//		}
//		m = m | 0x00800000;
//		int t = 14 - e;
//		int a = (1 << (t - 1)) - 1;
//		int b = (m >> t) & 1;
//
//		m = (m + a + b) >> t;
//
//		return (uint16_t) (s | m);
//	} else if (e == 0xff - (127 - 15)) {
//		if (m == 0) {
//			return (uint16_t) (s | 0x7c00);
//		} else {
//			m >>= 13;
//			return (uint16_t) (s | 0x7c00 | m | ((m == 0) ? 1 : 0));
//		}
//	} else {
//		m = m + 0x00000fff + ((m >> 13) & 1);
//		if ((m & 0x00800000) != 0) {
//			m = 0;
//			e += 1;
//		}
//		if (e > 30) {
//			return (uint16_t) (s | 0x7c00);
//		}
//		return (uint16_t) (s | (e << 10) | (m >> 13));
//	}
//}
