/*
 * Socket.h
 *
 *  Created on: Oct 21, 2013
 *      Author: miel
 */

#ifndef SOCKET_H_
#define SOCKET_H_

#include <qqq.h>
#include <qqq_except.h>
#include <string>
#include <vector>
#include <core/event.h>

struct sockaddr_in;

#if defined(__WIN32__)
struct WSAData;
#endif

namespace qqq {

class Socket;

struct SocketEventArgs {
	Socket* thiz;
};

class Socket {
    int 		mHandle;
    bool 		mIsBlocking;
    uint32_t 	mHostAddr;
    uint16_t 	mHostPort;
public:
    enum {
        E_TYPE_TCP, E_TYPE_UDP, E_TYPE_MAX
    };
private:
    int error();

public:
    Socket(const int type = E_TYPE_TCP);
    Socket(int, sockaddr_in*);
    virtual ~Socket();
    bool isValid() const;
    bool bind(uint16_t);
    Socket* accept();
    bool open(std::string const&, uint16_t);
    bool open(int, uint16_t);
    bool close();
    bool isClosed() const;
    bool listen();
    int receive(void*, size_t);
    int receiveFrom(void*, size_t, int*, uint16_t*);
    int send(const void*, size_t);
    int sendTo(const void*, size_t, int, uint16_t);
    bool setTimeout(int msec);
    //void onDisconnect();
    bool setBroadcast(bool broadcast);
    bool setBlocking(bool);

    uint32_t getHostAddress() const {
        return mHostAddr;
    }

    static std::string ip2string(const uint32_t);
    static uint32_t string2ip(std::string const&);
    static bool getHostAdaptorsByName(std::string const&,
            std::vector<uint32_t>*);
    static bool getHostAdaptorsByAddress(uint32_t const,
            std::vector<uint32_t>*);
    static bool getLocalAdaptors(std::vector<uint32_t>*);//, std::vector<uint32_t>*);

    EventHandler<SocketEventArgs> OnDisconnect;

};
}

#if defined(__WIN32__)
static class WSAHandler
{
    WSAData* _data;
public:
    WSAHandler();
    virtual ~WSAHandler();
}gWSAHandler;
#endif

#endif /* SOCKET_H_ */
