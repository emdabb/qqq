/*
 * Socket.cpp
 *
 *  Created on: Jul 5, 2013
 *      Author: miel
 */

#include "socket.h"

#if defined (__WIN32__)
# ifndef WIN32_LEAN_AND_MEAN
#  define WIN32_LEAN_AND_MEAN 1
# endif
# include <windows.h>
# include <winsock.h>
# define QQQ_SOCKET_ERROR     SOCKET_ERROR
# define QQQ_INVALID_SOCKET   INVALID_SOCKET
# define QQQ_EINPROGRESS      WSAEINPROGRESS - 10000
# define QQQ_EWOULDBLOCK      WSAEWOULDBLOCK - 10000
# define QQQ_SOCKLEN          int
#else
# include <unistd.h>
# include <sys/types.h>
# include <sys/socket.h>
# include <sys/ioctl.h>
# include <netinet/in.h>
# include <arpa/inet.h>
# include <netdb.h>
# include <errno.h>
#  if defined(__ANDROID__)
#   include <net/if.h>
#  else
#   include <ifaddrs.h>
#  endif
# define QQQ_SOCKET_ERROR     -1
# define QQQ_INVALID_SOCKET   -1
# define QQQ_EINPROGRESS      EINPROGRESS
# define QQQ_EWOULDBLOCK      EWOULDBLOCK
# define QQQ_SOCKLEN          socklen_t
#endif

#if defined(__WIN32__)
# pragma comment(lib, "ws2_32.lib")
#endif

#include <core/debug_log.h>

using namespace qqq;

#if defined(__WIN32__)
WSAHandler::WSAHandler()
{
    DEBUG_METHOD();
    _data = new WSADATA();
    int err = WSAStartup(MAKEWORD(1,1), _data);
    if(!err)
    {
        DEBUG_MESSAGE("WSAHandler::WSAHandler(): error %d", err);
    }
}

WSAHandler::~WSAHandler()
{
    DEBUG_METHOD();
    int err = WSACleanup();
    delete _data;
    if(!err)
    {

        DEBUG_MESSAGE("WSAHandler::~WSAHandler(): error %d", err);
    }
}
#endif


Socket::Socket(const int type) :
        mHandle(QQQ_INVALID_SOCKET), mIsBlocking(true), mHostAddr(0), mHostPort(
                0) {
    DEBUG_METHOD();
    int sotype = SOCK_STREAM;
    int soprot = IPPROTO_TCP;
    if(type == E_TYPE_UDP) {
        sotype = SOCK_DGRAM;
        soprot = IPPROTO_UDP;
    }
    mHandle = ::socket(PF_INET, sotype, soprot);
    //_handle = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    int value = 1;
    ::setsockopt(mHandle, SOL_SOCKET, SO_REUSEADDR, (char*) &value, sizeof(int));
#if defined(SO_NOSIGPIPE)
    ::setsockopt(mHandle, SOL_SOCKET, SO_NOSIGPIPE, (char*)&value, sizeof(int));
#endif

}

Socket::Socket(int32_t handle, sockaddr_in* addr) :
        mHandle(handle), mIsBlocking(true), mHostAddr(addr->sin_addr.s_addr), mHostPort(
                addr->sin_port) {
    DEBUG_METHOD();
    DEBUG_VALUE_AND_TYPE_OF(handle);
    DEBUG_VALUE_AND_TYPE_OF(addr);
    int value = 1;
    ::setsockopt(mHandle, SOL_SOCKET, SO_REUSEADDR, (char*) &value, sizeof(int));
#if defined(SO_NOSIGPIPE)
    ::setsockopt(mHandle, SOL_SOCKET, SO_NOSIGPIPE, (char*)&value, sizeof(int));
#endif
    struct timeval timeout;
    timeout.tv_sec = 1;
    timeout.tv_usec = 0;

    if (setsockopt (mHandle, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout,
                    sizeof(timeout)) < 0)
    DEBUG_MESSAGE("setsockopt failed");

    if (setsockopt (mHandle, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout,
                    sizeof(timeout)) < 0)
    DEBUG_MESSAGE("setsockopt failed");
}

Socket::~Socket() {

}

bool Socket::setBroadcast(bool broadcast) {
    DEBUG_METHOD();
    int value=broadcast;
    int result=setsockopt(mHandle,SOL_SOCKET,SO_BROADCAST,(char*)&value,sizeof(value));
    if(result==QQQ_SOCKET_ERROR) {
        DEBUG_MESSAGE("SOCKET_ERROR");
        return false;
    }
    return true;
}

bool Socket::setTimeout(int msec) {
    DEBUG_METHOD();
    struct timeval timeout;
    timeout.tv_sec = msec / 1000;
    timeout.tv_usec = msec * 1000;

    if (setsockopt (mHandle, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0) {
        DEBUG_MESSAGE("setsockopt failed");
        return false;
    }

    if (setsockopt (mHandle, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout)) < 0) {
        DEBUG_MESSAGE("setsockopt failed");
        return false;
    }
    return true;
}

bool Socket::isValid() const {
    return mHandle != QQQ_INVALID_SOCKET && mHostAddr != 0 && mHostPort != 0;
}

bool Socket::bind(uint16_t port) {
    DEBUG_METHOD();
    DEBUG_VALUE_AND_TYPE_OF(port);

    struct sockaddr_in addr = {0};
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    addr.sin_port = htons(port);

    int err = ::bind(mHandle, (struct sockaddr*) &addr, sizeof(addr));
    if (err == QQQ_SOCKET_ERROR) {
        return false;
    }
    return true;
}

bool Socket::open(int ip, uint16_t port) {
    DEBUG_METHOD();
    DEBUG_VALUE_AND_TYPE_OF(ip2string(ip).c_str());
    DEBUG_VALUE_AND_TYPE_OF(port);
    this->mHostAddr = ip;
    this->mHostPort = port;

    struct sockaddr_in address={0};
    address.sin_family		= AF_INET;
    address.sin_addr.s_addr	=mHostAddr;;
    address.sin_port		= htons(mHostPort);

    int result=::connect(mHandle, (struct sockaddr*)&address,sizeof(address));
    if(result==QQQ_SOCKET_ERROR){
            result=error();
            DEBUG_MESSAGE("SOCKET_ERROR %d", result);
            if(result == EINPROGRESS) {
                DEBUG_MESSAGE("EINPROGRESS");
            } else {
            	mHandle = QQQ_INVALID_SOCKET;
            }
            return false;
    }

    return true;
}

bool Socket::open(std::string const& name, uint16_t port) {
    std::vector<uint32_t> list;
    getHostAdaptorsByName(name, &list);
    if(list.size() > 0) {
        return open(list[0], port);
    }
    return false;
}

bool Socket::setBlocking(bool blocking) {
    DEBUG_METHOD();
    unsigned long value=!blocking;
    int result=0;
#if defined(__WIN32__)
    result=ioctlsocket(mHandle, FIONBIO, &value);
#else
    result=ioctl(mHandle, FIONBIO,&value);
#endif
    if(result==QQQ_SOCKET_ERROR) {

        DEBUG_MESSAGE("SOCKET_ERROR");

        return false;
    }
    this->mIsBlocking = blocking;
    return true;
}

bool Socket::isClosed() const {
    return mHandle == QQQ_INVALID_SOCKET || mHostAddr == 0 || mHostPort == 0;
}

bool Socket::close() {
    DEBUG_METHOD();
    if (mHandle != QQQ_INVALID_SOCKET) {
#if defined(__WIN32__)
        shutdown(mHandle,2);
        ::closesocket(mHandle);
#else
        shutdown(mHandle, SHUT_RDWR);
        ::close(mHandle);
#endif
        mHandle = QQQ_INVALID_SOCKET;
    }
    mHostAddr = 0;
    mHostPort = 0;
    return true;
}

bool Socket::listen() {
    DEBUG_METHOD();
    int err = ::listen(mHandle, 4);
    if (err == QQQ_SOCKET_ERROR) {
        err = error();
        DEBUG_VALUE_AND_TYPE_OF(err);
        return false;
    }
    return true;
}

Socket* Socket::accept() {
    DEBUG_METHOD();
    struct sockaddr_in clientAddr;
    QQQ_SOCKLEN clientAddrLen = sizeof(clientAddr);
    int clientHandle = ::accept(mHandle, (struct sockaddr*) &clientAddr, &clientAddrLen);
    DEBUG_VALUE_AND_TYPE_OF(clientHandle);
    if (clientHandle == QQQ_SOCKET_ERROR) {
        int result = error();
        if (result != QQQ_EWOULDBLOCK) {
        }
        return NULL;
    } else {
        Socket* sock = new Socket(clientHandle, &clientAddr);
        return sock;
    }
}

int Socket::receive(void* buffer, size_t len) {
    int flags = 0;
    int err = ::recv(mHandle, (char*) buffer, len, flags);
    if (err < 0) {
        DEBUG_METHOD();
        DEBUG_MESSAGE("net_socket::receive() failed");
        //err = -error();
    } else if(err == 0) {
        /**
         * no messages are available to be received and the peer has performed an orderly shutdown
         */
    	SocketEventArgs args;
    	args.thiz = this;
        OnDisconnect(args);
    }
    return err;
}

int Socket::receiveFrom(void* buf, size_t len, int* ip, uint16_t* port) {
    int flags = 0;
    struct sockaddr_in from = { 0 };
    QQQ_SOCKLEN fromLength = sizeof(sockaddr_in);
    int result = ::recvfrom(mHandle, (char*) buf, len, flags,
            (struct sockaddr*) &from, &fromLength);
    if (ip) {
        *ip = from.sin_addr.s_addr;
    }
    if (port) {
        *port = ntohs(from.sin_port);
    }
    if (result < 0) {
        result = -error();
        DEBUG_METHOD();
        DEBUG_MESSAGE("receive_from: error: %d", result);
    }
    return result;

}

//void Socket::onDisconnect() {
//    DEBUG_METHOD();
//}

int Socket::send(const void* buffer, size_t len) {
#if defined(MSG_NOSIGNAL)
    int flags = MSG_NOSIGNAL;
#else
    int flags = 0;
#endif
    int err = ::send(mHandle, (char*) buffer, len, flags);
    if (err < 0) {
        DEBUG_METHOD();
        DEBUG_MESSAGE("net_socket::send() failed");
        //err = -error();

    }
    return err;
}

int Socket::sendTo(const void* buf, size_t len, int ip, uint16_t port) {
#if defined(MSG_NOSIGNAL)
    int flags = MSG_NOSIGNAL;
#else
    int flags=0;
#endif
    struct sockaddr_in address = { 0 };
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = ip;
    address.sin_port = htons(port);
    int result = ::sendto(mHandle, (char*) buf, len, flags,
            (struct sockaddr*) &address, sizeof(address));
    if (result < 0) {
        result = -error();
        DEBUG_METHOD();
        DEBUG_MESSAGE("send_to: error: %d", result);
    }
    return result;

}

bool Socket::getHostAdaptorsByName(const std::string& name,
        std::vector<uint32_t>* adaptors) {
    DEBUG_METHOD();
    struct hostent *he = ::gethostbyname(name.c_str());
    if (he != NULL) {
        char **list = he->h_addr_list;
        while ((*list) != NULL) {
            DEBUG_VALUE_AND_TYPE_OF(*list);
            adaptors->push_back(*(uint32_t*) (*list));
            ++list;
        }
        return true;
    }
    return false;
}

bool Socket::getHostAdaptorsByAddress(const uint32_t addr,
        std::vector<uint32_t>* adaptors) {
    DEBUG_METHOD();
    struct hostent *he = gethostbyaddr((char*) &addr, 4, AF_INET);
    if (he != NULL) {
        char **list = he->h_addr_list;
        while ((*list) != NULL) {
            DEBUG_VALUE_AND_TYPE_OF(*list);
            adaptors->push_back(*(uint32_t*) (*list));
            ++list;
        }
        return true;
    }
    return false;
}

bool Socket::getLocalAdaptors(std::vector<uint32_t>* adaptors ) {//, std::vector<uint32_t>* netmask) {
    DEBUG_METHOD();
#if defined(__WIN32__)
    return getHostAdaptorsByName("", adaptors);
#elif defined(__ANDROID__)
    struct ifreq ifreqs[20];
    struct ifconf ifconf;

    memset(&ifconf, 0, sizeof(ifconf));
    ifconf.ifc_buf = (char*) (ifreqs);
    ifconf.ifc_len = sizeof(ifreqs);

    int sock = ::socket(AF_INET, SOCK_STREAM, 0);
    ioctl(sock, SIOCGIFCONF, (char*) &ifconf);
    ::close(sock);

    int count = ifconf.ifc_len / sizeof(struct ifreq);
    for (int i = 0; i < count; ++i)
    {
        adaptors->push_back(((struct sockaddr_in*) &ifreqs[i].ifr_addr)->sin_addr.s_addr);
    }
    return true;
#else
    struct ifaddrs *addrs, *a = NULL;
    getifaddrs(&addrs);
    for (a = addrs; a != NULL; a = a->ifa_next) {
        if (a->ifa_addr->sa_family == AF_INET) {
            adaptors->push_back(((struct sockaddr_in*) a->ifa_addr)->sin_addr.s_addr);

        }
    }
    freeifaddrs(addrs);
    return true;
#endif
}

int Socket::error() {
#if defined(__WIN32__)
    return WSAGetLastError()-10000;
#else
    return errno;
#endif
}

std::string Socket::ip2string(const uint32_t addr) {
    return inet_ntoa(*(in_addr*) &addr);
}

uint32_t Socket::string2ip(const std::string& addr) {
    return inet_addr(addr.c_str());
}
