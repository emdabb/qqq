/*
 * vector2.h
 *
 *  Created on: Jun 27, 2014
 *      Author: miel
 */

#ifndef VECTOR2_H_
#define VECTOR2_H_

#include <qqq.h>
#include <core/math_helper.h>

namespace qqq {
__declare_aligned(struct, 8) Vector2 {
  real X, Y;

  static const Vector2 Zero;
  static const Vector2 One;
  static const Vector2 Up;
  static const Vector2 Right;

  typedef Vector2* ptr;
  typedef const Vector2& const_ref;

  static real dot(const_ref a, const_ref b) {
    real res;
    dot(a, b, &res);
    return res;
  }
  static void dot(const_ref a, const_ref b, real* out) {
      *out = a.X * b.X + a.Y * b.Y;
  }

  static void normalized(const_ref a, Vector2* out);

  static Vector2 	normalized(const_ref a);

  static void normalize(ptr out) {
      real r  = (real)1.f / length(*out);
      out->X *= r;
      out->Y *= r;
  }

  static real length(const_ref in) {
    real out;
    length(in, &out);
    return out;
  }

  static inline void lengthSq(const_ref in, real* out) {
      *out = in.X * in.X + in.Y * in.Y;
  }

  static inline real lengthSq(const_ref in) {
      real out;
      lengthSq(in, &out);
      return out;
  }

  static Vector2 create_random(int seed);
  static void	  create_random(int seed, ptr out);

  __forceinline static void lerp(const_ref a, const_ref b, real const& t, ptr c) {
	  c->X = a.X + t * (b.X - a.X);
	  c->Y = a.Y + t * (b.Y - a.Y);
  }

  static void length(const_ref in, real* out) {
    *out = real_sqrt(in.X * in.X + in.Y * in.Y);
  }

  static Vector2 from_angle(real const& t) {
    Vector2 out;
    out.X = real_cos(t);
    out.Y = real_sin(t);
    return out;
  }

  static void from_angle(real const& t, ptr out) {
      out->X = real_cos(t);
      out->Y = real_sin(t);
    }

  static real to_angle(const_ref in);
  static void to_angle(const_ref in, real*);

  inline Vector2 operator - () const {
      Vector2 copy(*this);
      copy.X = -copy.X;
      copy.Y = -copy.Y;
      return copy;
    }

    inline Vector2 operator + (const_ref b) const {
      Vector2 copy(*this);
      copy += b;
      return copy;
    }
    inline Vector2 operator - (const_ref b) const {
      Vector2 copy(*this);
      copy -= b;
      return copy;
    }
    inline Vector2 operator * (const_ref b) const {
      Vector2 copy(*this);
      copy *= b;
      return copy;
    }
    inline Vector2 operator / (const_ref b) const {
      Vector2 copy(*this);
      copy /= b;
      return copy;
    }
    inline Vector2& operator += (const_ref b) {
      X += b.X;
      Y += b.Y;
      return *this;
    }
    inline Vector2& operator -= (const_ref b) {
      X -= b.X;
      Y -= b.Y;
      return *this;
    }
    inline Vector2& operator *= (const_ref b) {
      X *= b.X;
      Y *= b.Y;
      return *this;
    }
    inline Vector2& operator /= (const_ref b) {
      X /= b.X;
      Y /= b.Y;
      return *this;
    }

    inline Vector2 operator * (real const& a) const {
      Vector2 copy(*this);
      copy *= a;
      return copy;
    }

    inline Vector2& operator *= (real const& a) {
      X *= a;
      Y *= a;
      return *this;
    }

    inline Vector2 operator / (real const& a) const {
      Vector2 copy(*this);
      copy /= a;
      return copy;
    }

    inline Vector2& operator /= (real const& a) {
      // TODO: reciprocal?
      X /= a;
      Y /= a;
      return *this;
    }

};
}



#endif /* VECTOR2_H_ */
