/*
 * binary_reader.cpp
 *
 *  Created on: Jul 11, 2014
 *      Author: miel
 */

#include "binary_reader.h"

using namespace qqq;
namespace qqq {

void BinaryReader::read_7bit_encoded_int(int32_t* out) {
    *out = 0;

    for (int i = 0; i < 5; i++) {
        byte_t b;
        pin->read((char*) &b, 1);
        *out |= (b & 127) << (7 * i);
        if (!(b & 128)) {
            break;
        }
    }
}

void BinaryReader::read_string(char** out) {
    assert(*out == NULL && "string is not NULL.");
    int32_t cap;
    read_7bit_encoded_int(&cap);
    assert(cap > 0 && "string literal of length 0 encountered.");
    std::string str;
    str.resize(cap);
    pin->read(&str[0], cap);
    *out = (char*) str.c_str();
}

void BinaryReader::read(void* pout, size_t sz) {
    pin->read(static_cast<char*>(pout), sz);
}

void BinaryReader::read_int32(int32_t* val) {
    pin->read(reinterpret_cast<char*>(val), sizeof(int32_t));
}

}
