/*
 * zip_archive.h
 *
 *  Created on: Jul 15, 2014
 *      Author: miel
 */

#ifndef ZIP_ARCHIVE_H_
#define ZIP_ARCHIVE_H_

#include <qqq.h>

namespace qqq {

class zip_archive {
  void* pio;
  void* pdir;

  static std::vector<std::istream*> zip_istreams;
  static std::vector<std::ostream*> zip_ostreams;

public:
  zip_archive() : pio(NULL), pdir(NULL) {

  }
  virtual ~zip_archive() {

  }

};

}



#endif /* ZIP_ARCHIVE_H_ */
