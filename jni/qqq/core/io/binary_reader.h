/*
 * binary_reader.h
 *
 *  Created on: Jul 11, 2014
 *      Author: miel
 */

#ifndef BINARY_READER_H_
#define BINARY_READER_H_

#include <qqq.h>
#include <qqq_assert.h>

namespace qqq {

class BinaryReader {
protected:
  std::istream* pin;

public:

  BinaryReader(std::istream* s) : pin(s){
  }

  virtual ~BinaryReader() {

  }
  void read_7bit_encoded_int(int32_t* out);
  void read_string(char** out);
  void read(void* pout, size_t sz);
  void read_int32(int32_t*);

  std::istream& get_stream() const {
    return *pin;
  }


};

}



#endif /* BINARY_READER_H_ */
