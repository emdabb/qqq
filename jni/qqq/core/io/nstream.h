/*
 * nstream.h
 *
 *  Created on: Nov 8, 2013
 *      Author: miel
 */

#ifndef NSTREAM_H_
#define NSTREAM_H_

#include <istream>
#include <ostream>

#include <core/net/socket.h>

namespace qqq {

template<typename T, typename Traits = std::char_traits<T> >
class basic_inbuffer: public std::basic_streambuf<T, Traits> {
    typedef std::basic_streambuf<T, Traits> buf_type;
    typedef typename Traits::int_type int_type;
    const static int char_size = sizeof(T);
    const static int BUFFER_SIZE = 256;
    T gbuf[BUFFER_SIZE];
    net_socket* mSocket;
public:
    basic_inbuffer() :
            mSocket(NULL) {
        buf_type::setg(gbuf, gbuf, gbuf);
    }

    virtual ~basic_inbuffer() {
    }

    int_type underflow() {
        if (buf_type::gptr() < buf_type::egptr()) {
            return *buf_type::gptr();
        }
        int num;
        if ((num = mSocket->receive(reinterpret_cast<void*>(gbuf),
                BUFFER_SIZE * char_size)) == 0) {
            return Traits::eof();
        }
        buf_type::setg(gbuf, gbuf, gbuf + num);
        return *buf_type::gptr();
    }

    void set_socket(net_socket* sock) {
        mSocket = sock;
    }

    net_socket* get_socket() const {
        return mSocket;
    }
};

template<typename T, typename Traits = std::char_traits<T> >
class basic_onbuffer: public std::basic_streambuf<T, Traits> {
    typedef std::basic_streambuf<T, Traits> buf_type;
    typedef typename Traits::int_type int_type;

    const static int BUFFER_SIZE = 256;
    const static int char_size = sizeof(T);
    T pbuf[BUFFER_SIZE];
    net_socket* mSocket;
public:
    basic_onbuffer() :
            mSocket(NULL) {
        buf_type::setp(pbuf, pbuf + (BUFFER_SIZE - 1));
    }

    virtual ~basic_onbuffer() {
        sync();
    }

    int flush_buffer() {
        int num = buf_type::pptr() - buf_type::pbase();
        if (mSocket->send(reinterpret_cast<void*>(pbuf), num * char_size)
                != num) {
            return Traits::eof();
        }
        buf_type::pbump(-num);
        return num;
    }

    int_type overflow(int_type c = Traits::eof()) {
        if (c == Traits::eof()) {
            *buf_type::pptr() = c;
            buf_type::pbump(1);
        }
        if (flush_buffer() == Traits::eof()) {
            return Traits::eof();
        }
        return c;
    }

    int sync() {
        if (flush_buffer() == Traits::eof()) {
            return Traits::eof();
        }
        return 0;
    }

    void set_socket(net_socket* sock) {
        mSocket = sock;
    }

    net_socket* get_socket() const {
        return mSocket;
    }
};

template<typename T, typename Traits = std::char_traits<T> >
class basic_instream: public std::basic_istream<T, Traits> {
    typedef T char_type;
    typedef std::basic_istream<T, Traits> stream_type;
    typedef basic_inbuffer<T, Traits> buf_type;

    buf_type buf;
public:
    basic_instream() :
            stream_type(&buf) {

    }

    basic_instream(net_socket* s) :
            stream_type(&buf) {
        buf.set_socket(s);
    }

    virtual ~basic_instream() {

    }

    void close() {
        if (buf.get_socket() != NULL)
            buf.get_socket()->close();
        stream_type::clear();
    }

    net_socket* open(uint16_t port) {
        net_socket s;
        s.bind(port);
        s.listen();
        net_socket* ns = s.accept();
        buf.set_socket(ns);
        return ns;
    }

    const bool is_open() const {
        if (buf.get_socket() != NULL) {
            return buf.get_socket()->is_closed() != true;
        }
        return false;
    }
};

template<typename T, typename Traits = std::char_traits<T> >
class basic_onstream: public std::basic_ostream<T, Traits> {
    typedef T char_type;
    typedef std::basic_ostream<T, Traits> stream_type;
    typedef basic_onbuffer<T, Traits> buf_type;

    buf_type buf;

public:
    basic_onstream() :
            stream_type(&buf) {

    }
    basic_onstream(net_socket* s) :
            stream_type(&buf) {
        buf.set_socket(s);
    }

    virtual ~basic_onstream() {

    }

    void close() {
        if (buf.get_socket() != NULL)
            buf.get_socket()->close();
        stream_type::clear();
    }

    net_socket* open(const char* ip, uint16_t port) {
        close();
        net_socket* s = new net_socket;
        if (s->open(ip, port)) {
            buf.set_socket(s);
            return s;
        } else {
            stream_type::setstate(std::ios::failbit);
            return NULL;
        }
        return buf.get_socket();
    }
};

typedef basic_instream<char> instream;
typedef basic_onstream<char> onstream;
} // qqq

#endif /* NSTREAM_H_ */
