/**
 * @file jstream.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jul 12, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef JSTREAM_H_
#define JSTREAM_H_

#include <qqq.h>

namespace qqq {
class jstream {
  JNIEnv* env;
  jclass  clazz;
  jobject jobj;
  jmethodID jclose, jread, jreset, jskip, javail;
  int len;
  jbyteArray buffer_obj;
  int cur;
public:
  jstream(JNIEnv* arg0, jobject streamobj = NULL) {
    env = arg0;
    cur = 0;
    clazz = (jclass)env->NewGlobalRef(env->FindClass("java/io/InputStream"));

    jclose 	= env->GetMethodID(clazz, "close", "()V");
    jread	= env->GetMethodID(clazz, "read", "([BII)I");
    javail	= env->GetMethodID(clazz, "available", "()I");
  }
  virtual ~jstream();

};
}


#endif /* JSTREAM_H_ */
