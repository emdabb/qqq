/**
 * @file binary_writer.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jul 12, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef BINARY_WRITER_H_
#define BINARY_WRITER_H_

#include <qqq_types.h>
#include <ostream>

namespace qqq {
class BinaryWriter {
  std::ostream* pout;
public:
  BinaryWriter(std::ostream* s);

  virtual ~BinaryWriter();

  void write7BitEncodedInt(const int32_t& in);
  void writeString(const char* in);
  void write(const void*, size_t sz);

  std::ostream& getStream() const {
    return *pout;
  }
};

typedef BinaryWriter BinaryWriter;

}


#endif /* BINARY_WRITER_H_ */
