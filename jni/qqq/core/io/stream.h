/**
 * @file stream.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 6, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef STREAM_H_
#define STREAM_H_

#include <cstdio>
#include <iostream>
#include <streambuf>
#include <string>
#include <vector>

//template <typename T, typename TRAITS = std::char_traits<T> >
//struct basic_istream {
//	typedef std::basic_istream<T, TRAITS> type;
//};
//
//template <typename T, typename TRAITS = std::char_traits<T> >
//struct basic_ostream {
//	typedef std::basic_ostream<T, TRAITS> type;
//};
//
//template <typename T, typename TRAITS = std::char_traits<T> >
//struct basic_iostream {
//	typedef std::basic_iostream<T, TRAITS> type;
//};
//
//template <typename T, typename TRAITS = std::char_traits<T> >
//struct basic_streambuf {
//	typedef std::basic_streambuf<T, TRAITS> type;
//};
//
//typedef basic_istream<char>  	istream;
//typedef basic_ostream<char>		ostream;
//typedef basic_iostream<char>	iostream;
//typedef basic_streambuf<char>	streambuf;

#endif /* STREAM_H_ */
