/*
 * archive.h
 *
 *  Created on: Jul 14, 2014
 *      Author: miel
 */

#ifndef ARCHIVE_H_
#define ARCHIVE_H_

namespace qqq {

struct archive {
  virtual ~archive() {}
  std::istream* open_stream(const char*) = 0;
  std::ostream* open_stream(const char*) = 0;


};

}



#endif /* ARCHIVE_H_ */
