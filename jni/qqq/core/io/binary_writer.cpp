#include "binary_writer.h"
#include <qqq_assert.h>
#include <cstring>

using namespace qqq;

BinaryWriter::BinaryWriter(std::ostream* s) :
    pout(s) {
}

BinaryWriter::~BinaryWriter() {
}

void BinaryWriter::write7BitEncodedInt(const int32_t& in) {
  unsigned int num = (unsigned int) in;
  while (num >= 127)
  {
    byte_t b = (byte_t) (num & 127) | 128;
    pout->write((char*)&b, 1);
    //pout->flush();
    num >>= 7;
  }
  byte_t b = ((byte_t)num) & 127;
  pout->write((char*)&b, 1);
  //pout->flush();
}

void BinaryWriter::writeString(const char* in) {
  assert(in);
  const char* ptr = in;
  int32_t count = 0;
  while (*ptr++ != '\0') {
    count++;
  }
  write7BitEncodedInt(count);
  pout->write((char*) in, count);
  //pout->flush();
}

void BinaryWriter::write(const void* data, size_t sz) {
  pout->write(static_cast<const std::ostream::char_type*>(data), sz);
  //pout->flush();

}


