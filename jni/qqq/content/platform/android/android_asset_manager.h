/**
 * @file android_asset_manager.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 9, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef ANDROID_ASSET_MANAGER_H_
#define ANDROID_ASSET_MANAGER_H_

#include <content/asset_manager.h>

#if defined (__ANDROID__)
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>
namespace qqq {

class AndroidAssetManager : public AssetManager{
  AAssetManager* handle;
public:
  explicit AndroidAssetManager(AAssetManager* val);
  virtual ~AndroidAssetManager();
  virtual std::istream* openStream(const char*);
  virtual void setBasedir(const std::string& dir){}
};

}
#endif /* __ANDROID__ */
#endif /* ANDROID_ASSET_MANAGER_H_ */
