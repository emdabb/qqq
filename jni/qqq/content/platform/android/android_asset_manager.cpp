/**
 * @file android_asset_manager.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 9, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include "android_asset_manager.h"

#if defined(__ANDROID__)
#include <android/asset_manager.h>
#include <cstdio>
#include <errno.h>
#include <core/debug_log.h>
#include <core/io/stdiostream.h>

using namespace qqq;

static int android_read(void* cookie, char* buf, int size) {
	return AAsset_read((AAsset*)cookie, buf, size);
}

static int android_write(void* cookie, const char* buf, int size) {
	return EACCES; // can't provide write access to the apk
}

static fpos_t android_seek(void* cookie, fpos_t offset, int whence) {
	return AAsset_seek((AAsset*)cookie, offset, whence);
}

static int android_close(void* cookie) {
	AAsset_close((AAsset*)cookie);

	return 0;
}

static FILE* android_fopen(AAssetManager* mgr, const char* fname, const char* mode) {
	if(mode[0] == 'w') return NULL;

	AAsset* asset = AAssetManager_open(mgr, fname, 0);
	if(!asset) {
		DEBUG_METHOD();
		DEBUG_MESSAGE("File not found: \"%s\"", fname);
		return NULL;
	}
	return funopen(asset, android_read, android_write, android_seek, android_close);
}



AndroidAssetManager::AndroidAssetManager(AAssetManager* hnd) : handle(hnd) {

}

AndroidAssetManager::~AndroidAssetManager() {

}

std::istream* AndroidAssetManager::openStream(const char* fn) {
	assert(handle);
	FILE* fp = android_fopen(handle, fn, "r");
	if(!fp) {
		throw file_not_found(fn);
	}
	return new qqq::stdiostream(fp);
}

#endif

