/**
 * @file content_reader.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jul 12, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef CONTENT_READER_H_
#define CONTENT_READER_H_

#include <core/io/binary_reader.h>
#include <core/matrix.h>
#include <core/vector3.h>
#include <core/quaternion.h>

namespace qqq {

class ContentReader : public BinaryReader {
public:
  ContentReader(std::istream* is) : BinaryReader(is) {

  }

  virtual ~ContentReader() {

  }

  void readMatrix(Matrix* out) {
    *out = readObject<Matrix>();
  }
  void readVector3(Vector3* out) {
    *out = readObject<Vector3>();
  }
  void readQuaternion(Quaternion* out) {
    *out = readObject<Quaternion>();
  }

//  template <typename T>
//  void read_object(T* out) {
//    char bytes[sizeof(T)];
//    pin->read(bytes, sizeof(T));
//    *out = *((T*)bytes);
//  }

  template <typename T>
  T readObject() {
      T out;
      pin->read((char*)&out, sizeof(T));
      return out;
  }

//  template <typename T>
//  T read() {
//	T out;
//	read(out, sizeof(T));
//	return out;
//	}
};

}



#endif /* CONTENT_READER_H_ */
