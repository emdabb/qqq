/**
 * @file asset_manager.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 9, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef ASSET_MANAGER_H_
#define ASSET_MANAGER_H_

#include <iostream>
#include <map>
#include <string>
#include <core/singleton.h>
#include <core/dictionary.h>
#include "ContentReader.h"
#include "ContentTypeReader.h"

namespace qqq {

class IApplication;

class AssetManager {
  Dictionary<void*>::Type assets;
  std::string basedir;
  IApplication* msp;
public:
  AssetManager();
  virtual ~AssetManager();
  virtual std::istream* openStream(const char*);

  virtual void* loadInternal(const char*);

  template <typename T>
  T* load(const char* fn) {
    return reinterpret_cast<T*>(loadInternal(fn));
  }

  virtual void setBasedir(const std::string& dir)  {
    basedir = dir;
  }

  virtual void setServiceProvider(IApplication* app) {
    msp = app;
  }

  virtual IApplication& getServiceProvider() const {
    return *msp;
  }



};
} //qqq


#endif /* ASSET_MANAGER_H_ */
