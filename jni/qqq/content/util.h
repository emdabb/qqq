/**
 * @file util.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Sep 1, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef UTIL_H_
#define UTIL_H_

#include <string.h>

#if defined(__LINUX__) || defined(__ANDROID__)
#define PATH_DELIM	'/'
#else
#define PATH_DELIM '\\'
#endif


namespace qqq {
struct filename_utils {
  static std::string get_extension(const std::string& str) {
    int dot = str.find_last_of('.');
    return str.substr(dot + 1, std::string::npos);
  }

  static std::string get_path(const std::string& str) {
    int slash = str.find_last_of(PATH_DELIM);
    return str.substr(0, slash);
  }

  static std::string get_base(const std::string& in) {
    size_t dot = in.find_last_of('.');
    if (dot == std::string::npos) {
    return std::string("null");
    }
    return in.substr(0, dot);
  }
};
}



#endif /* UTIL_H_ */
