/**
 * @file content_type_reader.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jul 16, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef CONTENT_TYPE_READER_H_
#define CONTENT_TYPE_READER_H_

#include <qqq.h>

namespace qqq {

class ContentReader;
class ContentTypeReader {
public:
  virtual ~ContentTypeReader() {}
  template <typename T>
  T* readObject(ContentReader& in) {
    return reinterpret_cast<T*>(readInternal(in));
  }

  virtual void* readInternal(ContentReader&) = 0;
};

typedef ContentTypeReader IContentTypeReader;


}



#endif /* CONTENT_TYPE_READER_H_ */
