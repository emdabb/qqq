/**
 * @file ContentWriter.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Feb 19, 2015
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef JNI_QQQ_CONTENT_CONTENTWRITER_H_
#define JNI_QQQ_CONTENT_CONTENTWRITER_H_

#include <core/io/binary_writer.h>

namespace qqq {

class ContentWriter : public BinaryWriter {
public:
    ContentWriter(std::ostream* str) : BinaryWriter(str) {

    }

    virtual ~ContentWriter() {

    }

    template <typename S>
    void writeObject(const S& obj) {
        getStream().write((const char*)&obj, sizeof(S));
    }
};

}



#endif /* JNI_QQQ_CONTENT_CONTENTWRITER_H_ */
