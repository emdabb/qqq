/**
 * @file content_type_reader_hxm.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Aug 3, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include <qqq.h>
#include <content/ContentTypeReader.h>

namespace qqq {

class content_type_reader_hxm : public ContentTypeReader {
public:
  virtual void* readInternal(ContentReader& reader) {
    return NULL;
  }
};

QQQ_C_API ContentTypeReader* QQQ_CALL new_reader_hxm(){
  return new content_type_reader_hxm;
}

}

