/*
 * SpriteFontTypeReader.cpp
 *
 *  Created on: Feb 18, 2015
 *      Author: miel
 */

#include <qqq.h>
#include "../ContentTypeReader.h"
#include "../ContentReader.h"
#include <vector>
#include <map>
#include <gfx/SpriteFont.h>

namespace qqq {

class SpriteFontTypeReader : public IContentTypeReader {
public:
    virtual void* readInternal(ContentReader& reader) {
//
//        uint32_t tw 		= reader.readObject<uint32_t>();
//        uint32_t th 		= reader.readObject<uint32_t>();
//        uint32_t numGlyphs 	= reader.readObject<uint32_t>();
//
//
//        //std::map<unsigned short, >
//        std::vector<Glyph> chars;
//        chars.reserve(numGlyphs);
//
//        std::map<wchar_t, int> indices;
//
//        for(uint32_t i=0; i < numGlyphs; i++) {
//
//            wchar_t wc = reader.readObject<wchar_t>();
//            float u0, v0, u1, v1;
//            signed short w, h;
//            u0 = reader.readObject<float>();
//            u1 = reader.readObject<float>();
//            v0 = reader.readObject<float>();
//            v1 = reader.readObject<float>();
//
//            int16_t xadv = reader.readObject<int16_t>();
//            w = reader.readObject<int16_t>();
//            h = reader.readObject<int16_t>();
//
//            glyph_t& g = chars[i];
//            g.adv = xadv;
//            g.rc.x = u0 * tw;
//            g.rc.y = v0 * th;
//            g.rc.w = (u1 - u0) * tw;
//            g.rc.h = (v1 - v0) * th;
//        }
//
//        // TODO: create a texture somehow...
//        SpriteFont* pSpriteFont = new SpriteFont(&chars[0], chars.size());
//        return pSpriteFont;
        return NULL;
    }
};

QQQ_C_API ContentTypeReader* QQQ_CALL new_reader_ttf() {
    return new SpriteFontTypeReader;
}

}
