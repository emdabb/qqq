/*
 * content_type_reader_hxi.cpp
 *
 *  Created on: Aug 6, 2014
 *      Author: miel
 */

#include <qqq.h>
#include "../ContentReader.h"
#include "../ContentTypeReader.h"
#include <core/io/zstream.h>
#include <core/io/binary_writer.h>
#include <gfx/surface.h>
#include <lodepng/lodepng.h>

namespace qqq {

struct content_writer: public BinaryWriter {

};

struct content_type_writer_hxi {
    void write_internal(content_writer& out) {
//    ozstream oz(*out.get_stream());
//    int len;
//    char* buf = NULL;
//    oz.write(buf, len);
    }
};

struct ContentTypeReaderHxi: public ContentTypeReader {

    void* readInternal(ContentReader& in) {
#if 0
        int32_t width, height, dataSize;
        in.read_7bit_encoded_int(&width);
        in.read_7bit_encoded_int(&height);
        dataSize = width * height * 4;
        char* buf = new char[dataSize];
        ::memset(buf, 0, dataSize);
        in.read(buf, dataSize);
#else
        unsigned error;
        unsigned char* image;
        unsigned width, height;
        unsigned char* png;
        size_t pngsize;
        std::istream& t = in.get_stream();
        std::string data = std::string(std::istreambuf_iterator<char>(t), std::istreambuf_iterator<char>());
        error = lodepng_decode32(&image, &width, &height, (const unsigned char*)data.c_str(), pngsize);
        if(error) {
            printf("error %u: %s\n", error, lodepng_error_text(error));
        }

        //free(png);
#endif
        Surface* out = new Surface(width, height, SurfaceFormat::Color);
        out->set_data(image);
        return out;
    }
};

QQQ_C_API ContentTypeReader* QQQ_CALL new_reader_hxi() {
    return new ContentTypeReaderHxi();
}

}

