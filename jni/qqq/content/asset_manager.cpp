/*
 * asset_manager.cpp
 *
 *  Created on: Jun 10, 2014
 *      Author: miel
 */

#include "asset_manager.h"
#include "ContentType.h"
#include "util.h"
#include <qqq_except.h>
#include <base/application.h>
#include <core/io/stream.h>
#include <core/io/zstream.h>
#include <core/factory.h>
#include <fstream>

using namespace qqq;

QQQ_C_API ContentTypeReader* QQQ_CALL new_reader_ttf();
QQQ_C_API ContentTypeReader* QQQ_CALL new_reader_hxm();
QQQ_C_API ContentTypeReader* QQQ_CALL new_reader_hxi();


AssetManager::AssetManager() : msp(0){
  Factory<ContentTypeReader, int>::instance().registerClass(ContentType::E_TYPE_FONT, new_reader_ttf);
  Factory<ContentTypeReader, int>::instance().registerClass(ContentType::E_TYPE_MODEL, new_reader_hxm);
  Factory<ContentTypeReader, int>::instance().registerClass(ContentType::E_TYPE_IMAGE, new_reader_hxi);
}
AssetManager::~AssetManager() {

}

std::istream* AssetManager::openStream(const char* fn) {
    DEBUG_METHOD();
    DEBUG_VALUE_AND_TYPE_OF(fn);
  std::ifstream* is = new std::ifstream;
  std::string path = basedir + PATH_DELIM + fn;
  is->open(path.c_str(), std::ifstream::in);
  if(!is->is_open()) {
    throw FileNotFoundException(path);
  }
  return is;
}

void* AssetManager::loadInternal(const char* fn) {
#if 0
  std::istream* instream = openStream(fn);
  ContentReader reader(instream);

  ContentTypeReader* in = Factory<ContentTypeReader>::instance().create(ext);
  if(in) {
    return in->read_internal(reader);
  }
#else
  std::string ext = filename_utils::get_extension(fn);
  if(!strcmp(ext.c_str(), "hxb")) {
        char type;
        std::istream* is = openStream(fn);

        ContentReader reader(is);
        reader.read(&type, 1);
        ContentTypeReader* in = Factory<ContentTypeReader, int>::instance().create(type);
        if(in) {
            return in->readInternal(reader);
        }
  }

#endif
  return NULL;
}
