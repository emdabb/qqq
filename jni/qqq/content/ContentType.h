/**
 * @file content_type.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Sep 17, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef CONTENT_TYPE_H_
#define CONTENT_TYPE_H_


namespace qqq {
struct ContentType {
    enum {
        E_TYPE_FONT,
        E_TYPE_IMAGE,
        E_TYPE_MODEL,
        E_TYPE_ANIMATION,
        E_TYPE_SCENEGRAPH,
        E_TYPE_MAX
    };
};
}


#endif /* CONTENT_TYPE_H_ */
