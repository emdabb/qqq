/**
 * @file service_provider.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jul 31, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef SERVICE_PROVIDER_H_
#define SERVICE_PROVIDER_H_

#include <core/singleton.h>

namespace qqq {
class IApplication;
class IGraphicsDevice;
class IGraphicsContext;
class ServiceProvider : public Singleton<ServiceProvider> {
  IApplication* app;
public:
  ServiceProvider();
  void set_services(IApplication*);
};
}



#endif /* SERVICE_PROVIDER_H_ */
