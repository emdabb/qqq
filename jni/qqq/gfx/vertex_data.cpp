#include "vertex_data.h"
#include "vertex_declaration.h"

using namespace qqq;

VertexData::VertexData() :
        mOffset(0) {
}

VertexData::~VertexData() {
}

const VertexDeclaration* VertexData::get_declaration() {
    size_t nChannels = mChannels.size();
    if(nChannels == 0) {
        return NULL;
    }
    std::vector<VertexElement*> attr;
    VertexDeclarationBuilder builder;
    //builder.size(nChannels);

    for(size_t i=0; i < nChannels; i++) {
        IVertexChannel* c = mChannels[i];
        size_t off 	= c->offset();
        int type	= c->type();
        int binding = c->binding();

        builder.element(i, off, type, binding);
    }
    return builder.build();
}

