/*
 * pixel_format.cpp
 *
 *  Created on: Sep 18, 2014
 *      Author: miel
 */

#include "pixel_format.h"
#include <cstring>

using namespace qqq;


const int SurfaceFormat::Bpp[] = {
    32,		// < Color
    16,		// < Bgr565
    16,		// < Bgra5551
    16,		// < Bgra4444
    0,		// < Dxt1
    0,		// < Dxt3
    0,		// < Dxt5
    16,		// < NormalizedByte2
    32,		// < NormalizedByte4
    32,		// < Rgba1010102
    32,		// < Rg32
    64,		// < Rgba64
    8,		// < Alpha8
    32,		// < Single
    64,		// < Vector2
    128,	// < Vector4
    16,		// < HAlfSingle
    32,		// < HalfVector2
    64,		// < HalfVector4
    128		// < HdrRenderable
};

const int SurfaceFormat::RedBits[] = {
    8,		// < Color
    5,		// < Bgr565
    5,		// < Bgra5551
    4,		// < Bgra4444
    0,		// < Dxt1
    0,		// < Dxt3
    0,		// < Dxt5
    8,		// < NormalizedByte2
    8,		// < NormalizedByte4
    10,		// < Rgba1010102
    16,		// < Rg32
    16,		// < Rgba64
    0,		// < Alpha8
    32,		// < Single
    32,		// < Vector2
    32,		// < Vector4
    16,		// < HAlfSingle
    16,		// < HalfVector2
    16,		// < HalfVector4
    32		// < HdrRenderable
};

const int SurfaceFormat::GreenBits[] = {
    8,		// < Color
    6,		// < Bgr565
    5,		// < Bgra5551
    4,		// < Bgra4444
    0,		// < Dxt1
    0,		// < Dxt3
    0,		// < Dxt5
    8,		// < NormalizedByte2
    8,		// < NormalizedByte4
    10,		// < Rgba1010102
    16,		// < Rg32
    16,		// < Rgba64
    0,		// < Alpha8
    0,		// < Single
    32,		// < Vector2
    32,		// < Vector4
    0,		// < HAlfSingle
    16,		// < HalfVector2
    16,		// < HalfVector4
    32		// < HdrRenderable
};

const int SurfaceFormat::BlueBits[] = {
    8,		// < Color
    5,		// < Bgr565
    5,		// < Bgra5551
    4,		// < Bgra4444
    0,		// < Dxt1
    0,		// < Dxt3
    0,		// < Dxt5
    0,		// < NormalizedByte2
    8,		// < NormalizedByte4
    10,		// < Rgba1010102
    0,		// < Rg32
    16,		// < Rgba64
    0,		// < Alpha8
    0,		// < Single
    0,		// < Vector2
    32,		// < Vector4
    0,		// < HAlfSingle
    0,		// < HalfVector2
    16,		// < HalfVector4
    32		// < HdrRenderable
};

const int SurfaceFormat::AlphaBits[] = {
    8,		// < Color
    0,		// < Bgr565
    1,		// < Bgra5551
    4,		// < Bgra4444
    0,		// < Dxt1
    0,		// < Dxt3
    0,		// < Dxt5
    0,		// < NormalizedByte2
    8,		// < NormalizedByte4
    2,		// < Rgba1010102
    0,		// < Rg32
    16,		// < Rgba64
    8,		// < Alpha8
    0,		// < Single
    0,		// < Vector2
    32,		// < Vector4
    0,		// < HAlfSingle
    0,		// < HalfVector2
    16,		// < HalfVector4
    32		// < HdrRenderable
};


//const PixelFormat& PixelFormat::Color   = *new PixelFormat(E_FORMAT_RGBA, E_TYPE_UNSIGNED_INT,  	    (uint32_t[]){0xFF0000, 	0xFF00, 0xFF, 0xFF000000}, 4);
//const PixelFormat& PixelFormat::Bgr565  = *new PixelFormat(E_FORMAT_BGR,  E_TYPE_UNSIGNED_SHORT_565, (uint32_t[]){0x1F,     	0x7E0,  0xF800			}, 3);
//const PixelFormat& PixelFormat::Bgra5551= *new PixelFormat(E_FORMAT_BGRA, E_TYPE_UNSIGNED_SHORT_5551,(uint32_t[]){0xF800,   	0x7C0,  0x3E, 0x1		}, 4);
//const PixelFormat& PixelFormat::Alpha8  = *new PixelFormat(E_FORMAT_ALPHA,E_TYPE_UNSIGNED_BYTE, 		(uint32_t[]){0xffffffff							}, 1);
//
//
//
//PixelFormat::PixelFormat(int storageFormat, int storageType, const uint32_t masks[], int nChannels) {
//    assert(nChannels <= E_CHANNEL_MAX);
//    memset(mChannels, 0, E_CHANNEL_MAX * sizeof(channel));
//    mBitsPerPixel = 0;
//    for(int i=0; i < nChannels; i++) {
//      mChannels[i].shift = get_shift_bits(masks[i]);
//      mChannels[i].mask = masks[i];
//      mChannels[i].bits = get_bits(masks[i]);
//      mBitsPerPixel += mChannels[i].bits;
//    }
//    mStorage = storageFormat;
//    mFormat = storageType;
//}
