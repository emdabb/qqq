/**
 * @file graphics_context.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 5, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef GRAPHICS_CONTEXT_H_
#define GRAPHICS_CONTEXT_H_

#include <qqq.h>
#include <core/lockable.h>
#include <core/event.h>
#include <gfx/IRenderTarget.h>

namespace qqq {

struct GraphicsInitializer;

struct gfx_command {
  enum {
    E_CMD_FRAME_DRAW,
    E_CMD_SURFACE_CHANGED,
    E_CMD_MAX
  };
};

typedef int command_t;

struct GraphicsResourceEventArgs;
struct WindowResizeArgs;

struct IGraphicsContext : public ILockable, public IRenderTarget {

  virtual ~IGraphicsContext() {}
  virtual bool create(void* display, void* surface, GraphicsInitializer*) = 0;
  virtual void destroy() = 0;
  virtual bool swapBuffers() = 0;
  virtual void resize(const WindowResizeArgs&) = 0;
  virtual const char* api() const = 0;

  EventHandler<GraphicsResourceEventArgs> OnContextLost;
  EventHandler<GraphicsResourceEventArgs> OnContextReset;
};

typedef void (*PFNCREATEGRAPHICSCONTEXT)(IGraphicsContext**);
typedef void (*PFNDESTROYGRAPHICSCONTEXT)(IGraphicsContext*);
} // qqq

#endif /* GRAPHICS_CONTEXT_H_ */
