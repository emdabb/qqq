/**
 * @file sampler2d.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Sep 9, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef SAMPLER2D_H_
#define SAMPLER2D_H_

namespace qqq {

class Texture2D;

struct Sampler2D {
    Texture2D* TextureRef;
    int SamplerID;
    int Filter;
    int AddressU;
    int AddressV;
    int AddressW;

    //sampler2d();

    //bool operator != (const sampler2d& other);
};

}



#endif /* SAMPLER2D_H_ */
