/*
 * line_renderer.h
 *
 *  Created on: Jul 15, 2014
 *      Author: miel
 */

#ifndef LINE_RENDERER_H_
#define LINE_RENDERER_H_

#include <qqq.h>

namespace qqq {

class IGraphicsDevice;
class Vector3;
class Color;

class LineRenderer {
    struct impl;
    impl* mPimpl;
public:
  LineRenderer(IGraphicsDevice*, size_t count = 1024);
  virtual ~LineRenderer();
  void setVertexCount(size_t inCount);
  void setPosition(size_t, Vector3 const&);
  void setColor(size_t, Color const&);
  void draw();
};

}



#endif /* LINE_RENDERER_H_ */
