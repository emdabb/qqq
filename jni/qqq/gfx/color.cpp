/*
 * color.cpp
 *
 *  Created on: Jun 26, 2014
 *      Author: miel
 */


#include "color.h"

using namespace qqq;

const Color Color::AliceBlue 		= Color(0xFFFFF8F0);
const Color Color::AntiqueWhite 	= Color(0xFFD7EBFA);
const Color Color::Aqua 			= Color(0xFFFFFF00);
const Color Color::Aquamarine 		= Color(0xFFD4FF7F);
const Color Color::Azure 			= Color(0xFFFFFFF0);
const Color Color::Beige 			= Color(0xFFDCF5F5);
const Color Color::Bisque 			= Color(0xFFC4E4FF);
const Color Color::Black 			= Color(0xFF000000);
const Color Color::BlanchedAlmond 	= Color(0xFFCDEBFF);
const Color Color::Blue 			= Color(0xFFFF0000);
const Color Color::BlueViolet 		= Color(0xFFE22B8A);
const Color Color::Brown 			= Color(0xFF2A2AA5);
const Color Color::Burlywood 		= Color(0xFF87B8DE);
const Color Color::CadetBlue 		= Color(0xFFA09E5F);
const Color Color::Chartreuse 		= Color(0xFF00FF7F);
const Color Color::Chocolate 		= Color(0xFF1E69D2);
const Color Color::Coral 			= Color(0xFF507FFF);
const Color Color::Cornflower 		= Color(0xFFED9564);
const Color Color::Cornsilk 		= Color(0xFFDCF8FF);
const Color Color::Crimson 			= Color(0xFF3C14DC);
const Color Color::Cyan 			= Color(0xFFFFFF00);
const Color Color::DarkBlue 		= Color(0xFF8B0000);
const Color Color::DarkCyan 		= Color(0xFF8B8B00);
const Color Color::DarkGoldenrod 	= Color(0xFF0B86B8);
const Color Color::DarkGray 		= Color(0xFFA9A9A9);
const Color Color::DarkGreen 		= Color(0xFF006400);
const Color Color::DarkKhaki 		= Color(0xFF6BB7BD);
const Color Color::DarkMagenta 		= Color(0xFF8B008B);
const Color Color::DarkOliveGreen 	= Color(0xFF2F6B55);
const Color Color::DarkOrange 		= Color(0xFF008CFF);
const Color Color::DarkOrchid 		= Color(0xFFCC3299);

const Color Color::DarkRed 			= Color(0xFF00008B);
const Color Color::DarkSalmon 		= Color(0xFF7A96E9);
const Color Color::DarkSeaGreen 	= Color(0xFF8FBC8F);
const Color Color::DarkSlateBlue 	= Color(0xFF8B3D48);
const Color Color::DarkSlateGray 	= Color(0xFF4F4F2F);
const Color Color::DarkTurquoise 	= Color(0xFFD1CE00);
const Color Color::DarkViolet 		= Color(0xFFD30094);
const Color Color::DeepPink 		= Color(0xFF9314FF);
const Color Color::DeepSkyBlue 		= Color(0xFFFFBF00);
const Color Color::DimGray 			= Color(0xFF696969);
const Color Color::DodgerBlue 		= Color(0xFFFF901E);
const Color Color::Firebrick 		= Color(0xFF2222B2);
const Color Color::FloralWhite 		= Color(0xFFF0FAFF);
const Color Color::ForestGreen 		= Color(0xFF228B22);
const Color Color::Fuchsia 			= Color(0xFFFF00FF);
const Color Color::Gainsboro 		= Color(0xFFDCDCDC);
const Color Color::GhostWhite 		= Color(0xFFFFF8F8);
const Color Color::Gold 			= Color(0xFF00D7FF);
const Color Color::Goldenrod 		= Color(0xFF20A5DA);
const Color Color::Gray 			= Color(0xFFBEBEBE);
const Color Color::GrayW3C 			= Color(0xFF808080);
const Color Color::Green 			= Color(0xFF00FF00);
const Color Color::GreenW3C 		= Color(0xFF008000);
const Color Color::GreenYellow 		= Color(0xFF2FFFAD);
const Color Color::Honeydew 		= Color(0xFFF0FFF0);
const Color Color::HotPink 			= Color(0xFFB469FF);
const Color Color::IndianRed 		= Color(0xFF5C5CCD);
const Color Color::Indigo 			= Color(0xFF82004B);
const Color Color::Ivory 			= Color(0xFFF0FFFF);
const Color Color::Khaki 			= Color(0xFF8CE6F0);
const Color Color::Lavender 		= Color(0xFFFAE6E6);
const Color Color::LavenderBlush 	= Color(0xFFF5F0FF);
const Color Color::LawnGreen 		= Color(0xFF00FC7C);
const Color Color::LemonChiffon 	= Color(0xFFCDFAFF);
const Color Color::LightBlue 		= Color(0xFFE6D8AD);
const Color Color::LightCoral 		= Color(0xFF8080F0);
const Color Color::LightCyan 		= Color(0xFFFFFFE0);
const Color Color::LightGoldenrod 	= Color(0xFFD2FAFA);
const Color Color::LightGray 		= Color(0xFFD3D3D3);

const Color Color::LightGreen 		= Color(0xFF90EE90);
const Color Color::LightPink 		= Color(0xFFC1B6FF);
const Color Color::LightSalmon 		= Color(0xFF7AA0FF);
const Color Color::LightSeaGreen 	= Color(0xFFAAB220);
const Color Color::LightSkyBlue 	= Color(0xFFFACE87);
const Color Color::LightSlateGray 	= Color(0xFF998877);
const Color Color::LightSteelBlue 	= Color(0xFFDEC4B0);
const Color Color::LightYellow 		= Color(0xFFE0FFFF);
const Color Color::Lime 			= Color(0xFF00FF00);
const Color Color::LimeGreen 		= Color(0xFF32CD32);
const Color Color::Linen 			= Color(0xFFE6F0FA);
const Color Color::Magenta 			= Color(0xFFFF00FF);
const Color Color::Maroon 			= Color(0xFF6030B0);
const Color Color::MaroonW3C 		= Color(0xFF00007F);
const Color Color::MediumAquamarine = Color(0xFFAACD66);
const Color Color::MediumBlue 		= Color(0xFFCD0000);
const Color Color::MediumOrchid		= Color(0xFFD355BA);
const Color Color::MediumPurple 	= Color(0xFFDB7093);
const Color Color::MediumSeaGreen 	= Color(0xFF71B33C);
const Color Color::MediumSlateBlue 	= Color(0xFFEE687B);
const Color Color::MediumSpringGreen= Color(0xFF9AFA00);
const Color Color::MediumTurquoise 	= Color(0xFFCCD148);
const Color Color::MediumVioletRed 	= Color(0xFF8515C7);
const Color Color::MidnightBlue 	= Color(0xFF701919);
const Color Color::MintCream 		= Color(0xFFFAFFF5);
const Color Color::MistyRose 		= Color(0xFFE1E4FF);
const Color Color::Moccasin 		= Color(0xFFB5E4FF);
const Color Color::NavajoWhite 		= Color(0xFFADDEFF);
const Color Color::Navy 			= Color(0xFF800000);
const Color Color::OldLace 			= Color(0xFFE6F5FD);
const Color Color::Olive 			= Color(0xFF008080);
const Color Color::OliveDrab 		= Color(0xFF238E6B);
const Color Color::Orange 			= Color(0xFF00A5FF);
const Color Color::OrangeRed 		= Color(0xFF0045FF);
const Color Color::Orchid 			= Color(0xFFD670DA);
const Color Color::PaleGoldenrod 	= Color(0xFFAAE8EE);
const Color Color::PaleGreen 		= Color(0xFF98FB98);
const Color Color::PaleTurquoise 	= Color(0xFFEEEEAF);
const Color Color::PaleVioletRed 	= Color(0xFF9370DB);

const Color Color::PapayaWhip 		= Color(0xFFD5EFFF);
const Color Color::PeachPuff 		= Color(0xFFB9DAFF);
const Color Color::Peru 			= Color(0xFF3F85CD);
const Color Color::Pink 			= Color(0xFFCBC0FF);
const Color Color::Plum 			= Color(0xFFDDA0DD);
const Color Color::PowderBlue 		= Color(0xFFE6E0B0);
const Color Color::Purple 			= Color(0xFFF020A0);
const Color Color::PurpleW3C 		= Color(0xFF7F007F);
const Color Color::Red 				= Color(0xFF0000FF);
const Color Color::RosyBrown 		= Color(0xFF8F8FBC);
const Color Color::RoyalBlue 		= Color(0xFFE16941);
const Color Color::SaddleBrown 		= Color(0xFF13458B);
const Color Color::Salmon 			= Color(0xFF7280FA);
const Color Color::SandyBrown 		= Color(0xFF60A4F4);
const Color Color::SeaGreen 		= Color(0xFF578B2E);
const Color Color::Seashell 		= Color(0xFFEEF5FF);
const Color Color::Sienna 			= Color(0xFF2D52A0);
const Color Color::Silver 			= Color(0xFFC0C0C0);
const Color Color::SkyBlue 			= Color(0xFFEBCE87);
const Color Color::SlateBlue 		= Color(0xFFCD5A6A);
const Color Color::SlateGray 		= Color(0xFF908070);
const Color Color::Snow 			= Color(0xFFFAFAFF);
const Color Color::SpringGreen 		= Color(0xFF7FFF00);
const Color Color::SteelBlue 		= Color(0xFFB48246);
const Color Color::Tan 				= Color(0xFF8CB4D2);
const Color Color::Teal 			= Color(0xFF808000);
const Color Color::Thistle 			= Color(0xFFD8BFD8);
const Color Color::Tomato 			= Color(0xFF4763FF);
const Color Color::Transparent 		= Color(0x00FFFFFF);
const Color Color::TransparentBlack = Color(0x00000000);
const Color Color::Turquoise 		= Color(0xFFD0E040);
const Color Color::Violet 			= Color(0xFFEE82EE);
const Color Color::Wheat 			= Color(0xFFB3DEF5);
const Color Color::White 			= Color(0xFFFFFFFF);
const Color Color::WhiteSmoke 		= Color(0xFFF5F5F5);
const Color Color::Yellow 			= Color(0xFF00FFFF);
const Color Color::YellowGreen 		= Color(0xFF32CD9A);

