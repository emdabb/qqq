/*
 * line_renderer.cpp
 *
 *  Created on: Jul 15, 2014
 *      Author: miel
 */

#include "line_renderer.h"
#include "color.h"
#include "graphics_device.h"
#include "vertex_buffer.h"
#include "vertex_stream.h"
#include <core/vector3.h>
#include <gfx/PrimitiveType.h>

namespace qqq {
struct vertex_p3_c4 {
    Vector3	pos;
    Color 	tint;
    static const VertexElement el[];
    static const VertexDeclaration* decl;
};

struct LineRenderer::impl {
    IGraphicsDevice* mGraphicsDevice;
    VertexBuffer* mVertexBuffer;
    std::vector<vertex_p3_c4> mVertices;
    size_t mVertexCount;
    bool mIsValid;
};
}

using namespace qqq;

const VertexElement vertex_p3_c4::el[] = {
    { offsetof(vertex_p3_c4, pos), 		stream_type::E_TYPE_VECTOR3, 	stream_id::E_STREAM_POSITION 	},
    { offsetof(vertex_p3_c4, tint), 	stream_type::E_TYPE_COLOR, 		stream_id::E_STREAM_COLOR0 		}
};

const VertexDeclaration* vertex_p3_c4::decl = VertexDeclaration::create(vertex_p3_c4::el, 2, sizeof(vertex_p3_c4));

LineRenderer::LineRenderer(IGraphicsDevice* device, size_t numVertices) :
        mPimpl(new impl)
{
    DEBUG_METHOD();
    DEBUG_VALUE_AND_TYPE_OF(device);
    DEBUG_VALUE_AND_TYPE_OF(numVertices);

    mPimpl->mGraphicsDevice = (device);
    mPimpl->mVertexBuffer = (NULL);
    mPimpl->mVertexCount = 0;
    mPimpl->mIsValid = false;

    mPimpl->mVertices.resize(numVertices);
    mPimpl->mVertexBuffer = new VertexBuffer(device, vertex_p3_c4::decl, numVertices, sizeof(vertex_p3_c4));
}

LineRenderer::~LineRenderer()
{
    delete mPimpl;
    mPimpl = NULL;
}

void LineRenderer::setVertexCount(size_t count)
{
    mPimpl->mVertexCount = count;
}

void LineRenderer::setPosition(size_t i, const Vector3& value)
{
    mPimpl->mVertices[i].pos.X = value.X;
    mPimpl->mVertices[i].pos.Y = value.Y;
    mPimpl->mVertices[i].pos.Z = value.Z;
    mPimpl->mVertexCount = i > mPimpl->mVertexCount ? i : mPimpl->mVertexCount;
    mPimpl->mIsValid = false;
}

void LineRenderer::setColor(size_t i, const Color& value)
{
    mPimpl->mVertices[i].tint.value = value.value;
    mPimpl->mVertexCount = i > mPimpl->mVertexCount ? i : mPimpl->mVertexCount;
    mPimpl->mIsValid = false;
}

void LineRenderer::draw()
{
    if (mPimpl->mVertexCount != 0)
    {
        if (!mPimpl->mIsValid)
        {
            mPimpl->mVertexBuffer->get_impl().lock();
            mPimpl->mVertexBuffer->set_data<vertex_p3_c4>(&mPimpl->mVertices[0], 0, mPimpl->mVertexCount);
            mPimpl->mVertexBuffer->get_impl().unlock();
            mPimpl->mIsValid = true;
        }
        mPimpl->mGraphicsDevice->setIndexBuffer(NULL);
        mPimpl->mGraphicsDevice->setStreamSource(mPimpl->mVertexBuffer);
        mPimpl->mGraphicsDevice->drawPrimitives(PrimitiveType::Lines, 0, mPimpl->mVertexCount, 1);
    }
}

