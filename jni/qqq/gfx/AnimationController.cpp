/**
 * @file AnimationController.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date May 8, 2015
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include "AnimationController.h"
#include "Model.h"

using namespace qqq;

AnimationController::AnimationController(AnimationInfo* info) :
        mBones(NULL), mNumBones(0), mPoses(NULL), mAnimationClip(NULL), mCrossfadeAnimationClip(
                NULL), mAnimationInfo(info), mIsFinished(false), mIsPlaying(
                false), mIsCrossfadeEnabled(false), mCurrentTime(0.f), mCrossfadeLerp(
                0.f), mCrossfadeElapsed(0.f), mCrossfadeTime(0.f) {
    mNumBones = info->Channels.size();
    mBones = new ModelBone[mNumBones];
    mPoses = new Transform[mNumBones];
}


AnimationController::~AnimationController() {
    delete[] mBones;
    delete[] mPoses;
}

void AnimationController::start(AnimationClip* animationClip) {
    mAnimationClip = animationClip;
    mIsFinished = false;
    mIsPlaying = true;
    mCurrentTime = (float) ((animationClip->Begin));
}

void AnimationController::crossfade(AnimationClip* crossfadeAnimationClip,
        float t) {
    if (mIsCrossfadeEnabled) {
        mCrossfadeLerp = 0.f;
        mCrossfadeTime = 0.f;
        mCrossfadeElapsed = 0.f;
        start(crossfadeAnimationClip);
    } else {
        mCrossfadeAnimationClip = crossfadeAnimationClip;
        mCrossfadeTime = t;
        mCrossfadeElapsed = 0.f;
        mIsCrossfadeEnabled = true;
    }
}

void AnimationController::update(int t, int dt) {
    if (!mIsFinished) {
        float fDt = ((float) ((dt)) / 1000.f) * 60.f;
        if (mAnimationClip) {
            updateAnimationTime(fDt);
            if (mIsCrossfadeEnabled) {
                updateCrossfadeTime(fDt);
            }
            updateBonePoses();
        }
        updateAbsoluteBoneTransforms();
    }
}

void AnimationController::copyAbsoluteBoneTransformsTo(Matrix array[]) {
    for (size_t i = 0; i < mNumBones; i++) {
        if (!mBones[i].hasParent()) {
            array[i] = mBones[i].WorldToBone;
            continue;
        }
        int j = mBones[i].ParentId;
        array[i] = Matrix::mul(mBones[i].WorldToBone, array[j]);
    }

    for (size_t i = 0; i < mNumBones; i++) {
        array[i] = Matrix::mul(mBones[i].BindPoseInv, array[i]);
    }
}

void AnimationController::updateAnimationTime(float dt) {
    mCurrentTime += dt;
    if(mCurrentTime > mAnimationClip->End) {
        AnimationEventArgs args;
        args.Controller	= this;
        args.Animation 	= mAnimationClip;
        args.Info		= mAnimationInfo;
        OnAnimationDone(args);

        if(mAnimationClip->Loop) {
            mCurrentTime -= mAnimationClip->End - mAnimationClip->Begin;
        } else {
            mCurrentTime 	= mAnimationClip->End;
            mIsPlaying 		= false;
            mIsFinished 	= true;
        }
    }
}

void AnimationController::updateCrossfadeTime(float dt) {
    mCrossfadeElapsed += dt;
    if(mCrossfadeElapsed > mCrossfadeTime) {
        mIsCrossfadeEnabled = false;
        mCrossfadeLerp 		= 0.f;
        mCrossfadeElapsed	= 0.f;
        mCrossfadeTime		= 0.f;
        start(mCrossfadeAnimationClip);
    } else {
        mCrossfadeLerp = mCrossfadeElapsed / mCrossfadeTime;
    }
}

void AnimationController::updateBonePoses() {
    AnimationChannel* animationChannel;
    Transform pose;

    for(size_t i=0; i < mNumBones; i++) {
        std::string channelName = mBones[i].Name;
        if(mAnimationInfo->Channels.count(channelName) > 0) {
            animationChannel = &mAnimationInfo->Channels[channelName];
            interpolateTransform(*animationChannel, mCurrentTime, &pose);
        }
    }
}

void AnimationController::updateAbsoluteBoneTransforms() {
    for(size_t i=0; i < mNumBones; i++) {
        const Transform& pose 	= mPoses[i];
        Matrix scale, rotation, translation;

        Matrix::createScale(pose.Scale, &scale);
        Matrix::createRotation(pose.Orientation, &rotation);
        Matrix::createTranslation(pose.Position, &translation);
        Matrix frame			= Matrix::mul(Matrix::mul(scale, rotation), translation);

        mBones[i].WorldToBone	= frame;
    }
}

void AnimationController::interpolateTransform(const AnimationChannel& animationChannel, float at, Transform* out){
    int current = (size_t)(at + 0.5f);
    int next	= (current + 1) % animationChannel.NumFrames;

    const Keyframe& a = animationChannel.Frames[current];
    const Keyframe& b = animationChannel.Frames[next];

    size_t duration = current == (animationChannel.NumFrames - 1)
            ? mAnimationClip->End - a.Time
            : b.Time - a.Time;

    if(duration > 0) {
        float elapsedKeyframeTime = at - (float)a.Time;
        float interpolationFactor = elapsedKeyframeTime / (float)duration;
        Transform::lerp(a.Pose, b.Pose, interpolationFactor, out);
    } else {
        *out = a.Pose;
    }
}


