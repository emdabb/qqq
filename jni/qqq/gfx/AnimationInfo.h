/**
 * @file AnimationInfo.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Apr 21, 2015
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef JNI_HELIX2_GFX_ANIMATIONINFO_H_
#define JNI_HELIX2_GFX_ANIMATIONINFO_H_

#include <map>
#include <string>
#include <core/vector3.h>
#include <core/quaternion.h>

namespace qqq {

template<typename T>
struct Dictionary {
    typedef std::map<std::string, T> Type;
};

__declare_aligned(struct, 16) Transform {
    typedef const Transform& 	const_ref;
    typedef 	  Transform*	ptr;

    Vector3		Position;
    Vector3		Scale;
    Quaternion	Orientation;

    static void lerp(const_ref a, const_ref b, float, ptr);
};

struct Keyframe {
    Transform 	Pose;
    size_t 		Time;
};

struct AnimationChannel {
    char 	Name[256];
    Keyframe* 	Frames;
    size_t 		NumFrames;
};

struct AnimationInfo {
    size_t Duration;
    Dictionary<AnimationChannel>::Type Channels;
};

}



#endif /* JNI_HELIX2_GFX_ANIMATIONINFO_H_ */
