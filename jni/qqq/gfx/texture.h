/**
 * @file texture.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jul 10, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef TEXTURE_H_
#define TEXTURE_H_

#include <gfx/IGraphicsResource.h>

namespace qqq {

struct texture_usage {
  enum {
    E_USAGE_NONE = 0,
    E_USAGE_STATIC = 1 << 0,
    E_USAGE_DYNAMIC = 1 << 1,
    E_USAGE_STREAM = 1 << 2,
    E_USAGE_RENDERTARGET = 1 << 3,
    E_USAGE_MAX
  };
};

struct TextureFilter {
  enum {
	  Linear,						//	Use linear filtering.
	  Point,						//	Use point filtering.
	  Anisotropic,					//	Use anisotropic filtering.
	  LinearMipPoint,				//	Use linear filtering to shrink or expand, and point filtering between mipmap levels (mip).
	  PointMipLinear,				//	Use point filtering to shrink (minify) or expand (magnify), and linear filtering between mipmap levels.
	  MinLinearMagPointMipLinear,	//	Use linear filtering to shrink, point filtering to expand, and linear filtering between mipmap levels.
	  MinLinearMagPointMipPoint,	//	Use linear filtering to shrink, point filtering to expand, and point filtering between mipmap levels.
	  MinPointMagLinearMipLinear,	//	Use point filtering to shrink, linear filtering to expand, and linear filtering between mipmap levels.
	  MinPointMagLinearMipPoint		//  Use point filtering to shrink, linear filtering to expand, and point filtering between mipmap levels.
  };
};

struct TextureAaddressMmode {
  enum {
	  Clam,		//	Texture coordinates outside the range [0.0, 1.0] are set to the texture color at 0.0 or 1.0, respectively.
	  Mirror,	//	Similar to Wrap, except that the texture is flipped at every integer junction. For u values between 0 and 1, for example, the texture is addressed normally; between 1 and 2, the texture is flipped (mirrored); between 2 and 3, the texture is normal again, and so on.
	  Wrap		//	Tile the texture at every integer junction. For example, for u values between 0 and 3, the texture is repeated three times; no mirroring is performed.
  };
};

class texture : public IGraphicsResource {
public:
  virtual ~texture() {}
  virtual bool has_levels() const = 0;
  virtual int  get_format() const = 0;
};
}



#endif /* TEXTURE_H_ */
