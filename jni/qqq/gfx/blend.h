/*
 * blend.h
 *
 *  Created on: Jun 27, 2014
 *      Author: miel
 */

#ifndef BLEND_H_
#define BLEND_H_

namespace qqq {
struct blend_function {
  enum {
    E_ADD,
    E_SUBTRACT,
    E_SUBTRACT_REV
  };
};

struct blend {
  enum {
    E_ZERO,
    E_ONE,
    E_SRC_COLOR,
    E_SRC_COLOR_INV,
    E_SRC_ALPHA,
    E_SRC_ALPHA_INV,
    E_DST_COLOR,
    E_DST_COLOR_INV,
    E_DST_ALPHA,
    E_DST_ALPHA_INV,
    E_SRC_ALPHA_SAT,
    E_BLEND_FACTOR,
    E_BLEND_FACTOR_INV
  };
};

}



#endif /* BLEND_H_ */
