/**
 * @file graphics_device.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 5, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef GRAPHICS_DEVICE_H_
#define GRAPHICS_DEVICE_H_

#include <qqq.h>
#include <gfx/ClearOptions.h>
#include <gfx/IGraphicsResource.h>

namespace qqq {

struct IGraphicsContext;
struct Color;
class  VertexBuffer;
class  IndexBuffer;
class  DepthStencilBuffer;
class  Texture2D;
struct BlendState;
struct RasterizerState;
struct DepthStencilState;
struct Viewport;
struct WindowResizeArgs;


struct IGraphicsDevice : public IGraphicsResource{

  virtual ~IGraphicsDevice() {}
  virtual bool create(IGraphicsContext*) = 0;
  virtual void setStreamSource(VertexBuffer*) = 0;
  virtual void setIndexBuffer(IndexBuffer*) = 0;
  virtual void onContextResize(const WindowResizeArgs&) = 0;

  virtual void addContextListener(IGraphicsResource* res)=0;

  virtual bool beginFrame() = 0;
  virtual void endFrame() = 0;
  virtual void clear(Color const&, const int mask = ClearOptions::ClearAll, float clearDepth = 1.0f, int clearStencil = 0) = 0;

  virtual void drawIndexedPrimitives(const int type, const int base, const int imin, const size_t count, const int i0, const size_t primcount) = 0;
  virtual void drawPrimitives(const int type, const int start, const size_t count, size_t primcount) = 0;

  virtual const char* api() const = 0;

  virtual void setBlendState(const BlendState&) = 0;
  virtual void getBlendState(BlendState*) = 0;

  virtual void setRasterizerState(const RasterizerState&) = 0;
  virtual void getRasterizerState(RasterizerState*) = 0;

  virtual void setDepthStencilState(const DepthStencilState&) = 0;
  virtual void getDepthStencilState(DepthStencilState*) = 0;

  virtual void getDimensions(int*, int*) = 0;
  virtual void getViewport(Viewport*) = 0;

  virtual void pushViewport(const Viewport&) = 0;
  virtual bool popViewport() = 0;

  virtual void getDepthStencilBuffer(DepthStencilBuffer**) = 0;
  virtual void setDepthStencilBuffer(DepthStencilBuffer*) = 0;

  virtual void setRenderTarget(size_t n, Texture2D* dst) = 0;
  virtual void resolveRenderTarget(int n) = 0;
};

typedef void(*PFNCREATEGRAPHICSDEVICE)(IGraphicsDevice**);
extern "C" void hxCreateGraphicsDevice(IGraphicsDevice** ppDevice);
} // qqq

#endif /* GRAPHICS_DEVICE_H_ */
