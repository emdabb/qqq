/**
 * @file sprite_font.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 18, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include <gfx/SpriteFont.h>
#include <core/rectangle.h>
#include <gfx/graphics_device.h>
#include "pixel_format.h"
#include "texture2d.h"
//#include <helix/core/BinaryTree.h>
#include <core/debug_log.h>
#include <core/rectangle.h>
#include <core/math_helper.h>
#include <algorithm>
#include <ft2build.h>
#include FT_FREETYPE_H

using namespace qqq;

int32_t nextPow2(const int32_t value) {
    int32_t n = value;
    n--;
    n |= n >> 1;   // Divide by 2^k for consecutive doublings of k up to 32,
    n |= n >> 2;   // and then or the results.
    n |= n >> 4;
    n |= n >> 8;
    n |= n >> 16;
    n++;           // The result is a number of 1 bits equal to the number
    // of bits in the original number, plus 1. That's the
    // next highest power of 2.
    return n;
}

float roundUp(float x, float mul) {
    return x + mul - fmod(x, mul);
}

int calc_texture_size(int count, int maxCharWidth, int maxCharHeight,
        bool powTwo) {
    int area = maxCharWidth * maxCharHeight * count;
    float len = real_sqrt((real ) area);
    len = roundUp(len,
            (maxCharWidth > maxCharHeight ? maxCharWidth : maxCharHeight));
    return powTwo ? nextPow2(len) : len;
}

struct node {
    node* left;
    node* right;
    qqq::Rectangle rc;
    int imgID;

    node() :
            imgID(-1) {
        left = NULL;
        right = NULL;
    }
    virtual ~node() {
    }
    node* insert(const qqq::Rectangle& rect) {
        if (left || right) {
            node* new_node = left->insert(rect);
            if (new_node != NULL) {
                return new_node;
            }
            return right->insert(rect);

        } else {
            if (imgID != -1) {
                return NULL;
            }
            if (qqq::Rectangle::getArea(rc) < qqq::Rectangle::getArea(rect)) {
                return NULL;
            }
            if (qqq::Rectangle::getArea(rc) == qqq::Rectangle::getArea(rect)) {
                return this;
            }

            left = new node;
            right = new node;

            int h = rect.H;
            int w = rect.W;
            int dw = rc.W - rect.W;
            int dh = rc.H - rect.H;

            if (dw > dh) {
                left->rc = { rc.X, rc.Y, w, rc.H };
                right->rc = { rc.X + w, rc.Y, rc.W - w, rc.H };
            } else {
                left->rc = { rc.X, rc.Y, rc.W, h };
                right->rc = { rc.X, rc.Y + h, rc.W, rc.H - h };
            }

            return left->insert(rect);
        }
    }
};

SpriteFont::SpriteFont(GlyphInfo info[], size_t numGlyphs, Texture2D* tex) {
    DEBUG_METHOD();
    mTexture = tex;
    mInfos.resize(numGlyphs);
    for (size_t i = 0; i < numGlyphs; i++) {
        mInfos[i] = info[i];
    }
}

SpriteFont::~SpriteFont() {
}

void SpriteFont::setLineHeight(int n) {
    mMetrics.lineHeight = n;
}

int SpriteFont::getLineHeight() {
    return mMetrics.lineHeight;
}

real SpriteFont::getSpacing() {
    return 0;
}

char SpriteFont::getDefaultCharacter() {
    return 0x20;
}

void SpriteFont::setDefaultCharacter(char c) {
}

Vector2 SpriteFont::measureString(const char* str) {
    Vector2 size = Vector2::Zero;
    int maxx = 0;
    int maxy = 0;
    int xx=0;
    int yy = 0;

    for (size_t i = 0; i < strlen(str); i++) {
        GlyphInfo& info = mInfos[str[i] - 32];

        if(str[i] == '\n') {
            yy += getLineHeight();
            maxx = std::max(maxx, xx);
            xx = 0;
            continue;
        }

        xx += info.advance;
        yy = std::max(yy, info.rect.H);
    }

    size.X = std::max(maxx, xx);
    size.Y = std::max(maxy, yy);

    return size;
}

const GlyphInfo& SpriteFont::getGlyphInfo(int c) const {
    return mInfos[c - 32];
}

GlyphInfo& SpriteFont::getGlyphInfo(int c) {
    return const_cast<GlyphInfo&>(static_cast<const SpriteFont*>(this)->getGlyphInfo(
            c));
}

Texture2D* SpriteFont::getTexture() {
    return mTexture;
}

/**
 * TODO: HACK
 */
SpriteFont* SpriteFont::loadFromFile(IGraphicsDevice* pDevice,
        InputStream* is, size_t ptsize) {
    DEBUG_METHOD();
    static FT_Library library;
    static bool once = false;
    FT_Face face;

    if(!once) {
        DEBUG_MESSAGE("intializing freetype...");
        once = true;
        FT_Error err = FT_Init_FreeType(&library);
        if(FT_Err_Ok != err) {
            throw std::runtime_error("FT_Init_FreeType failed!");
        }

    }

    std::string ft_source = std::string(std::istreambuf_iterator<char>(*is), std::istreambuf_iterator<char>());

    FT_Error err = FT_Err_Ok;
    DEBUG_MESSAGE("creating font face...");
    if(FT_New_Memory_Face(library, (const FT_Byte*)ft_source.c_str(), ft_source.length(), 0, &face)) {
        throw std::runtime_error("FT_New_Face failed!");
    }
    DEBUG_MESSAGE("char_size = %d...", ptsize << 6);
    if(FT_Set_Char_Size(face, ptsize << 6, ptsize << 6, 96, 96)) {
        throw std::runtime_error("FT_Set_Char_Size failed!");
    }

    Texture2D* GlyphAtlas = NULL;

    unsigned int w = 0;
    unsigned int h = 0;

    for (int i = 32; i < 128; i++) {
        if (FT_Load_Char(face, i, FT_LOAD_RENDER)) {
            continue;
        }
        FT_GlyphSlot slot = face->glyph;
        FT_Bitmap& bitmap = slot->bitmap;

        w = std::max(w, bitmap.width);
        h = std::max(h, bitmap.rows);
    }

    int textureSize = calc_texture_size(128, w, h, false);
    //pDevice->createTexture2D(&GlyphAtlas, textureSize, textureSize, PixelFormat::Color);


    node root;
    root.rc = { 0, 0, textureSize, textureSize };

    GlyphAtlas = new Texture2D(pDevice, textureSize, textureSize, SurfaceFormat::Color, false);

    std::vector<GlyphInfo> infos;

    for (int i = 32; i < 128; i++) {
        if (FT_Load_Char(face, i, FT_LOAD_RENDER | FT_LOAD_NO_HINTING)) {
            continue;
        }

        FT_GlyphSlot slot = face->glyph;
        FT_Bitmap& bitmap = slot->bitmap;


        qqq::Rectangle rrc = { 0, 0, (int32_t)bitmap.width, (int32_t)bitmap.rows };
        node* new_node = root.insert(rrc);
        new_node->imgID = i;
        const qqq::Rectangle& rc = new_node->rc;

        if (new_node->imgID && bitmap.buffer) {
            byte_t* image = new byte_t[bitmap.width * bitmap.rows * 4];
            byte_t* ptr = &image[0];
            for (int y = 0; y < bitmap.rows; y++) {
                for (int x = 0; x < bitmap.width; x++) {
                    int i = x + y * bitmap.width;
                    *ptr++ = bitmap.buffer[i];
                    *ptr++ = bitmap.buffer[i];
                    *ptr++ = bitmap.buffer[i];
                    *ptr++ = bitmap.buffer[i];
                }
            }

            GlyphAtlas->setData<byte_t>(image, rc.X, rc.Y, rc.W, rc.H);
            delete[] image;
        }

        GlyphInfo info;
        info.rect = rc;
        info.advance = slot->advance.x >> 6;
        info.ascent = face->ascender >> 6;
        info.bearing.X = slot->metrics.horiBearingX >> 6;
        info.bearing.Y = slot->metrics.horiBearingY >> 6;
        infos.push_back(info);
    }

    face->height;

    SpriteFont* res = new SpriteFont(&infos[0], infos.size(), GlyphAtlas);
    res->setLineHeight(face->height / 64);

    FT_Done_Face(face);

    return res;
}

