/**
 * @file graphics_initializer.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 5, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef GRAPHICS_INITIALIZER_H_
#define GRAPHICS_INITIALIZER_H_

#include "pixel_format.h"

namespace qqq {

class GraphicsInitializer {
    int mX, mY;
    size_t mWidth, mHeight;
    PixelFormat mPixelFormat;
    bool mIsFullscreen;
    int mDepthBits;
    int mStencilBits;

public:
    GraphicsInitializer();
    virtual ~GraphicsInitializer();
    GraphicsInitializer& x(int);
    GraphicsInitializer& y(int);
    GraphicsInitializer& width(size_t);
    GraphicsInitializer& height(size_t);
    GraphicsInitializer& pixelFormat(const PixelFormat&);
    GraphicsInitializer& fullscreen(bool);
    GraphicsInitializer& depthBits(int);
    GraphicsInitializer& stencilBits(int);

    const int x() const;
    const int y() const;
    const size_t width() const;
    const size_t height() const;
    const PixelFormat& pixelFormat() const;
    const bool fullscreen() const;
    const int depthBits() const;
    const int stencilBits() const;
};
}


#endif /* GRAPHICS_INITIALIZER_H_ */
