/**
 * @file SamplerState.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Apr 1, 2015
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include <gfx/SamplerState.h>

using namespace qqq;

const real SamplerState::DefaultMipMapLevelOfDetailBias = (real)0;

const SamplerState SamplerState::AnisotropicClamp = {
        TextureAddressMode::Clamp,
        TextureAddressMode::Clamp,
        TextureAddressMode::Clamp,
        TextureFilter::Anisotropic,
        SamplerState::DefaultMaxAnisotropy,
        SamplerState::DefaultMaxMipLevel,
        SamplerState::DefaultMipMapLevelOfDetailBias
};

const SamplerState SamplerState::AnisotropicWrap = {
        TextureAddressMode::Wrap,
        TextureAddressMode::Wrap,
        TextureAddressMode::Wrap,
        TextureFilter::Anisotropic,
        SamplerState::DefaultMaxAnisotropy,
        SamplerState::DefaultMaxMipLevel,
        SamplerState::DefaultMipMapLevelOfDetailBias
};

const SamplerState SamplerState::LinearClamp = {
        TextureAddressMode::Clamp,
        TextureAddressMode::Clamp,
        TextureAddressMode::Clamp,
        TextureFilter::Linear,
        SamplerState::DefaultMaxAnisotropy,
        SamplerState::DefaultMaxMipLevel,
        SamplerState::DefaultMipMapLevelOfDetailBias
};

const SamplerState SamplerState::LinearWrap = {
        TextureAddressMode::Wrap,
        TextureAddressMode::Wrap,
        TextureAddressMode::Wrap,
        TextureFilter::Linear,
        SamplerState::DefaultMaxAnisotropy,
        SamplerState::DefaultMaxMipLevel,
        SamplerState::DefaultMipMapLevelOfDetailBias
};

const SamplerState SamplerState::PointClamp = {
        TextureAddressMode::Clamp,
        TextureAddressMode::Clamp,
        TextureAddressMode::Clamp,
        TextureFilter::Point,
        SamplerState::DefaultMaxAnisotropy,
        SamplerState::DefaultMaxMipLevel,
        SamplerState::DefaultMipMapLevelOfDetailBias
};

const SamplerState SamplerState::PointWrap = {
        TextureAddressMode::Wrap,
        TextureAddressMode::Wrap,
        TextureAddressMode::Wrap,
        TextureFilter::Point,
        SamplerState::DefaultMaxAnisotropy,
        SamplerState::DefaultMaxMipLevel,
        SamplerState::DefaultMipMapLevelOfDetailBias
};
