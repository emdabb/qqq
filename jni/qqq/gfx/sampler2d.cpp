/**
 * @file sampler2d.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Oct 28, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include <qqq.h>
#include "sampler2d.h"

using namespace qqq;

//sampler2d::sampler2d()
//: tex(NULL)
//, samplerID(-1)
//, minFilter(-1)
//, magFilter(-1)
//{
//
//}

//bool sampler2d::operator != (const sampler2d& other) {
//    bool isEqual = true;
//    isEqual &= this->tex != other.tex;
//    isEqual &= this->samplerID != other.samplerID;
//    isEqual &= this->minFilter != other.minFilter;
//    isEqual &= this->magFilter != other.magFilter;
//    return isEqual;
//}


