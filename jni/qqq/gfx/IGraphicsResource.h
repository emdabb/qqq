/**
 * @file graphics_resource.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jul 10, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef GRAPHICS_RESOURCE_H_
#define GRAPHICS_RESOURCE_H_

#include <qqq.h>
#include <core/event.h>
#include <core/IResource.h>

namespace qqq {

struct IGraphicsDevice;
struct IGraphicsContext;
struct IGraphicsResource;

struct GraphicsResourceEventArgs {
    IGraphicsContext* 	GraphicsContext;
    IGraphicsDevice*	GraphicsDevice;
    //IGraphicsResource* 	ResourcePtr;
};

struct IGraphicsResource : public IResource {
  //IGraphicsDevice* GraphicsDevice;

  virtual ~IGraphicsResource() {}
  virtual void onContextLost(const GraphicsResourceEventArgs&) = 0;
  virtual void onContextReset(const GraphicsResourceEventArgs&) = 0;
};

}



#endif /* GRAPHICS_RESOURCE_H_ */
