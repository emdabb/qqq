/**
 * @file surface.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Sep 25, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */


#include "surface.h"
#include <core/rectangle.h>
#include <core/aligned_malloc.h>

using namespace qqq;

Surface::Surface(size_t w, size_t h, const PixelFormat& pfd)
: mWidth(w)
, mHeight(h)
, mFormat(pfd) {
    mPixelSize = SurfaceFormat::Bpp[pfd] >> 3;
    mPitch = w * mPixelSize;
    mData = malloc(h * mPitch);
}

Surface::~Surface() {
    free(mData);
}
const size_t Surface::get_width() const {
    return mWidth;
}

const size_t Surface::get_height() const {
    return mHeight;
}

const char* Surface::get_data() const {
    return static_cast<const char*>(mData);
}

const PixelFormat& Surface::get_format() const {
    return mFormat;
}

void Surface::set_data(const void* in) {
    Rectangle src = { 0, 0, (int32_t)mWidth, (int32_t)mHeight };
    blit(in, src, src);
}

void Surface::blit(const void* in, const Rectangle& src, const Rectangle& dst) {
    char* row = static_cast<char*>(mData);
    const char* inrow = static_cast<const char*>(in);

    int sy0 = src.Y;
    int dy0 = dst.Y;
    int sy1 = src.Y + src.H;
    int dy1 = dst.Y + dst.H;

    int dx0 = dst.X * mPixelSize;
    int sx0 = src.X * mPixelSize;

    for(;(sy0 < sy1), (dy0 < dy1); sy0++, dy0++) {
        memcpy(&row[dx0 + dy0 * mPitch], &inrow[sx0 + sy0 * src.W * mPixelSize], src.W * mPixelSize);
    }
}

void Surface::plot(int x, int y, const Color& col) {
    //mFormat.build_pixel(col);
}


