/**
 * @file Model.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Sep 21, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include "Model.h"
#include "index_buffer.h"
#include "graphics_device.h"
#include <gfx/vertex_data.h>
#include <core/io/binary_reader.h>
#include <core/vector4.h>
#include <core/vector3.h>
#include <core/vector2.h>
#include <gfx/PrimitiveType.h>

using namespace qqq;

Model::Model(IGraphicsDevice* pGraphicsDevice) :
        mGraphicsDevice(pGraphicsDevice) {

}

Model::~Model() {

}

void Model::addMesh(ModelMesh* pModelMesh) {
    mMeshes.push_back(pModelMesh);
}

void Model::draw() {
    for (size_t i = 0; i < mMeshes.size(); i++) {
        mGraphicsDevice->setStreamSource(mMeshes[i]->Vertices);
        mGraphicsDevice->setIndexBuffer(mMeshes[i]->Indices);
        mGraphicsDevice->drawIndexedPrimitives(PrimitiveType::Triangles, 0,
                0, mMeshes[i]->Indices->count(), 0, 1);
    }
}

void Model::createFromStream(Model** dst, std::istream* is) {
    BinaryReader reader(is);

    char name[256];
    reader.read_string((char**)&name);

    int32_t hasBones, hasAnimations;
    reader.read_int32(&hasBones);
    reader.read_int32(&hasAnimations);

    int32_t numMaterials;
    reader.read_int32(&numMaterials);

    for(int i=0; i < numMaterials; i++) {
        char textureFilename[256];
        reader.read_string((char**)&textureFilename);
    }

    int32_t numVertices;
    reader.read_int32(&numVertices);

    size_t sz_pos = numVertices * sizeof(Vector3);
    size_t sz_nrm = numVertices * sizeof(Vector3);
    size_t sz_tex = numVertices * sizeof(Vector2);
    size_t sz_bix = numVertices * sizeof(short4);
    size_t sz_bwe = numVertices * sizeof(Vector4);

    VertexData data;





}
