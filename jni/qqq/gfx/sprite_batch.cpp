/*
 * sprite_batch.cpp
 *
 *  Created on: Jun 27, 2014
 *      Author: miel
 */

#include "sprite_batch.h"
#include "SpriteFont.h"
#include <assert.h>
#include <core/math_helper.h>
#include <gfx/PrimitiveType.h>
#include "vertex_stream.h"
#include "viewport.h"
#include "texture.h"

using namespace qqq;

const VertexElement SpriteVertex::el[] = {
    { offsetof(SpriteVertex, pos), stream_type::E_TYPE_VECTOR3, stream_id::E_STREAM_POSITION },
    { offsetof(SpriteVertex, coord), stream_type::E_TYPE_VECTOR2, stream_id::E_STREAM_TEXCOORD0 },
    { offsetof(SpriteVertex, tint), stream_type::E_TYPE_COLOR, stream_id::E_STREAM_COLOR0 }
};

const VertexDeclaration* SpriteVertex::decl = VertexDeclaration::create(SpriteVertex::el, 3, sizeof(SpriteVertex));


SpriteBatch::SpriteBatch(IGraphicsDevice* dev, int capacity)
: mShader(NULL)
, mCapacity(capacity)
, mBaseSamplerParameter(NULL)
, mWorldViewProjParameter(NULL)
{
  assert(capacity > MinBatchSize && capacity <= MaxBatchSize);
  mNumSpritesToDraw = 0;
  mVertexBuffer = new VertexBuffer(dev, SpriteVertex::decl, 2 * capacity * VerticesPerSprite, sizeof(SpriteVertex));
  mGraphicsDevice =dev;
}

SpriteBatch::~SpriteBatch() {
  delete mVertexBuffer;
  mVertexBuffer = NULL;
}

void SpriteBatch::begin() {
    begin(0, BlendState::AlphaBlend, Matrix::Identity, NULL);
}

void SpriteBatch::begin(const int fx, const BlendState& blend, const Matrix& transform, ShaderProgram* sh = NULL) {
  mGraphicsDevice->getBlendState(&mCachedBlendState);
  mGraphicsDevice->getRasterizerState(&mCachedRasterizerState);
  BlendState state = mCachedBlendState;
  RasterizerState rasterizerState = mCachedRasterizerState;
  rasterizerState.CullMode= CullMode::CullNone;
  mGraphicsDevice->setBlendState(blend);
  mGraphicsDevice->setRasterizerState(rasterizerState);

  int w, h;
//  viewport vp;
//  mGraphicsDevice->get_viewport(&vp);
//  w = vp.rect.w;
//  h = vp.rect.h;
  mGraphicsDevice->getDimensions(&w, &h);
  Matrix orthoTransform;
  Matrix::createOrthoOffcentre(0, w, h, 0, -1, 1, &orthoTransform);
  Matrix::mul(transform, orthoTransform, &mTransform);

  mShader = sh;

  if(mShader != NULL) {
      mBaseSamplerParameter   = mShader->get_uniform_by_name<Sampler2D>("BaseSampler");
      mWorldViewProjParameter = mShader->get_uniform_by_name<Matrix>("_WorldViewProj");
  } else {
      mBaseSamplerParameter = NULL;
      mWorldViewProjParameter = NULL;
  }
}

void SpriteBatch::draw(Texture2D* tex, const qqq::Rectangle& dst, const Color& col) {
  draw(tex, dst, NULL, col);
}
void SpriteBatch::draw(Texture2D* tex, const qqq::Rectangle& dst, qqq::Rectangle* src, const Color& col) {
  draw(tex, dst, src, col, (real)0., Vector2::Zero, 0, (real)0.);
}

void SpriteBatch::draw(Texture2D* tex, const qqq::Rectangle& dst, qqq::Rectangle* src, const Color& col, const real& rot, const Vector2& pivot, const int fx, const real& depth) {
  draw(tex, dst, src, col, rot, pivot, Vector2::One, fx, depth);
}

void SpriteBatch::draw(
    Texture2D* tex,
    const qqq::Rectangle& dst,
    qqq::Rectangle* src,
    const Color& col,
    const real& rot,
    const Vector2& pivot,
    const Vector2& scale,
    const int fx,
    const real& depth) {
  SpriteInfo* info = &mQueue[mNumSpritesToDraw];
  real sx, sy, sw, sh;
  if(src) {
    sx = (real)src->X;
    sy = (real)src->Y;
    sw = (real)src->W;
    sh = (real)src->H;
  } else {
    sx = (real)0.;
    sy = (real)0.;
    sw = (real)tex->getWidth();
    sh = (real)tex->getHeight();
  }

  info->tex = tex;
  info->dst.X = (real) dst.X;
  info->dst.Y = (real) dst.Y;
  info->dst.Z = (real) dst.W * scale.X;
  info->dst.W = (real) dst.H * scale.Y;
  info->orig = pivot;
  info->rot = rot;
  info->depth = depth;
  info->fx = fx;
  info->tint = col;
  info->src.X = (real) sx;
  info->src.Y = (real) sy;
  info->src.Z = (real) sw;
  info->src.W = (real) sh;

  mNumSpritesToDraw++;
  if(mNumSpritesToDraw == mCapacity) {
      flush();
  }
}

void SpriteBatch::draw(Texture2D* tex, const Vector2& pos, qqq::Rectangle* src, const Color& col, const real& rot, const Vector2& pivot, const real& scale, const int fx, const real& depth) {
    Vector2 vScale = { scale, scale };
    draw(tex, pos, src, col, rot, pivot, vScale, fx, depth);
}

void SpriteBatch::draw(Texture2D* tex, const Vector2& pos, qqq::Rectangle* src, const Color& col, const real& rot, const Vector2& pivot, const Vector2& scale, const int fx, const real& depth) {
  SpriteInfo* info = &mQueue[mNumSpritesToDraw];
  real sx, sy, sw, sh;
  if(src) {
    sx = (real)src->X;
    sy = (real)src->Y;
    sw = (real)src->W;
    sh = (real)src->H;
  } else {
    sx = (real)0.;
    sy = (real)0.;
    sw = (real)tex->getWidth();
    sh = (real)tex->getHeight();
  }

  info->tex = tex;
  info->dst.X = (real) pos.X;
  info->dst.Y = (real) pos.Y;
  info->dst.Z = (real) sw * scale.X;
  info->dst.W = (real) sh * scale.Y;
  info->orig = pivot;
  info->rot = rot;
  info->depth = depth;
  info->fx = fx;
  info->tint = col;
  info->src.X = (real) sx;
  info->src.Y = (real) sy;
  info->src.Z = (real) sw;
  info->src.W = (real) sh;

  mNumSpritesToDraw++;
  if(mNumSpritesToDraw == mCapacity) {
    flush();
  }
}

void SpriteBatch::draw(Texture2D* tex, const Vector2& at, const Color& col) {
  qqq::Rectangle dst;
  dst.X = (int32_t)at.X;
  dst.Y = (int32_t)at.Y;
  dst.W = tex->getWidth();
  dst.H = tex->getHeight();
  draw(tex, dst, NULL, col, (real)0., Vector2::Zero, Vector2::One, SpriteEffect::NoEffect, (real)0.);
}

void SpriteBatch::draw(Texture2D* tex, const Vector2& at, qqq::Rectangle* src, const Color& col) {
    qqq::Rectangle dst;
    dst.X = (int32_t)at.X;
    dst.Y = (int32_t)at.Y;
    dst.W = tex->getWidth();
    dst.H = tex->getHeight();
    draw(tex, dst, src, col, (real)0., Vector2::Zero, Vector2::One, SpriteEffect::NoEffect, (real)0.);
}

void SpriteBatch::drawString(SpriteFont* font, const char* text, const Vector2& at, const Color& color) {
    Vector2 cursor = at;
    Texture2D* tex = font->getTexture();

    for (size_t i = 0; i < strlen(text); i++) {
        if(text[i] == '\n') {
            cursor.Y += (float)font->getLineHeight();
            cursor.X = at.X;
        }
        GlyphInfo& info = font->getGlyphInfo((int) text[i]);

        real px = cursor.X + info.bearing.X;
        real py = cursor.Y - info.rect.H + (info.rect.H - info.bearing.Y);

        qqq::Rectangle dst = {
            (int32_t)px,
            (int32_t)py,
            info.rect.W,
            info.rect.H
        };

        draw(tex, dst, &info.rect, color, 0.0f, Vector2::Zero, SpriteEffect::NoEffect, 0.0f);

        cursor.X += info.advance;
    }
}
void SpriteBatch::drawString(SpriteFont* font, const char* text, const Vector2& at, const Color& col, const real& rot, const Vector2& origin, const real& scale, const int fx, const real& depth) {

}

void SpriteBatch::end() {
  flush();
  mGraphicsDevice->setBlendState(mCachedBlendState);
}

void SpriteBatch::getTransform(Matrix* out) {
    *out = mTransform;
}

//__attribute__((optimize("unroll-loops")))
void SpriteBatch::copyToVertices(const SpriteInfo& src, SpriteVertex* dst) {
  real dx = (real)1. / (real)src.tex->getWidth();
  real dy = (real)1. / (real)src.tex->getHeight();

  static const Vector2 corner_offsets[] = {
      Vector2::Zero,
      Vector2::Right,
      Vector2::One,
      Vector2::Up
  };

  Vector2 rot = src.rot != (real)1. ?
      Vector2::from_angle(src.rot) :
      Vector2::Right;

  Vector2 orig = src.orig;
  orig.X /= src.src.Z == (real)0. ? math::EPSILON : src.src.Z;
  orig.Y /= src.src.W == (real)0. ? math::EPSILON : src.src.W;

  for(int i=0; i < VerticesPerSprite; i++) {
    Vector2 corner = corner_offsets[i];
    Vector2 pos;
    pos.X = (corner.X - orig.X) * src.dst.Z;
    pos.Y = (corner.Y - orig.Y) * src.dst.W;

    dst->pos.X = src.dst.X + (pos.X * rot.X) - (pos.Y * rot.Y);
    dst->pos.Y = src.dst.Y + (pos.X * rot.Y) + (pos.Y * rot.X);
    dst->pos.Z = src.depth;
    dst->tint  = src.tint;

    corner = corner_offsets[i ^ src.fx];
    dst->coord.X = (src.src.X + corner.X * src.src.Z) * dx;
    dst->coord.Y = (src.src.Y + corner.Y * src.src.W) * dy;
    dst++;
  }
}

void SpriteBatch::flush() {
  if(mNumSpritesToDraw <= 0) {
    return;
  }
  /**
   * copy sprite information into respective vertices
   */
  for(int i=0; i < mNumSpritesToDraw; i++) {
    copyToVertices(mQueue[i], &mVertices[i * VerticesPerSprite]);
  }
  /**
   *
   * set the vertex buffer data
   *
   */
  mVertexBuffer->set_data<SpriteVertex>(&mVertices[0], 0, mNumSpritesToDraw * VerticesPerSprite);
  mGraphicsDevice->setStreamSource(mVertexBuffer);
  int beg = 0;
  Texture2D* tex = NULL;
  for(int i=0; i < mNumSpritesToDraw; i++) {
    if(tex != mQueue[i].tex) {
      if(i > beg) {
        renderBatch(tex, beg, i - beg);
      }
      tex = mQueue[i].tex;
      beg = i;
    }
  }
  renderBatch(tex, beg, mNumSpritesToDraw - beg);
  mNumSpritesToDraw = 0;

}
void SpriteBatch::renderBatch(Texture2D* tex, int off, size_t count) {
  size_t beg = off * VerticesPerSprite;
  //sampler2d smp0 = { tex, 0, 0, 0 };
  mSampler2D.TextureRef = tex;
  mSampler2D.SamplerID = 0;
  mSampler2D.Filter = TextureFilter::Linear;


  if(mShader) {
      mBaseSamplerParameter->setValue(&mSampler2D);
      mWorldViewProjParameter->setValue(&mTransform);
      mShader->lock();
  }

  mGraphicsDevice->drawPrimitives(PrimitiveType::TriangleFan, beg, VerticesPerSprite, count);
  if(mShader) {
      mShader->unlock();
  }
}
