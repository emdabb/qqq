/**
 * @file depth_stencil_buffer.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jul 7, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef DEPTH_STENCIL_BUFFER_H_
#define DEPTH_STENCIL_BUFFER_H_

#include <qqq.h>
#include <core/lockable.h>

namespace qqq {

struct depth_format {
  enum {
    E_DEPTH16,
    E_DEPTH24,
    E_DEPTH24_STENCIL8,
    E_DEPTH32,
    E_DEPTH32F,
    E_DEPTH_MAX
  };
};

class IGraphicsDevice;

struct IDepthStencilBuffer : public ILockable {
  virtual ~IDepthStencilBuffer() {}
  virtual void create(size_t, size_t, const int) = 0;
  virtual void bind() = 0;
};

class DepthStencilBuffer {
  IDepthStencilBuffer* pimpl;
public:
  DepthStencilBuffer(IGraphicsDevice*, size_t, size_t, const int);
  virtual ~DepthStencilBuffer();
  IDepthStencilBuffer& get_impl() const {
    return *pimpl;
  }
};

}



#endif /* DEPTH_STENCIL_BUFFER_H_ */
