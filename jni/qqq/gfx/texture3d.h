/**
 * @file texture3D.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 5, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef TEXTURE3D_H_
#define TEXTURE3D_H_

#include <gfx/IGraphicsResource.h>
#include <core/lockable.h>

namespace qqq {

class texture3d;
class IGraphicsDevice;

struct texture3d_impl : public ILockable {
  virtual ~texture3d_impl() {}
  virtual void create(texture3d*) = 0;
  virtual void set_data(const void*, int, size_t, size_t, size_t, size_t, size_t, size_t) = 0;
};

class texture3d : public IGraphicsResource {
    IGraphicsDevice* mGraphicsDevice;
  texture3d_impl* pimpl;
  size_t size_x, size_y, size_z;
  const int fmt;
public:
  texture3d(IGraphicsDevice*, size_t, size_t, size_t, const int);
  virtual ~texture3d();

  template <typename T>
  void set_data(const T* data, size_t w, size_t h, size_t d) {
    synchronized(pimpl) {
      pimpl->set_data(data, 0, 0, 0, 0, w, h, d);
    }
  }

  template <typename T>
  void set_data(const T* data, size_t x, size_t y, size_t z, size_t w, size_t h, size_t d) {
    synchronized(pimpl) {
      pimpl->set_data(data, 0, x, y, z, w, h, d);
    }
  }

  const int format() const {
    return fmt;
  }

  const size_t width() const {
    return size_x;
  }

  const size_t height() const {
    return size_y;
  }

  const size_t depth() const {
    return size_z;
  }

  bool has_levels() const {
    return false;
  }

  virtual void context_lost(const GraphicsResourceEventArgs&);
  virtual void onContextReset(const GraphicsResourceEventArgs&);

  texture3d_impl& get_impl() const {
    return *pimpl;
  }

};

}



#endif /* TEXTURE3D_H_ */
