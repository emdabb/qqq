/**
 * @file graphics_profiler.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 5, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef GRAPHICS_PROFILER_H_
#define GRAPHICS_PROFILER_H_

#include <core/thread.h>

#define MAX_MARKER_NAME_LENGTH			32
#define INVALID_TIME					((uint64_t)(-1))
#define NB_RECORDED_FRAMES				3
#define NB_MAX_CPU_MARKERS_PER_FRAME 	100
#define NB_MARKERS_PER_CPU_THREAD 		NB_RECORDED_FRAMES * NB_MAX_CPU_MARKERS_PER_FRAME
#define NB_MAX_GPU_MARKERS_PER_FRAME 	10
#define NB_GPU_MARKERS 					NB_RECORDED_FRAMES * NB_MAX_GPU_MARKERS_PER_FRAME
#define NB_MAX_CPU_THREADS				32

namespace qqq {

class profiler {
public:
  struct marker {
    uint16_t start, end;
    size_t layer;
    int frame;
    char name[MAX_MARKER_NAME_LENGTH];
  };

  typedef marker cpu_marker;

  struct gpu_marker {
    unsigned int id_query_start;
    unsigned int id_query_end;
  };

  struct cpu_thread_info {
    thread_id id;
    cpu_marker markers[NB_MARKERS_PER_CPU_THREAD];
    int cur_write_id, cur_read_id;
    int next_read_id;
    size_t nb_pushed_markers;
  };

  struct gpu_thread_info {
    gpu_marker markers[NB_GPU_MARKERS];
    int cur_write_id, cur_read_id;
    int next_read_id;
    size_t nb_pushed_markers;
  };

  struct frame_info {
    int frame;
    uint64_t time_sync_start, time_sync_end;
  };

  enum freeze_state {
    E_STATE_UNFROZEN = 0,
    E_STATE_WAITING_FOR_FREEZE,
    E_STATE_FROZEN,
    E_STATE_WAITING_FOR_UNFREEZE
  };
public:

  virtual ~profiler() {}
  virtual void push_gpu_marker() = 0;
  virtual void push_cpu_marker() = 0;
  virtual void pop_gpu_marker() = 0;
  virtual void pop_cpu_marker() = 0;

};
} // qqq


#endif /* GRAPHICS_PROFILER_H_ */
