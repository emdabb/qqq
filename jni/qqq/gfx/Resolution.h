/**
 * @file resolution.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Sep 19, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef RESOLUTION_H_
#define RESOLUTION_H_

#include <core/matrix.h>
#include "viewport.h"


namespace qqq {

struct WindowResizeArgs;
class  IGraphicsDevice;

class Resolution {
    int 			 mWidth;
    int 			 mHeight;
    int 			 mVirtualWidth;
    int 			 mVirtualHeight;
    bool 			 mHasChanged;
    bool 			 mIsFullscreen;
    Matrix 			 mTransform;
    Viewport 		 mViewport;
    IGraphicsDevice* mGraphicsDevice;
public:
    Resolution(IGraphicsDevice* device);
    virtual ~Resolution();
    void getTransformation(Matrix*);
    void updateTransformation();
    void setResolution(int x, int y, bool fullscreen);
    void setVirtualResolution(int x, int y);
    void beginDraw();
    void fullViewport();
    real getVirtualAspectRatio();
    void resetViewport();
    int  getVirtualWidth() const;
    int  getVirtualHeight() const;
    void project(int px, int py, int* ppx, int* ppy);
    void unproject(float px, float py, float* ppx, float* ppy);
    //void onContextResized(const WindowResizeArgs&);
};

}



#endif /* RESOLUTION_H_ */
