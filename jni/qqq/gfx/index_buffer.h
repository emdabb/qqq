/**
 * @file index_buffer.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 5, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef INDEX_BUFFER_H_
#define INDEX_BUFFER_H_

#include <qqq.h>
#include <iostream>
#include <core/factory.h>
#include <core/lockable.h>
#include <gfx/graphics_device.h>
#include <assert.h>
#include <gfx/vertex_declaration.h>
#include <gfx/IGraphicsResource.h>
#include <gfx/graphics_context.h>
#include <gfx/IBasicBuffer.h>

namespace qqq {

struct IndexType {
  enum {
    UnsignedByte,
    UnsignedShort,
    UnsignedInt
  };
};

struct index_buffer_impl : public basic_buffer_impl{

};

class IndexBuffer : public IBasicBuffer {
    basic_buffer_impl* mPimpl;
    size_t mNumElements;
    size_t mElementSize;
    const int mElementType;
public:
    IndexBuffer(IGraphicsDevice*, const int, size_t, size_t);
    virtual ~IndexBuffer();
    template <typename T>
    void set_data(const T* in, size_t off, size_t num) {
        synchronized(mPimpl) {
            mPimpl->set_data(static_cast<const void*>(in), off, sizeof(T) * num);
        }
    }

    template <typename T>
    void get_data(T* in, size_t off, size_t count) {

    }

    size_t size() const {
        return mNumElements * mElementSize;
    }

    size_t count() const {
        return mNumElements;
    }

    const int type() const {
        return mElementType;
    }

    basic_buffer_impl& get_impl();

    void onContextLost(const GraphicsResourceEventArgs&);

    void onContextReset(const GraphicsResourceEventArgs&);

    virtual void destroy();
};

} // qqq



#endif /* INDEX_BUFFER_H_ */
