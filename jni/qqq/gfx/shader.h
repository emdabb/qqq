/**
 * @file shader.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 24, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef SHADER_H_
#define SHADER_H_

#include <qqq.h>
#include <core/lockable.h>
#include <core/event.h>
#include <cassert>
#include <vector>
#include <string>
#include <cstring>
#include <map>

namespace qqq {

class IGraphicsDevice;
class ShaderParameterBase;

struct uniform_event_args {
    ShaderParameterBase* thiz;
    int loc;
};

class ShaderProgram;

struct IShaderProgram {
    virtual ~IShaderProgram() {}
    virtual void cacheUniforms(ShaderProgram*) = 0;
    virtual bool compile(const char*) = 0;
    virtual bool link() = 0;
    virtual void enable() = 0;
    virtual void disable() =0;
};

class ShaderParameterBase {
protected:
    std::string mName;
    std::vector<ShaderProgram*> mOwner;
    std::vector<int> mLocation;
    bool mIsValid;
public:
    ShaderParameterBase(ShaderProgram* sh, const std::string& name, int loc);
    virtual ~ShaderParameterBase();
    void add_owner(ShaderProgram* sh, int loc);
    void invalidate();
    bool is_valid() const;
    const std::string& get_name() const {
        return mName;
    }
    virtual void validate() = 0;
public:
    EventHandler<uniform_event_args> on_invalidate;
};

template <typename T>
class ShaderParameter : public ShaderParameterBase {
protected:
    T* mValue;
    size_t mCount;
public:
    typedef T type;
public:
    ShaderParameter(ShaderProgram* sh, const std::string& name, int loc, size_t count)
    : ShaderParameterBase(sh, name, loc)
    , mCount(count) {
        mValue = new T[mCount];
    }

    virtual ~ShaderParameter() {
        delete[] mValue;
        mValue = NULL;
    }

    void setValue(T* val) {

//        for(int i=0; i < elementCount; i++) {
//            if(mValue[i] != val[i]) {
//                mValue[i] = val[i];
//                isValid   = false;
//            }
//        }
        if(memcmp(&mValue[0], &val[0], mCount * sizeof(T))) {
            memcpy(&mValue[0], &val[0], mCount * sizeof(T));
            invalidate();
            uniform_event_args args;
            args.thiz = this;
            on_invalidate(args);
        }
    }

    void setValue(const T& val, size_t i) {
        assert(i < mCount);
        mValue[i] = val;
        invalidate();
        uniform_event_args args;
        args.thiz = this;
        on_invalidate(args);
    }

    const T& get_value(int n = 0) const {
        return mValue[n];
    }

    const T* get_values() const {
        return &mValue[0];
    }

    const size_t count() const {
        return mCount;
    }
};

class ShaderProgram : public ILockable {
protected:
    std::vector<ShaderParameterBase*> mUniform;
    std::vector<int> mLocation;
    IShaderProgram* pimpl;
    std::map<std::string, ShaderParameterBase*> mUniformNames;
    bool mIsActive;
public:
    ShaderProgram(IGraphicsDevice*);
    virtual ~ShaderProgram();
    void compile_and_link(const char*);
    void on_invalidate(const uniform_event_args& args);
    void add_uniform(ShaderParameterBase* param, int loc);
    int lock();
    int unlock();
    size_t num_uniforms() const {
        return mUniform.size();
    }

    ShaderParameterBase* get_uniform(size_t n) const {
        return mUniform[n];
    }

    template <typename T>
    ShaderParameter<T>* get_uniform_by_name(const char* name) {
        return reinterpret_cast<ShaderParameter<T>* >(mUniformNames[name]);
    }

    static ShaderProgram* from_stream(IGraphicsDevice*, std::istream*);
};

}

#endif /* SHADER_H_ */
