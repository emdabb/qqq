/**
 * @file pixel_buffer.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 5, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef PIXEL_BUFFER_H_
#define PIXEL_BUFFER_H_

#include <iostream>
#include <core/lockable.h>
#include <core/factory.h>
#include <gfx/graphics_device.h>
#include <gfx/IBasicBuffer.h>
#include <assert.h>

namespace qqq {

struct pixel_buffer_impl : public basic_buffer_impl {

};

class pixel_buffer{
public:

};

} // qqq


#endif /* PIXEL_BUFFER_H_ */
