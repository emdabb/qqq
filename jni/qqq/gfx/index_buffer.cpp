/**
 * @file index_buffer.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Sep 12, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include "index_buffer.h"


using namespace qqq;

IndexBuffer::IndexBuffer(IGraphicsDevice* pGraphicsDevice, const int indexType, size_t numElements, size_t elementSize)
: mNumElements(numElements)
, mElementSize(elementSize)
, mElementType(indexType) {
    mPimpl = Factory<index_buffer_impl>::instance().create(pGraphicsDevice->api());
    mPimpl->lock();
    mPimpl->create(this);
    mPimpl->unlock();
    pGraphicsDevice->addContextListener(this);
}

IndexBuffer::~IndexBuffer() {
    delete mPimpl;
    mPimpl = NULL;
}

basic_buffer_impl& IndexBuffer::get_impl() {
    return *mPimpl;
}

void IndexBuffer::onContextLost(const GraphicsResourceEventArgs& args){

}

void IndexBuffer::onContextReset(const GraphicsResourceEventArgs& args){

}

void IndexBuffer::destroy() {

}
