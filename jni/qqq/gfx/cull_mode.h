/*
 * cull_mode.h
 *
 *  Created on: Jun 27, 2014
 *      Author: miel
 */

#ifndef CULL_H_
#define CULL_H_


namespace qqq {
struct cull_mode {
  enum {
      E_NONE,
    E_CW,
    E_CCW,
    E_MAX
  };
};
}


#endif /* CULL_H_ */
