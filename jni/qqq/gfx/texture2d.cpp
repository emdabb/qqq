/*
 * texture2d.cpp
 *
 *  Created on: Jun 26, 2014
 *      Author: miel
 */

#include "texture2d.h"
#include "graphics_device.h"
#include "surface.h"
#include <core/factory.h>
#include <assert.h>

using namespace qqq;

Texture2D::Texture2D(IGraphicsDevice* dev, size_t w, size_t h, const int sf, bool has_levels)
: _width(w)
, _height(h)
, pimpl(NULL) {
    DEBUG_METHOD();
    DEBUG_VALUE_AND_TYPE_OF(w);
    DEBUG_VALUE_AND_TYPE_OF(h);
    DEBUG_VALUE_AND_TYPE_OF(sf);
    DEBUG_VALUE_AND_TYPE_OF(has_levels);
    const char* apiString =dev->api();
  pimpl = Factory<texture2d_impl>::instance().create(apiString);
  assert(pimpl);
  pimpl->lock();
  pimpl->create(_width, _height, sf, has_levels);
  pimpl->unlock();
}

Texture2D* Texture2D::createFromSurface(IGraphicsDevice* graphicsDevice, const Surface& src, bool hasLevels) {
    Texture2D* out = new Texture2D(graphicsDevice, src.get_width(), src.get_height(), SurfaceFormat::Color, hasLevels);
    out->setData<char>(src.get_data(), src.get_width(), src.get_height());
    return out;
}

void Texture2D::destroy() {

}
void Texture2D::onContextReset(const GraphicsResourceEventArgs&) {

}
void Texture2D::onContextLost(const GraphicsResourceEventArgs&) {

}


