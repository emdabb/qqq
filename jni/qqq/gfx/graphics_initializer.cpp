/**
 * @file graphics_initializer.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date May 6, 2015
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include "graphics_initializer.h"

using namespace qqq;

GraphicsInitializer::GraphicsInitializer()
: mX(0), mY(0)
, mWidth(1024), mHeight(768)
, mPixelFormat(SurfaceFormat::Bgr565)
, mIsFullscreen(false)
, mDepthBits(16)
, mStencilBits(8)
{

}
GraphicsInitializer::~GraphicsInitializer() {
}
GraphicsInitializer& GraphicsInitializer::x(int val) {
    mX = val;
    return *this;
}
GraphicsInitializer& GraphicsInitializer::y(int val) {
    mY = val;
        return *this;
}
GraphicsInitializer& GraphicsInitializer::width(size_t val) {
    mWidth = val;
    return *this;
}
GraphicsInitializer& GraphicsInitializer::height(size_t val) {
    mHeight = val;
    return *this;
}
GraphicsInitializer& GraphicsInitializer::pixelFormat(const PixelFormat& val) {
    mPixelFormat= val;
    return *this;
}
GraphicsInitializer& GraphicsInitializer::fullscreen(bool val) {
    mIsFullscreen = val;
    return *this;
}

GraphicsInitializer& GraphicsInitializer::depthBits(int val) {
    mDepthBits = val;
    return *this;
}

GraphicsInitializer& GraphicsInitializer::stencilBits(int val) {
    mStencilBits = val;
    return *this;
}

const int GraphicsInitializer::x() const {
    return mX;
}
const int GraphicsInitializer::y() const {
    return mY;
}
const size_t GraphicsInitializer::width() const {
    return mWidth;
}
const size_t GraphicsInitializer::height() const {
    return mHeight;
}
const PixelFormat& GraphicsInitializer::pixelFormat() const {
    return mPixelFormat;
}
const bool GraphicsInitializer::fullscreen() const {
    return mIsFullscreen;
}

const int GraphicsInitializer::depthBits() const {
    return mDepthBits;
}
const int GraphicsInitializer::stencilBits() const {
    return mStencilBits;
}


