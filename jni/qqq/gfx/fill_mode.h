/*
 * fill_mode.h
 *
 *  Created on: Jun 27, 2014
 *      Author: miel
 */

#ifndef FILL_MODE_H_
#define FILL_MODE_H_

namespace qqq {
struct fill_mode {
  enum {
    E_SOLID,
    E_WIREFRAME,
    E_MAX
  };
};
}



#endif /* FILL_MODE_H_ */
