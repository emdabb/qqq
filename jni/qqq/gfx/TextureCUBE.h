/**
 * @file texture_cube.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 5, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef TEXTURE_CUBE_H_
#define TEXTURE_CUBE_H_

#include <gfx/IGraphicsResource.h>

namespace qqq {

class TextureCUBE;

struct texture_cube_face {
  enum {
    E_FACE_RIGHT,
    E_FACE_LEFT,
    E_FACE_TOP,
    E_FACE_BOTTOM,
    E_FACE_FRONT,
    E_FACE_BACK,
    E_FACE_MAX
  };
};

struct texture_cube_impl {
  virtual ~texture_cube_impl() {}
  virtual void create(TextureCUBE*) = 0;
  virtual void set_data(const void*, const int) = 0;
  virtual void get_data(void*, const int) = 0;
};

class TextureCUBE : public IGraphicsResource {
  texture_cube_impl* pimpl;
  IGraphicsDevice* mGraphicsDevice;
public:
  TextureCUBE(IGraphicsDevice*, size_t, const int);
  virtual ~TextureCUBE();
  virtual void destroy();
  virtual void onContextLost(const GraphicsResourceEventArgs&);
  virtual void onContextReset(const GraphicsResourceEventArgs&);
};

}



#endif /* TEXTURE_CUBE_H_ */
