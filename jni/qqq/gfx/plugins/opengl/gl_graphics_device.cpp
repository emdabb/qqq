/**
 * @file gl_graphics_device.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 5, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include <gfx/graphics_device.h>
#include <gfx/graphics_context.h>
#include <gfx/vertex_buffer.h>
#include <gfx/index_buffer.h>
#include <gfx/pixel_buffer.h>
#include <gfx/texture2d.h>
#include <gfx/texture3d.h>
#include <gfx/color.h>
#include <gfx/depth_stencil_buffer.h>
#include <gfx/plugins/opengl/qqq_gl.h>
#include <gfx/render_state.h>
#include <gfx/shader.h>
#include <gfx/viewport.h>
#include <gfx/cull_mode.h>
#include <base/application.h>
#include <stdio.h>
#include <core/debug_log.h>
#include <core/factory.h>
#include <stack>
#include "qqq_gl.h"

#define PRINT_GL_STRING(name) { \
  const char* name##_VALUE = (const char*)glGetString(name);\
  if(name##_VALUE) { \
      DEBUG_VALUE_AND_TYPE_OF(name##_VALUE);\
  } else {\
      DEBUG_MESSAGE("error getting value.");\
  }\
}

#define XSTR2(x)	#x
#define XSTR(x)		XSTR2(#x)
#define BUFFER_OFFSET(m)   ((char *)NULL + ((m)))

namespace qqq {

QQQ_C_API vertex_buffer_impl* QQQ_CALL new_gl_vertex_buffer();
QQQ_C_API index_buffer_impl* QQQ_CALL new_gl_index_buffer();
QQQ_C_API texture2d_impl* QQQ_CALL new_gl_texture2d();
QQQ_C_API texture3d_impl* QQQ_CALL new_gl_texture3d();
QQQ_C_API pixel_buffer_impl* QQQ_CALL new_gl_pixel_buffer();
QQQ_C_API IDepthStencilBuffer* QQQ_CALL new_gl_depth_stencil_buffer();
QQQ_C_API IShaderProgram* QQQ_CALL new_gl_shader_program();

namespace impl {

#if defined(QQQ_HAVE_OPENGL_CORE)
void GLAPIENTRY glErrorCallback(GLenum source,
        GLenum type,
        GLuint id,
        GLenum severity,
        GLsizei length,
        const GLchar* message,
        const void* userParam) {
    if(type == GL_DEBUG_TYPE_OTHER) {
        return;
    }
    DEBUG_METHOD();
    DEBUG_MESSAGE(message);
    switch (type) {
        case GL_DEBUG_TYPE_ERROR:
        DEBUG_MESSAGE("type= [ERROR]");
        break;
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
        DEBUG_MESSAGE("type= [DEPRECATED_BEHAVIOR]");
        break;
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
        DEBUG_MESSAGE("type= [UNDEFINED_BEHAVIOR]");
        break;
        case GL_DEBUG_TYPE_PORTABILITY:
        DEBUG_MESSAGE("type= [PORTABILITY]");
        break;
        case GL_DEBUG_TYPE_PERFORMANCE:
        DEBUG_MESSAGE("type= [PERFORMANCE]");
        break;
        case GL_DEBUG_TYPE_OTHER:
        DEBUG_MESSAGE("type= [OTHER]");
        break;
    }

    DEBUG_VALUE_AND_TYPE_OF(id);
    switch (severity) {
        case GL_DEBUG_SEVERITY_LOW:
        DEBUG_MESSAGE("severity= [GL_DEBUG_SEVERITY_LOW]");
        break;
        case GL_DEBUG_SEVERITY_MEDIUM:
        DEBUG_MESSAGE("severity= [GL_DEBUG_SEVERITY_MEDIUM]");
        break;
        case GL_DEBUG_SEVERITY_HIGH:
        DEBUG_MESSAGE("severity= [GL_DEBUG_SEVERITY_HIGH]");
        break;
    }
}
#endif

class GLStateManager {
    std::map<GLenum, bool> mBooleanState;
    float mLineWidth;
    GLenum mSrcBlendRGB, mDstBlendRGB;
    GLenum mSrcBlendAlpha, mDstBlendAlpha;
    GLenum mDepthFunc;

    GLenum mStencilFunc;
    GLint mStencilRef;
    GLuint mStencilMask;
    GLenum mStencilOpFail;
    GLenum mStencilOpDepthFail;
    GLenum mStencilOpPass;
    std::stack<GLuint> mTexture2D;
    std::vector<bool> mVertexAttribArray;
    bool mWriteToDepthBuffer;
    std::map<GLenum, GLuint> mFrameBufferObjects;
    Color mBlendColor;
    GLenum mBlendEquationRGB;
    GLenum mBlendEquationA;
    Viewport mViewport;
    GLenum mCullFace;
    GLenum mFrontFace;
protected:
    void initBoolean(GLenum which) {
        GLboolean isEnabled = glIsEnabled(which);
        mBooleanState[which] = isEnabled != GL_FALSE ? true : false;
    }
public:
    void init() {
        /** Optimization: guarantees a stack size of at least 1 */
        mTexture2D.push(GL_NONE);

        //glBlendEquation(GL_FUNC_ADD);
        /**
         *
         * Get initial blend values.
         *
         */
        glGetIntegerv(GL_BLEND_SRC_RGB, (GLint*) &mSrcBlendRGB);
        glGetIntegerv(GL_BLEND_SRC_ALPHA, (GLint*) &mSrcBlendAlpha);
        glGetIntegerv(GL_BLEND_DST_RGB, (GLint*) &mDstBlendRGB);
        glGetIntegerv(GL_BLEND_DST_ALPHA, (GLint*) &mDstBlendAlpha);
        /**
         *
         * Get initial float values.
         *
         */
        glGetFloatv(GL_LINE_WIDTH, (GLfloat*) &mLineWidth);
        /**
         *
         * Get initial boolean values. (WIP)
         *
         */
        initBoolean(GL_BLEND); // 	glBlendFunc, glLogicOp
        initBoolean(GL_CULL_FACE); // 	glCullFace
        initBoolean(GL_DEPTH_TEST); // 	glDepthFunc, glDepthRange
        initBoolean(GL_DITHER); // 	glEnable
        initBoolean(GL_POLYGON_OFFSET_FILL); // 	glPolygonOffset
        initBoolean(GL_SAMPLE_ALPHA_TO_COVERAGE); // 	glSampleCoverage
        initBoolean(GL_SAMPLE_COVERAGE); // 	glSampleCoverage
        initBoolean(GL_SCISSOR_TEST); // 	glScissor
        initBoolean(GL_STENCIL_TEST); // 	glStencilFunc, glStencilOp

#if defined(QQQ_HAVE_OPENGL_CORE)
        initBoolean(GL_COLOR_LOGIC_OP); // 	glLogicOp
        initBoolean(GL_DEPTH_CLAMP);// 	glEnable
        initBoolean(GL_DEBUG_OUTPUT);// 	glEnable
        initBoolean(GL_DEBUG_OUTPUT_SYNCHRONOUS);// 	glEnable
        //initBoolean(GL_CLIP_DISTANCEi);// 	glEnable
        initBoolean(GL_FRAMEBUFFER_SRGB);// 	glEnable
        initBoolean(GL_LINE_SMOOTH);// 	glLineWidth
        initBoolean(GL_MULTISAMPLE);// 	glSampleCoverage
        initBoolean(GL_POLYGON_SMOOTH);// 	glPolygonMode
        initBoolean(GL_POLYGON_OFFSET_LINE);// 	glPolygonOffset
        initBoolean(GL_POLYGON_OFFSET_POINT);// 	glPolygonOffset
        initBoolean(GL_PROGRAM_POINT_SIZE);// 	glEnable
        initBoolean(GL_PRIMITIVE_RESTART);// 	glEnable, glPrimitiveRestartIndex
        initBoolean(GL_SAMPLE_ALPHA_TO_ONE);// 	glSampleCoverage
        initBoolean(GL_SAMPLE_MASK);// 	glEnable
        initBoolean(GL_TEXTURE_CUBE_MAP_SEAMLESS);//
#endif
        /**
         *
         * Get depth-write mask seperately... :(
         *
         */
        glGetIntegerv(GL_DEPTH_WRITEMASK, (GLint*) &mWriteToDepthBuffer);
        /**
         *
         * Get the stencil buffer's state
         *
         */
        glGetIntegerv(GL_STENCIL_FAIL, (GLint*) &mStencilOpFail);
        glGetIntegerv(GL_STENCIL_PASS_DEPTH_FAIL,
                (GLint*) &mStencilOpDepthFail);
        glGetIntegerv(GL_STENCIL_PASS_DEPTH_PASS, (GLint*) &mStencilOpPass);
        glGetIntegerv(GL_STENCIL_FUNC, (GLint*) &mStencilFunc);
        glGetIntegerv(GL_STENCIL_VALUE_MASK, (GLint*) &mStencilMask);
        glGetIntegerv(GL_STENCIL_REF, (GLint*) &mStencilRef);

        glGetIntegerv(GL_DEPTH_FUNC, (GLint*)&mDepthFunc);
        /**
         *
         * Get initial vertex attribute array values and caps.
         *
         */
        GLuint maxVertexAttribs;
        glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, (GLint*) &maxVertexAttribs);
        mVertexAttribArray.resize(maxVertexAttribs);
        GLint isEnabled = 0;
        for (GLuint i = 0; i < maxVertexAttribs; i++) {
            glGetVertexAttribiv(i, GL_VERTEX_ATTRIB_ARRAY_ENABLED, &isEnabled);
            mVertexAttribArray[i] = isEnabled > 0 ? true : false;
        }
        /**
         *
         * FBO stuff.
         *
         */
        this->bindFramebuffer(GL_FRAMEBUFFER, 0);
        //this->bindFramebuffer(GL_FRAMEBUFFER, 0);

        glGetIntegerv(GL_CULL_FACE_MODE, (GLint*)&mCullFace);
        glGetIntegerv(GL_FRONT_FACE, (GLint*)&mFrontFace);


        mViewport.farPlaneDistance = 1.f;
        mViewport.nearPlaneDistance = 1000.f;

        mViewport.X = -1;
        mViewport.Y = -1;
        mViewport.W = -1;
        mViewport.H = -1;
    }

    bool bindFramebuffer(GLenum dst, GLuint src) {
        if (mFrameBufferObjects[dst] != src) {
            mFrameBufferObjects[dst] = src;
            glBindFramebuffer(dst, src);
            return true;
        }
        return false;
    }

    bool setBoolean(GLenum which, bool value) {
        bool prevValue = mBooleanState[which];
        if (prevValue != value) {
            value ? glEnable(which) : glDisable(which);
            mBooleanState[which] = value;
            return true;
        }
        return false;
    }

    bool setBlendColor(const uint32_t cc) {
        if (mBlendColor.value != cc) {
            mBlendColor.value = cc;
            GLfloat fR = mBlendColor.r / 255.f;
            GLfloat fG = mBlendColor.g / 255.f;
            GLfloat fB = mBlendColor.b / 255.f;
            GLfloat fA = mBlendColor.a / 255.f;

            glBlendColor(fR, fG, fB, fA);
            return true;
        }
        return false;
    }

    bool setBlendFunc(GLenum src, GLenum dst) {
        bool isValid = true;
        if (mSrcBlendRGB != src) {
            mSrcBlendRGB = src;
            isValid = false;
        }
        if (mDstBlendRGB != dst) {
            mDstBlendRGB = dst;
            isValid = false;
        }

        if (isValid != true) {
            glBlendFuncSeparate(mSrcBlendRGB, mDstBlendRGB, mSrcBlendRGB,
                    mDstBlendRGB);
            return true;
        }
        return false;
    }

    bool setLineWidth(float val) {
        if (mLineWidth != val) {
            mLineWidth = val;
            glLineWidth(mLineWidth);
            return true;
        }
        return false;
    }

    bool pushTexture2D(GLuint id) {
        bool isValid = true;
        if (mTexture2D.top() != id) {
            isValid = false;
        }

        mTexture2D.push(id);
        if (!isValid) {
            glBindTexture(GL_TEXTURE_2D, id);
            return true;
        }
        return false;
    }

    bool popTexture2D() {
        GLuint id = mTexture2D.top();
        mTexture2D.pop();
        if (id != mTexture2D.top()) {
            glBindTexture(GL_TEXTURE_2D, mTexture2D.top());
            return true;
        }
        return false;
    }

    bool setVertexAttribArray(GLuint which, bool val) {
        if (mVertexAttribArray[which] != val) {
            mVertexAttribArray[which] = val;
            val ? glEnableVertexAttribArray(which) : glDisableVertexAttribArray(
                            which);
            return true;
        }
        return false;
    }

    bool setDepthWrite(bool val) {
        if (val != mWriteToDepthBuffer) {
            mWriteToDepthBuffer = val;
            glDepthMask(mWriteToDepthBuffer ? GL_TRUE : GL_FALSE);
            return true;
        }
        return false;
    }

//    bool setBlendState(BlendState const& state) {
//        bool res = setBlendFunc(state.AlphaSourceBlend, state.AlphaDestinationBlend);
//        return res;
//    }
//
//    bool setDepthStencilState(DepthStencilState const& state) {
//        bool res = setBoolean(GL_DEPTH_TEST, (bool) state.DepthBufferEnable);
//        res &= setDepthWrite((bool) state.DepthBufferWriteEnable);
//        res &= setBoolean(GL_STENCIL_TEST, (bool) state.StencilEnable);
//        return res;
//    }
//
//    bool setRasterizerState(RasterizerState const& state) {
//        bool res = setBoolean(GL_CULL_FACE, (bool) state.CullMode);
//        return res;
//    }

    bool setBlendEquation(const GLenum eRGB, const GLenum eA) {
        bool isValid = true;
        if (mBlendEquationRGB != eRGB) {
            mBlendEquationRGB = eRGB;
            isValid = false;
        }

        if (mBlendEquationA != eA) {
            mBlendEquationA = eA;
            isValid = false;
        }

        if (!isValid) {
            glBlendEquationSeparate(mBlendEquationRGB, mBlendEquationA);
            return true;
        }
        return false;
    }

    bool stencilOp(GLenum sfail, GLenum dfail, GLenum pass) {
        bool isValid = true;
        if (mStencilOpFail != sfail) {
            mStencilOpFail = sfail;
            isValid = false;
        }

        if (mStencilOpDepthFail != dfail) {
            mStencilOpDepthFail = dfail;
            isValid = false;
        }

        if (mStencilOpPass != pass) {
            mStencilOpPass = pass;
            isValid = false;
        }

        if (!isValid) {
            glStencilOp(mStencilOpFail, mStencilOpDepthFail, mStencilOpPass);
            return true;
        }
        return false;
    }

    bool depthFunc(GLenum func) {
        if(mDepthFunc != func) {
            mDepthFunc = func;
            glDepthFunc(func);
            return true;
        }
        return false;
    }

    bool stencilFunc(GLenum func, GLint ref, GLuint mask) {
        bool isValid = true;
        if (mStencilFunc != func) {
            mStencilFunc = func;
            isValid = false;
        }
        if (mStencilRef != ref) {
            mStencilRef = ref;
            isValid = false;
        }
        if (mStencilMask != mask) {
            mStencilMask = mask;
            isValid = false;
        }

        if (!isValid) {
            glStencilFunc(mStencilFunc, mStencilRef, mStencilMask);
            return true;
        }
        return false;
    }

    bool setViewport(const Viewport& a) {
//        if(		(a.rect.X != mViewport.rect.X) ||
//                (a.rect.Y != mViewport.rect.Y) ||
//                (a.rect.W != mViewport.rect.W) ||
//                (a.rect.H != mViewport.rect.H)) {
//            mViewport.X = a.X;
//            mViewport.Y = a.Y;
//            mViewport.W = a.W;
//            mViewport.H = a.H;

            glViewport(a.X, a.Y, a.W, a.H);
            glScissor(a.X, a.Y, a.W, a.H);

            return true;
        //}


        //return false;
    }

    bool cullFace(GLenum val) {
        if(mCullFace != val) {
            mCullFace = val;
            glCullFace(mCullFace);
            return true;
        }
        return false;
    }

    bool frontFace(GLenum val) {
        if(mFrontFace != val) {
            mFrontFace = val;
            glFrontFace(mFrontFace);
            return true;
        }
        return false;
    }
};

#define USE_STATE_MANAGER

#define REGISTER_GFX_FACTORY(x, y, z) { \
        Factory<x>::instance().registerClass(y, z);\
            DEBUG_MESSAGE("registered factory -> %s for %s", #x, #y); \
        }

class gl_graphics_device: public IGraphicsDevice {
    enum {
        E_FBO_READ, E_FBO_WRITE, E_FBO_MAX
    };

    IGraphicsContext* mGraphicsContext;
    size_t mWidth;
    size_t mHeight;
    GLuint mFrameBufferObject[E_FBO_MAX];
    DepthStencilBuffer* mDepthStencilBuffer;
    BlendState mBlendState;
    RasterizerState mRasterizerState;
    DepthStencilState mDepthStencilState;

    IndexBuffer* mIndexBuffer;
    VertexBuffer* mVertexBuffer;
    GLuint mVAO;
    //Viewport mViewport;
    size_t mMaxDrawBuffers;
    GLenum* mDrawBuffers;
    size_t mMaxRenderTargets;
    int mNumRenderTargetsAttached;
    std::stack<Viewport> mViewportStack;
#if defined(USE_STATE_MANAGER)
    GLStateManager mStateManager;
#endif
public:
    gl_graphics_device() :
            mGraphicsContext(0), mWidth(0), mHeight(0), mDepthStencilBuffer(0), mIndexBuffer(
                    0), mVertexBuffer(0), mVAO(0), mMaxDrawBuffers(0), mDrawBuffers(
            NULL), mMaxRenderTargets(0), mNumRenderTargetsAttached(0) {

    }

    virtual ~gl_graphics_device() {

    }

    virtual IGraphicsContext* getContext() {
        return mGraphicsContext;
    }

    void addContextListener(IGraphicsResource* res) {
        getContext()->OnContextLost += event(res,
                &IGraphicsResource::onContextLost);
        getContext()->OnContextReset += event(res,
                &IGraphicsResource::onContextReset);
    }

    virtual void onContextLost(const GraphicsResourceEventArgs& args) {
        destroy();
    }

    virtual void onContextReset(const GraphicsResourceEventArgs& args) {
        create(args.GraphicsContext);
    }

    void register_factories() {
        DEBUG_METHOD();
        REGISTER_GFX_FACTORY(vertex_buffer_impl, QQQ_APISTRING_OPENGL, &new_gl_vertex_buffer);
        REGISTER_GFX_FACTORY(vertex_buffer_impl, QQQ_APISTRING_OPENGL, &new_gl_vertex_buffer);
        REGISTER_GFX_FACTORY(index_buffer_impl, QQQ_APISTRING_OPENGL, &new_gl_index_buffer);
        REGISTER_GFX_FACTORY(texture2d_impl, QQQ_APISTRING_OPENGL, &new_gl_texture2d);
        REGISTER_GFX_FACTORY(texture3d_impl, QQQ_APISTRING_OPENGL, &new_gl_texture3d);
        REGISTER_GFX_FACTORY(pixel_buffer_impl, QQQ_APISTRING_OPENGL, &new_gl_pixel_buffer);
        REGISTER_GFX_FACTORY(IDepthStencilBuffer, QQQ_APISTRING_OPENGL, &new_gl_depth_stencil_buffer);
        REGISTER_GFX_FACTORY(IShaderProgram, QQQ_APISTRING_OPENGL, &new_gl_shader_program);
    }
    virtual bool create(IGraphicsContext* ctx) {
        DEBUG_METHOD();
        mGraphicsContext = ctx;
        register_factories();

        synchronized(mGraphicsContext) {

            //glewInit();
            PRINT_GL_STRING(GL_VENDOR);
            PRINT_GL_STRING(GL_VERSION);
            PRINT_GL_STRING(GL_RENDERER);
            PRINT_GL_STRING(GL_SHADING_LANGUAGE_VERSION);

#if defined(QQQ_HAVE_OPENGL_CORE)
            GLint num_ext = 0;
            glGetIntegerv(GL_NUM_EXTENSIONS, &num_ext);
            for(int i=0; i < num_ext; i++) {
                DEBUG_VALUE_AND_TYPE_OF(glGetStringi(GL_EXTENSIONS, i));
            }
#else
            PRINT_GL_STRING(GL_EXTENSIONS);
#endif
            /**
             *
             * Initialize GL state.
             *
             */
#if defined(USE_STATE_MANAGER)
            mStateManager.init();
//            mStateManager.setBoolean(GL_DEPTH_TEST, false);
            mStateManager.setBoolean(GL_CULL_FACE, true);
            mStateManager.setBoolean(GL_BLEND, true);
//            mStateManager.cullFace(GL_BACK);
//            mStateManager.frontFace(GL_CCW);

#if defined(QQQ_HAVE_OPENGL_CORE)
            mStateManager.setBoolean(GL_LINE_SMOOTH, true);
            mStateManager.setBoolean(GL_POLYGON_SMOOTH, true);
            glHint( GL_LINE_SMOOTH_HINT, GL_NICEST );
            glHint( GL_POLYGON_SMOOTH_HINT, GL_NICEST );
#endif

            mStateManager.setBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
            mStateManager.setLineWidth(1.5f);
#else
            glDisable(GL_DEPTH_TEST);
            glDisable(GL_CULL_FACE);
            glEnable(GL_BLEND);
            glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
#endif

#if defined(_DEBUG) && defined(QQQ_HAVE_OPENGL_CORE)
            //glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
#if defined(USE_STATE_MANAGER)
            mStateManager.setBoolean(GL_DEBUG_OUTPUT_SYNCHRONOUS, true);
#else
            glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
#endif
            glDebugMessageCallback(glErrorCallback, this);
            GLuint unusedIds = 0;
            glDebugMessageControl(
                    GL_DONT_CARE,
                    GL_DONT_CARE,
                    GL_DONT_CARE,
                    0,
                    &unusedIds,
                    true
            );
#endif
            /**
             *
             * Initialize render state (TODO: from GLStateManager)
             *
             */
            setBlendState(BlendState::Default);
            setDepthStencilState(DepthStencilState::Default);
            setRasterizerState(RasterizerState::Default);
            /**
             *
             * check and set render target caps.
             *
             */

#if defined(QQQ_HAVE_OPENGL_CORE)
            glGetIntegerv(GL_MAX_DRAW_BUFFERS, (GLint*)&mMaxDrawBuffers);
            glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, (GLint*)&mMaxRenderTargets);
#else
            mMaxRenderTargets = 1;
            mMaxDrawBuffers = 1;
#endif
            mDrawBuffers = new GLenum[mMaxDrawBuffers];
            memset(mDrawBuffers, GL_NONE, sizeof(GLenum) * mMaxDrawBuffers);
            /**
             *
             * Misc.
             *
             */
            glGenFramebuffers(E_FBO_MAX, mFrameBufferObject);
            glGenVertexArrays(1, &mVAO);
            // TODO: DEPTH16 vs DEPTH_STENCIL... Android vs Desktop
            mDepthStencilBuffer = new DepthStencilBuffer(this, 1, 1, depth_format::E_DEPTH16);
        }



        return true;
    }

    virtual void destroy() {

    }

    virtual bool beginFrame() {
        if(mGraphicsContext->lock()) {
            Viewport vp;
            vp.X = 0;
            vp.Y = 0;
            vp.W = mWidth;
            vp.H = mHeight;
            vp.nearPlaneDistance=.1f;
            vp.farPlaneDistance	=100.f;
            pushViewport(vp);
            return true;
        }

        return false;
    }
    virtual void endFrame() {
#if 0
        glFinish();
#endif
        while(mViewportStack.size()) {
            mViewportStack.pop();
        }
        mGraphicsContext->swapBuffers();
        mGraphicsContext->unlock();
    }
    virtual void clear(Color const& tint, const int mask, float clearDepth, int clearStencil) {
        glClearDepthf(clearDepth);
        glClearStencil(clearStencil);

        glClearColor(
                tint.r / 255.f,
                tint.g / 255.f,
                tint.b / 255.f,
                tint.a / 255.f);

        GLenum glMask = GL_NONE;
        glMask |= mask & ClearOptions::ClearColor ? GL_COLOR_BUFFER_BIT : GL_NONE;
        glMask |= mask & ClearOptions::ClearDepth ? GL_DEPTH_BUFFER_BIT : GL_NONE;
        glMask |= mask & ClearOptions::ClearStencil ? GL_STENCIL_BUFFER_BIT : GL_NONE;

        glClear(glMask);
    }

    virtual void onContextResize(const WindowResizeArgs& args) {

        if(mWidth != args.width || mHeight != args.height) {
            DEBUG_METHOD();
            mWidth = args.width;
            mHeight = args.height;

            DEBUG_VALUE_AND_TYPE_OF(mWidth);
            DEBUG_VALUE_AND_TYPE_OF(mHeight);
            synchronized(mGraphicsContext) {
                delete mDepthStencilBuffer;
                // XXX: Depth16?? only on Android... *sigh*
                mDepthStencilBuffer = new DepthStencilBuffer(this, mWidth, mHeight, depth_format::E_DEPTH16);
            }
        }
    }

    virtual const char* api() const {
        return QQQ_APISTRING_OPENGL;
    }

    void drawPrimitives(const int type, const int start, const size_t count, size_t primcount) {
        for(size_t i=0; i < primcount; i++) {
            glDrawArrays(
                    QQQ_GL_PRIM[type],
                    start + count * i,
                    count);
        }
    }

    void drawIndexedPrimitives(const int type, const int base, const int imin, const size_t count, const int i0, const size_t primcount) {
        for(size_t i=0; i < primcount; i++) {
            glDrawElements(
                    QQQ_GL_PRIM[type],
                    count,
                    QQQ_GL_INDEX_TYPE[mIndexBuffer->type()],
                    BUFFER_OFFSET(i0));
        }
    }

    void setIndexBuffer(IndexBuffer* ib) {
        if(mIndexBuffer !=ib ) {
            if(mIndexBuffer) {
                mIndexBuffer->get_impl().unlock();
            }
            mIndexBuffer = ib;
            if(mIndexBuffer) {
                mIndexBuffer->get_impl().lock();
            }
        }
    }

    void setStreamSource(VertexBuffer* vb) {
        if(mVertexBuffer != vb) {
            VertexBuffer* prev = mVertexBuffer;

            if(prev) {
                if(prev->get_vertex_declaration() != vb->get_vertex_declaration()) {
                    disableStreams(prev->get_vertex_declaration());
                }
                prev->get_impl().unlock();
            }
            glBindVertexArray(mVAO);
            mVertexBuffer = vb;
            if(mVertexBuffer != NULL) {
                mVertexBuffer->get_impl().lock();
                enableStreams(mVertexBuffer->get_vertex_declaration());
            } else {
                glBindBuffer(GL_ARRAY_BUFFER, 0);
                glBindVertexArray(0);
            }
        }
    }

    void disableStreams(const VertexDeclaration* d) {
        //glBindVertexArray(mVAO);
        for(uint16_t i=0; i < d->size(); i++) {
            mStateManager.setVertexAttribArray(d->at(i).vertexElementUsage, false);
        }
        //glBindVertexArray(0);
    }

    void enableStreams(const VertexDeclaration* d) {
        //glBindVertexArray(mVAO);
        for(uint16_t i=0; i < d->size(); i++) {

            mStateManager.setVertexAttribArray(d->at(i).vertexElementUsage, true);

            glVertexAttribPointer(
                    d->at(i).vertexElementUsage,
                    QQQ_GL_VERTEX_FORMAT_SIZE[d->at(i).vertexElementFormat], // count
                    QQQ_GL_VERTEX_FORMAT_TYPE[d->at(i).vertexElementFormat],// type
                    QQQ_GL_VERTEX_FORMAT_NORM[d->at(i).vertexElementFormat],// normalized
                    d->stride(),
                    BUFFER_OFFSET(d->at(i).offset)
            );
        }
        //glBindVertexArray(0);
    }

    void setRenderTarget(size_t n, Texture2D* dst) {
        assert(n < mMaxRenderTargets);
        GLuint texID;

        if(mNumRenderTargetsAttached == 0) {
            mStateManager.bindFramebuffer(GL_FRAMEBUFFER, mFrameBufferObject[E_FBO_READ]);
        }
        GLenum at = GL_COLOR_ATTACHMENT0 + n;
        dst->getImpl().lock();
        glGetIntegerv(GL_TEXTURE_BINDING_2D, (GLint*)&texID);
        dst->getImpl().unlock();
        glFramebufferTexture2D(GL_FRAMEBUFFER, at, GL_TEXTURE_2D, texID, 0);
        if(this->mDepthStencilBuffer) {
            mDepthStencilBuffer->get_impl().lock();
            GLuint depthBuffer = 0;
            glGetIntegerv(GL_RENDERBUFFER_BINDING, (GLint*)&depthBuffer);
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);
        } else {
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, 0);
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_RENDERBUFFER, 0);
        }

//      mDrawBuffers[mNumRenderTargetsAttached] = at;
//      glDrawBuffers(mNumRenderTargetsAttached, mDrawBuffers);
        GLenum err = glCheckFramebufferStatus(GL_FRAMEBUFFER);
        if(err != GL_FRAMEBUFFER_COMPLETE) {
            assert(0);
        }

        mNumRenderTargetsAttached++;
    }

    void resolveRenderTarget(int n) {
        mNumRenderTargetsAttached--;
        /**
         *
         * Set drawbuffer n to GL_NONE, removing it from the draw targets.
         *
         */
        //mDrawBuffers[mNumRenderTargetsAttached] = GL_NONE;
        //glDrawBuffers(mNumRenderTargetsAttached, mDrawBuffers);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + n, GL_TEXTURE_2D, 0, 0);
        /**
         *
         * If there are no more render targets attached, detach the framebuffer object as well. This
         * returns the GL state machine to the default setting (back buffer or front buffer, depending
         * on whether or not double buffering is enabled (default)).
         *
         */

        if(mNumRenderTargetsAttached == 0) {
            mStateManager.bindFramebuffer(GL_FRAMEBUFFER, 0);
        }

    }

    virtual void setBlendState(const BlendState& a) {
        static const GLenum GLBLEND[] = {
            GL_ZERO,      //E_ZERO,
            GL_ONE,//E_ONE,
            GL_SRC_COLOR,//E_SRC_COLOR,
            GL_ONE_MINUS_SRC_COLOR,//E_SRC_COLOR_INV,
            GL_SRC_ALPHA,//E_SRC_ALPHA,
            GL_ONE_MINUS_SRC_ALPHA,//E_SRC_ALPHA_INV,
            GL_DST_ALPHA,//E_DST_ALPHA,
            GL_ONE_MINUS_DST_ALPHA,//E_DST_ALPHA_INV,
            GL_DST_COLOR,//E_DST_COLOR,
            GL_ONE_MINUS_DST_COLOR,//E_DST_COLOR_INV,
            GL_SRC_ALPHA_SATURATE,//E_SRC_ALPHA_SAT,
            GL_CONSTANT_COLOR,
            GL_ONE_MINUS_CONSTANT_COLOR
        };

        static const GLenum GLBLENDEQ[] = {
            GL_FUNC_ADD,
            GL_MAX,
            GL_MIN,
            GL_FUNC_REVERSE_SUBTRACT,
            GL_FUNC_SUBTRACT
        };

        mBlendState = a;
        mStateManager.setBlendFunc(GLBLEND[a.AlphaSourceBlend], GLBLEND[a.AlphaDestinationBlend]);
        mStateManager.setBlendColor(a.BlendFactor);
        mStateManager.setBlendEquation(GLBLENDEQ[a.ColorBlendFunction], GLBLENDEQ[a.AlphaBlendFunction]);
    }

    virtual void getBlendState(BlendState* pval) {
        *pval = mBlendState;
    }

    virtual void setRasterizerState(const RasterizerState& val) {

        static const GLenum OGL_FRONT_FACE[] = {
                GL_CCW,
                GL_CCW,
                GL_CW
        };


        //CullNone, //	Do not cull back faces.
        //CullClockwiseFace,//	Cull back faces with clockwise vertices.
        //CullCounterClockwiseFace//	Cull back faces with counterclockwise vertices.

        mRasterizerState = val;
        mStateManager.setBoolean(GL_CULL_FACE, val.CullMode != CullMode::CullNone);
        mStateManager.frontFace(OGL_FRONT_FACE[val.CullMode]);
        mStateManager.setBoolean(GL_SCISSOR_TEST, val.ScissorTestEnable);
    }
    virtual void getRasterizerState(RasterizerState* val) {
        *val = mRasterizerState;
    }

    virtual void setDepthStencilState(const DepthStencilState& val) {
        mDepthStencilState=val;

        mStateManager.setDepthWrite(val.DepthBufferEnable != 0);
        mStateManager.setBoolean(GL_DEPTH_TEST, val.DepthBufferEnable != 0);

        static const GLenum GL_STENCILOP[] = {
                GL_DECR,//Decrement,				//	Decrements the stencil-buffer entry, wrapping to the maximum value if the new value is less than 0.
                GL_DECR_WRAP,//DecrementSaturation,	//	Decrements the stencil-buffer entry, clamping to 0.
                GL_INCR,//Increment,				//	Increments the stencil-buffer entry, wrapping to 0 if the new value exceeds the maximum value.
                GL_INCR_WRAP,//IncrementSaturation,	//	Increments the stencil-buffer entry, clamping to the maximum value.
                GL_INVERT,//Invert,					//	Inverts the bits in the stencil-buffer entry.
                GL_KEEP,//Keep,					//	Does not update the stencil-buffer entry. This is the default value.
                GL_REPLACE,//Replace,				//	Replaces the stencil-buffer entry with a reference value.
                GL_ZERO//Zero					//	Sets the stencil-buffer entry to 0.
        };

        static const GLenum GL_COMPAREFUNC[] = {
                GL_ALWAYS,//AlwaysPass,			//	Always pass the test.
                GL_EQUAL,//Equal,			//	Accept the new pixel if its value is equal to the value of the current pixel.
                GL_GREATER,//Greater,		//	Accept the new pixel if its value is greater than the value of the current pixel.
                GL_GEQUAL,//GreaterEqual,	//	Accept the new pixel if its value is greater than or equal to the value of the current pixel.
                GL_LESS,//Less,			//	Accept the new pixel if its value is less than the value of the current pixel.
                GL_LEQUAL,//LessEqual,		//	Accept the new pixel if its value is less than or equal to the value of the current pixel.
                GL_NEVER,//Never,			//	Always fail the test.
                GL_NOTEQUAL//NotEqual		//	Accept the new pixel if its value does not equal the value of the current pixel.
        };

        mStateManager.depthFunc(GL_COMPAREFUNC[val.DepthBufferFunction]);
        mStateManager.stencilFunc(GL_COMPAREFUNC[val.StencilFunction], val.ReferenceStencil, val.StencilMask);
        mStateManager.stencilOp(GL_STENCILOP[val.StencilFail], GL_STENCILOP[val.StencilPass], GL_STENCILOP[val.StencilDepthBufferFail]);

    }
    virtual void getDepthStencilState(DepthStencilState* val) {
        *val = mDepthStencilState;
    }

    void getDimensions(int* pWidth, int* pHeight) {
        if(pWidth) {
            *pWidth = this->mWidth;
        }
        if(pHeight) {
            *pHeight = this->mHeight;
        }
    }

    virtual void getViewport(Viewport* out) {
        assert(out);
//        out->rect = mViewport.rect;
//        out->nearPlaneDistance = mViewport.nearPlaneDistance;
//        out->farPlaneDistance = mViewport.farPlaneDistance;
        if(mViewportStack.size() > 0) {
            *out = mViewportStack.top();
        } else {
            out->X = 0;
            out->Y = 0;
            out->W = mWidth;
            out->H = mHeight;
            out->nearPlaneDistance = 0.1f;
            out->farPlaneDistance  = 100.0f;
        }
    }

//    virtual void setViewport(const Viewport& in) {
//        mViewport.rect = in.rect;
//        mViewport.nearPlaneDistance = in.nearPlaneDistance;
//        mViewport.farPlaneDistance = in.farPlaneDistance;
//
//        glViewport(
//                mViewport.rect.X,
//                mViewport.rect.Y,
//                mViewport.rect.W,
//                mViewport.rect.H);
//    }

    virtual void pushViewport(const Viewport& a) {
        mViewportStack.push(a);
        mStateManager.setViewport(a);
    }

    virtual bool popViewport() {
        if(mViewportStack.size() > 1) {
            mViewportStack.pop();
            mStateManager.setViewport(mViewportStack.top());
            return true;
        }
        return false;
    }

    virtual void setDepthStencilBuffer(DepthStencilBuffer* pBuffer) {
        mDepthStencilBuffer = pBuffer;
    }
    virtual void getDepthStencilBuffer(DepthStencilBuffer** ppBuffer) {
        *ppBuffer = mDepthStencilBuffer;
    }

};

//QQQ_C_API void new_gl_graphics_device(graphics_device** ppDevice) {
//    *ppDevice = new gl_graphics_device();
//}

QQQ_C_API void hxCreateGraphicsDevice(IGraphicsDevice** ppDevice) {
    *ppDevice = new gl_graphics_device();
}
} // impl
} // qqq
