/*
 * qqq_gl.h
 *
 *  Created on: Jun 26, 2014
 *      Author: miel
 */

#ifndef QQQ_GL_H_
#define QQQ_GL_H_

#include <qqq.h>
#include <map>
#include <stack>
#include <gfx/pixel_format.h>
#define GL_GLEXT_PROTOTYPES 1


#if defined(QQQ_EGL_API_OPENGL_API)

#include <GL/gl.h>
#include <GL/glext.h>
//#include <GL/glu.h>

# if defined(__LINUX__)
#include <GL/glx.h>
#include <GL/glxext.h>
# elif defined(__WIN32__)
#include <GL/wgl.h>
# endif
#else

#if defined(__MACOSX__)

#include <OpenGL/OpenGL.h>
#include <AGL/agl.h>
#endif

#if QQQ_EGL_API_OPENGL_ES_API
#if defined(__ANDROID__) || defined(__LINUX__)
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#else
#include <OpenGLES/ES2/gl.h>
#include <OpenGLES/ES2/glext.h>
#endif
#endif

#if (QQQ_EGL_API_OPENGL_ES_API)

#define GL_HALF_FLOAT 				GL_HALF_FLOAT_OES
#define GL_DEPTH_STENCIL_ATTACHMENT GL_DEPTH_STENCIL_OES
#define GL_DEPTH32_STENCIL8			GL_DEPTH24_STENCIL8_OES
#define GL_DEPTH_COMPONENT24		GL_DEPTH_COMPONENT24_OES
#define GL_DEPTH24_STENCIL8			GL_DEPTH24_STENCIL8_OES
#define GL_DEPTH_COMPONENT32		GL_DEPTH_COMPONENT32_OES
#define GL_DEPTH_COMPONENT32F		GL_NONE

#define GL_TEXTURE_3D				GL_TEXTURE_3D_OES
#define GL_TEXTURE_BINDING_3D		GL_TEXTURE_BINDING_3D_OES

#define GL_BGR						GL_RGB
#if !defined(GL_BGRA)
#define GL_BGRA						GL_RGBA
#endif
#define GL_RED						GL_RGB //RED_EXT
#define GL_RG						GL_RGB //GL_RG_EXT

#if !defined(GL_UNSIGNED_INT_10_10_10_2)
# if defined(GL_UNSIGNED_INT_10_10_10_2_OES)
#  define GL_UNSIGNED_INT_10_10_10_2	GL_UNSIGNED_INT_10_10_10_2_OES
# elif defined (GL_UNSIGNED_INT_2_10_10_10_REV)
#  define GL_UNSIGNED_INT_10_10_10_2	GL_UNSIGNED_INT_2_10_10_10_REV
# else
/* GL_OES_vertex_type_10_10_10_2 */
#  define GL_UNSIGNED_INT_10_10_10_2_OES	0x8DF6
#  define GL_INT_10_10_10_2_OES             0x8DF7
#  define GL_UNSIGNED_INT_10_10_10_2		GL_UNSIGNED_INT_10_10_10_2_OES
# endif
#endif

#define GL_MAP_READ_BIT_EXT                  0x0001
#define GL_MAP_WRITE_BIT_EXT                 0x0002
#define GL_MAP_INVALIDATE_RANGE_BIT_EXT      0x0004
#define GL_MAP_INVALIDATE_BUFFER_BIT_EXT     0x0008
#define GL_MAP_FLUSH_EXPLICIT_BIT_EXT        0x0010
#define GL_MAP_UNSYNCHRONIZED_BIT_EXT        0x0020

#define GL_MAP_READ_BIT                  0x0001
#define GL_MAP_WRITE_BIT                 0x0002
#define GL_MAP_INVALIDATE_RANGE_BIT      0x0004
#define GL_MAP_INVALIDATE_BUFFER_BIT     0x0008
#define GL_MAP_FLUSH_EXPLICIT_BIT        0x0010
#define GL_MAP_UNSYNCHRONIZED_BIT        0x0020

#define glMapBufferRange			glMapBufferRangeEXT
#define glFlushMappedBufferRange	glFlushMappedBufferRangeEXT
#define glUnmapBuffer				glUnmapBufferOES
#define glTexImage3D				glTexImage3DOES
#define glTexSubImage3D				glTexSubImage3DOES
#define glGenVertexArrays(x, y)
#define glBindVertexArray(x)
#define glDrawBuffers(x, y)
#define glMapBuffer					glMapBufferOES
#define GL_WRITE_ONLY				GL_WRITE_ONLY_OES

#if !defined(GL_MAX)
#define GL_MAX						GL_FUNC_ADD
#endif
#if !defined(GL_MIN)
#define GL_MIN						GL_FUNC_ADD
#endif

#endif // __HAS_GLES__

//typedef void *(*PFNGLMAPBUFFERRANGEEXTPROC)(GLenum target, GLintptr offset, GLsizeiptr length, GLbitfield access);
//typedef void  (*PFNGLFLUSHMAPPEDBUFFERRANGEEXT)(GLenum target, GLintptr offset,	GLsizeiptr length);
//typedef GLboolean  (*PFNGLUNMAPBUFFEROESPROC)(GLenum target);

//extern PFNGLMAPBUFFERRANGEEXTPROC 		glMapBufferRangeEXT;
//extern PFNGLFLUSHMAPPEDBUFFERRANGEEXT 	glFlushMappedBufferRangeEXT;
//extern PFNGLUNMAPBUFFEROESPROC			glUnmapBufferOES;

//extern PFNGLTEXIMAGE3DOESPROC			glTexImage3DOES;
//extern PFNGLTEXSUBIMAGE3DOESPROC		glTexSubImage3DOES;

//extern PFNGLCOPYTEXSUBIMAGE3DOESPROC;
//extern PFNGLCOMPRESSEDTEXIMAGE3DOESPROC;
//extern PFNGLCOMPRESSEDTEXSUBIMAGE3DOESPROC;
//extern PFNGLFRAMEBUFFERTEXTURE3DOESPROC;

#endif

namespace qqq {
static const GLint QQQ_GL_VERTEX_FORMAT_TYPE[] = {
    GL_FLOAT,			// single
    GL_FLOAT,			// vec2
    GL_FLOAT,			// vec3
    GL_FLOAT,			// vec4
    GL_HALF_FLOAT,		// half
    GL_HALF_FLOAT,		// vec2h
    GL_HALF_FLOAT,		// vec3h
    GL_HALF_FLOAT,		// vec4h
    GL_UNSIGNED_BYTE,
    GL_SHORT,
    GL_SHORT,
    GL_SHORT,
    GL_SHORT,
    GL_BYTE

};

static const GLenum QQQ_GL_VERTEX_FORMAT_SIZE[] = {
    1,		2,		3,		4,
    1,		2,		3,		4,
    4, 		2, 		4, 		2,
    4, 		4
};

static const GLboolean QQQ_GL_VERTEX_FORMAT_NORM[] = {
    GL_FALSE, // vec
    GL_FALSE,
    GL_FALSE,
    GL_FALSE,
    GL_FALSE, // vech
    GL_FALSE,
    GL_FALSE,
    GL_FALSE,
    GL_TRUE,
    GL_FALSE,
    GL_FALSE, // short2, 4
    GL_TRUE,
    GL_TRUE, // short2, 4 norm
    GL_FALSE // byte4

};

static const GLenum QQQ_GL_INDEX_TYPE[] = {
    GL_UNSIGNED_BYTE, //E_TYPE_UNSIGNED_INT8,
    GL_UNSIGNED_SHORT, //E_TYPE_UNSIGNED_INT16,
    GL_UNSIGNED_INT, //E_TYPE_UNSIGNED_INT32,
    GL_NONE
};

static const GLenum QQQ_GL_PRIM[] = {
    GL_POINTS,
    GL_LINES,
    GL_LINE_STRIP,
    GL_TRIANGLES,
    GL_TRIANGLE_STRIP,
    GL_TRIANGLE_FAN
};

//static const GLenum QQQ_GL_TEXTURE_FORMAT[] = {
//
//};
//
//static const GLenum QQQ_GL_TEXTURE_TYPE[] = {
//
//};




}


#endif /* QQQ_GL_H_ */
