/**
 * @file egl_graphics_context.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 5, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */
#include <qqq.h>
#include <gfx/graphics_context.h>
#include <gfx/graphics_initializer.h>
#include <gfx/IGraphicsResource.h>
#define EGL_EGLEXT_PROTOTYPES 1
#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <gfx/plugins/opengl/qqq_gl.h>
#include <assert.h>
#include <core/debug_log.h>
#include <malloc.h>
#include <core/wait_condition.h>
#include <core/mutex.h>
#include <core/string_util.h>
#include <base/application.h>
#include <algorithm>

#define eglPrintConfigAttribI(dp, config, name) {\
  EGLint name##_VALUE = 0; \
  eglGetConfigAttrib(dpy, config, name, &name##_VALUE);\
  DEBUG_VALUE_AND_TYPE_OF(name##_VALUE); \
}

#if QQQ_EGL_API_OPENGL_ES_API
# define QQQ_EGL_API		EGL_OPENGL_ES_API
# define QQQ_EGL_BIT		EGL_OPENGL_ES2_BIT
#elif QQQ_EGL_API_OPENGL_API
# define QQQ_EGL_API		EGL_OPENGL_API
# define QQQ_EGL_BIT		EGL_OPENGL_BIT
#else
#endif

namespace qqq {

namespace impl {

static const char* eglErrorStrings[] = { "EGL_SUCCESS", "EGL_NOT_INITIALIZED",
        "EGL_BAD_ACCESS", "EGL_BAD_ALLOC", "EGL_BAD_ATTRIBUTE",
        "EGL_BAD_CONFIG", "EGL_BAD_CONTEXT", "EGL_BAD_CURRENT_SURFACE",
        "EGL_BAD_DISPLAY", "EGL_BAD_MATCH", "EGL_BAD_NATIVE_PIXMAP",
        "EGL_BAD_NATIVE_WINDOW", "EGL_BAD_PARAMETER", "EGL_BAD_SURFACE" };

QQQ_C_API EGLBoolean QQQ_CALL eglIsExtensionSupported(EGLDisplay dpy,
        const std::string& name) {
    std::vector<std::string> ext;
    std::string sext = eglQueryString(dpy, EGL_EXTENSIONS);
    string_util::split(sext, " ", &ext);
    return std::find(ext.begin(), ext.end(), name) == ext.end() ?
            EGL_FALSE : EGL_TRUE;
}

QQQ_C_API void QQQ_CALL eglPrintConfigInfo(EGLDisplay dpy, EGLConfig config) {
    DEBUG_METHOD();

    DEBUG_VALUE_AND_TYPE_OF(dpy);
    DEBUG_VALUE_AND_TYPE_OF(config);

    eglPrintConfigAttribI(mDisplay, config, EGL_RED_SIZE);
    eglPrintConfigAttribI(mDisplay, config, EGL_GREEN_SIZE);
    eglPrintConfigAttribI(mDisplay, config, EGL_BLUE_SIZE);
    eglPrintConfigAttribI(mDisplay, config, EGL_ALPHA_SIZE);
    eglPrintConfigAttribI(mDisplay, config, EGL_DEPTH_SIZE);
    eglPrintConfigAttribI(mDisplay, config, EGL_STENCIL_SIZE);
    eglPrintConfigAttribI(mDisplay, config, EGL_CONFIG_ID);
    eglPrintConfigAttribI(mDisplay, config, EGL_NATIVE_RENDERABLE);
    eglPrintConfigAttribI(mDisplay, config, EGL_CONFIG_CAVEAT);
    eglPrintConfigAttribI(mDisplay, config, EGL_NATIVE_VISUAL_ID);

    EGLint caveat = 0;
    eglGetConfigAttrib(dpy, config, EGL_CONFIG_CAVEAT, &caveat);

    if(caveat != EGL_NONE) {
        DEBUG_MESSAGE("CAVEAT_DETAIL=");
        if(caveat == EGL_SLOW_CONFIG) DEBUG_MESSAGE("EGL_SLOW_CONFIG ");
        if(caveat == EGL_NON_CONFORMANT_CONFIG) DEBUG_MESSAGE("EGL_NON_CONFORMANT_CONFIG ");

    }
}

#define PRINT_EGL_STRING(dpy, name) { \
  const char* name##_VALUE = eglQueryString(dpy, name);\
  if(name##_VALUE) { \
    DEBUG_VALUE_AND_TYPE_OF(name##_VALUE);\
  }\
}

class EGLGraphicsContext: public IGraphicsContext {
    EGLNativeDisplayType mNativeDisplay;
    EGLNativeWindowType mNativeSurface;
    EGLDisplay mDisplay;
    EGLConfig mConfig;
    EGLContext mContext;
    EGLSurface mWindow;
    EGLBoolean mIsVerbose;
    EGLint mVersionMajor, mVersionMinor;

    IMutex* mMutex;
    IWaitCondition* mWaitEvent;
    bool mIsLocked;
    bool mIsInitialized;
    int mWidth;
    int mHeight;
public:
    EGLGraphicsContext(EGLint bit) {
        mWindow = EGL_NO_SURFACE;
        mContext = EGL_NO_CONTEXT;
        mNativeSurface = (EGLNativeWindowType) NULL;
        mConfig = NULL;
        mIsVerbose = EGL_FALSE;
        mNativeDisplay = NULL;
        mDisplay = EGL_NO_DISPLAY;
        mVersionMinor = mVersionMajor = 0;
        mIsLocked = false;
        mIsInitialized = false;
        mWidth = 0;
        mHeight = 0;
        //eapi = bit == EGL_OPENGL_BIT ? EGL_OPENGL_API : EGL_OPENGL_ES_API;
        //rbit = bit == EGL_OPENGL_BIT ? EGL_OPENGL_BIT : EGL_OPENGL_ES2_BIT;

        qqqCreateMutex(&mMutex);
        qqqCreateWaitCondition(&mWaitEvent);

        mWaitEvent->set();
    }
    virtual ~EGLGraphicsContext() {

    }
    virtual bool create(void* display, void* surface,
            GraphicsInitializer* with) {
        DEBUG_METHOD();
        DEBUG_VALUE_AND_TYPE_OF(display);
        DEBUG_VALUE_AND_TYPE_OF(surface);

        if(mIsInitialized) {
            destroy();
        }

        if(!mIsInitialized) {

            //egl_graphics_context* this = this;

            EGLint attribs[] = {
                EGL_SURFACE_TYPE, EGL_WINDOW_BIT, /* may be changed later */
                EGL_RED_SIZE, SurfaceFormat::RedBits[with->pixelFormat()],
                EGL_GREEN_SIZE, SurfaceFormat::GreenBits[with->pixelFormat()],
                EGL_BLUE_SIZE, SurfaceFormat::BlueBits[with->pixelFormat()],
                EGL_ALPHA_SIZE, SurfaceFormat::AlphaBits[with->pixelFormat()],
                EGL_DEPTH_SIZE, with->depthBits(),
                EGL_STENCIL_SIZE, with->stencilBits(),

                EGL_RENDERABLE_TYPE, QQQ_EGL_BIT,
                //EGL_CONFORMANT, QQQ_EGL_BIT,

                EGL_NONE
            };

            DEBUG_MESSAGE("SurfaceFormat: [R:G:B:A:D:S] [%d:%d:%d:%d:%d:%d]",
                    SurfaceFormat::RedBits[with->pixelFormat()],
                    SurfaceFormat::GreenBits[with->pixelFormat()],
                    SurfaceFormat::BlueBits[with->pixelFormat()],
                    SurfaceFormat::AlphaBits[with->pixelFormat()],
                    with->depthBits(),
                    with->stencilBits());

            if (!this)
            return false;

            EGLNativeDisplayType xdpy = (EGLNativeDisplayType)display;
            EGLNativeWindowType xwin = (EGLNativeWindowType)surface;
            const char *ver;
            EGLint num_conf;
            EGLint err = EGL_SUCCESS;

            this->mNativeDisplay = xdpy;
            this->mNativeSurface = xwin;

            this->mDisplay = eglGetDisplay(this->mNativeDisplay);
            DEBUG_VALUE_AND_TYPE_OF(this->mDisplay);
            if (this->mDisplay == EGL_NO_DISPLAY) {
                DEBUG_MESSAGE("eglGetDisplay() failed");
                destroy();
                return false;
            }

            if (!eglInitialize(this->mDisplay, &this->mVersionMajor, &this->mVersionMinor)) {
                DEBUG_MESSAGE("eglInitialize() failed");
                destroy();
                return false;
            }

            eglBindAPI(QQQ_EGL_API);

            ver = eglQueryString(this->mDisplay, EGL_VERSION);
            DEBUG_MESSAGE("EGL_VERSION = %s", ver);

            eglChooseConfig(this->mDisplay, attribs, NULL, 0, &num_conf);
            EGLConfig* configs = new EGLConfig[num_conf];
            eglChooseConfig(this->mDisplay, attribs, configs, num_conf, &num_conf);

//        for(EGLint i=0; i < num_conf; i++) {
//            eglPrintConfigInfo(this->dpy, configs[i]);
//        }

            if (!eglChooseConfig(this->mDisplay, attribs, &this->mConfig, 1, &num_conf) ||
                    !num_conf) {
                DEBUG_MESSAGE("eglChooseConfig() failed");
                eglTerminate(this->mDisplay);
                destroy();
                return false;
            }
            DEBUG_MESSAGE("*** EGL: selected config:");
            eglPrintConfigInfo(this->mDisplay, this->mConfig);

            PRINT_EGL_STRING(this->mDisplay, EGL_VENDOR);
            PRINT_EGL_STRING(this->mDisplay, EGL_VERSION);
            PRINT_EGL_STRING(this->mDisplay, EGL_EXTENSIONS);
            PRINT_EGL_STRING(this->mDisplay, EGL_CLIENT_APIS);

            if(QQQ_EGL_BIT == EGL_OPENGL_BIT) {
                if(!eglIsExtensionSupported(mDisplay, "EGL_KHR_create_context")) {
                    DEBUG_MESSAGE("EGL_KHR_create_context ... not supported!");
                    static const EGLint core_attribs[] = {
                        EGL_CONTEXT_CLIENT_VERSION, 3,
                        EGL_NONE
                    };
                    this->mContext = eglCreateContext(this->mDisplay, this->mConfig, EGL_NO_CONTEXT, core_attribs);
                } else {
                    DEBUG_MESSAGE("EGL_KHR_create_context ... supported!");
#if (QQQ_EGL_API == EGL_OPENGL_API)
                    static const EGLint core_attribs[] = {
                        EGL_CONTEXT_OPENGL_PROFILE_MASK_KHR, EGL_CONTEXT_OPENGL_CORE_PROFILE_BIT_KHR,
                        EGL_CONTEXT_MAJOR_VERSION_KHR, 3,
                        EGL_CONTEXT_MINOR_VERSION_KHR, 2,
#ifdef _DEBUG
                        EGL_CONTEXT_FLAGS_KHR, EGL_CONTEXT_OPENGL_DEBUG_BIT_KHR,
#endif // _DEBUG
                        EGL_NONE
                    };
#else
                    static const EGLint core_attribs[] = {
                        EGL_CONTEXT_CLIENT_VERSION, 2,
                        EGL_NONE
                    };
#endif
                    this->mContext = eglCreateContext(this->mDisplay, this->mConfig, EGL_NO_CONTEXT, core_attribs);
                }

            } else {
#if (QQQ_EGL_API == EGL_OPENGL_ES_API)
                static const EGLint core_attribs[] = {
                    EGL_CONTEXT_CLIENT_VERSION, 2,
                    EGL_NONE
                };
#elif (QQQ_EGL_API == EGL_OPENVG_API)
                static const EGLint* core_attribs = NULL;
#elif (QQQ_EGL_API == EGL_OPENGL_API)
                static const EGLint core_attribs[] = {
                    //EGL_CONTEXT_CLIENT_VERSION, 2,
                    EGL_NONE
                };
#endif
                this->mContext = eglCreateContext(this->mDisplay, this->mConfig, EGL_NO_CONTEXT, core_attribs);
            }
            if (this->mContext == EGL_NO_CONTEXT) {
                DEBUG_MESSAGE("eglCreateContext() failed with: %s", eglErrorStrings[eglGetError() - 0x3000]);

                eglTerminate(this->mDisplay);
                assert(0);
                return false;
            }

#if defined (__ANDROID__)
            DEBUG_MESSAGE("*** Setting Android buffer geometry...");
            EGLint native_visual_id;
            if (!eglGetConfigAttrib(this->mDisplay, this->mConfig, EGL_NATIVE_VISUAL_ID, &native_visual_id)) {
                return false;
            }
            DEBUG_VALUE_AND_TYPE_OF(surface);
            DEBUG_VALUE_AND_TYPE_OF(native_visual_id);
            ANativeWindow_setBuffersGeometry((ANativeWindow*)surface, 0, 0, native_visual_id);
#endif

            this->mWindow = eglCreateWindowSurface(this->mDisplay, this->mConfig, this->mNativeSurface, NULL);
            if (this->mWindow == EGL_NO_SURFACE) {
                err = eglGetError();
                DEBUG_MESSAGE("eglCreateWindowSurface() failed with erro code 0x%x", err);
                return EGL_FALSE;
            }

            synchronized(this) {

#ifndef QQQ_HAVE_OPENGL_CORE
                //DEBUG_VALUE_AND_TYPE_OF(glMapBufferRangeEXT = (PFNGLMAPBUFFERRANGEEXTPROC)eglGetProcAddress("glMapBufferRangeEXT"));
                //glUnmapBufferOES	= (PFNGLUNMAPBUFFEROESPROC)eglGetProcAddress("glUnmapBufferOES");
                //DEBUG_VALUE_AND_TYPE_OF(glFlushMappedBufferRangeEXT = (PFNGLFLUSHMAPPEDBUFFERRANGEEXT)eglGetProcAddress("glFlushMappedBufferRangeEXT"));
                //glTexImage3DOES = (PFNGLTEXIMAGE3DOESPROC)eglGetProcAddress("glTexImage3DOES");
                //glTexSubImage3DOES = (PFNGLTEXSUBIMAGE3DOESPROC)eglGetProcAddress("glTexSubImage3DOES");
#endif
                if(eglSwapInterval(mDisplay, 0) != EGL_TRUE) {
                    DEBUG_METHOD();
                    DEBUG_VALUE_AND_TYPE_OF(eglGetError());
                    DEBUG_VALUE_AND_TYPE_OF(eglGetCurrentDisplay());
                    DEBUG_VALUE_AND_TYPE_OF(eglGetCurrentContext());
                    DEBUG_VALUE_AND_TYPE_OF(eglGetCurrentSurface(EGL_DRAW));
//					return false;
                }
                mIsInitialized = true;
                return true;
            }
            return false;
        }
        return mIsInitialized != false;

    }
    virtual void destroy() {
        //delete_graphics_context(this);
    }

    virtual int lock() {

        mMutex->lock();
        eglBindAPI(QQQ_EGL_API);
        EGLBoolean res = eglMakeCurrent(mDisplay, mWindow, mWindow, mContext);
        mIsLocked = true;
        return res != EGL_FALSE;
    }

    virtual int unlock() {

        mIsLocked = false;
        EGLBoolean res = eglMakeCurrent(mDisplay, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
        mMutex->unlock();

        return res != EGL_FALSE;
    }

    virtual bool swapBuffers() {

        EGLBoolean b = eglSwapBuffers(mDisplay, mWindow);
        if(EGL_TRUE != b) {
            EGLint err = eglGetError();
            if(EGL_CONTEXT_LOST == err) {
                GraphicsResourceEventArgs args;
                args.GraphicsContext = this;
                //args.ResourcePtr = NULL;
                OnContextLost(args);
            }
        }
        return EGL_TRUE == b;
    }


    void resize(const WindowResizeArgs& wh) {
        mWidth  = wh.width;
        mHeight = wh.height;
    }

    virtual const char* api() const {
        return QQQ_APISTRING_OPENGL;
    }

    virtual bool isPrimary() {
        return true;
    }

    virtual bool isValid() {
        return mContext != (EGLContext)EGL_NO_CONTEXT && mNativeSurface != (EGLNativeWindowType)EGL_NO_SURFACE;
    }

    virtual int getWidth() {
        return mWidth;
    }

    virtual int getHeight() {
        return mHeight;
    }
};

}
 // impl
//QQQ_C_API void QQQ_CALL new_egl_graphics_context(graphics_context** pContext) {
//
//}
//
//QQQ_C_API void QQQ_CALL new_egl_graphics_context_opengl(graphics_context** pContext) {
//
//}

#if defined(__LINUX__)
QQQ_C_API void QQQ_CALL hxCreateGraphicsContext(IGraphicsContext** ppctx) {
*ppctx = new impl::EGLGraphicsContext(EGL_OPENGL_BIT);
}
#elif defined(__ANDROID__)
QQQ_C_API void QQQ_CALL hxCreateGraphicsContext(IGraphicsContext** ppctx) {
*ppctx = new impl::EGLGraphicsContext(EGL_OPENGL_ES2_BIT);
}
#endif

}
 // qqq
