/**
 * @file glx_graphics_context.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 8, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include <qqq.h>
#include <gfx/plugins/opengl/qqq_gl.h>
#include <gfx/graphics_context.h>
#include <gfx/graphics_initializer.h>
#include <base/application.h>
#include <core/wait_condition.h>
#include <core/mutex.h>
#include <core/debug_log.h>
#include <X11/X.h>
#include <X11/Xlib.h>
#include <queue>
#include <cstdlib>

//extern PFNGLMAPBUFFERRANGEEXTPROC 		glMapBufferRangeEXT;
//extern PFNGLFLUSHMAPPEDBUFFERRANGEEXT 	glFlushMappedBufferRangeEXT;
//extern PFNGLUNMAPBUFFEROESPROC			glUnmapBufferOES;

#if defined(QQQ_HAVE_OPENGL_CORE)

#define CHECK_RES(x, y)	if((((y))) == (((x)))) { \
                            DEBUG_MESSAGE("GLX error: \"%s\"", "error"); \
                            exit(-__LINE__);\
                        } else { \
                            DEBUG_MESSAGE("%s ok", #x); \
                        }

namespace qqq {
namespace impl {

typedef GLXContext (*glXCreateContextAttribsARBProc)(Display*, GLXFBConfig,
        GLXContext, Bool, const int*);
typedef void (*PFNGLXSWAPINTERVALEXTPROC)(Display *dpy, GLXDrawable drawable,
        int interval);

class glx_graphics_context: public IGraphicsContext {

    GLXContext mContext;
    GLXDrawable mWindow;

    Display* dpy;

    int version_minor, version_major;

    IMutex* pmutex, *queue_mutex;
    IWaitCondition* event, *queue_event;
    bool is_locked;
    bool is_initialized;
    int mWidth;
    int mHeight;

    static GLXFBConfig getFBConfigFromVisual(::Display* dpy,
            XVisualInfo* visualInfo) {

        int screen = visualInfo->screen;
        int nelements;
        GLXFBConfig *configs = glXGetFBConfigs(dpy, screen, &nelements);
        for (int i = 0; i < nelements; i++) {
            int visual_id;
            if (glXGetFBConfigAttrib(dpy, configs[i], GLX_VISUAL_ID, &visual_id)
                    == 0) {
                if ((unsigned int) visual_id == visualInfo->visualid)
                    return configs[i];
            }
        }
        return NULL;
    }

public:
    glx_graphics_context() :
            mContext(None), mWindow(None), dpy(NULL), version_minor(0), version_major(0), is_locked(
                    false), is_initialized(false), mWidth(0), mHeight(0) {

    }
    virtual bool create(void* display, void* surface,
            GraphicsInitializer* with) {
        DEBUG_METHOD();
        if(is_initialized && display != NULL && surface != NULL ) {
            destroy();
        }
        if(!is_initialized) {
            dpy = (Display*)display;
            mWindow = (GLXDrawable)surface;

            glXCreateContextAttribsARBProc glXCreateContextAttribs = NULL;
            //PFNGLXSWAPINTERVALEXTPROC glXSwapInterval = NULL;

            static int attribList[] =
            {
                //GLX_RENDER_TYPE, GLX_RGBA_BIT,
                GLX_RGBA,
                //GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
                GLX_DOUBLEBUFFER, True,
                GLX_RED_SIZE, SurfaceFormat::RedBits[with->pixelFormat()],
                GLX_GREEN_SIZE, SurfaceFormat::GreenBits[with->pixelFormat()],
                GLX_BLUE_SIZE, SurfaceFormat::BlueBits[with->pixelFormat()],
                GLX_ALPHA_SIZE, SurfaceFormat::AlphaBits[with->pixelFormat()],
                GLX_DEPTH_SIZE, with->depthBits(),
                GLX_STENCIL_SIZE, with->stencilBits(),
                None
            };

            XWindowAttributes attributes;
            XGetWindowAttributes(dpy, (Window)surface, &attributes);

            int screen = XScreenNumberOfScreen(attributes.screen);

            XVisualInfo *visualInfo = glXChooseVisual(dpy, screen, (int*)attribList);

            GLXFBConfig fbc = getFBConfigFromVisual(dpy, visualInfo);
            CHECK_RES(fbc, NULL);

            XVisualInfo *vi = glXGetVisualFromFBConfig(dpy, fbc);

            // Create an old style context first, to get the correct function pointer for glXCreateContextAttribsARB
            GLXContext ctx_old = glXCreateContext(dpy, vi, 0, GL_TRUE);
            glXCreateContextAttribs = (glXCreateContextAttribsARBProc)glXGetProcAddress((const GLubyte*)"glXCreateContextAttribsARB");
            //glXSwapInterval = (PFNGLXSWAPINTERVALEXTPROC)glXGetProcAddress((const GLubyte*)"glXSwapIntervalEXT");

            CHECK_RES(glXMakeCurrent(dpy, 0, 0), False);
            glXDestroyContext(dpy, ctx_old);
            CHECK_RES(glXCreateContextAttribs, NULL);

            static int context_attribs[] =
            {
                GLX_CONTEXT_MAJOR_VERSION_ARB, 3,
                GLX_CONTEXT_MINOR_VERSION_ARB, 0,
                GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
#ifdef _DEBUG
                GLX_CONTEXT_FLAGS_ARB, GLX_CONTEXT_DEBUG_BIT_ARB,
#endif

                None
            };
            if(glXCreateContextAttribs) {
                mContext = glXCreateContextAttribs(dpy, fbc, NULL, true, context_attribs);
            }
            CHECK_RES(mContext, NULL);

            synchronized(this) {
//				glMapBufferRangeEXT = (PFNGLMAPBUFFERRANGEEXTPROC)glXGetProcAddress((const GLubyte*)"glMapBufferRangeEXT");
//				glUnmapBufferOES	= (PFNGLUNMAPBUFFEROESPROC)glXGetProcAddress((const GLubyte*)"glUnmapBufferOES");
//				glFlushMappedBufferRangeEXT = (PFNGLFLUSHMAPPEDBUFFERRANGEEXT)glXGetProcAddress((const GLubyte*)"glFlushMappedBufferRangeEXT");

//				unsigned int swap, maxSwap;
//				glXQueryDrawable(dpy, win, GLX_SWAP_INTERVAL_EXT, &swap);
//				glXQueryDrawable(dpy, win, GLX_MAX_SWAP_INTERVAL_EXT, &maxSwap);

                CHECK_RES(glXQueryVersion(dpy, &version_major, &version_minor), False);
                DEBUG_METHOD();
                DEBUG_VALUE_AND_TYPE_OF(version_major);
                DEBUG_VALUE_AND_TYPE_OF(version_minor);

            }
            is_initialized = true;
        }

        return true;
    }
    virtual void destroy() {
        is_initialized = false;
    }
    virtual bool swapBuffers() {
        glXSwapBuffers(dpy, mWindow);
        return true;
    }
    virtual bool lock() {
        //event->wait();
        Bool res = glXMakeContextCurrent(dpy, mWindow, mWindow, mContext);
        return res != False;
    }

    virtual bool unlock() {
        bool res = glXMakeContextCurrent(dpy, None, None, NULL) == True;

        //event->set();

        return res != false;
    }

    void draw_frame() {
        if(!dpy) {
            return;
        }

//		glViewport(0, 0, width, height);
    }

    void handle_command(command_t cmd) {
        switch(cmd) {

        }
    }

    void resize(const WindowResizeArgs& wh) {
        DEBUG_METHOD();
        mWidth = wh.width;
        mHeight = wh.height;
//    DEBUG_VALUE_AND_TYPE_OF(wh.width);
//    DEBUG_VALUE_AND_TYPE_OF(wh.height);
    }

    virtual bool isPrimary() {
        return true;
    }

    virtual bool isValid() {
        return this->mContext != (GLXContext)GLX_NONE && this->mWindow != (GLXDrawable)GLX_NONE;
    }

    virtual int getWidth() {
        return mWidth;
    }

    virtual int getHeight() {
        return mHeight;
    }

    const char* api() const {
        return QQQ_APISTRING_OPENGLES2;
    }
//
//	virtual void* get_native_handle() const {
//		return (void*)ctx;
//	}

    std::queue<command_t> command_queue;
};

}
 // impl

QQQ_C_API void QQQ_CALL hxCreateGraphicsContext(IGraphicsContext** ppContext) {
*ppContext = new impl::glx_graphics_context;
}

//QQQ_C_API void QQQ_CALL new_glx_graphics_context(graphics_context** ppContext) {
//  *ppContext = new impl::glx_graphics_context;
//}

//QQQ_C_API void delete_graphics_context(graphics_context*) {
//
//}

}// qqq

#endif
