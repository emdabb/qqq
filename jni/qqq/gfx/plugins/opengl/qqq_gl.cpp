/**
 * @file qqq_gl.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jul 10, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include "qqq_gl.h"
#include <gfx/pixel_format.h>

using namespace qqq;
#if 0
#ifndef QQQ_HAVE_OPENGL_CORE
PFNGLFLUSHMAPPEDBUFFERRANGEEXT 	glFlushMappedBufferRangeEXT = NULL;
PFNGLMAPBUFFERRANGEEXTPROC     	glMapBufferRangeEXT = NULL;
//PFNGLUNMAPBUFFEROESPROC			glUnmapBufferOES 	= NULL;
//PFNGLTEXIMAGE3DOESPROC			glTexImage3DOES		= NULL;
//PFNGLTEXSUBIMAGE3DOESPROC		glTexSubImage3DOES 	= NULL;
#endif
#endif
