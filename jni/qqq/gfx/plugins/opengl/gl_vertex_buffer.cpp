/**
 * @file gl_vertex_buffer.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 5, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include <gfx/vertex_buffer.h>
#include <string.h>
#include "qqq_gl.h"

#ifndef HAVE_OPENGL_CORE
//extern PFNGLMAPBUFFERRANGEEXTPROC 		glMapBufferRangeEXT;
//extern PFNGLFLUSHMAPPEDBUFFERRANGEEXT 	glFlushMappedBufferRangeEXT;
//extern PFNGLUNMAPBUFFEROESPROC			glUnmapBufferOES;
#endif
namespace qqq {
namespace impl {

class gl_vertex_buffer : public vertex_buffer_impl {
  GLuint handle;
  GLuint old;
  IBasicBuffer* mIn;
public:
  gl_vertex_buffer() : handle(0), old(0), mIn(NULL) { //, base(NULL) {
    glGenBuffers(1, &handle);
  }

  virtual ~gl_vertex_buffer() {
    glDeleteBuffers(1, &handle);
  }

  virtual void create(IBasicBuffer* in) {
    mIn = in;
    glBufferData(GL_ARRAY_BUFFER, mIn->size(), NULL, GL_DYNAMIC_DRAW);
  }

  virtual void set_data(const void* in, size_t beg, size_t end) {
	  size_t bufferSize = mIn->size();
#if QQQ_EGL_API_OPENGL_API
    GLubyte* ptr = (GLubyte*)glMapBufferRange(GL_ARRAY_BUFFER, beg, end - beg, GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);//GL_MAP_INVALIDATE_RANGE_BIT);

    if(ptr) {
      memcpy(&ptr[beg], in, end - beg);
      glUnmapBuffer(GL_ARRAY_BUFFER);
    }
#else
    /**
     * Orphan the buffer first.
     */

    glBufferData(GL_ARRAY_BUFFER, bufferSize, NULL, GL_DYNAMIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, beg, end-beg, in);
    //glBufferData(GL_ARRAY_BUFFER, end - beg, NULL, GL_DYNAMIC_DRAW);
    //GLubyte* ptr = (GLubyte*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
#endif
  }

  virtual void get_data(void* ptr, size_t beg, size_t end) {
#if (QQQ_EGL_API_OPENGL_API)
    void* in = glMapBufferRange(GL_ARRAY_BUFFER, beg, end - beg, GL_MAP_READ_BIT);
#else
    /**
     * Orphan the buffer first.
     */
    glBufferData(GL_ARRAY_BUFFER, end - beg, NULL, GL_DYNAMIC_DRAW);
    GLubyte* in = (GLubyte*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY_OES);
#endif
    if(ptr) {
      memcpy((GLubyte*)(&ptr)[beg], in, beg - end);
      glUnmapBuffer(GL_ARRAY_BUFFER);
    }
  }

  int lock() {
    glGetIntegerv(GL_ARRAY_BUFFER_BINDING, (GLint*)&old);
    glBindBuffer(GL_ARRAY_BUFFER, handle);
    return (int)handle;
  }

  int unlock() {
    glBindBuffer(GL_ARRAY_BUFFER, old);
    return (int)old;
  }
};

}

QQQ_C_API vertex_buffer_impl* QQQ_CALL  new_gl_vertex_buffer() {
  return new impl::gl_vertex_buffer;
}

}
