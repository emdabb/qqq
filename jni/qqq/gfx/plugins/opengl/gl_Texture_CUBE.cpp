/*
 * gl_texture_cube.cpp
 *
 *  Created on: Jul 15, 2014
 *      Author: miel
 */

#include "qqq_gl.h"
#include <gfx/TextureCUBE.h>

namespace qqq {
static const GLenum gl_cube_face[] = {
    GL_TEXTURE_CUBE_MAP_POSITIVE_X,
    GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
    GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
    GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
    GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
    GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
};

class gl_texture_cube_impl : public texture_cube_impl {
  GLuint handle;
public:

};

};

