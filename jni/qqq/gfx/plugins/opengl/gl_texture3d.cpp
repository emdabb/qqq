/**
 * @file gl_texture3d.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jul 10, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include <gfx/texture3d.h>
#include <gfx/plugins/opengl/qqq_gl.h>

namespace qqq {
namespace impl {
class gl_texture3d : public texture3d_impl {
  GLuint handle, old;
  GLenum format, type, iformat;
public:
  gl_texture3d() : handle(0), old(0), format(0), type(0), iformat(0) {
    glGenTextures(1, &handle);
  }

  virtual ~gl_texture3d() {
    glDeleteTextures(1, &handle);
  }

  virtual void create(texture3d* base) {
//    format = QQQ_GL_TEXTURE_FORMAT[base->format()];
//    type   = QQQ_GL_TEXTURE_TYPE[base->format()];
#if defined(__IOS__)
#else
    glTexImage3D(GL_TEXTURE_3D,
        0,
        iformat,
        base->width(),
        base->height(),
        base->depth(),
        0,
        format,
        type,
        (const GLvoid*)NULL);
#endif
  }

  virtual void set_data(const void* src, int mip, size_t x, size_t y, size_t z, size_t size_x, size_t size_y, size_t size_z) {
#if defined(__IOS__)
	  return false;
#else
    glTexSubImage3D(
        GL_TEXTURE_3D,
        mip,
        x, y, z,
        size_x,
        size_y,
        size_z,
        format,
        type,
        (const GLvoid*)src);
#endif
  }

  int lock() {
#if defined(__IOS__)
	  return false;
#else
    glGetIntegerv(GL_TEXTURE_BINDING_3D, (GLint*)&old);
    glBindTexture(GL_TEXTURE_3D, handle);
    return (int)handle;


#endif

  }

  int unlock() {
#if defined(__IOS__)
	  return false;
#else
    glBindTexture(GL_TEXTURE_3D, old);
    return (int)old;
#endif
  }
};
}
QQQ_C_API texture3d_impl* QQQ_CALL new_gl_texture3d() {
  return new impl::gl_texture3d;
}
}


