/**
 * @file gl_index_buffer.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Sep 2, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include <gfx/index_buffer.h>
#include "qqq_gl.h"

namespace qqq {
namespace impl {

struct gl_index_buffer : public index_buffer_impl {
  GLuint handle;
  GLuint old;
  gl_index_buffer() : handle(0), old(0) {
    glGenBuffers(1, &handle);
  }
  virtual ~gl_index_buffer() {
    glDeleteBuffers(1, &handle);
  }
  virtual void create(IBasicBuffer* in) {
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, in->size(), NULL, GL_DYNAMIC_DRAW);
  }
  virtual void set_data(const void* in, size_t beg, size_t end) {
#if QQQ_HAVE_OPENGL_CORE
    GLubyte* ptr = (GLubyte*)glMapBufferRange(GL_ELEMENT_ARRAY_BUFFER, beg, end - beg, GL_MAP_WRITE_BIT);
#else
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, end - beg, NULL, GL_DYNAMIC_DRAW);
    GLubyte* ptr = (GLubyte*)glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY);
#endif
    if(ptr) {
      memcpy(&ptr[beg], in, end - beg);
      glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
    }
  }

  virtual void get_data(void* ptr, size_t beg, size_t end) {
#if QQQ_HAVE_OPENGL_CORE
    //void* in = glMapBufferRange(GL_ARRAY_BUFFER, beg, end - beg, GL_MAP_READ_BIT);
#else
//    glBufferData(GL_ARRAY_BUFFER, end - beg, NULL, GL_DYNAMIC_DRAW);
//    GLubyte* in = (GLubyte*)glMapBuffer(GL_ARRAY_BUFFER, GL_READ_ONLY);
#endif
//    if(ptr) {
//    memcpy((GLubyte*)(&ptr)[beg], in, beg - end);
//    glUnmapBuffer(GL_ARRAY_BUFFER);
//  }
}

  int lock() {
      glGetIntegerv(GL_ELEMENT_ARRAY_BUFFER_BINDING, (GLint*)&old);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle);
      return (int)handle;
  }
  int unlock() {
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, old);
      return (int)old;
  }

};

}

QQQ_C_API index_buffer_impl* QQQ_CALL  new_gl_index_buffer() {
  return new impl::gl_index_buffer;
}

}




