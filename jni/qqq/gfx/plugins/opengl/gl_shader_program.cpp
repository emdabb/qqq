/**
 * @file gl_shader_program.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Aug 3, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include <gfx/shader.h>
#include <gfx/sampler2d.h>
#include <gfx/texture2d.h>
#include <gfx/texture3d.h>
#include <gfx/TextureCUBE.h>
#include <gfx/vertex_stream.h>
#include <core/debug_log.h>
#include <core/matrix.h>
#include <core/vector2.h>
#include <core/vector3.h>
#include <core/vector4.h>
#include <cstring>
#include <cassert>
#include "qqq_gl.h"
#include <qqq_except.h>
//#include <python2.7/Python.h>

namespace qqq {
namespace impl {

template <typename T, size_t N>
struct gl_uniformT : public ShaderParameter<T> {
    GLuint mHandle;

    gl_uniformT(ShaderProgram* sh, const std::string& name, int loc, size_t count)
    : ShaderParameter<T>(sh, name, loc, count)
    {
        mHandle = (GLuint)loc;

    }

    void validate() {
        throw UnimplementedMethodException();
    }
};

typedef gl_uniformT<float, 1> uniform1f;
typedef gl_uniformT<float, 2> uniform2f;
typedef gl_uniformT<float, 3> uniform3f;
typedef gl_uniformT<float, 4> uniform4f;

typedef gl_uniformT<Sampler2D, 1> uniform_sampler2d;
typedef gl_uniformT<Matrix, 1> uniform_mat4;
typedef gl_uniformT<Vector2, 1> uniform_vec2;
typedef gl_uniformT<Vector3, 1> uniform_vec3;
typedef gl_uniformT<Vector4, 1> uniform_vec4;

//typedef gl_uniformT<vector4> uniform_vec4;
//
//template <typename T>
//struct gl_uniform_arrayT : public uniform {
//    GLuint mHandle;
//    size_t mSize;
//    T* 		mValue;
//
//    gl_uniform_arrayT(shader* sh, const std::string& name, int loc, size_t size)
//    : uniform(sh, name, loc)
//    , mHandle(loc)
//    , mSize(size)
//    {
//        mValue = new T[mSize];
//    }
//
//    virtual ~gl_uniform_arrayT() {
//        delete[] mValue;
//        mValue = NULL;
//    }
//
//    void validate() {
//        throw unimplemented_method_exception();
//    }
//};
//
//typedef gl_uniform_arrayT<float> gl_uniform_arrayf;
//typedef gl_uniform_arrayT<vector2> gl_uniform_array_vec2;
//
//template <>
//void gl_uniform_arrayf::validate() {
//    glUniform1fv(mHandle, mSize, reinterpret_cast<const GLfloat*>(&this->mValue[0]));
//}
//
//template <>
//void gl_uniform_array_vec2::validate() {
//    glUniform2fv(mHandle, mSize, reinterpret_cast<const GLfloat*>(&this->mValue[0]));
//}

#include <gfx/texture.h>

const static GLenum GL_TEX_FILTER[][2] = {
	{ GL_LINEAR,  GL_LINEAR }, 					//Linear,						//	Use linear filtering.
	{ GL_NEAREST, GL_NEAREST },					//Point,						//	Use point filtering.
	{ GL_LINEAR,  GL_LINEAR },					//Anisotropic,					//	Use anisotropic filtering.
	{ GL_LINEAR,  GL_LINEAR_MIPMAP_NEAREST },	//LinearMipPoint,				//	Use linear filtering to shrink or expand, and point filtering between mipmap levels (mip).
	{ GL_NEAREST, GL_NEAREST_MIPMAP_LINEAR  },	//PointMipLinear,				//	Use point filtering to shrink (minify) or expand (magnify), and linear filtering between mipmap levels.
	{ GL_LINEAR,  GL_NEAREST_MIPMAP_LINEAR },	//MinLinearMagPointMipLinear,	//	Use linear filtering to shrink, point filtering to expand, and linear filtering between mipmap levels.
	{ GL_LINEAR,  GL_NEAREST_MIPMAP_NEAREST },	//MinLinearMagPointMipPoint,	//	Use linear filtering to shrink, point filtering to expand, and point filtering between mipmap levels.
	{ GL_NEAREST, GL_LINEAR_MIPMAP_LINEAR },	//MinPointMagLinearMipLinear,	//	Use point filtering to shrink, linear filtering to expand, and linear filtering between mipmap levels.
	{ GL_NEAREST, GL_LINEAR_MIPMAP_NEAREST }	//MinPointMagLinearMipPoint
};


template <>
void uniform_sampler2d::validate() {
    for(size_t i=0; i < mCount; i++) {
        if(mValue[i].TextureRef) {
#if 1
        	glActiveTexture(GL_TEXTURE0 + mValue[i].SamplerID);
        	GLuint texId = (GLuint)mValue[i].TextureRef->getImpl().lock();
        	glBindTexture(GL_TEXTURE_2D, texId);
        	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_TEX_FILTER[mValue[i].Filter][0]);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_TEX_FILTER[mValue[i].Filter][1]);
			//glBindTexture(GL_TEXTURE_2D, 0);



            glUniform1i(mHandle, mValue[i].SamplerID);
#else
        	glActiveTexture(GL_TEXTURE0 + mValue[i].SamplerID);
			mValue[i].TextureRef->getImpl().lock();
			glUniform1i(mHandle, mValue[i].SamplerID);
#endif
        } else {
            glBindTexture(GL_TEXTURE_2D, 0);
        }
    }
}

template <>
void uniform_vec2::validate() {
    glUniform2fv(mHandle, mCount, (const GLfloat*)&mValue[0]);
}

template <>
void uniform_vec3::validate() {
    glUniform3fv(mHandle, mCount, (const GLfloat*)&mValue[0]);
}

template <>
void uniform_vec4::validate() {
    glUniform4fv(mHandle, mCount, (const GLfloat*)&mValue[0]);
}

template <>
void uniform1f::validate() {
    glUniform1fv(mHandle, mCount, &mValue[0]);
}

template <>
void uniform2f::validate() {
    glUniform2fv(mHandle, mCount, &mValue[0]);
}

template <>
void uniform3f::validate() {
    glUniform3fv(mHandle, mCount, &mValue[0]);
}

template <>
void uniform4f::validate() {
    glUniform4fv(mHandle, mCount, &mValue[0]);
}

//template <>
//void uniform_vec4::validate() {
//    glUniform4f(mHandle, mValue[0].x, mValue[0].y, mValue[0].z, mValue[0].w);
//}

template<>
void uniform_mat4::validate() {
    Matrix T;
    Matrix::transpose(mValue[0], &T);
    glUniformMatrix4fv(mHandle, mCount, GL_FALSE, (GLfloat*)&T);
}


struct gl_shader : public IShaderProgram {
    GLuint mHandle;
    GLuint mVS, mPS;

    gl_shader() : mHandle(0), mVS(0), mPS(0) {
        mHandle = glCreateProgram();
        mVS = glCreateShader(GL_VERTEX_SHADER);
        mPS = glCreateShader(GL_FRAGMENT_SHADER);
        assert(mHandle && mVS && mPS);
    }

    virtual ~gl_shader() {
        glDetachShader(mHandle, mVS);
        glDeleteShader(mVS);
        glDetachShader(mHandle, mPS);
        glDeleteShader(mPS);
        glDeleteProgram(mHandle);
    }
    virtual void cacheUniforms(ShaderProgram* sh) {
        DEBUG_METHOD();
        int count;
        glGetProgramiv( mHandle, GL_ACTIVE_UNIFORMS, &count);
        static const int BUFF_SIZE = 1024;

        for (int i=0; i < count; ++i)
        {
            char name[BUFF_SIZE]; // for holding the variable name
            GLint size = BUFF_SIZE;
            GLenum type;
            GLsizei length;
            GLsizei bufSize=BUFF_SIZE;
            glGetActiveUniform( mHandle, i, bufSize, &length, &size, &type, name );
            int location = glGetUniformLocation(mHandle, name);

            //glGetUniformfv(mHandle, location, params);

            //DEBUG_MESSAGE("uniform %s @ 0x%x", name, location);
            DEBUG_VALUE_AND_TYPE_OF(name);
            DEBUG_VALUE_AND_TYPE_OF(type);
            DEBUG_VALUE_AND_TYPE_OF(size);
            DEBUG_VALUE_AND_TYPE_OF(location);

            ShaderParameterBase* pUniform = NULL;


            switch(type) {
                case GL_FLOAT:
                    pUniform = new gl_uniformT<float, 1>(sh, name, location, size);
                    break;
                case GL_FLOAT_VEC2:
                    pUniform = new gl_uniformT<Vector2, 1>(sh, name, location, size);
                    break;
                case GL_FLOAT_VEC3:
                    pUniform = new gl_uniformT<Vector3, 1>(sh, name, location, size);
                    break;
                case GL_FLOAT_VEC4:
                    pUniform = new gl_uniformT<Vector4, 1>(sh, name, location, size);
                    break;
                case GL_INT:
                    pUniform = new gl_uniformT<int,1>(sh, name, location, size);
                    break;
                case GL_INT_VEC2:
                    pUniform = new gl_uniformT<int,2>(sh, name, location, size * 2);
                    break;
                case GL_INT_VEC3:
                    pUniform = new gl_uniformT<int,3>(sh, name, location, size * 3);
                    break;
                case GL_INT_VEC4:
                    pUniform = new gl_uniformT<int,4>(sh, name, location, size * 4);
                    break;
                case GL_BOOL:
                    pUniform = new gl_uniformT<bool,1>(sh, name, location, size);
                    break;
                case GL_BOOL_VEC2:
                    pUniform = new gl_uniformT<bool,2>(sh, name, location, size * 2);
                    break;
                case GL_BOOL_VEC3:
                    pUniform = new gl_uniformT<bool,3>(sh, name, location, size * 3);
                    break;
                case GL_BOOL_VEC4:
                    pUniform = new gl_uniformT<bool,4>(sh, name, location, size * 4);
                    break;
                case GL_FLOAT_MAT2:
                    pUniform = new gl_uniformT<float,1>(sh, name, location, size * 4);
                    break;
                case GL_FLOAT_MAT3:
                    pUniform = new gl_uniformT<float,1>(sh, name, location, size * 9);
                    break;
                case GL_FLOAT_MAT4:
                    pUniform = new gl_uniformT<Matrix,1>(sh, name, location, size);
                    break;
                case GL_SAMPLER_2D:
                    pUniform = new gl_uniformT<Sampler2D,1>(sh, name, location, size);
                    break;
    #if 0
                case GL_SAMPLER_3D:
                    pUniform = new gl_uniformT<texture3d, 1>(sh, name, location, size);
                    break;
    #endif
                case GL_SAMPLER_CUBE:
                    pUniform = new gl_uniformT<TextureCUBE*,1>(sh, name, location, size);
                    break;
                default:
                    break;
                }

            assert(pUniform);
            sh->add_uniform(pUniform, location);
        }
    }

    void update_uniforms(ShaderProgram* sh) {
        glUseProgram(mHandle);
        for(unsigned int i=0; i < sh->num_uniforms(); i++) {
            ShaderParameterBase* pUniform = sh->get_uniform(i);
            if(pUniform->is_valid()){
                pUniform->validate();
            }
        }
    }

    bool compile(const char* src) {
        //
        //TODO: live conversion of Cg to GLSL shaders. This is ranked as a *HIGH* priority.
        //
#if 0
        wchar_t* argv = NULL;
        Py_SetProgramName(argv);
        Py_Initialize();
        int res = PyRun_SimpleString(	"from time import time,ctime\n"
                            "print('Today is', ctime(time()))\n");
        Py_Finalize();
#endif
        const char* pSourceVS[3]= {
                "#undef 	FRAGMENT	 \n",
                "#define 	VERTEX		1\n",
                src
        };

        glShaderSource(mVS, 3, pSourceVS, NULL);
        compile_shader(mVS);

        const char* pSourcePS[3] = {
                "#define	FRAGMENT	1\n",
                "#undef 	VERTEX		 \n",
                src
        };

        glShaderSource(mPS, 3, pSourcePS, NULL);
        compile_shader(mPS);
        return false;
    }

    bool link() {
        DEBUG_METHOD();

        glAttachShader(mHandle, mVS);
        glAttachShader(mHandle, mPS);
        static const char* SEMANTIC[] = {
                "VertexCoord",
                "BlendWeight",
                "Normal",
                "Color0",
                "Color1",
                "FogCoord",
                "PointSize",
                "BlendIndex0",
                "TexCoord0",
                "TexCoord1",
                "TexCoord2",
                "TexCoord3",
                "TexCoord4",
                "TexCoord5",
                "TexCoord6",
                "TexCoord7"
        };

        for(int i=0; i < stream_id::E_STREAM_MAX; i++) {
            const char* str = SEMANTIC[i];
            glBindAttribLocation(mHandle, i, str);
        }

        glLinkProgram(mHandle);
        GLint success = GL_FALSE;
        glGetProgramiv(mHandle, GL_LINK_STATUS, &success);
        if(success == GL_FALSE)
        {
            GLchar errorLog[1024] = {0};
            glGetProgramInfoLog(mHandle, 1024, NULL, errorLog);
            DEBUG_MESSAGE("error linking program: %s", errorLog);
            return false;
        }
        glValidateProgram(mHandle);
        success = GL_FALSE;
        glValidateProgram(mHandle);
        glGetProgramiv(mHandle, GL_VALIDATE_STATUS, &success);
        if(success == GL_FALSE)
        {
            GLchar errorLog[1024] = {0};
            glGetProgramInfoLog(mHandle, 1024, NULL, errorLog);
            DEBUG_MESSAGE("error validating program: %s", errorLog);
            return false;
        }
        return true;
    }

    bool compile_shader(const GLuint sh) {
        DEBUG_METHOD();
        glCompileShader(sh);
        GLint success;
        glGetShaderiv(mVS, GL_COMPILE_STATUS, &success);
        if(!success)
        {
            GLchar error[1024];
            glGetShaderInfoLog(sh, 1024, NULL, error);
            DEBUG_MESSAGE("error compiling shader: %s", error);
        }
        return success > 0;
    }

    void enable()
    {
        glUseProgram(mHandle);
    }

    virtual void disable() {
        glUseProgram(0);
    }


};

}

QQQ_C_API void QQQ_CALL create_shader(IShaderProgram** pprog) {
  assert(!*pprog);
  *pprog = new impl::gl_shader();
}

QQQ_C_API IShaderProgram* QQQ_CALL new_gl_shader_program() {
  IShaderProgram* pimpl = NULL;
  create_shader(&pimpl);
  return pimpl;
}

}



