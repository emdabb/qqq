/*
 * gl_texture2d.cpp
 *
 *  Created on: Jun 26, 2014
 *      Author: miel
 */

#include <gfx/texture2d.h>

#include <assert.h>
#include "qqq_gl.h"

namespace qqq {
namespace impl {

const GLenum QQQ_GL_SURFACE_FORMAT[] = {
GL_RGB,
GL_BGR,
GL_RGBA,
GL_BGRA,
GL_RGB,
GL_BGR,
GL_RGB,
GL_BGR,
GL_RED,
GL_RED,
GL_RGB,
GL_RED,
GL_RGB,
GL_RGBA,
GL_RGBA,
GL_RGB,
GL_RG,
GL_RED,
GL_RGBA,
GL_RGB,
GL_RG,
GL_RED };

const GLint QQQ_GL_PIXEL_STORE[] = { 4, 4, 4, 4, 4, 4, 4, 4, 1, 1, 4, 1, 4, 4,
		4, 4, 2, 1, 4, 4, 2, 1 };

const GLenum QQQ_GL_SURFACE_IFORMAT[] = {
GL_RGB,
GL_BGR,
GL_RGBA,
GL_BGRA,
GL_RGB,
GL_BGR,
GL_RGB,
GL_BGR,
GL_RED,
GL_RED,
GL_RGB,
GL_RED,
GL_RGB,
GL_RGBA,
GL_RGBA,
GL_RGB,
GL_RG,
GL_RED,
GL_RGBA,
GL_RGB,
GL_RG,
GL_RED };

const GLenum QQQ_GL_SURFACE_TYPE[] = {
GL_UNSIGNED_BYTE,
GL_UNSIGNED_BYTE,
GL_UNSIGNED_BYTE,
GL_UNSIGNED_BYTE,
GL_UNSIGNED_SHORT_5_6_5,
GL_UNSIGNED_SHORT_5_6_5,
GL_UNSIGNED_SHORT_5_5_5_1,
GL_UNSIGNED_SHORT_5_5_5_1,
GL_UNSIGNED_BYTE,
GL_HALF_FLOAT,
GL_HALF_FLOAT,
GL_FLOAT,
GL_FLOAT,
GL_UNSIGNED_BYTE,
GL_FLOAT,
GL_FLOAT,
GL_FLOAT,
GL_FLOAT,
GL_HALF_FLOAT,
GL_HALF_FLOAT,
GL_HALF_FLOAT,
GL_HALF_FLOAT };

static void glConvertSurfaceFormat(const int sf, GLint* fmt, GLint* ifmt,
		GLenum* type) {
	switch (sf) {
	case SurfaceFormat::Color:
		*fmt = GL_RGBA;
		*ifmt = GL_RGBA;
		*type = GL_UNSIGNED_BYTE;
		break;
	case SurfaceFormat::Bgr565:
		*fmt = GL_BGR;
		*ifmt = GL_BGR;
		*type = GL_UNSIGNED_SHORT_5_6_5;
		break;
	case SurfaceFormat::Bgra5551:
		*fmt = GL_RGBA;
		*ifmt = GL_RGBA;
		*type = GL_UNSIGNED_SHORT_5_5_5_1;
		break;
	case SurfaceFormat::Bgra4444:
		*fmt = GL_BGRA;
		*ifmt = GL_BGRA;
		*type = GL_UNSIGNED_SHORT_4_4_4_4;
		break;
	case SurfaceFormat::NormalizedByte2:
		*fmt = GL_RG;
		*ifmt = GL_RG;
		*type = GL_UNSIGNED_BYTE;
		break;
	case SurfaceFormat::NormalizedByte4:
		*fmt = GL_BGRA;
		*ifmt = GL_BGRA;
		*type = GL_UNSIGNED_BYTE;
		break;
	case SurfaceFormat::Rgba1010102:
		*fmt = GL_RGBA;
		*ifmt = GL_RGBA;
		*type = GL_UNSIGNED_INT_10_10_10_2;
		break;
	case SurfaceFormat::Rg32:
		*fmt = GL_RG;
		*ifmt = GL_RG;
		*type = GL_UNSIGNED_SHORT;
		break;
	case SurfaceFormat::Rgba64:
		*fmt = GL_RGBA;
		*ifmt = GL_RGBA;
		*type = GL_UNSIGNED_SHORT;
		break;
	case SurfaceFormat::Alpha8:
		*fmt = GL_ALPHA;
		*ifmt = GL_ALPHA;
		*type = GL_UNSIGNED_BYTE;
		break;
	case SurfaceFormat::Single:
		*fmt = GL_RED;
		*ifmt = GL_RED; //R32F_EXT;
		*type = GL_FLOAT;
		break;
	case SurfaceFormat::Vector2:
		*fmt = GL_RG;
		*ifmt = GL_RG;
		*type = GL_FLOAT;
		break;
	case SurfaceFormat::Vector4:
		*fmt = GL_RGBA;
		*ifmt = GL_RGBA;
		*type = GL_FLOAT;
		break;
	case SurfaceFormat::HalfSingle:
		//*fmt  = GL_BGRA;
		*fmt = GL_RED;
		*ifmt = GL_RED;
		*type = GL_HALF_FLOAT;
		break;
	case SurfaceFormat::HalfVector2:
		*fmt = GL_RG;
		*ifmt = GL_RG;
		*type = GL_HALF_FLOAT;
		break;
	case SurfaceFormat::HalfVector4:
		*fmt = GL_RGBA;
		*ifmt = GL_RGBA;
		*type = GL_HALF_FLOAT;
		break;
	case SurfaceFormat::HdrBlendable:
		*fmt = GL_RGBA;
		*ifmt = GL_RGBA;
		*type = GL_FLOAT;
		break;

	}
}

class gl_texture2d: public texture2d_impl {
	GLuint handle;
	GLuint old;
	GLint format, internalFormat;
	GLenum type;
	int nlevels;
public:
	gl_texture2d() :
			handle(0), old(0), format(0), internalFormat(0), type(0), nlevels(0) {
		glGenTextures(1, &handle);
		assert(handle);
	}

	virtual ~gl_texture2d() {
		glDeleteTextures(1, &handle);
	}

	void create_mip_levels(int level, size_t w, size_t h, const void* data) {
		//
		// TODO: introduce filtering (2x2 box)
		//
		nlevels++;
		glTexImage2D( GL_TEXTURE_2D, level, internalFormat, w, h, 0, format,
				type, data);
		if (w > 2 && h > 2) {
			create_mip_levels(level + 1, w >> 1, h >> 1, data);
		}
	}

	void create(size_t w, size_t h, const int sf, bool has_levels) {
		nlevels = (int) has_levels;
		glConvertSurfaceFormat(sf, &format, &internalFormat, &type);
		glPixelStorei(GL_PACK_ALIGNMENT, QQQ_GL_PIXEL_STORE[sf]);
		glTexImage2D( GL_TEXTURE_2D, 	// target
				0, 				// level
				internalFormat, w, h, 0, 				// border
				format, type,
				NULL 			// data
				);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		if (!has_levels) {
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		}

		else {
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
					GL_NEAREST_MIPMAP_NEAREST);
			//create_mip_levels(1, w >> 1, h >> 1, NULL);
			glGenerateMipmap(GL_TEXTURE_2D);
		}

	}

	virtual void set_data(const void* data, int level, size_t offx, size_t offy,
			size_t sizex, size_t sizey) {

		glTexSubImage2D(
		GL_TEXTURE_2D, level,	// level
				offx,	// offset x
				offy,	// offset y
				sizex, sizey, format, type, data);
		if (this->nlevels > 0) {
			glGenerateMipmap(GL_TEXTURE_2D);
		}
	}

	virtual int num_levels() const {
		return nlevels;
	}

	virtual int lock() {
		glGetIntegerv(GL_TEXTURE_BINDING_2D, (GLint*) &old);
		glBindTexture(GL_TEXTURE_2D, handle);
		return (int)handle;
	}

	virtual int unlock() {
		glBindTexture(GL_TEXTURE_2D, old);
		return (int)old;
	}
};

}
QQQ_C_API texture2d_impl* QQQ_CALL new_gl_texture2d() {
	return new impl::gl_texture2d;
}
}

