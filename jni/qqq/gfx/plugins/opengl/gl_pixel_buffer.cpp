/*
 * gl_pixel_buffer.cpp
 *
 *  Created on: Jun 28, 2014
 *      Author: miel
 */

#include <gfx/pixel_buffer.h>
#include <gfx/graphics_device.h>
#include <gfx/texture2d.h>
#include "qqq_gl.h"
#include <assert.h>
#include <string.h>

namespace qqq {
namespace impl {

class gl_pixel_buffer : public pixel_buffer_impl {
  enum {
    E_BUF_MAX = 1
  };
  GLuint handle;
  GLuint old;

  Texture2D* target;
public:
  gl_pixel_buffer() : old(0), target(NULL) {
    glGenBuffers(E_BUF_MAX, &handle);
  }

  ~gl_pixel_buffer() {
    glDeleteBuffers(E_BUF_MAX, &handle);
  }

  virtual void create(IBasicBuffer* buf) {
#if defined(HAVE_OPENGL_CORE)
    glBufferData(GL_PIXEL_UNPACK_BUFFER, size, NULL, GL_STREAM_DRAW);
#endif
  }

  virtual void set_data(const void* src, size_t beg, size_t end) {

    assert(target);
    target->getImpl().lock();
    synchronized(this) {
      target->setData<uint8_t>(NULL, target->getWidth(), target->getHeight());
#if defined(HAVE_OPENGL_CORE)
      glBufferData(GL_PIXEL_UNPACK_BUFFER, end - beg, NULL, GL_STREAM_DRAW);
      GLubyte* dst = (GLubyte*)glMapBuffer(GL_PIXEL_UNPACK_BUFFER, GL_WRITE_ONLY);
      if(dst) {
        memcpy(&dst[beg], src, end - beg);
        glUnmapBuffer(GL_PIXEL_UNPACK_BUFFER);
      }
#endif
    }
  }

  virtual void get_data(void* dst, size_t beg, size_t end) {
#if defined(HAVE_OPENGL_CORE)
    GLubyte* src = (GLubyte*)glMapBuffer(GL_PIXEL_UNPACK_BUFFER, handle);
    if(src) {
      memcpy((GLubyte*)(&dst)[beg], src, beg - end);
    }
#endif
  }

  int lock() {
#if defined(HAVE_OPENGL_CORE)
    glGetIntegerv(GL_PIXEL_UNPACK_BUFFER_BINDING, (GLint*)&old);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, handle);
#endif
    return true;
  }

  int unlock() {
#if defined(HAVE_OPENGL_CORE)
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, old);
#endif
    return true;
  }
};

}

QQQ_C_API pixel_buffer_impl* QQQ_CALL new_gl_pixel_buffer() {
  return new impl::gl_pixel_buffer;
}

}


