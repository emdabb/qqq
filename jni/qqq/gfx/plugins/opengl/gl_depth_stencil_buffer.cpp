/**
 * @file gl_depth_stencil_buffer.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jul 7, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include <gfx/depth_stencil_buffer.h>
#include <gfx/plugins/opengl/qqq_gl.h>

namespace qqq {

void glConvertDepthFormat(const int in, GLenum* out, GLenum* att) {
  switch(in) {
  case depth_format::E_DEPTH16:
    *out = GL_DEPTH_COMPONENT16;
    *att = GL_DEPTH_ATTACHMENT;
    break;
  case depth_format::E_DEPTH24:
    *out = GL_DEPTH_COMPONENT24;
    *att = GL_DEPTH_ATTACHMENT;
    break;

  case depth_format::E_DEPTH24_STENCIL8:
    *out = GL_DEPTH24_STENCIL8;
    *att = GL_DEPTH_STENCIL_ATTACHMENT;
    break;
  case depth_format::E_DEPTH32:
    *out = GL_DEPTH_COMPONENT32;
    *att = GL_DEPTH_ATTACHMENT;
    break;
  case depth_format::E_DEPTH32F:
    *out = GL_DEPTH_COMPONENT32F;
    *att = GL_DEPTH_ATTACHMENT;
    break;
  default:
    *out = 0;
    *att = 0;
    break;
  }
}

namespace impl {
class gl_depth_stencil_buffer : public IDepthStencilBuffer {
  GLuint handle;
  GLuint old;

  GLenum type, att;
public:

  gl_depth_stencil_buffer() : handle(0), old(0), type(0), att(0) {
    glGenRenderbuffers(1, &handle);
  }

  virtual ~gl_depth_stencil_buffer() {
    glDeleteRenderbuffers(1, &handle);
  }

  virtual void create(size_t w, size_t h, const int df) {
    glConvertDepthFormat(df, &type, &att);
    glRenderbufferStorage(GL_RENDERBUFFER, type, w, h);
  }

  virtual int lock() {
    glGetIntegerv(GL_RENDERBUFFER_BINDING, (GLint*)&old);
    glBindRenderbuffer(GL_RENDERBUFFER, handle);
    return (int)handle;
  }

  virtual int unlock() {
    glBindRenderbuffer(GL_RENDERBUFFER, old);
    return (int)old;;
  }

  virtual void bind() {
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, att, GL_RENDERBUFFER, handle);
  }
};
}
QQQ_C_API IDepthStencilBuffer* QQQ_CALL new_gl_depth_stencil_buffer() {
  return new impl::gl_depth_stencil_buffer;
}
}
