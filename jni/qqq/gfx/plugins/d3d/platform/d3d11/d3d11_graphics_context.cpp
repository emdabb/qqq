/*
 * d3d11_graphics_context.cpp
 *
 *  Created on: Jul 15, 2014
 *      Author: miel
 */

#include <qqq_platform.h>

namespace qqq {

class d3d11_graphics_context {
#if defined(__WP8__)
#define D3D_FEATURELEVEL	D3D_9_3
#elif defined(__WIN32__)
#define D3D_FEATURELEVEL	D3D_11_1
#endif
};

}
