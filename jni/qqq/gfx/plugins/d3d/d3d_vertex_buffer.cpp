/**
 * @file d3d_vertex_buffer.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jul 10, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef D3D_VERTEX_BUFFER_CPP_
#define D3D_VERTEX_BUFFER_CPP_





#endif /* D3D_VERTEX_BUFFER_CPP_ */
