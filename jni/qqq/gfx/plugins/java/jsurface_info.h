/**
 * @file jsurface_info.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jul 12, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef JSURFACE_INFO_H_
#define JSURFACE_INFO_H_

#if defined(HAVE_JAVA)

#include <jni.h>

namespace qqq {
class jsurface_info {
  struct impl;
  impl* pimpl;
public:
  jsurface_info(JNIEnv*, jobject);
  virtual ~jsurface_info();
  void* get_display() const;
  void* get_surface() const;

};
}

#endif // HAVE_JAVA


#endif /* JSURFACE_INFO_H_ */
