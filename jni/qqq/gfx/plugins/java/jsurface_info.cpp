/**
 * @file jsurface_info.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jul 12, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include "jsurface_info.h"

#if defined(HAVE_JAVA)

#if defined(__ANDROID__)
#include <android/native_window.h>
#include <android/native_window_jni.h>
typedef void* 			JAWT_NativeDrawingSurfaceInfo;
typedef ANativeWindow* 	JAWT_NativeWindowType;
typedef void*			JAWT_NativeDisplayType;
#elif defined(__LINUX__)
#include "jawt_md.h"
typedef JAWT_X11DrawingSurfaceInfo 	JAWT_NativeDrawingSurfaceInfo;
typedef Drawable					JAWT_NativeWindowType;
typedef Display*					JAWT_NativeDisplayType;
#elif defined(__WIN32__)
typedef JAWT_Win32DrawingSurfaceInfo 	JAWT_NativeDrawingSurfaceInfo;
typedef HWND							JAWT_NativeWindowType;
typedef HDC								JAWT_NativeDisplayType;
#endif

namespace qqq {
  struct jsurface_info::impl {
    JAWT_NativeWindowType 	win;
    JAWT_NativeDisplayType 	dpy;
#if defined(__LINUX__)
    JAWT awt;
    JAWT_DrawingSurface* 			ds;
    JAWT_DrawingSurfaceInfo* 		dsi;
    JAWT_NativeDrawingSurfaceInfo	ndsi;
#endif

    impl(JNIEnv* env, jobject surface) {
#if defined(__ANDROID__)
      win = ANativeWindow_fromSurface(env, surface);
      dpy = NULL;
#elif defined(__LINUX__) || defined(__WIN32__)
      jboolean result;
      jint lock;

      // Get the AWT
      awt.version = JAWT_VERSION_1_3;
      result = JAWT_GetAWT(jenv, &awt);
      assert(result != JNI_FALSE);
      // Get the drawing surface
      ds = awt.GetDrawingSurface(jenv, jobj);
      assert(ds != NULL);
      if (ds == NULL) {
        return;
      }
      // Lock the drawing surface
      lock = ds->Lock(ds);
      assert((lock & JAWT_LOCK_ERROR) == 0);

      // Get the drawing surface info
      dsi = ds->GetDrawingSurfaceInfo(ds);
      assert(dsi);
      DEBUG_VALUE_AND_TYPE_OF(dsi->bounds.width);
      DEBUG_VALUE_AND_TYPE_OF(dsi->bounds.height);
      // Get the platform-specific drawing info
      ndsi = (JAWT_NativeDrawingSurfaceInfo*) dsi->platformInfo;
      assert(ndsi);

      surface = ndsi->drawable;
      display = ndsi->display;

      ds->Unlock(ds);
#endif
    }

    virtual ~impl() {}
  };
}

using namespace qqq;

jsurface_info::jsurface_info(JNIEnv* env, jobject obj)
: pimpl(new impl(env, obj))
{

}

jsurface_info::~jsurface_info() {
  delete pimpl;
  pimpl = NULL;
}

void* jsurface_info::get_display() const {
  return (void*)pimpl->dpy;
}

void* jsurface_info::get_surface() const {
  return (void*)pimpl->win;
}

#endif
