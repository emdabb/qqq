/**
 * @file vg_graphics_device.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Oct 21, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef VG_GRAPHICS_DEVICE_CPP_
#define VG_GRAPHICS_DEVICE_CPP_

#include <gfx/graphics_device.h>
#include <VG/openvg.h>
#include <VG/vgu.h>

namespace qqq {
class vg_graphics_device : public IGraphicsDevice {
public:

};
}

#endif /* VG_GRAPHICS_DEVICE_CPP_ */
