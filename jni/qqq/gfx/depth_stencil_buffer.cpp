#include "depth_stencil_buffer.h"
#include "graphics_device.h"
#include <core/factory.h>
#include <assert.h>

using namespace qqq;

DepthStencilBuffer::DepthStencilBuffer(IGraphicsDevice* pdev, size_t size_x, size_t size_y,
    const int sf) {
  pimpl = Factory<IDepthStencilBuffer>::instance().create(pdev->api());
  assert(pimpl);
  pimpl->lock();
  pimpl->create(size_x, size_y, sf);
  pimpl->unlock();
}

DepthStencilBuffer::~DepthStencilBuffer() {
  delete pimpl;
  pimpl = NULL;
}


