/**
 * @file vertex_buffer.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Sep 12, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */



#include "vertex_buffer.h"
#include <core/factory.h>

using namespace qqq;

VertexBuffer::VertexBuffer(IGraphicsDevice* pGraphicsDevice, const VertexDeclaration* pVertexDeclaration, size_t nElements, size_t nElementSize) {
    mVertexDeclaration 	= pVertexDeclaration;
    mElementCount 		= nElements;
    mElementSize  		= nElementSize;
    mPimpl = Factory<vertex_buffer_impl>::instance().create(pGraphicsDevice->api());
    assert(mPimpl);
    synchronized(mPimpl) {
        mPimpl->create(this);
    }
    pGraphicsDevice->addContextListener(this);

}

VertexBuffer::~VertexBuffer() {
    if(mPimpl != NULL) {
        delete mPimpl;
        mPimpl=NULL;
    }
}

size_t VertexBuffer::size() const {
    return mElementCount * mElementSize;
}

size_t VertexBuffer::count() const {
    return mElementCount;
}

basic_buffer_impl& VertexBuffer::get_impl() {
    return *mPimpl;
}

void VertexBuffer::destroy() {
    delete mPimpl;
}

const VertexDeclaration* VertexBuffer::get_vertex_declaration() const {
    return mVertexDeclaration;
}

void VertexBuffer::onContextLost(const GraphicsResourceEventArgs&){
    DEBUG_METHOD();
}
void VertexBuffer::onContextReset(const GraphicsResourceEventArgs&){
    DEBUG_METHOD();
}
