/**
 * @file sprite_font.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 18, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef SPRITE_FONT_H_
#define SPRITE_FONT_H_

#include <qqq.h>
#include <core/rectangle.h>
#include <core/vector2.h>
#include <vector>
#include <map>
#include <qqq.h>

namespace qqq {

class Texture2D;
struct IGraphicsDevice;

struct GlyphInfo {
    GlyphInfo() :
            advance(0), ascent(0), bearing(Vector2::Zero), rect(Rectangle()) {

    }
    GlyphInfo(GlyphInfo const& other) :
            advance(other.advance), ascent(other.ascent), bearing(
                    other.bearing), rect(other.rect) {

    }

    GlyphInfo& operator =(const GlyphInfo& other) {
        this->advance = other.advance;
        this->ascent = other.ascent;
        this->bearing = other.bearing;
        this->rect = other.rect;

        return *this;
    }

    real advance;
    real ascent;
    Vector2 bearing;
    Rectangle rect;
};

struct SpriteFontMetrics {
    int ascent;
    int capHeight;
    int medianHeight;
    int descent;
    int lineHeight;
    int maxAdvance;

};

class SpriteFont {
    Texture2D* mTexture;
    SpriteFontMetrics mMetrics;
    std::vector<GlyphInfo> mInfos;
public:
    SpriteFont(GlyphInfo[], size_t, Texture2D*);
    virtual ~SpriteFont();

    void setLineHeight(int);
    /**
     *
     * @return The vertical distance between the base lines of two consecutive lines of text, in pixels.
     */
    int getLineHeight();
    /**
     *
     * @return The spacing, in pixels, of the font characters.
     */
    real getSpacing();
    /**
     * Returns the font's default character. This character will be automatically substituted any time an attempt is made to
     * draw or measure characters that are not in the font.
     *
     * If set to null, missing characters will produce an exception.
     *
     * @return The font's default character.
     */
    char getDefaultCharacter();
    /**
     * If set to anything other than null, this character will be automatically substituted any time an attempt is made to draw
     * or measure characters that are not in the font.
     *
     * If set to null, missing characters will produce an exception.
     *
     * @param The default character to set.
     */
    void setDefaultCharacter(char);
    /**
     *
     * @param
     * @return
     */
    Vector2 measureString(const char*);
    /**
     *
     * @return
     */
    Texture2D* getTexture();
    /**
     *
     * @param
     * @return
     */
    const GlyphInfo& getGlyphInfo(int) const;
    /**
     *
     * @param
     * @return
     */
    GlyphInfo& getGlyphInfo(int);
    /**
     *
     * @param pDevice
     * @param filename
     * @return
     */
    static SpriteFont* loadFromFile(IGraphicsDevice* pDevice, InputStream*, size_t);

};
}

#endif /* SPRITE_FONT_H_ */
