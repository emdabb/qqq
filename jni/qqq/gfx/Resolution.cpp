/**
 * @file resolution.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Sep 19, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include "graphics_device.h"
#include "color.h"
#include <core/vector3.h>
#include <gfx/Resolution.h>
#include <gfx/render_state.h>

using namespace qqq;

Resolution::Resolution(IGraphicsDevice* device)
        : mGraphicsDevice(device) {
    //mWidth = mDevice->getPreferredWidth();
    //mHeight = mDevice->getPreferredHeight();
    //mGraphicsDevice->getDimensions(&mWidth, &mHeight);
    mVirtualWidth = mVirtualHeight = 1;
    mWidth = mHeight = 0;
    mIsFullscreen = false;
    mHasChanged = true;

}

Resolution::~Resolution() {

}

void Resolution::getTransformation(Matrix* outMatrix) {
    int dx, dy;
    mGraphicsDevice->getDimensions(&dx, &dy);
    if (dx != mWidth || dy != mHeight) {
        setResolution(dx, dy, mIsFullscreen);
    }
    if (mHasChanged) {
        updateTransformation();
    }
    *outMatrix = mTransform;
}

void Resolution::updateTransformation() {
    int dx, dy;
    mGraphicsDevice->getDimensions(&dx, &dy);
    mHasChanged = false;
    Matrix::createScale((real) dx / (real) mVirtualWidth, (real) dy / (real) mVirtualHeight, (real)1., &mTransform);
}

void Resolution::setResolution(int x, int y, bool fullscreen) {
    mWidth = x;
    mHeight = y;
    mIsFullscreen = fullscreen;
    mHasChanged = true;
}

void Resolution::setVirtualResolution(int x, int y) {
    mVirtualWidth = x;
    mVirtualHeight = y;
    mHasChanged = true;
}

void Resolution::beginDraw() {
    fullViewport();
    mGraphicsDevice->clear(Color::Black);
    resetViewport();
    RasterizerState rasterizerState = RasterizerState::Default;
    rasterizerState.ScissorTestEnable = 1;
    mGraphicsDevice->setRasterizerState(rasterizerState);
    //mGraphicsDevice->setRasterizerState(RasterizerState::Default);
}

void Resolution::fullViewport() {
    int size_x, size_y;
    mGraphicsDevice->getDimensions(&size_x, &size_y);
    Viewport vp;
    vp.X = 0;
    vp.Y = 0;
    vp.W = size_x;
    vp.H = size_y;
    mGraphicsDevice->pushViewport(vp);
}

real Resolution::getVirtualAspectRatio() {
    return (real) mVirtualWidth / (real) mVirtualHeight;
}

void Resolution::resetViewport() {
    float targetAspectRatio = getVirtualAspectRatio();
    // figure out the largest area that fits in this resolution at the desired aspect ratio
    int size_y, size_x;//= mDevice->getPreferredWidth();
    mGraphicsDevice->getDimensions(&size_x, &size_y);
    int width = size_x;
    int height = (int) (width / targetAspectRatio + .5f);
    bool changed = false;



    if (height > size_y) {
        height = size_y;
        // PillarBox
        width = (int) (height * targetAspectRatio + .5f);
        changed = true;
    }

    // set up the new viewport centered in the backbuffer

    mViewport.X = (size_x / 2) - (width / 2);
    mViewport.Y = (size_y / 2) - (height / 2);
    mViewport.W = width;
    mViewport.H = height;

    //mWidth = width;
    //mHeight = height;

    if (changed) {
        mHasChanged = true;
    }

    mGraphicsDevice->pushViewport(mViewport);
}

int Resolution::getVirtualWidth() const {
    return mVirtualWidth;
}

int Resolution::getVirtualHeight() const {
    return mVirtualHeight;
}


void Resolution::project(int px, int py, int* ppx, int* ppy) {
    if(mHasChanged) {
        updateTransformation();
    }
    Matrix I;
    Matrix::invert(mTransform, &I);
    Vector3 Pa = { (real)(px - mViewport.X), (real)(py - mViewport.Y), 0.0f };
    Pa = Vector3::transform(Pa, I);
    *ppx = (int)Pa.X;
    *ppy = (int)Pa.Y;
}

void Resolution::unproject(float px, float py, float* ppx, float* ppy) {

}


