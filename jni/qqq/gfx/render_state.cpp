/*
 * render_state.cpp
 *
 *  Created on: Jun 27, 2014
 *      Author: miel
 */

#include "render_state.h"
#include "blend.h"
#include "compare_function.h"
#include "stencil_operation.h"
#include "fill_mode.h"
#include "cull_mode.h"
#include "texture.h"

using namespace qqq;

const BlendState BlendState::Default = {
    BlendFunction::Add,
    Blend::One,
    Blend::One,
    0xffffffff,
    BlendFunction::Add,
    Blend::One,
    Blend::One,
    ColorWriteChannels::WriteToNone,
    ColorWriteChannels::WriteToNone,
    ColorWriteChannels::WriteToNone,
    ColorWriteChannels::WriteToNone,
    0xffffffff
};

const BlendState BlendState::Additive = {
    BlendFunction::Add,
    Blend::One,
    Blend::SourceAlpha,
    0xffffffff,
    BlendFunction::Add,
    Blend::One,
    Blend::SourceAlpha,
    ColorWriteChannels::WriteToNone,
    ColorWriteChannels::WriteToNone,
    ColorWriteChannels::WriteToNone,
    ColorWriteChannels::WriteToNone,
    0xffffffff
};

const BlendState BlendState::AlphaBlend = {
    BlendFunction::Add,
    Blend::InverseSourceAlpha,
    Blend::One,
    0xffffffff,
    BlendFunction::Add,
    Blend::InverseSourceAlpha,
    Blend::One,
    ColorWriteChannels::WriteToNone,
    ColorWriteChannels::WriteToNone,
    ColorWriteChannels::WriteToNone,
    ColorWriteChannels::WriteToNone,
    0xffffffff
};

const BlendState BlendState::NonPremultiplied = {
    BlendFunction::Add,
    Blend::InverseSourceAlpha,
    Blend::SourceAlpha,
    0xffffffff,
    BlendFunction::Add,
    Blend::InverseSourceAlpha,
    Blend::SourceAlpha,
    ColorWriteChannels::WriteToNone,
    ColorWriteChannels::WriteToNone,
    ColorWriteChannels::WriteToNone,
    ColorWriteChannels::WriteToNone,
    0xffffffff
};

const BlendState BlendState::Opaque = {
    BlendFunction::Add,
    Blend::Zero,
    Blend::One,
    0xffffffff,
    BlendFunction::Add,
    Blend::Zero,
    Blend::One,
    ColorWriteChannels::WriteToNone,
    ColorWriteChannels::WriteToNone,
    ColorWriteChannels::WriteToNone,
    ColorWriteChannels::WriteToNone,
    0xffffffff
};

const DepthStencilState DepthStencilState::Default = {
    StencilOperation::Keep,
    StencilOperation::Keep,
    CompareFunction::AlwaysPass,
    StencilOperation::Keep,
    1,
    CompareFunction::LessEqual,
    1,
    0,
    StencilOperation::Keep,
    0,
    StencilOperation::Keep,
    CompareFunction::AlwaysPass,
    __INT32_MAX__,
    0
};
const DepthStencilState DepthStencilState::DepthRead = {
    StencilOperation::Keep,
    StencilOperation::Keep,
    CompareFunction::AlwaysPass,
    StencilOperation::Keep,
    1,
    CompareFunction::LessEqual,
    0,
    0,
    StencilOperation::Keep,
    0,
    StencilOperation::Keep,
    CompareFunction::AlwaysPass,
    __INT32_MAX__,
    0
};
const DepthStencilState DepthStencilState::NoDepthStencilBuffer = {
    StencilOperation::Keep,
    StencilOperation::Keep,
    CompareFunction::AlwaysPass,
    StencilOperation::Keep,
    0,
    CompareFunction::LessEqual,
    0,
    0,
    StencilOperation::Keep,
    0,
    StencilOperation::Keep,
    CompareFunction::AlwaysPass,
    __INT32_MAX__,
    0
};

const RasterizerState RasterizerState::Default = {
    CullMode::CullCounterClockwiseFace,
    (real)0.,
    FillMode::Solid,
    1,
    0,
    (real)0.
};
const RasterizerState RasterizerState::CullClockwise = {
    CullMode::CullClockwiseFace,
    (real)0.,
    FillMode::Solid,
    1,
    0,
    (real)0.

};
const RasterizerState RasterizerState::CullCounterClockwise = {
    CullMode::CullCounterClockwiseFace,
    (real)0.,
    FillMode::Solid,
    1,
    0,
    (real)0.
};
const RasterizerState RasterizerState::CullNone = {
    CullMode::CullNone,
    (real)0.,
    FillMode::Solid,
    1,
    0,
    (real)0.
};
