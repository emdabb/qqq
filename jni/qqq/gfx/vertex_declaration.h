/*
 * vertex_declaration.h
 *
 *  Created on: Jun 27, 2014
 *      Author: miel
 */

#ifndef VERTEX_DECLARATION_H_
#define VERTEX_DECLARATION_H_

#include <vector>
#include <string.h>
#include <core/aligned_malloc.h>

namespace qqq {


struct VertexStream {
    /**
     * Vertex stream position.
     */
    enum {
        Position = 0,           //!< Position
        BlendWeight = 1,        //!< BlendWeight
        Normal = 2,             //!< Normal
        PrimaryColor = 3,       //!< PrimaryColor
        SecondaryColor = 4,     //!< SecondaryColor
        FogCoordinate = 5,      //!< FogCoordinate
        PointSize = 6,          //!< PointSize
        BlendIndex = 7,         //!< BlendIndex
        TextureCoordinate0 = 8, //!< TextureCoordinate0
        TextureCoordinate1 = 9, //!< TextureCoordinate1
        TextureCoordinate2 = 10, //!< TextureCoordinate2
        TextureCoordinate3 = 11, //!< TextureCoordinate3
        TextureCoordinate4 = 12, //!< TextureCoordinate4
        TextureCoordinate5 = 13, //!< TextureCoordinate5
        TextureCoordinate6 = 14, //!< TextureCoordinate6
        TextureCoordinate7 = 15, //!< TextureCoordinate7
        //        Tangent = 16,
        //        Binormal = 17,
        //        VertexID = 18,
        //        PrimitiveID = 19,
        MaxElementAttributes    //!< MaxElementAttributes
    };
};

struct VertexElementFormat {
    enum {
        /**
         *  Single-component, 32-bit floating-point, expanded to (float, 0, 0, 1).
         */
        Single,
        /**
         *  Two-component, 32-bit floating-point, expanded to (float, Float32 value, 0, 1).
         */
        Vector2,
        /**
         *  Three-component, 32-bit floating point, expanded to (float, float, float, 1).
         */
        Vector3,
        /**
         *  Four-component, 32-bit floating point, expanded to (float, float, float, float).
         */
        Vector4,
        /**
         *  16-bit floating point expanded to value. This type is valid for vertex shader version 2.0 or higher.
         */
        HalfSingle,
        /**
         *  Two-component, 16-bit floating point expanded to (value, value). This type is valid for vertex shader version 2.0 or higher.
         */
        HalfVector2,
        /**
         *  Three-component, 16-bit floating point expanded to (value, value, value). This type is valid for vertex shader version 2.0 or higher.
         */
        HalfVector3,
        /**
         *  Four-component, 16-bit floating-point expanded to (value, value, value, value). This type is valid for vertex shader version 2.0 or higher.
         */
        HalfVector4,
        /**
         *  Four-component, packed, signed int, mapped to 0 to 1 range. Input is in Int32 format (ARGB) expanded to (R, G, B, A).
         */
        Int4,
        /**
         *  Four-component, packed, unsigned byte, mapped to 0 to 1 range. Input is in Int32 format (ABGR) expanded to (R, G, B, A).
         */
        Color,
        /**
         *  Normalized, two-component, signed short, expanded to (first short/32767.0, second short/32767.0, 0, 1). This type is valid for vertex shader version 2.0 or higher.
         */
        NormalizedShort2,
        /**
         *  Normalized, four-component, signed short, expanded to (first short/32767.0, second short/32767.0, third short/32767.0, fourth short/32767.0). This type is valid for vertex shader version 2.0 or higher.
         */
        NormalizedShort4,
        /**
         *  Two-component, signed short expanded to (value, value, 0, 1).
         */
        Short2,
        /**
         *  Four-component, signed short expanded to (value, value, value, value).
         */
        Short4,
        /**
         *  Four-component, unsigned byte.
         */
        Byte4
    };

};

struct VertexElement {
    int offset;
    int vertexElementFormat;
    int vertexElementUsage;
};



class VertexDeclaration {
    VertexElement*	mVertexElements;
    size_t 		 	mNumVertexElements;
    size_t 		 	mVertexStride;
protected:
    //DISALLOW_COPY_AND_ASSIGN(VertexDeclaration);
public:
    /**
     *
     * @param vertexElements
     * @param numElements
     * @param stride
     */
    VertexDeclaration(const VertexElement*, size_t, size_t);
    /**
     *
     */
    virtual ~VertexDeclaration();
    /**
     *
     * @param n
     * @return
     */
    const VertexElement& at(size_t n) const;
    /**
     *
     * @return
     */
    const size_t size() const;
    /**
     *
     * @return
     */
    const size_t stride() const;

    /**
     *
     * @param elements
     * @param numElements
     * @param stride
     * @return
     */
    static const VertexDeclaration* create(const VertexElement* elements, size_t numElements, size_t stride);
};



class VertexDeclarationBuilder {
    std::vector<VertexElement> mElements;
    size_t mStride;
public:
    /**
     *
     */
    VertexDeclarationBuilder();
    /**
     *
     */
    virtual ~VertexDeclarationBuilder();
    /**
     *
     * @param n
     * @param offset
     * @param type
     * @param streamId
     * @return
     */
    VertexDeclarationBuilder& element(size_t n, size_t offset, const int type, const int streamId);
    //VertexDeclarationBuilder& element(const VertexElement& a);
    /**
     *
     * @param a
     * @return
     */

    VertexDeclarationBuilder& stride(size_t a);
    /**
     *
     * @return
     */
    VertexDeclaration* build();
};

}

#endif /* VERTEX_DECLARATION_H_ */
