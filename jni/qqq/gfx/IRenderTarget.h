/**
 * @file IRenderTarget.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date May 27, 2015
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef JNI_QQQ_GFX_IRENDERTARGET_H_
#define JNI_QQQ_GFX_IRENDERTARGET_H_

#include <core/IResource.h>

namespace qqq {

struct IRenderTarget {
    virtual ~IRenderTarget() {}
    virtual bool isPrimary() = 0;
    virtual bool isValid() = 0;
    virtual int  getWidth() = 0;
    virtual int  getHeight() = 0;
};

}


#endif /* JNI_QQQ_GFX_IRENDERTARGET_H_ */
