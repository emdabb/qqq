/*
 * compare_function.h
 *
 *  Created on: Jun 27, 2014
 *      Author: miel
 */

#ifndef COMPARE_FUNCTION_H_
#define COMPARE_FUNCTION_H_

namespace qqq {
struct compare_function {
  enum {
    E_NEVER,
    E_LESS,
    E_LESS_EQUEAL,
    E_GREATER,
    E_GREATER_EQUAL,
    E_EQUAL,
    E_NOT_EQUAL,
    E_ALWAYS
  };
};
}



#endif /* COMPARE_FUNCTION_H_ */
