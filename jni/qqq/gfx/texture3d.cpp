/**
 * @file texture3d.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jul 10, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include "texture3d.h"
#include <gfx/graphics_context.h>
#include <gfx/graphics_device.h>
#include <core/factory.h>

using namespace qqq;

texture3d::texture3d(IGraphicsDevice* pdev, size_t width, size_t height, size_t depth, const int sf)
: size_x(width)
, size_y(height)
, size_z(depth)
, fmt(sf)
{
    mGraphicsDevice = pdev;
  pimpl = Factory<texture3d_impl>::instance().create(mGraphicsDevice->api());
  synchronized(pimpl) {
    pimpl->create(this);
  }
}
texture3d::~texture3d() {
  delete pimpl;
}

void texture3d::context_lost(const GraphicsResourceEventArgs& args) {
  args.GraphicsContext->lock();
  delete pimpl;
  pimpl = NULL;
  args.GraphicsContext->unlock();
}

void texture3d::onContextReset(const GraphicsResourceEventArgs& args) {
  if(!pimpl) {
    args.GraphicsContext->lock();
    pimpl = Factory<texture3d_impl>::instance().create(mGraphicsDevice->api());
    pimpl->create(this);
    args.GraphicsContext->unlock();
  }
}
