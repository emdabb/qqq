/*
 * sprite_batch.h
 *
 *  Created on: Jun 27, 2014
 *      Author: miel
 */

#ifndef SPRITE_BATCH_H_
#define SPRITE_BATCH_H_

#include <qqq.h>
#include <core/matrix.h>
#include <core/vector4.h>
#include <core/vector3.h>
#include <core/vector2.h>
#include <core/rectangle.h>
#include <gfx/color.h>
#include <gfx/texture2d.h>
#include <gfx/sampler2d.h>
#include <gfx/vertex_declaration.h>
#include <gfx/vertex_buffer.h>
#include <gfx/render_state.h>
#include <gfx/shader.h>

namespace qqq {
struct Rectangle;
class SpriteFont;

struct SpriteVertex{
  Vector3 pos;
  Vector2 coord;
  Color   tint;

  static const VertexDeclaration* decl;
  static const VertexElement el[];

};

struct SpriteEffect {
  enum {
    NoEffect,
    FlipHorizontal,
    FlipVertical,
    FlipBoth
  };
};

__declare_aligned(struct, 16) SpriteInfo {
  Vector4 src;
  Vector4 dst;
  Vector2 orig;
  real rot;
  real depth;
  int fx;
  Color tint;
  Texture2D* tex;
};

struct blend_state;
struct sampler_state;
struct depth_stencil_state;
struct resterizer_state;

class sprite_batch_shader : public ShaderProgram {
public:
    ShaderParameter<Sampler2D>* mBaseSamplerParameter;
    ShaderParameter<Matrix>*    mWorldViewProjectionMatrixParameter;
public:
    sprite_batch_shader(IGraphicsDevice*);
    virtual ~sprite_batch_shader();

};

class SpriteBatch {
  const static int MaxBatchSize = 1024;
  const static int MinBatchSize = 128;
  const static int InitialQueueSize = 64;
  const static int VerticesPerSprite = 4;
  const static int IndicesPerSprite = 6;
  const static int MaxVertexCount = MaxBatchSize * VerticesPerSprite;
  const static int MaxIndexCount = MaxBatchSize * IndicesPerSprite;

  int mNumSpritesToDraw;
  SpriteInfo mQueue[MaxBatchSize];
  SpriteVertex mVertices[MaxVertexCount];
  VertexBuffer* mVertexBuffer;

  Matrix mTransform;

  ShaderParameter<Matrix>* 	mWorldViewProjParameter;
  ShaderParameter<Sampler2D>* mBaseSamplerParameter;

  Sampler2D mSampler2D;

  BlendState mCachedBlendState;
  RasterizerState mCachedRasterizerState;
  DepthStencilState mCachedDepthStencilState;

  IGraphicsDevice* mGraphicsDevice;
  ShaderProgram* mShader;
  int mCapacity;
public:
  SpriteBatch(IGraphicsDevice*, int capacity = MaxBatchSize);
  virtual ~SpriteBatch();
  void begin();
  void begin(const int, const BlendState&, const Matrix&, ShaderProgram*);

  void draw(Texture2D* tex, const qqq::Rectangle& dst, const Color& col);
  void draw(Texture2D* tex, const qqq::Rectangle& dst, qqq::Rectangle* src, const Color& col);
  void draw(Texture2D* tex, const qqq::Rectangle& dst, qqq::Rectangle* src, const Color& col, const real& rot, const Vector2& pivot, const int fx, const real& depth);
  void draw(Texture2D* tex, const qqq::Rectangle& dst, qqq::Rectangle* src, const Color& col, const real& rot, const Vector2& pivot, const Vector2& scale, const int fx, const real& depth);

  void draw(Texture2D* tex, const Vector2& pos, const Color& col);
  void draw(Texture2D* tex, const Vector2& pos, qqq::Rectangle* src, const Color& col);
  void draw(Texture2D* tex, const Vector2& pos, qqq::Rectangle* src, const Color& col, const real& rot, const Vector2& pivot, const real& scale, const int fx, const real& depth);
  void draw(Texture2D* tex, const Vector2& pos, qqq::Rectangle* src, const Color& col, const real& rot, const Vector2& pivot, const Vector2& scale, const int fx, const real& depth);

  void drawString(SpriteFont*, const char*, const Vector2&, const Color&);
  void drawString(SpriteFont*, const char*, const Vector2&, const Color&, const real&, const Vector2&, const real&, const int, const real&);
  void end();

  void getTransform(Matrix*);
protected:
  void flush();
  void renderBatch(Texture2D*, int off, size_t count);
  void copyToVertices(const SpriteInfo&, SpriteVertex*);
};
}



#endif /* SPRITE_BATCH_H_ */
