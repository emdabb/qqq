/**
 * @file model.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 9, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef MODEL_H_
#define MODEL_H_

#include <core/io/stream.h>
#include <core/matrix.h>

namespace qqq {

class VertexBuffer;
class IndexBuffer;
class Texture2D;
class IGraphicsDevice;

struct ModelMesh {
    VertexBuffer* 	Vertices;
    IndexBuffer* 	Indices;
};

struct ModelBone {
    std::string Name;
    Matrix WorldToBone;
    Matrix BindPoseInv;
    int ParentId;

    __forceinline bool hasParent() const {
        return ParentId >= 0;
    }
};

struct Material {
    Texture2D* map_Kd;
    Texture2D* map_Ks;
};

class Model {
    IGraphicsDevice* mGraphicsDevice;
    std::vector<ModelMesh*> mMeshes;
public:
    explicit Model(IGraphicsDevice* pGraphicsDevice);
    virtual ~Model();

    void addMesh(ModelMesh*);

    const ModelMesh* getMesh(size_t meshIndex);

    void draw();

    static void createFromStream(Model**, std::istream*);
    static void saveToStream(Model*, std::ostream);
};
}

#endif /* MODEL_H_ */
