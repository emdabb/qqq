/**
 * @file vertex_buffer.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 5, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef VERTEX_BUFFER_H_
#define VERTEX_BUFFER_H_

#include <qqq.h>
#include <iostream>
#include <core/factory.h>
#include <core/lockable.h>
#include <gfx/graphics_device.h>
#include <assert.h>
#include <gfx/vertex_declaration.h>
#include <gfx/IGraphicsResource.h>
#include <gfx/graphics_context.h>
#include <gfx/IBasicBuffer.h>

namespace qqq {
class VertexBuffer;

struct vertex_buffer_impl : public basic_buffer_impl{
    /**
     *
     */
    virtual ~vertex_buffer_impl() {}
};

class VertexBuffer : public IBasicBuffer {
    vertex_buffer_impl* mPimpl;
    const VertexDeclaration* mVertexDeclaration;
    size_t mElementCount;
    size_t mElementSize;
public:
    /**
     *
     * @param pGraphicsDevice
     * @param pVertexDeclaration
     * @param nElements
     * @param elementSize
     */
    VertexBuffer(IGraphicsDevice* pGraphicsDevice, const VertexDeclaration* pVertexDeclaration, size_t nElements, size_t elementSize);
    /**
     *
     */
    virtual ~VertexBuffer();
    /**
     *
     * @param src
     * @param beg
     * @param end
     */
    template <typename T>
    void set_data(const T* src, size_t off, size_t num) {
        synchronized(mPimpl) {
            mPimpl->set_data(src, off, off + num * sizeof(T));
        }
    }
    /**
     *
     * @param dst
     * @param beg
     * @param end
     */
    template <typename T>
    void get_data(T* dst, size_t off, size_t num) {
        synchronized(mPimpl) {
            mPimpl->get_data(dst, off, num * sizeof(T));
        }
    }
    /**
     *
     * @return
     */
    const VertexDeclaration* get_vertex_declaration() const;
    /**
     *
     * @return
     */
    size_t size() const;
    /**
     *
     * @return
     */
    size_t count() const;
    /**
     *
     * @return
     */
    basic_buffer_impl& get_impl();

    virtual void destroy();

    virtual void onContextLost(const GraphicsResourceEventArgs&);
    virtual void onContextReset(const GraphicsResourceEventArgs&);
};

} /// qqq




#endif /* VERTEX_BUFFER_H_ */
