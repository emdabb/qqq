/**
 * @file surface.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jul 31, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef SURFACE_H_
#define SURFACE_H_

#include <cassert>
#include <cstring>
#include "pixel_format.h"

namespace qqq {

class Color;
struct Rectangle;

class Surface {
    size_t mWidth;
    size_t mHeight;
    size_t mPitch;
    size_t mPixelSize;
    PixelFormat mFormat;
    void* mData;
public:
    Surface(size_t w, size_t h, const PixelFormat& pfd);
    virtual ~Surface();
    const size_t get_width() const;
    const size_t get_height() const;
    const char* get_data() const;
    const PixelFormat& get_format() const;
    void set_data(const void* in);
    void blit(const void* in, const Rectangle& src, const Rectangle& dst);
    void draw_rect(const void* in, int x0, int y0, int x1, int y1);
    void plot(int x, int y, const Color&);

};

}



#endif /* SURFACE_H_ */
