/*
 * stencil_operation.h
 *
 *  Created on: Jun 27, 2014
 *      Author: miel
 */

#ifndef STENCIL_OPERATION_H_
#define STENCIL_OPERATION_H_

namespace qqq {
struct stencil_operation {
  enum {
    E_ZERO,
    E_KEEP,
    E_REPLACE,
    E_INC,
    E_INC_WRAP,
    E_DEC,
    E_DEC_WRAP,
    E_INV
  };
};
}



#endif /* STENCIL_OPERATION_H_ */
