/**
 * @file SamplerState.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Apr 1, 2015
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef JNI_HELIX2_GFX_SAMPLERSTATE_H_
#define JNI_HELIX2_GFX_SAMPLERSTATE_H_

#include <qqq.h>

namespace qqq {

struct TextureAddressMode {
    enum {
        /**
         * Texture coordinates outside the range [0.0, 1.0] are set to the texture color at 0.0 or 1.0, respectively.
         */
        Clamp,
        /**
         * Similar to Wrap, except that the texture is flipped at every integer junction. For u values between 0 and 1, for example, the texture is addressed normally; between 1 and 2, the texture is flipped (mirrored); between 2 and 3, the texture is normal again, and so on.
         */
        Mirror,
        /**
         * Tile the texture at every integer junction. For example, for u values between 0 and 3, the texture is repeated three times; no mirroring is performed.
         */
        Wrap
    };
};

struct TextureFilter {
    enum {
        /**
         * Use linear filtering.
         */
        Linear,
        /**
         * Use point filtering.
         */
        Point,
        /**
         * Use anisotropic filtering.
         */
        Anisotropic,
        /**
         * Use linear filtering to shrink or expand, and point filtering between mipmap levels (mip).
         */
        LinearMipPoint,
        /**
         * Use point filtering to shrink (minify) or expand (magnify), and linear filtering between mipmap levels.
         */
        PointMipLinear,
        /**
         * Use linear filtering to shrink, point filtering to expand, and linear filtering between mipmap levels.
         */
        MinLinearMagPointMipLinear,
        /**
         * Use linear filtering to shrink, point filtering to expand, and point filtering between mipmap levels.
         */
        MinLinearMagPointMipPoint,
        /**
         * Use point filtering to shrink, linear filtering to expand, and linear filtering between mipmap levels.
         */
        MinPointMagLinearMipLinear,
        /**
         * Use point filtering to shrink, linear filtering to expand, and point filtering between mipmap levels.
         */
        MinPointMagLinearMipPoint
    };
};

struct SamplerState {
    const int 	AddressU;
    const int 	AddressV;
    const int 	AddressW;
    const int 	Filter;
    const int 	MaxAnisotropy;
    const int 	MaxMipLevel;
    const real 	MipMapLevelOfDetailBias;

    static const int  DefaultMaxAnisotropy = 4;
    static const int  DefaultMaxMipLevel = 0;
    static const real DefaultMipMapLevelOfDetailBias;

    static const SamplerState AnisotropicClamp;
    static const SamplerState AnisotropicWrap;
    static const SamplerState LinearClamp;
    static const SamplerState LinearWrap;
    static const SamplerState PointClamp;
    static const SamplerState PointWrap;
};

}


#endif /* JNI_HELIX2_GFX_SAMPLERSTATE_H_ */
