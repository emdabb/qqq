/**
 * @file texture2D.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 5, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef TEXTURE2D_H_
#define TEXTURE2D_H_

#include <qqq.h>
#include <gfx/IGraphicsResource.h>
#include <core/lockable.h>

namespace qqq {
class IGraphicsDevice;
class Surface;

struct texture2d_impl: public ILockable {
	virtual ~texture2d_impl() {
	}
	virtual void create(size_t, size_t, const int, bool) = 0;
	virtual void set_data(const void*, int, size_t, size_t, size_t, size_t) = 0;
	virtual int num_levels() const = 0;
	//virtual void setFilter(const int) = 0;
};
class Texture2D: public IGraphicsResource {
	IGraphicsDevice* mGraphicsDevice;
	size_t _width, _height;
	texture2d_impl* pimpl;
public:
	Texture2D(IGraphicsDevice* dev, size_t w, size_t h, const int, bool);

	virtual ~Texture2D() {
	}

	template<typename T>
	void setData(const T* data, size_t w, size_t h) {
		synchronized(pimpl)
		{
			pimpl->set_data(data, 0, 0, 0, w, h);
		}
	}

	template<typename T>
	void setData(const T* data, size_t x, size_t y, size_t w, size_t h) {
		synchronized(pimpl)
		{
			pimpl->set_data(data, 0, x, y, w, h);
		}
	}

	size_t getWidth() const {
		return _width;
	}

	virtual void destroy();
	virtual void onContextReset(const GraphicsResourceEventArgs&);
	virtual void onContextLost(const GraphicsResourceEventArgs&);

	size_t getHeight() const {
		return _height;
	}

	texture2d_impl& getImpl() const {
		return *pimpl;
	}

	static Texture2D* createFromSurface(IGraphicsDevice*, const Surface&, bool =
			false);
};
} // qqq

#endif /* TEXTURE2D_H_ */
