/**
 * @file vertex_declaration.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date May 8, 2015
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include "vertex_declaration.h"
//#include "vertex_element.h"

using namespace qqq;

VertexDeclaration::VertexDeclaration(const VertexElement* pElement, size_t num, size_t stride)
: mVertexElements(NULL)
, mNumVertexElements(num)
, mVertexStride(stride)
{
    mVertexElements = new VertexElement[num];
    for(size_t i=0; i < num; i++) {
        mVertexElements[i] = { pElement[i].offset, pElement[i].vertexElementFormat, pElement[i].vertexElementUsage };
    }
}

VertexDeclaration::~VertexDeclaration() {
    delete[] mVertexElements;
}

const VertexElement& VertexDeclaration::at(size_t n) const {
    return mVertexElements[n];
}

const size_t VertexDeclaration::size() const {
    return this->mNumVertexElements;
}

const size_t VertexDeclaration::stride() const {
    return this->mVertexStride;
}

const VertexDeclaration* VertexDeclaration::create(const VertexElement* elements, size_t numElements, size_t stride) {
    VertexDeclarationBuilder builder;
    for(size_t i=0; i < numElements; i++) {
        builder.element(i, elements[i].offset, elements[i].vertexElementFormat, elements[i].vertexElementUsage);
    }
    builder.stride(stride);
    return builder.build();
}


VertexDeclarationBuilder::VertexDeclarationBuilder()
: mStride(0) {
    mElements.clear();
}

VertexDeclarationBuilder::~VertexDeclarationBuilder() {

}

VertexDeclarationBuilder& VertexDeclarationBuilder::element(size_t id, size_t offset, const int type, const int streamId) {
    if(id >= mElements.size()) {
        mElements.resize(id + 1);
    }
    mElements[id].offset  			  = offset;
    mElements[id].vertexElementFormat = type;
    mElements[id].vertexElementUsage  = streamId;
    return *this;
}

//VertexDeclarationBuilder& VertexDeclarationBuilder::element(const VertexElement& a) {
//    mElements.push_back(a);
//    return *this;
//}
VertexDeclarationBuilder& VertexDeclarationBuilder::stride(size_t a) {
    mStride = a;
    return *this;
}

VertexDeclaration* VertexDeclarationBuilder::build() {
    return new VertexDeclaration(&mElements[0], mElements.size(), mStride);
}
