/**
 * @file shader.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Aug 3, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include "shader.h"
#include "graphics_device.h"
#include <core/factory.h>
#include <cassert>

using namespace qqq;

ShaderProgram::ShaderProgram(IGraphicsDevice* dev) {
    pimpl = Factory<IShaderProgram>::instance().create(dev->api());
    assert(pimpl);
    mIsActive = false;
}

ShaderProgram::~ShaderProgram() {

}

void ShaderProgram::compile_and_link(const char* src) {
//    DEBUG_METHOD();
//    DEBUG_MESSAGE(src);
    if(!pimpl->compile(src)) {

    }

    if(!pimpl->link()) {

    }

    pimpl->cacheUniforms(this);
}

void ShaderProgram::on_invalidate(const uniform_event_args& args) {
    if(this->mIsActive) {
        args.thiz->validate();
    }
}

void ShaderProgram::add_uniform(ShaderParameterBase* param, int loc) {
    mUniform.push_back(param);
    mLocation.push_back(loc);
    if(mUniformNames.count(param->get_name()) > 0) {
        throw std::runtime_error("duplicate uniform name found: " + param->get_name());
    }
    mUniformNames[param->get_name()] = param;
}

int ShaderProgram::lock() {

    pimpl->enable();
    mIsActive = true;
    unsigned int n = mUniform.size();
    for(unsigned int i=0; i < n; i++) {
        if(!mUniform[i]->is_valid()) {
            mUniform[i]->validate();
        }
    }
    return 1;
}

int ShaderProgram::unlock() {
    pimpl->disable();
    mIsActive = false;
    return 1;
}

ShaderParameterBase::ShaderParameterBase(ShaderProgram* sh, const std::string& name, int loc) : mName(name), mIsValid(false) {
    add_owner(sh, loc);
}
ShaderParameterBase::~ShaderParameterBase() {

}
void ShaderParameterBase::add_owner(ShaderProgram* sh, int loc) {
    this->on_invalidate += event(sh, &ShaderProgram::on_invalidate);
}

void ShaderParameterBase::invalidate() {
    mIsValid = false;
}

bool ShaderParameterBase::is_valid() const {
    return mIsValid != false;
}

ShaderProgram* ShaderProgram::from_stream(IGraphicsDevice* pDevice, std::istream* is) {
    ShaderProgram* out = new ShaderProgram(pDevice);
    is->seekg(0, is->beg);
    int a = is->tellg();
    is->seekg(0, is->end);
    int b = is->tellg();
    int sz = b - a;
    is->seekg(0, is->beg);
    char* src = new char[sz + 1];
    src[sz] = '\0';
    is->read(src, sz);
    out->compile_and_link(src);
    delete[] src;
    return out;
}
