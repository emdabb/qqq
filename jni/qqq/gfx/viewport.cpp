/**
 * @file viewport.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Sep 29, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include "viewport.h"
#include <core/math_helper.h>
#include <core/matrix.h>
#include <core/vector3.h>

using namespace qqq;

void Viewport::project(const Vector3& in, const Matrix& wvp, Vector3* out) {
    Vector3 vector = Vector3::transform(in, wvp);
    float a = (((in.X * wvp.m14) + (in.Y * wvp.m24)) + (in.Z * wvp.m34)) + wvp.m44;
    if (!math::within_epsilon(a - 1.f))
    {
        vector = (vector / a);
    }
    out->X = (((vector.X + 1.f) * 0.5f) * W) + X;
    out->Y = (((-vector.Y + 1.f) * 0.5f) * H) + Y;
    out->Z = (vector.Z * (farPlaneDistance - nearPlaneDistance)) + nearPlaneDistance;

}

void Viewport::unproject(const Vector3& in, const Matrix& wvp, Vector3* out) {
    Matrix wvpI;
    Matrix::invert(wvp, &wvpI);
    Vector3 vec;
    vec.X = (((in.X - X) / ((float) W)) * 2.f) - 1.f;
    vec.Y = -((((in.Y - Y) / ((float) H)) * 2.f) - 1.f);
    vec.Z = (in.Z - nearPlaneDistance) / (farPlaneDistance - nearPlaneDistance);
    Vector3 vector = Vector3::transform(vec, wvpI);
    float a = (((vec.X * wvpI.m14) + (vec.Y * wvpI.m24)) + (vec.Z * wvpI.m34)) + wvpI.m44;

    if (!math::within_epsilon(a - 1.f))
    {
        *out = (vector / a);
        return;
    }
    *out = vector;
}


