/**
 * @file texture_cube.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jul 10, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include "TextureCUBE.h"
#include <gfx/graphics_context.h>
#include <gfx/graphics_device.h>
#include <core/factory.h>

using namespace qqq;


TextureCUBE::TextureCUBE(IGraphicsDevice* pdev, size_t sz, const int fmt) {
    mGraphicsDevice = pdev;
  pimpl = Factory<texture_cube_impl>::instance().create(mGraphicsDevice->api());
  pimpl->create(this);
}

TextureCUBE::~TextureCUBE() {
  delete pimpl;
  pimpl = NULL;
}

void TextureCUBE::onContextLost(const GraphicsResourceEventArgs& args) {
  delete pimpl;
  pimpl = NULL;
}

void TextureCUBE::onContextReset(const GraphicsResourceEventArgs& args) {
  if(!pimpl) {
    pimpl = Factory<texture_cube_impl>::instance().create(mGraphicsDevice->api());
    pimpl->create(this);
  }
}

void TextureCUBE::destroy() {
    delete pimpl;
}
