/**
 * @file framebuffer.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 24, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef FRAMEBUFFER_H_
#define FRAMEBUFFER_H_

namespace qqq {

struct framebuffer_impl {
  virtual ~framebuffer_impl() {}

};

class framebuffer {
public:
  virtual ~framebuffer() {}
};

}


#endif /* FRAMEBUFFER_H_ */
