/**
 * @file viewport.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Sep 19, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef VIEWPORT_H_
#define VIEWPORT_H_

#include <core/rectangle.h>

namespace qqq {
struct Vector3;
struct Matrix;

struct Viewport : public Rectangle {
    real nearPlaneDistance;
    real farPlaneDistance;

    /**
     *  Projects a 3D vector from object space into screen space.
     *
     * @param in	Source vector
     * @param in	World-view-projection matrix
     * @param out 	Projected vector.
     */
    void project(const Vector3&, const Matrix&, Vector3*);
    /**
     *  Converts a screen space point into a corresponding point in world space.
     *
     * @param in	Source vector
     * @param in	World-view-projection matrix
     * @param out	Unprojected vector.
     */
    void unproject(const Vector3&, const Matrix&, Vector3*);
};
}



#endif /* VIEWPORT_H_ */
