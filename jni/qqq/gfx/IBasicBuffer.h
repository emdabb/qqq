/**
 * @file basic_buffer.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Sep 12, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef BASIC_BUFFER_H_
#define BASIC_BUFFER_H_

#include <core/lockable.h>
#include "IGraphicsResource.h"

namespace qqq {

struct IBasicBuffer;

struct basic_buffer_impl : public ILockable {
    virtual ~basic_buffer_impl() {}
    virtual void set_data(const void*, size_t, size_t) = 0;
    virtual void get_data(void*, size_t, size_t) = 0;
    virtual void create(IBasicBuffer*) = 0;
};

struct IBasicBuffer : public IGraphicsResource{
    virtual ~IBasicBuffer() {}
    virtual size_t size() const = 0;
    virtual size_t count() const = 0;
    virtual basic_buffer_impl& get_impl() = 0;
};

}



#endif /* BASIC_BUFFER_H_ */
