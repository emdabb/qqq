/**
 * @file input_device.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jul 16, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef INPUT_DEVICE_H_
#define INPUT_DEVICE_H_

#include <qqq.h>
#include <core/event.h>

namespace qqq {

__declare_aligned(struct, 16) InputEventArgs {
  int type;
  real x[16];
};

struct InputDeviceType {
  enum {
    Unknown = 0,   //!< Unknown
    Joystick,      //!< Joystick
    Linear,        //!< Linear
    Angular,       //!< Angular
    Light,         //!< Light
    Proximity,     //!< Proximity
    Magnetic,      //!< Magnetic
    Location,      //!< Location
    MaxInputDevices		      //!< MaxInputDevices
  };
};

struct IInputDevice {
  virtual ~IInputDevice() {}
  virtual bool create() = 0;
  virtual void destroy() = 0;
  virtual void setAlpha(real const&) = 0;
  virtual void setSampleTime(int) = 0;
  virtual bool start() = 0;
  virtual void stop() = 0;
  virtual int  type() const = 0;

  EventHandler<InputEventArgs> OnInputDetected;
};

}



#endif /* INPUT_DEVICE_H_ */
