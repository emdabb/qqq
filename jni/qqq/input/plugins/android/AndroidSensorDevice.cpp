/**
 * @file android_sensor.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jul 16, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include <qqq.h>
#if defined(__ANDROID__)
#include <cassert>
#include <input/BaseInputDevice.h>
#include <android/sensor.h>

namespace qqq {
class AndroidSensorDevice: public BaseInputDevice {
public:
    ASensorManager* mSensorManager;
    ASensorEventQueue* mEventQueue;
    ASensorRef mSensorRef;
    int mType;
    int mInputType;
    bool mIsRrunning;
protected:
    static int callback(int fd, int events, void* data) {
        AndroidSensorDevice* my = static_cast<AndroidSensorDevice*>(data);
        my->onSensorChanged();
        return 1;
    }

    int input_type_from_sensor_type(int stype) {
        switch (stype) {
        case ASENSOR_TYPE_ACCELEROMETER:
            return InputDeviceType::Linear;
        case ASENSOR_TYPE_GYROSCOPE:
            return InputDeviceType::Angular;
        case ASENSOR_TYPE_LIGHT:
            return InputDeviceType::Light;
        case ASENSOR_TYPE_PROXIMITY:
            return InputDeviceType::Light;
        case ASENSOR_TYPE_MAGNETIC_FIELD:
            return InputDeviceType::Magnetic;
        default:
            return InputDeviceType::Unknown;
        }
    }

    void onSensorChanged() {
        ASensorEvent event;
        int numEvents = 0;
        while ((numEvents = ASensorEventQueue_getEvents(mEventQueue, &event, 1))
                > 0) {
            //mData.time = event.timestamp / 1000;
            InputEventArgs args;
            memset(&args.x[0], 0, sizeof(float) * 6);
            args.type = mInputType;

            switch (mInputType) {
            case InputDeviceType::Linear:
                args.x[0] = event.acceleration.x;
                args.x[1] = event.acceleration.y;
                args.x[2] = event.acceleration.z;
                break;
            case InputDeviceType::Angular:
                args.x[0] = event.acceleration.azimuth;
                args.x[1] = event.acceleration.pitch;
                args.x[2] = event.acceleration.roll;
                break;
            case InputDeviceType::Light:
                args.x[0] = event.light;
                break;
            case InputDeviceType::Proximity:
                args.x[0] = event.distance;
                break;
            case InputDeviceType::Magnetic:
                args.x[0] = event.magnetic.x;
                args.x[1] = event.magnetic.y;
                args.x[2] = event.magnetic.z;
                break;
            default:
                memcpy(&args.x[0], event.data, sizeof(float) * 4);
                break;
            }

            OnInputDetected(args);
        }
    }

public:
    AndroidSensorDevice(const int stype) {
        mSensorManager = ASensorManager_getInstance();
        mType = stype;
        mEventQueue = NULL;
        mSensorRef = NULL;
        mIsRrunning = false;
        mInputType = input_type_from_sensor_type(stype);
    }
    virtual ~AndroidSensorDevice() {

    }
    virtual bool create() {
        ALooper* looper = ALooper_forThread();
        if(!looper) {
            looper = ALooper_prepare(ALOOPER_PREPARE_ALLOW_NON_CALLBACKS);
        }
        mEventQueue = ASensorManager_createEventQueue(mSensorManager, looper, 0, callback, this);
        mSensorRef = ASensorManager_getDefaultSensor(mSensorManager, mType);
        setSampleTime(20);
        return (mSensorRef != NULL);
    }
    virtual void destroy() {
        stop();
        mSensorRef = NULL;
        if(mEventQueue) {
            ASensorManager_destroyEventQueue(mSensorManager, mEventQueue);
            mEventQueue = NULL;
        }
    }
    virtual void setAlpha(real const&) {

    }
    virtual void setSampleTime(int dt) {
        if(mSensorRef) {
            ASensorEventQueue_setEventRate(mEventQueue, mSensorRef, dt * 1000);
        }
    }
    virtual bool start() {
        if(mSensorRef) {
            mIsRrunning = ASensorEventQueue_enableSensor(mEventQueue, mSensorRef) >= 0;
        }
        return (mIsRrunning != false);
    }
    virtual void stop() {
        if(mSensorRef) {
            ASensorEventQueue_disableSensor(mEventQueue, mSensorRef);
        }
        mIsRrunning = false;
    }
    virtual int type() const {
        return mType;
    }

}
;

extern "C" void qqqCreateAndroidSensorDevice(IInputDevice** ppdevice,
        const int stype) {
    *ppdevice = new AndroidSensorDevice(stype);
}
} // qqq
#endif
