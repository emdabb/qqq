/*
 * input_device_base.h
 *
 *  Created on: Jul 30, 2014
 *      Author: miel
 */

#ifndef INPUT_DEVICE_BASE_H_
#define INPUT_DEVICE_BASE_H_

#include <input/input_device.h>

namespace qqq {

class BaseInputDevice : public IInputDevice {
public:
    virtual bool create() { return false; }
    virtual void destroy() {}
    virtual bool start() { return false; }
    virtual void update(int) {}
    virtual void stop() {}
    virtual const int getType() const { return InputDeviceType::Unknown; }
    virtual void setSampleTime(int) {}
    virtual void setAlpha(real const&) {}
};

}



#endif /* INPUT_DEVICE_BASE_H_ */
