/**
 * @file types.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 5, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef QQQ_TYPES_H_
#define QQQ_TYPES_H_

#include <stdint.h>
#include <stddef.h>
#include <limits>
#include <iostream>

#include "qqq_config.h"

#ifndef NULL
#ifdef __cplusplus
#define NULL 0
#else
#define NULL (char*)0
#endif
#endif

#if defined(_MSC_VER)
#define __aligned_type(x) __declspec(align(x))
#else
#if defined(__GNUC__)
#define __aligned_type(x) __attribute__ ((aligned(x)))
#endif
#endif

#define __declare_aligned(t,x) t __aligned_type(x)


#if QQQ_PRECISION == QQQ_PRECISION_FIXED
#include <core/fixed.h>

namespace qqq {
typedef fixed<int32_t, 16> fx32;
typedef fixed<int16_t,  8> fx16;
typedef fixed<int8_t,   4> fx8;
typedef fx32 		real;
#elif QQQ_PRECISION == QQQ_PRECISION_SINGLE
namespace qqq {
#ifndef _MSC_VER
typedef float		__declare_aligned(real, 4);
#else
typedef float 		real;
#endif


#elif QQQ_PRECISION == QQQ_PRECISION_DOUBLE
namespace qqq {
typedef double		__declare_alignd(real, 8);
#else
#endif
typedef int8_t			 byte_t;
typedef uint16_t		 half;
typedef real* __restrict real_ptr;

typedef std::istream InputStream;
typedef std::ostream OutputStream;

__declare_aligned(struct, 8) short4 { int16_t x,y,z,w; };

} // qqq

//__declare_aligned(double, 16) aligned_double_t;
//
//__declare_aligned(struct, CACHE_LINE) tagALIGNEDSTRUCT
//{
//    /*STRUCT MEMBERS GO HERE*/
//}aligned_struct_t;

template<typename _Tx>
struct float_length {
  const static std::streamsize value = std::numeric_limits<_Tx>::is_signed + std::numeric_limits<_Tx>::is_specialized
      + std::numeric_limits<_Tx>::digits10 * 2;
};

template<typename _Tx>
struct int_length {
  const static std::streamsize value = std::numeric_limits<_Tx>::digits + std::numeric_limits<_Tx>::digits10 + 1;
};

// Return a dummy value when we're not dealing with numbers
template<typename _Tx>
struct type_length {
  const static std::streamsize value = 14;
};

// floating point data types
template<> struct type_length<float> {
  const static std::streamsize value = float_length<float>::value;
};
template<> struct type_length<double> {
  const static std::streamsize value = float_length<double>::value;
};
template<> struct type_length<long double> {
  const static std::streamsize value = float_length<long double>::value;
};

// integer data types
template<> struct type_length<short> {
  const static std::streamsize value = int_length<short>::value;
};
template<> struct type_length<int> {
  const static std::streamsize value = int_length<int>::value;
};
template<> struct type_length<long> {
  const static std::streamsize value = int_length<long>::value;
};
template<> struct type_length<long long> {
  const static std::streamsize value = int_length<long long>::value;
};
template<> struct type_length<char> {
  const static std::streamsize value = int_length<char>::value;
};
template<> struct type_length<wchar_t> {
  const static std::streamsize value = int_length<wchar_t>::value;
};
template<> struct type_length<signed char> {
  const static std::streamsize value = int_length<signed char>::value;
};
template<> struct type_length<bool> {
  const static std::streamsize value = int_length<bool>::value;
};

template<> struct type_length<unsigned short> {
  const static std::streamsize value = int_length<unsigned short>::value;
};
template<> struct type_length<unsigned int> {
  const static std::streamsize value = int_length<unsigned int>::value;
};
template<> struct type_length<unsigned long> {
  const static std::streamsize value = int_length<unsigned long>::value;
};
template<> struct type_length<unsigned long long> {
  const static std::streamsize value = int_length<unsigned long long>::value;
};
template<> struct type_length<unsigned char> {
  const static std::streamsize value = int_length<unsigned char>::value;
};
/**
 *
 * Type promotion templates.
 *
 */
template<typename B, typename U = void>
 struct promote_type {
//#ifdef _MSC_VER
//   typedef promote_typetype_not_specialized_for_thistype type;
//#endif
 };

 template<typename U>
 struct promote_type<int8_t, U> {
   typedef int16_t type;
 };

 template<typename U>
 struct promote_type<uint8_t, U> {
   typedef uint16_t type;
 };

 template<typename U>
 struct promote_type<int16_t, U> {
   typedef int32_t type;
 };

 template<typename U>
 struct promote_type<uint16_t, U> {
   typedef uint32_t type;
 };

 template<typename U>
 struct promote_type<int32_t, U> {
   typedef int64_t type;
 };

 template<typename U>
 struct promote_type<uint32_t, U> {
   typedef uint64_t type;
 };

#endif /* QQQ_TYPES_H_ */
