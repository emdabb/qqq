/*
 * LuaScript.cpp
 *
 *  Created on: Jul 29, 2013
 *      Author: miel
 */

#include <engine/LuaScript.h>
#include <engine/LuaHelper.h>
#include <engine/LuaVirtualMachine.h>
#include <engine/LuaRestoreStack.h>
#include <engine/LuaThis.h>

#include <assert.h>

#define LUA_CHECKED(vm)		lua_State* state = (lua_State*)(vm); \
                            if(vm.isValid())

using namespace qqq;

LuaScript::LuaScript(LuaVirtualMachine& vm)
    : mVM(vm), mNumMethods(0), mThisRef(0), mNumArgs(0), mFunctionName(NULL)
{
  DEBUG_METHOD();
  LUA_CHECKED(vm)
  {
    lua_newtable(state);
    mThisRef = luaL_ref(state, LUA_REGISTRYINDEX);
    LuaRestoreStack rs(vm);
    lua_rawgeti(state, LUA_REGISTRYINDEX, mThisRef);
    lua_pushlightuserdata(state, static_cast<void*>(this));
    lua_rawseti(state, -2, 0);
  }
}

LuaScript::~LuaScript()
{
  LuaRestoreStack rs(mVM);
  LUA_CHECKED(mVM)
  {
    lua_rawgeti(state, LUA_REGISTRYINDEX, mThisRef);
    lua_pushnil(state);
    lua_rawseti(state, -2, 0);
  }
}

bool LuaScript::compile(const char* buffer, size_t len)
{
  DEBUG_METHOD();
  assert(buffer && "LuaScript.compile() => buffer == NULL.");
  assert(len && "LuaScript.compile() => len == 0.");
  assert(mVM.isValid() && "LuaScript.compile() => VM not valid.");

  LuaThis luaThis(mVM, mThisRef);
  return mVM.runBuffer(buffer, len);
}

int LuaScript::registerFunction(const char* fnname)
{
  assert(fnname && "LuaScript.registerFunction() => fn_name == NULL.");
  assert(mVM.isValid() && "LuaScript.registerFunction => VM not valid.");

  int methodIndex = -1;

  LuaRestoreStack rs(mVM);

  LUA_CHECKED(mVM) {
    methodIndex = ++mNumMethods;
    /**
     * Add a function to the 'this' table.
     */
    lua_rawgeti(state, LUA_REGISTRYINDEX, mThisRef);
    /**
     * Push the functions and parameters.
     */
    lua_pushstring(state, fnname);
    lua_pushnumber(state, (lua_Number) methodIndex);
    lua_pushcclosure(state, LuaHelper::luaCallback, 1);
    lua_settable(state, -3);
  }
  return methodIndex;
}

bool LuaScript::selectScriptFunction(const char* fnname)
{
  assert(fnname);
  assert(mVM.isValid());
  LUA_CHECKED(mVM)
  {
    /**
     * Lookup function name.
     */
    lua_rawgeti(state, LUA_REGISTRYINDEX, mThisRef);
    lua_pushstring(state, fnname);
    lua_rawget(state, -2);
    lua_remove(state, -2);
    /**
     * Put the 'this' table back.
     */
    lua_rawgeti(state, LUA_REGISTRYINDEX, mThisRef);
    /**
     * Check if the function is valid.
     */
    if (!lua_isfunction(state, -2))
    {
      lua_pop(state, 2);
    } else
    {
      mNumArgs = 0;
      mFunctionName = fnname;
      return true;
    }
  }
  return false;
}

bool LuaScript::hasFunction(const char* fnname)
{
  assert(fnname);
  assert(mVM.isValid());
  LuaRestoreStack rs(mVM);

  LUA_CHECKED(mVM)
  {
    lua_rawgeti(state, LUA_REGISTRYINDEX, mThisRef);
    lua_pushstring(state, fnname);
    lua_rawget(state, -2);
    lua_remove(state, -2);
    if (lua_isfunction(state, -1))
    {
      return true;
    }
  }
  return false;
}

int LuaScript::numMethods() const
{
  return mNumMethods;
}

void LuaScript::addParam(const char* value)
{
  assert(value);
  assert(mVM.isValid());

  LUA_CHECKED(mVM)
  {
    lua_pushstring(state, value);
    mNumArgs++;
  }
}

void LuaScript::addParam(int value)
{
  assert(mVM.isValid());
  LUA_CHECKED(mVM)
  {
    lua_pushnumber(state, (lua_Number) value);
    mNumArgs++;
  }
}

void LuaScript::addParam(float value)
{
  assert(mVM.isValid());
  LUA_CHECKED(mVM)
  {
    lua_pushnumber(state, (lua_Number) value);
    mNumArgs++;
  }
}

bool LuaScript::go(int returns)
{
  assert(mVM.isValid());
  bool success = mVM.call(mNumArgs + 1, returns);
  if (success && returns)
  {
    handleReturns(mVM, mFunctionName);
    lua_pop((lua_State* )mVM, returns);
  }
  return success != false;
}

//int LuaScript::scriptCall(LuaVirtualMachine& vm, int id)
//{
//  ScriptEvent e;
//  e.L = vm;
//  e.id = id;
//  e.Msg = NULL;
//  OnScriptCall(e);
//  return 0;
//}
//
//void LuaScript::handleReturns(LuaVirtualMachine& vm, const char* msg)
//{
//  ScriptEvent e;
//  e.L = vm;
//  e.id = -1;
//  e.Msg = msg;
//  OnScriptReturn(e);
//}
