/*
 * LuaThis.cpp
 *
 *  Created on: Jul 29, 2013
 *      Author: miel
 */

#include <engine/LuaThis.h>
#include <engine/LuaVirtualMachine.h>

using namespace qqq;

LuaThis::LuaThis(LuaVirtualMachine& vm, int ref) : mOldRef(0), mVM(vm) {
    lua_State* state = (lua_State*)vm;
    if(mVM.isValid()) {
        /**
         * save the old 'this' table...
         */
        lua_getglobal(state, "this");
        mOldRef = luaL_ref(state, LUA_REGISTRYINDEX);
        /**
         * And replace with our own...
         */
        lua_rawgeti(state, LUA_REGISTRYINDEX, ref);
        lua_setglobal(state, "this");
    }
}

LuaThis::~LuaThis() {
    lua_State* state = (lua_State*)mVM;
    if(mOldRef > 0 && mVM.isValid()) {
        lua_rawgeti(state, LUA_REGISTRYINDEX, mOldRef);
        lua_setglobal(state, "this");
        luaL_unref(state, LUA_REGISTRYINDEX, mOldRef);
    }
}


