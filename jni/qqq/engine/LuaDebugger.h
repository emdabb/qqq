/*
 * LuaDebugger.h
 *
 *  Created on: Jul 29, 2013
 *      Author: miel
 */

#ifndef LUADEBUGGER_H_
#define LUADEBUGGER_H_

namespace qqq {

class LuaVirtualMachine;

class LuaDebugger {
    int mCountMask;
    LuaVirtualMachine& mVM;
public:
    explicit LuaDebugger(LuaVirtualMachine&);
    virtual ~LuaDebugger();
    void setHook(int);
    void setCount(int);
    void error(int);
};

}

#endif /* LUADEBUGGER_H_ */
