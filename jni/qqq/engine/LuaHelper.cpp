#include "LuaHelper.h"
#include "LuaScript.h"
#include <string.h>
#include <iostream>

using namespace qqq;

int LuaHelper::printMessage(lua_State* state) {
    //DEBUG_METHOD();
    const char* msg = lua_tostring(state, 1);
    lua_Debug ar;
    memset(&ar, 0, sizeof(ar));
    lua_getstack(state, 1, &ar);
    lua_getinfo(state, "Snl", &ar);
    std::cout << "lua: " << msg << " @ " << ar.source << ":" << ar.currentline
            << std::endl;
    return 0;
}

void LuaHelper::luaHookCall(lua_State* lua) {
    DEBUG_METHOD();
    DEBUG_MESSAGE("---call stack---");
    lua_Debug ar;
    // Look at call stack
    for (int iLevel = 0; lua_getstack(lua, iLevel, &ar) != 0; ++iLevel) {
        if (lua_getinfo(lua, "Snlu", &ar) != 0) {
            printf("%d %s %s %d @%d %s\n", iLevel, ar.namewhat, ar.name,
                    ar.nups, ar.linedefined, ar.short_src);
        }
    }
}

void LuaHelper::luaHookCount(lua_State* lua) {
    luaHookLine(lua);
}

void LuaHelper::luaHook(lua_State* lua, lua_Debug* ar) {
    DEBUG_METHOD();
    switch (ar->event) {
    case LUA_HOOKCALL:
        /** case LUA_HOOKTAILCALL: */
        luaHookCall(lua);
        break;
    case LUA_HOOKRET:

        luaHookRet(lua);
        break;
    case LUA_HOOKLINE:
        luaHookLine(lua);
        break;
    case LUA_HOOKCOUNT:
        luaHookCount(lua);
        break;
    }
}

void LuaHelper::luaHookRet(lua_State*) {
    DEBUG_METHOD();
}

void LuaHelper::luaHookLine(lua_State* lua) {
    DEBUG_METHOD();
    lua_Debug ar;
    lua_getstack(lua, 0, &ar);
    if (lua_getinfo(lua, "Sl", &ar) != 0) {
        DEBUG_MESSAGE("exe %s on line %d", ar.short_src, ar.currentline);
    }
}

int LuaHelper::luaCallback(lua_State* lua) {
    //DEBUG_METHOD();
    int numberIndex = lua_upvalueindex(1);
    //DEBUG_VALUE_AND_TYPE_OF(numberIndex);
    int returnsOnStack = 0;
    bool success = false;
    if (lua_istable(lua, 1)) {
        //DEBUG_INFO("Argument is vtable.");
        lua_rawgeti(lua, 1, 0);
        if (lua_islightuserdata(lua, -1)) {
            //DEBUG_INFO("Argument is light user data.");
            LuaScript* pThis = (LuaScript*) lua_touserdata(lua, -1);
            //DEBUG_VALUE_AND_TYPE_OF(pThis);
            int methodIndex = (int) lua_tonumber(lua, numberIndex);
            //DEBUG_VALUE_AND_TYPE_OF(methodIndex);
            assert(!(methodIndex > pThis->numMethods()));
            lua_remove(lua, 1);
            lua_remove(lua, -1);

            ScriptEvent ev;
            ev.L 	= lua;
            ev.id 	= methodIndex;
            ev.Msg 	= NULL;

            //pThis->OnScriptCall(ev);

            returnsOnStack = pThis->scriptCalling(lua, methodIndex);
            success = true;
        }
    }
    if (!success) {
        lua_pushstring(lua,
                "luaCallback() => Failed to call the class function.");
        lua_error(lua);
    }
    return returnsOnStack;
}

