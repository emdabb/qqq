/*
 * LuaRestoreStack.h
 *
 *  Created on: Jul 29, 2013
 *      Author: miel
 */

#ifndef LUARESTORESTACK_H_
#define LUARESTORESTACK_H_

namespace qqq {

class LuaVirtualMachine;

class LuaRestoreStack {
    LuaVirtualMachine& mVM;
    int mTop;
public:
    explicit LuaRestoreStack(LuaVirtualMachine&);
    virtual ~LuaRestoreStack();
};

}

#endif /* LUARESTORESTACK_H_ */
