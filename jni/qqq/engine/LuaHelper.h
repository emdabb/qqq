/*
 * LuaHelper.h
 *
 *  Created on: Jul 29, 2013
 *      Author: miel
 */

#ifndef LUAHELPER_H_
#define LUAHELPER_H_

#include <assert.h>
#include <core/debug_log.h>
extern "C" {
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
}
namespace qqq {

struct LuaHelper {

    static int printMessage(lua_State*);

    static void luaHookCall(lua_State*);

    static void luaHookRet(lua_State*);

    static void luaHookLine(lua_State*);

    static void luaHookCount(lua_State*);

    static void luaHook(lua_State*, lua_Debug*);

    static int luaCallback(lua_State* lua);
};

}

#endif /* LUAHELPER_H_ */
