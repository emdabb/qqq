/*
 * LuaThis.h
 *
 *  Created on: Jul 29, 2013
 *      Author: miel
 */

#ifndef LUATHIS_H_
#define LUATHIS_H_

extern "C" {
#include <lua.h>
}

namespace qqq
{

class LuaVirtualMachine;

class LuaThis {
    int mOldRef;
    LuaVirtualMachine& mVM;
public:
    LuaThis(LuaVirtualMachine&, int);
    virtual ~LuaThis();

};

}

#endif /* LUATHIS_H_ */
