/*
 * win32_application.cpp
 *
 *  Created on: Jun 26, 2014
 *      Author: miel
 */

#include "win32_application.h"

#if defined(__WIN32__) || defined (__WP8__)

#include "pch.h"

namespace qqq {

	ref class App sealed : public Windows::ApplicationModel::Core::IFrameworkView {
	public:
	};

}

#endif
