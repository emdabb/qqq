/*
 * win32_application.h
 *
 *  Created on: Jun 26, 2014
 *      Author: miel
 */

#ifndef WIN32_APPLICATION_H_
#define WIN32_APPLICATION_H_

#include <qqq_platform.h>

#if defined(__WIN32__)
#include <base/application_base.h>
#include <map>
#include <Windows.h>
namespace qqq {

  class win32_application : public ApplicationBase {
    int mWidth, mHeight;
    int mPositionX, mPositionY;
    const char* mTitle;
    bool mFullscreen;
    struct win32;
    win32* pimpl;
    static std::map<HWND, IApplication*> mApplicationMap;
  public:
      static LRESULT WINAPI winproc( HWND wnd, UINT msg, WPARAM wParam, LPARAM lParam );

    virtual bool create(const char* gapi, const char* aapi) {
      return create_window();
    }

    bool create_window();
  public:
    virtual void resize(uint16_t w, uint16_t h);
    virtual void set_position(uint16_t x, uint16_t y);
    virtual void set_mouse_relative(bool);
    virtual void* get_display() const;
    virtual void* get_surface() const;
    virtual void start();
    virtual void stop();
    virtual void pause();
    virtual void resume();
};

} // qqq


#endif
#endif /* WIN32_APPLICATION_H_ */
