/**
 * @file application_x11.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 5, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef APPLICATION_X11_H_
#define APPLICATION_X11_H_

#include <qqq.h>
#include <core/runnable.h>
#include <core/threadpool.h>
#include <core/timer.h>
#include <base/application_base.h>

#include <stdlib.h>
#include <gfx/graphics_context.h>
#include <gfx/graphics_initializer.h>

namespace qqq {

struct IThread;

class X11Application: public ApplicationBase {
	struct x11;
	x11* mX11;
	bool is_running;
	const char* name;

	bool skip_next_move;
	int last_mousex, last_mousey;
	bool is_mouse_relative;
	int width, height;
	int xpos, ypos;
	///GraphicsInitializer mGraphicsInitializer;

public:
	X11Application();

	virtual ~X11Application();

	virtual void preCreate();

	virtual void postCreate();

	virtual bool createWindow();

	void destroyWindow();

	void createCursor();

	virtual void start();
	virtual void stop();
	virtual void pause();
	virtual void resume();

	virtual void* getDisplay() const;
	virtual void* getSurface() const;

	virtual void setMouseRelative(bool);

	virtual void setFullscreen(bool);

	void configured(int16_t x, int16_t y, uint16_t w, uint16_t h);

	void processInput();

	void postMouseScroll(int x, int y, int dir);
	void internal_mouse_moved(int x, int y);
	void internal_mouse_pressed(int x, int y, int id, int);
	void internal_mouse_released(int x, int y, int id, int);
	void internal_key_pressed(int);
	void internal_key_released(int);
	int translate_key(int keysym);

	virtual void resize(uint16_t w, uint16_t h);
	virtual void setPosition(uint16_t x, uint16_t y);

	virtual void getWidth(uint32_t*);
	virtual void getHeight(uint32_t*);

	virtual int run();

};
} // qqq
#endif /* APPLICATION_X11_H_ */
