/**
 * @file application_x11.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 9, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include "x11_application.h"
#include <gfx/graphics_device.h>
#include <audio/audio_device.h>
#include <core/factory.h>

using namespace qqq;

#if defined(__LINUX__)

#include <X11/X.h>
#include <X11/Xutil.h>
#include <X11/Xlib.h>
#include <X11/keysym.h>
#include <X11/XKBlib.h>
#include <X11/extensions/XInput.h>
#include <linux/joystick.h>
#include <linux/gameport.h>

struct X11Application::x11 {
	Window win;
	Display* dpy;
	Cursor blank_cursor;
	Atom destroyWindowAtom;
};

X11Application::X11Application() :
		mX11(new x11), is_running(false),  name(NULL), skip_next_move(false), last_mousex(
				0), last_mousey(0), is_mouse_relative(false), width(0), height(
				0), xpos(0), ypos(0)

{
	mX11->blank_cursor = 0;
	mX11->win = None;
	mX11->dpy = NULL;
	name = "X11Application";
}

X11Application::~X11Application() {
	//_format = (graphics_initializer){ 5, 6, 5, 0, 24, 8, false };
}

void X11Application::start() {
	if (!is_running) {
		run();
	}
	is_running = true;
}
void X11Application::stop() {
	is_running = false;
}
void X11Application::pause() {
	//is_paused = true;
}
void X11Application::resume() {
	//is_paused = false;
}

void X11Application::setFullscreen(bool a) {
	mGraphicsInitializer.fullscreen(a);
}

void* X11Application::getDisplay() const {
	return mX11->dpy;
}
void* X11Application::getSurface() const {
	return (void*) mX11->win;
}

void X11Application::setMouseRelative(bool val) {
	is_mouse_relative = val;
	skip_next_move = true;
	if (mX11->win && mX11->dpy) {
		if (is_mouse_relative) {
			XDefineCursor(mX11->dpy, mX11->win, mX11->blank_cursor);
		} else {
			XUndefineCursor(mX11->dpy, mX11->win);
		}
	}
}

void X11Application::preCreate() {
	DEBUG_METHOD();
	//mGraphicsInitializer = (GraphicsInitializer){ 5, 6, 5, 0, 24, 8, false };
	mGraphicsInitializer
	.pixelFormat(SurfaceFormat::Color)
	.depthBits(24)
	.stencilBits(8)
	.fullscreen(false);
	createWindow();
	createCursor();
}

void X11Application::postCreate() {

}

void X11Application::createCursor() {
	DEBUG_METHOD();
	static char cursorData[1] = {0};
	XColor cursorColor;
	Pixmap cursorPixmap = XCreateBitmapFromData(mX11->dpy, mX11->win, cursorData, 1, 1);
	mX11->blank_cursor = XCreatePixmapCursor(mX11->dpy, cursorPixmap, cursorPixmap,
			&cursorColor, &cursorColor, 0, 0);
	XFreePixmap(mX11->dpy, cursorPixmap);
}

void X11Application::internal_mouse_pressed(int x, int y, int id,
		int buttonId) {
	MouseEventArgs args = { (int16_t)x, (int16_t)y, (int16_t)id, (int16_t)buttonId };
	OnMousePressed(args);
}

void X11Application::internal_mouse_released(int x, int y, int pointerId,
		int buttonId) {
	MouseEventArgs args = { (short)x, (short)y, (short)pointerId, (short)buttonId };
	OnMouseReleased(args);
}

void X11Application::internal_key_pressed(int in) {
	KeyEventArgs args = { (uint16_t)in };
	//args.code = (uint16_t)in;
	OnKeyPressed(args);
}
void X11Application::internal_key_released(int in) {
	KeyEventArgs args = { (uint16_t)in };
	//args.code = (uint16_t)in;
	OnKeyReleased(args);
}

void X11Application::internal_mouse_moved(int x, int y) {
	if (skip_next_move) {
		last_mousex = x;
		last_mousey = y;
		skip_next_move = false;
		return;
	}
	//MouseEventArgs ee;
	if (is_mouse_relative) {
		// We check to see if we are at the warp-point, instead of using SkipNextMove
		//  since it seems on X11 that user mouse-move commands can get processed before the skip, getting things confused
		// We still use SkipNextMove when the DifferenceMouse is turned on to avoid the first jump
		if (x == width / 2 && y == height / 2) {
			last_mousex = x;
			last_mousey = y;
			return;
		}
		int dx = x - last_mousex, dy = y - last_mousey;
		XWarpPointer(mX11->dpy, None, mX11->win, 0, 0, 0, 0, width / 2,
				height / 2);
		x = dx;
		y = dy;
	}

	MouseEventArgs args = { (short)x, (short)y, 0, 0 };
	OnMouseMove(args);
}

void X11Application::postMouseScroll(int x, int y, int dir) {
	MouseEventArgs args = { (short)x, (short)y, 0, (short int)(127 + dir) };
	OnMousePressed(args);
}

void X11Application::resize(uint16_t w, uint16_t h) {
	DEBUG_METHOD();
	width = w;
	height = h;
	if (mX11->win != None) {
		XResizeWindow(mX11->dpy, mX11->win, width, height);
		XFlush(mX11->dpy);
	}
	WindowResizeArgs args = {w, h};
	OnResize(args);
}

void X11Application::setPosition(uint16_t x, uint16_t y) {
	DEBUG_METHOD();
	xpos = x;
	ypos = y;
	if (mX11->win != None) {
		XMoveWindow(mX11->dpy, mX11->win, xpos, ypos);
		XFlush(mX11->dpy);
	}
}

void X11Application::configured(int16_t x, int16_t y, uint16_t w, uint16_t h) {
	DEBUG_METHOD();
	DEBUG_VALUE_AND_TYPE_OF(x);
	DEBUG_VALUE_AND_TYPE_OF(y);
	DEBUG_VALUE_AND_TYPE_OF(w);
	DEBUG_VALUE_AND_TYPE_OF(h);
	//uint32_t wh = ((uint32_t)w << 16) | h;
	WindowResizeArgs args = {w, h};

	OnResize(args);
}

void X11Application::destroyWindow() {
	if(mX11->blank_cursor){
		XFreeCursor(mX11->dpy, mX11->blank_cursor);
		mX11->blank_cursor=None;
	}

	// Just to be safe
	if(mX11->dpy){
		XSync(mX11->dpy,true);
	}

	//originalResolution();

	//originalEnv();

	if(mX11->win){
		XDestroyWindow(mX11->dpy,mX11->win);
		mX11->win=None;
	}

	if(mX11->dpy){
		XCloseDisplay(mX11->dpy);
		mX11->dpy=None;
	}
}

bool X11Application::createWindow() {
	DEBUG_METHOD();
	mX11->dpy = XOpenDisplay(getenv("DISPLAY"));
	XVisualInfo* vinfo;
	int scrnum = XDefaultScreen(mX11->dpy);
	XVisualInfo info;
	if (!XMatchVisualInfo(mX11->dpy, scrnum, 24, TrueColor, &info)) {
		return false;
	}
	info.screen = 0;
	int n = 0;
	vinfo = XGetVisualInfo(mX11->dpy, VisualAllMask, &info, &n);
	if (vinfo == None) {
		return false;
	}
	int x, y, w, h;
	x = ypos;
	y = xpos;
	w = width;
	h = height;
	Bool fullscreen = mGraphicsInitializer.fullscreen() ? True : False;
	Window root = DefaultRootWindow(mX11->dpy);
	if (fullscreen) {
		x = y = 0;
		w = DisplayWidth(mX11->dpy, DefaultScreen(mX11->dpy));
		h = DisplayHeight(mX11->dpy, DefaultScreen(mX11->dpy));
	}
	// Set the window attributes
	XSetWindowAttributes attrs;
	unsigned long mask;
	attrs.background_pixel = 0;
	attrs.border_pixel = 0;
	attrs.colormap = XCreateColormap(mX11->dpy, root, vinfo->visual, AllocNone);
	attrs.event_mask = StructureNotifyMask | ExposureMask | KeyReleaseMask
	| KeyPressMask | PointerMotionMask | ButtonPressMask | ButtonReleaseMask | ExposureMask;
	attrs.override_redirect = mGraphicsInitializer.fullscreen() ? True : False;
	mask = CWBackPixel | CWBorderPixel | CWColormap | CWEventMask
	| CWOverrideRedirect;
	mX11->win = XCreateWindow(mX11->dpy, root, x, y, w, h, 0, vinfo->depth, InputOutput,
			vinfo->visual, mask, &attrs);


	XFree(vinfo);
	/* set hints and properties */
	{
		XSizeHints sizehints;
		sizehints.x = x;
		sizehints.y = y;
		sizehints.width = sizehints.min_width = sizehints.max_width = w;
		sizehints.height = sizehints.min_height = sizehints.max_height = h;
		sizehints.flags = PPosition | USSize |PMinSize | PMaxSize;
		XSetNormalHints(mX11->dpy, mX11->win, &sizehints);
		XSetStandardProperties(mX11->dpy, mX11->win, name, name, None, (char **) NULL, 0,
				&sizehints);
	}
	XMapWindow(mX11->dpy, mX11->win);
	XFlush(mX11->dpy);
	/* QQQ: enforce window positioning */
	XMoveWindow(mX11->dpy, mX11->win, x, y);


	mX11->destroyWindowAtom=XInternAtom(mX11->dpy, "WM_DELETE_WINDOW", false);
	XSetWMProtocols(mX11->dpy,mX11->win,&mX11->destroyWindowAtom,1);

	return true;
}

void X11Application::processInput() {
	XEvent ev;
	KeySym key;

	while (mX11->dpy && XPending(mX11->dpy)) {
		XNextEvent(mX11->dpy, &ev);
		switch (ev.type) {
		default:
			break;
		case ConfigureNotify:
			configured(ev.xconfigure.x, ev.xconfigure.y, ev.xconfigure.width,
					ev.xconfigure.height);
			break;
		case MotionNotify:
			internal_mouse_moved(ev.xmotion.x, ev.xmotion.y);
			break;
		case ButtonPress:
			switch (ev.xbutton.button) {
			case Button1:
				internal_mouse_pressed(ev.xbutton.x, ev.xbutton.y, 0, 0);
				break;
			case Button2:
				internal_mouse_pressed(ev.xbutton.x, ev.xbutton.y, 0, 1);
				break;
			case Button3:
				internal_mouse_pressed(ev.xbutton.x, ev.xbutton.y, 0, 2);
				break;
			case Button4:
				postMouseScroll(ev.xbutton.x, ev.xbutton.y, 1);
				break;
			case Button5:
				postMouseScroll(ev.xbutton.x, ev.xbutton.y, -1);
				break;
			default:
				break;
			}
			break;

		case ButtonRelease:
			switch (ev.xbutton.button) {
			case Button1:
				internal_mouse_released(ev.xbutton.x, ev.xbutton.y, 0, 0);
				break;
			case Button2:
				internal_mouse_released(ev.xbutton.x, ev.xbutton.y, 0, 1);
				break;
			case Button3:
				internal_mouse_released(ev.xbutton.x, ev.xbutton.y, 0, 2);
				break;
			case Button4:
				break;
			case Button5:
				break;
			default:
				break;
			}
			break;
		case KeyPress:
			key = XkbKeycodeToKeysym(mX11->dpy, ev.xkey.keycode, 0, 0);
			internal_key_pressed(translate_key(key));
			break;

		case KeyRelease:
			key = XkbKeycodeToKeysym(mX11->dpy, ev.xkey.keycode, 0, 0);
			internal_key_released(translate_key(key));
			break;
		case ResizeRequest:
			break;
		case ClientMessage:
			if(ev.xclient.data.l[0] == (long)mX11->destroyWindowAtom){
				stop();
				break;
			}
			break;

		}
	}

}

int X11Application::run() {
	DEBUG_METHOD();
	timer thread_timer;
	int32_t dt = (real) ((1000)) / (real) ((60));
	int32_t timeCurrentMs = 0;
	int32_t timeAccumulatedMs = dt;
	int32_t timeDeltaMs = 0;
	int32_t timeLastMs = thread_timer.elapsed();
	int32_t t = 0;
	is_running = true;

	while (is_running) {
		if (m_applet) {

			processInput();

			timeCurrentMs = thread_timer.elapsed();
			timeDeltaMs = timeCurrentMs - timeLastMs;
			timeAccumulatedMs += timeDeltaMs;
			while (timeAccumulatedMs >= dt) {

				m_applet->update(t, dt);
				timeAccumulatedMs -= dt;
				t += dt;
			}
			m_applet->draw();

			timeLastMs = timeCurrentMs;
		}
	}
	destroyWindow();
	return 0;
}

void X11Application::getWidth(uint32_t* val) {
	*val = width;
}

void X11Application::getHeight(uint32_t* val) {
	*val = height;
}

int X11Application::translate_key(int keysym) {

	switch (keysym) {
	case XK_Escape:
		return KeyEventArgs::Key_ESC;
	case XK_Pause:
		return KeyEventArgs::Key_PAUSE;
	case XK_Left:
		return KeyEventArgs::Key_LEFT;
	case XK_Right:
		return KeyEventArgs::Key_RIGHT;
	case XK_Up:
		return KeyEventArgs::Key_UP;
	case XK_Down:
		return KeyEventArgs::Key_DOWN;
	case XK_Shift_L:
	case XK_Shift_R:
		return KeyEventArgs::Key_SHIFT;
	case XK_Control_L:
	case XK_Control_R:
		return KeyEventArgs::Key_CTRL;
	case XK_Alt_L:
	case XK_Alt_R:
		return KeyEventArgs::Key_ALT;
	case XK_Super_L:
	case XK_Super_R:
		return KeyEventArgs::Key_SPECIAL;
	case XK_Return:
		return KeyEventArgs::Key_ENTER;
	case XK_Tab:
		return KeyEventArgs::Key_TAB;
	case XK_BackSpace:
		return KeyEventArgs::Key_BACKSPACE;
	case XK_Delete:
		return KeyEventArgs::Key_DELETE;
	case 32:
		return KeyEventArgs::Key_SPACE;
	default:
		break;
	}

	return keysym;
}

QQQ_C_API void QQQ_CALL hxCreateApplication(IApplication** ppapp) {
	*ppapp = new X11Application;
}
#endif
