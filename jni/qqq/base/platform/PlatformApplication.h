/**
 * @file PlatformApplication.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date May 13, 2015
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef JNI_QQQ_BASE_PLATFORM_PLATFORMAPPLICATION_H_
#define JNI_QQQ_BASE_PLATFORM_PLATFORMAPPLICATION_H_

#include <qqq_platform.h>

#if defined(__ANDROID__)
#include <base/platform/android/android_application.h>
namespace qqq {
typedef AndroidApplication PlatformApplication;
}
#elif defined(__LINUX__)
#include <base/platform/x11/x11_application.h>
namespace qqq {
typedef X11Application PlatformApplication;
}
#elif defined(__IOS__) || defined(__MACOSX__)
#include <base/platform/osx/OSXApplication.h>
    namespace qqq {
    typedef OSXApplication PlatformApplication;
    }
#endif

#endif /* JNI_QQQ_BASE_PLATFORM_PLATFORMAPPLICATION_H_ */
