/**
 * @file android_application.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jul 23, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include "android_application.h"
#if defined(__ANDROID__)
#include <android/window.h>
#include <android/sensor.h>

#include <base/applet.h>
#include <core/lockable.h>
#include <core/timer.h>
#include <core/debug_log.h>
#include <gfx/graphics_device.h>
#include <gfx/graphics_context.h>
#include <gfx/graphics_initializer.h>
#include <gfx/IGraphicsResource.h>
#include <audio/audio_device.h>
#include <content/platform/android/android_asset_manager.h>

using namespace qqq;

QQQ_C_API void qqqCreateAndroidSensorDevice(IInputDevice** ppdevice,
        const int stype);
QQQ_C_API void hxCreateGraphicsContext(IGraphicsContext**);
QQQ_C_API void hxCreateGraphicsDevice(IGraphicsDevice**);
QQQ_C_API void hxCreateAudioDevice(IAudioDevice**);

void AndroidApplication::onDestroy(ANativeActivity *activity) {

}
void AndroidApplication::onStart(ANativeActivity *activity) {
    DEBUG_METHOD();
    ANativeActivity_setWindowFlags(activity, AWINDOW_FLAG_KEEP_SCREEN_ON, 0);
    static_cast<AndroidApplication*>(activity->instance)->start();
}
void AndroidApplication::onStop(ANativeActivity *activity) {
    DEBUG_METHOD();
    static_cast<AndroidApplication*>(activity->instance)->stop();
}

void AndroidApplication::onPause(ANativeActivity *activity) {
    DEBUG_METHOD();
    static_cast<AndroidApplication*>(activity->instance)->pause();
}
void AndroidApplication::onResume(ANativeActivity *activity) {
    DEBUG_METHOD();
    static_cast<AndroidApplication*>(activity->instance)->resume();
}

void AndroidApplication::onNativeWindowCreated(ANativeActivity *activity,
        ANativeWindow *window) {
    DEBUG_METHOD();
    static_cast<AndroidApplication*>(activity->instance)->notifyWindowCreated(window);

}
void AndroidApplication::onNativeWindowDestroyed(ANativeActivity *activity,
        ANativeWindow *window) {
    DEBUG_METHOD();

}
void AndroidApplication::onNativeWindowResized(ANativeActivity *activity,
        ANativeWindow *window) {

    DEBUG_METHOD();
    static_cast<AndroidApplication*>(activity->instance)->notifyWindowResized(window);

}
void AndroidApplication::onInputQueueCreated(ANativeActivity *activity,
        AInputQueue *queue) {
    DEBUG_METHOD();
    static_cast<AndroidApplication*>(activity->instance)->notifyQueueCreated(queue);

}
void AndroidApplication::onInputQueueDestroyed(ANativeActivity *activity,
        AInputQueue *queue) {
    DEBUG_METHOD();
    static_cast<AndroidApplication*>(activity->instance)->notifyQueueDestroyed(queue);
}
void AndroidApplication::onConfigurationChanged(ANativeActivity* activity) {
    DEBUG_METHOD();

}

AndroidApplication::AndroidApplication()
: mThread(NULL)
, mInputQueue(NULL)
, mAudioDevice(NULL)
, mGraphicsDevice(NULL)
, mGraphicsContext(NULL)
, mNotifyQueueCreated(NULL)
, mNotifyQueueDestroyed(NULL)
, mNotifyWindowCreated(NULL)
, mNotifyWindowDestroyed(NULL)
, mNotifyWindowResized(NULL)
{
    qqqCreateMutex(&mLock);
    qqqCreateMutex(&mWindowLock);
    qqqCreateWaitCondition(&mWaitWindow);
    qqqCreateWaitCondition(&mWaitInputQueue);
    mWindow = NULL;
    qqqCreateThread(&mThread, this);
    mSkipNextMouseMove = false;
}

AndroidApplication::~AndroidApplication() {

}

void AndroidApplication::getWidth(uint32_t* val) {
    *val = mWidth;
}

void AndroidApplication::getHeight(uint32_t* val) {
    *val = mHeight;
}

void AndroidApplication::resize(uint16_t w, uint16_t h) {

}
void AndroidApplication::setPosition(uint16_t x, uint16_t y) {

}
void AndroidApplication::setMouseRelative(bool val) {
    this->is_mouse_relative = val;
    mSkipNextMouseMove = true;
}
void* AndroidApplication::getDisplay() const {
    return NULL;
}
void* AndroidApplication::getSurface() const {
    return static_cast<void*>(mWindow);
}
void AndroidApplication::setApplet(IApplet* val) {
    synchronized(mLock)
    {
        mApplet = val;
    }
}

bool AndroidApplication::create(const char* gapi, const char* aapi) {
    mConfig = AConfiguration_new();
    JNIEnv* env = activity->env;
    jobject obj = activity->clazz;

    memset(mInputDevices, 0,
            InputDeviceType::MaxInputDevices * sizeof(IInputDevice*));
    qqqCreateAndroidSensorDevice(&mInputDevices[InputDeviceType::Linear],
            ASENSOR_TYPE_ACCELEROMETER);
    qqqCreateAndroidSensorDevice(&mInputDevices[InputDeviceType::Angular],
            ASENSOR_TYPE_GYROSCOPE);
    qqqCreateAndroidSensorDevice(&mInputDevices[InputDeviceType::Light],
            ASENSOR_TYPE_LIGHT);
    qqqCreateAndroidSensorDevice(&mInputDevices[InputDeviceType::Proximity],
            ASENSOR_TYPE_PROXIMITY);
    qqqCreateAndroidSensorDevice(&mInputDevices[InputDeviceType::Magnetic],
            ASENSOR_TYPE_MAGNETIC_FIELD);
    for (int i = 0; i < InputDeviceType::MaxInputDevices; i++) {
        if (mInputDevices[i] != NULL) {
            mInputDevices[i]->create();
        }
    }
    return true;
}

void AndroidApplication::destroy() {

    DEBUG_METHOD();
//	if (mEngine != NULL) {
//		mEngine->destroy();
//	}

//deactivate();

    if (mApplet != NULL) {
        mApplet->destroy();
        mApplet = NULL;
    }

    int i;
    for (i = 0; i < InputDeviceType::MaxInputDevices; ++i) {
        if (mInputDevices[i] != NULL) {
            mInputDevices[i]->destroy();
            mInputDevices[i] = NULL;
        }
    }

    if (mInputQueue != NULL) {
        AInputQueue_detachLooper(mInputQueue);
        mInputQueue = NULL;
    }

    if (mConfig != NULL) {
        AConfiguration_delete(mConfig);
        mConfig = NULL;
    }

    mInputLooper = NULL;
}

void AndroidApplication::start() {
    DEBUG_METHOD();
    synchronized(mLock)
    {
        mIsRunning = true;
        mThread->start();
    }
}

void AndroidApplication::stop() {
    DEBUG_METHOD();
    synchronized(mLock)
    {
        mIsRunning = false;
        //padev->stop(-1);
    }
}

void AndroidApplication::pause() {
    DEBUG_METHOD();
    synchronized(mLock)
    {
        is_paused = true;
        //padev->pause(-1);

        if(mGraphicsContext) {
            GraphicsResourceEventArgs args;
            args.GraphicsContext= mGraphicsContext;
            mGraphicsContext->OnContextLost(args);
        }
    }
}

void AndroidApplication::resume() {
    synchronized(mLock)
    {
        is_paused = false;
        //padev->resume(-1);

        if(mGraphicsContext) {
            GraphicsResourceEventArgs args;
            args.GraphicsContext= mGraphicsContext;
            mGraphicsContext->OnContextReset(args);
        }
    }
}

int AndroidApplication::run() {
    DEBUG_METHOD();
    mInputLooper = ALooper_prepare(ALOOPER_PREPARE_ALLOW_NON_CALLBACKS);
    if (mInputQueue != NULL) {
        DEBUG_MESSAGE("Attaching looper to input queue...");
        AInputQueue_attachLooper(mInputQueue, mInputLooper, 0, NULL, this);
    }
    timer thread_timer;
    int32_t dt = (real) ((1000)) / (real) ((60));
    int32_t timeCurrentMs = thread_timer.elapsed();
    int32_t timeAccumulatedMs = 0;
    int32_t timeDeltaMs = 0;
    int32_t timeLastMs = 0;
    int32_t t = 0;

    while (mIsRunning) {

        timeLastMs = timeCurrentMs;
        timeCurrentMs = thread_timer.elapsed();
        timeDeltaMs = timeCurrentMs - timeLastMs;
        timeAccumulatedMs += timeDeltaMs;

        if (mGraphicsDevice != NULL && mApplet) {

            while (timeAccumulatedMs >= dt) {
                stepEventLoop();
                mApplet->update(t, dt);
                timeAccumulatedMs -= dt;
                t += dt;
            }

            mApplet->draw();
        }

        synchronized(mWaitWindow) {

            if (mNotifyWindowCreated != NULL) {
                windowCreated(mNotifyWindowCreated);

                mNotifyWindowCreated = NULL;
                mWaitWindow->set();
            }
            if (mNotifyWindowDestroyed != NULL) {
                windowDestroyed(mNotifyWindowDestroyed);

                mNotifyWindowDestroyed = NULL;
                mWaitWindow->set();
            }
            if (mNotifyWindowResized) {
                windowResized(mNotifyWindowResized);

                mNotifyWindowResized = NULL;
                mWaitWindow->set();
            }
        }
        synchronized(mWaitInputQueue) {
            if (mNotifyQueueCreated != NULL) {
                queueCreated(mNotifyQueueCreated);

                mNotifyQueueCreated = NULL;
                mWaitInputQueue->set();
            }
            if (mNotifyQueueDestroyed != NULL) {
                queueDestroyed(mNotifyQueueDestroyed);

                mNotifyQueueDestroyed = NULL;
                mWaitInputQueue->set();
            }
        }

        //lastTime = currentTime;

        //System::sleep(0);

    }

    if (mInputQueue != NULL) {
        AInputQueue_detachLooper(mInputQueue);
    }
    mInputLooper = NULL;
    return 0;
}

void AndroidApplication::mousePressed(short x, short y, short buttonID,
        short pointerID) {
    MouseEventArgs args = { x, y, pointerID, buttonID };
    OnMousePressed(args);
}

void AndroidApplication::mouseReleased(short x, short y, short buttonID,
        short pointerID) {
    MouseEventArgs args = { x, y, pointerID, buttonID };
    OnMouseReleased(args);
}

void AndroidApplication::mouseMoved(short x, short y, short buttonID, short pointerID) {
//    MouseEventArgs args = { x, y, pointerID, buttonID };
//    OnMouseMove(args);

    short ix = x;
    short iy = y;

    if (mSkipNextMouseMove) {
        pmousex = ix;
        pmousey = iy;
        mSkipNextMouseMove = false;
        return;
      }
      //MouseEventArgs ee;
      if (is_mouse_relative) {
        // We check to see if we are at the warp-point, instead of using SkipNextMove
        //  since it seems on X11 that user mouse-move commands can get processed before the skip, getting things confused
        // We still use SkipNextMove when the DifferenceMouse is turned on to avoid the first jump

        int dx = ix - pmousex, dy = iy - pmousey;

        pmousex = ix;
        pmousey = iy;

        //XWarpPointer(mX11->dpy, None, mX11->win, 0, 0, 0, 0, width / 2, height / 2);
        ix = dx;
        iy = dy;
      }

      MouseEventArgs args = { ix, iy, pointerID, buttonID };
      OnMouseMove(args);
}

void AndroidApplication::stepEventLoop() {
    if (!mInputQueue) {
        return;
    }
    int nevents = 0;
    while (ALooper_pollAll(0, NULL, &nevents, NULL) >= 0) {
        AInputEvent* ev = NULL;
        int32_t handled = 0;
        if (AInputQueue_getEvent(mInputQueue, &ev) >= 0) {
            if (AInputQueue_preDispatchEvent(mInputQueue, ev)) {
                return;
            }
            int nhandled = 0;
            switch (AInputEvent_getType(ev)) {
            case AINPUT_EVENT_TYPE_KEY: {
                break;
            }
            case AINPUT_EVENT_TYPE_MOTION: {
                int action = AMotionEvent_getAction(ev);
                int flags = FLAGS_FOR_ACTION(action);
                int index = INDEX_FOR_ACTION(action);
                int npointers = AMotionEvent_getPointerCount(ev);
                int pointer = AMotionEvent_getPointerId(ev, index);
                float psi = AMotionEvent_getPressure(ev, index);
                int pos_x = AMotionEvent_getX(ev, index);
                int pos_y = AMotionEvent_getY(ev, index);
                switch (flags) {
                case AMOTION_EVENT_ACTION_MOVE: {
//                    if (is_mouse_relative) {
//                        mouseMoved(pmousex - pos_x, pmousey - pos_y, 0, pointer);
//                        pmousex = pos_x;
//                        pmousey = pos_y;
//
//                    } else {
                        mouseMoved(pos_x, pos_y, 0, pointer);
//                    }
                    break;
                }
                case AMOTION_EVENT_ACTION_DOWN:
                case AMOTION_EVENT_ACTION_POINTER_DOWN: {
                    if (is_mouse_relative) {
                        mouseMoved(0, 0, 0, pointer);
                        pmousex = pos_x;
                        pmousey = pos_y;
                    } else {
                        mouseMoved(pos_x, pos_y, 0, pointer);
                    }
                    mousePressed(pos_x, pos_y, 0, pointer);
                    break;
                }
                case AMOTION_EVENT_ACTION_UP:
                case AMOTION_EVENT_ACTION_POINTER_UP: {
                    mouseReleased(pos_x, pos_y, 0, pointer);

                }
                }
                break;
            }
            }
            AInputQueue_finishEvent(mInputQueue, ev, handled);
        }
    }
}

void AndroidApplication::setNativeActivity(ANativeActivity* val) {
    DEBUG_METHOD();
    val->instance = this;
    val->callbacks->onDestroy = onDestroy;
    val->callbacks->onStart = onStart;
    val->callbacks->onResume = onResume;
//  val->callbacks->onSaveInstanceState 	= onSaveInstanceState;
    val->callbacks->onPause = onPause;
    val->callbacks->onStop = onStop;
    val->callbacks->onConfigurationChanged = onConfigurationChanged;
//  val->callbacks->onLowMemory 			= onLowMemory;
//  val->callbacks->onWindowFocusChanged 	= onWindowFocusChanged;
    val->callbacks->onNativeWindowCreated = onNativeWindowCreated;
    val->callbacks->onNativeWindowDestroyed = onNativeWindowDestroyed;
    val->callbacks->onNativeWindowResized = onNativeWindowResized;
    val->callbacks->onInputQueueCreated = onInputQueueCreated;
    val->callbacks->onInputQueueDestroyed = onInputQueueDestroyed;

    this->activity = val;
}

void AndroidApplication::notifyWindowCreated(ANativeWindow* win) {
    DEBUG_METHOD();
    synchronized(mWaitWindow) {
        if (mIsRunning) {
            mNotifyWindowCreated = win;
            mWaitWindow->wait();
        } else {
            windowCreated(win);
        }
    }
}

void AndroidApplication::notifyWindowResized(ANativeWindow* win) {
    DEBUG_METHOD();
    synchronized(mWaitWindow) {
        if (mIsRunning) {
            mNotifyWindowResized = win;
            mWaitWindow->wait();
        } else {
            windowResized(win);
        }
    }
}

void AndroidApplication::notifyQueueCreated(AInputQueue* q) {
    DEBUG_METHOD();
    synchronized(mWaitInputQueue) {
        if (mIsRunning) {
            mNotifyQueueCreated = q;
            mWaitInputQueue->wait();
        } else {
            queueCreated(q);
        }
    }
}

void AndroidApplication::notifyQueueDestroyed(AInputQueue* q) {
    DEBUG_METHOD();
    synchronized(mWaitInputQueue) {
        if (mIsRunning) {
            mNotifyQueueCreated = q;
            mWaitInputQueue->wait();
        } else {
            queueDestroyed(q);
        }
    }
}

void AndroidApplication::windowCreated(ANativeWindow* win) {
    DEBUG_METHOD();
    GraphicsInitializer gformat;
    int nativeFormat = ANativeWindow_getFormat(win);
    if (nativeFormat == WINDOW_FORMAT_RGB_565) {
        gformat.pixelFormat(SurfaceFormat::Bgr565).depthBits(16).stencilBits(8);
        DEBUG_MESSAGE("using SurfaceFormat::Bgr565");
        //mFormat->pixelFormat(PixelFormat::Bgr565);
        //mFormat->setPixelFormat(TextureFormat::Format_RGB_5_6_5);
    } else {
        gformat.pixelFormat(SurfaceFormat::Color).depthBits(24).stencilBits(8);
        DEBUG_MESSAGE("using SurfaceFormat::Color");
        //mFormat->pixelFormat(PixelFormat::Color);
        //mFormat->setPixelFormat(TextureFormat::Format_RGBA_8);
    }

    mGraphicsContext = NULL;
    hxCreateGraphicsContext(&mGraphicsContext);
    mGraphicsContext->create(getDisplay(), static_cast<void*>(win), &gformat);
    hxCreateGraphicsDevice(&mGraphicsDevice);
    mGraphicsDevice->create(mGraphicsContext);
    OnResize += event(mGraphicsDevice, &IGraphicsDevice::onContextResize);
    //smWindow = win;

    mWidth = ANativeWindow_getWidth(win);
    mHeight = ANativeWindow_getHeight(win);

    WindowResizeArgs args = {mWidth, mHeight};
    OnResize(args);

    AudioInitializer ainit;
    ainit.UserData = (void*)activity;
    mAudioDevice = NULL;
    hxCreateAudioDevice(&mAudioDevice);
    mAudioDevice->create(ainit);

    mApplet->create(new AndroidAssetManager(activity->assetManager));
}

void AndroidApplication::windowDestroyed(ANativeWindow*) {
    DEBUG_METHOD();
}

void AndroidApplication::windowResized(ANativeWindow* win) {
    DEBUG_METHOD();
    mWidth = ANativeWindow_getWidth(win);
    mHeight = ANativeWindow_getHeight(win);

    WindowResizeArgs args = {mWidth, mHeight};
    OnResize(args);
}

void AndroidApplication::queueCreated(AInputQueue* in) {
    DEBUG_METHOD();
    mInputQueue = in;
    AInputQueue_attachLooper(mInputQueue, mInputLooper, 0, NULL, this);
}

void AndroidApplication::queueDestroyed(AInputQueue* in) {
    DEBUG_METHOD();
    AInputQueue_detachLooper(mInputQueue);
    mInputQueue = NULL;
}

#endif
