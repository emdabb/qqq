/**
 * @file android_application.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jul 12, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef ANDROID_APPLICATION_H_
#define ANDROID_APPLICATION_H_

#include <qqq.h>

#if defined(__ANDROID__)

#include <android/native_activity.h>
#include <android/native_window.h>
#include <android/configuration.h>
#include <android/input.h>

#include <core/runnable.h>
#include <core/mutex.h>
#include <core/thread.h>
#include <core/wait_condition.h>
#include <base/application.h>
#include <input/input_device.h>

#ifndef QQQ_PACKAGE_NAME
#define QQQ_PACKAGE_NAME	QQQ
#endif

#define FLAGS_FOR_ACTION(a) (((a)) & AMOTION_EVENT_ACTION_MASK)
#define INDEX_FOR_ACTION(a) ((((a)) & AMOTION_EVENT_ACTION_POINTER_INDEX_MASK) >> AMOTION_EVENT_ACTION_POINTER_INDEX_SHIFT)
#define CONCAT (a, b) a ## b
#define CONCAT2(a, b) CONCAT(a, b)
#define NDK_BRIDGE(x) CONCAT2(CONCAT2(CONCAT2(Java_, QQQ_PACKAGE_NAME), _), x)

namespace qqq {

class IInputDevice;
class IApplet;

class AndroidApplication : public IApplication {
  ANativeActivity* activity;
  AConfiguration* mConfig;
  ALooper* mInputLooper;

  ANativeWindow* mWindow;
  ANativeWindow* mNotifyWindowCreated;
  ANativeWindow* mNotifyWindowDestroyed;
  ANativeWindow* mNotifyWindowResized;

  AInputQueue* mInputQueue;
  AInputQueue* mNotifyQueueCreated;
  AInputQueue* mNotifyQueueDestroyed;

  IWaitCondition* mWaitWindow;
  IWaitCondition* mWaitInputQueue;

  IApplet* mApplet;
  bool mIsRunning, is_paused, is_mouse_relative;
  bool mSkipNextMouseMove;

  int mousex, mousey;
  int pmousex, pmousey;

  uint16_t mWidth, mHeight;

  IGraphicsDevice* 	mGraphicsDevice;
  IGraphicsContext* mGraphicsContext;
  IComputeContext*	mComputeContext;
  IAudioDevice*		mAudioDevice;

  IMutex* mLock;
  IThread* mThread;
  IMutex* mWindowLock;

  IInputDevice* mInputDevices[InputDeviceType::MaxInputDevices];

public:
  AndroidApplication();
  virtual ~AndroidApplication();
public:
  /** application members */
  virtual bool create(const char* gapi, const char* aapi);
  virtual void destroy();
  virtual IGraphicsDevice* getGraphicsDevice() const {
    return mGraphicsDevice;
  }
  virtual IGraphicsContext* getGraphicsContext() const {
    return mGraphicsContext;
  }
  virtual IComputeContext* getComputeContext() const {
    return mComputeContext;
  }
  virtual IAudioDevice* getAudioDevice() const {
    return mAudioDevice;
  }
  virtual void resize(uint16_t w, uint16_t h);
  virtual void setPosition(uint16_t x, uint16_t y);
  virtual void setMouseRelative(bool);
  virtual void* getDisplay() const;
  virtual void* getSurface() const;
  virtual void setApplet(IApplet* val);
  virtual IApplet* getApplet() const {
    return mApplet;
  }
  virtual void setFullscreen(bool) {}
  virtual void start();
  virtual void stop();
  virtual void pause();
  virtual void resume();
  virtual void loadPlugin(const char*, const char*, int) {}
  virtual void getWidth(uint32_t* val);
  virtual void getHeight(uint32_t* val);
public:
  /** runnable members */
  virtual int run();
public:
  void notifyWindowCreated(ANativeWindow*);
  void notifyWindowDestroyed(ANativeWindow*);
  void notifyWindowResized(ANativeWindow*);
  void notifyQueueCreated(AInputQueue*);
  void notifyQueueDestroyed(AInputQueue*);
protected:
  static void onDestroy(ANativeActivity *activity);
  static void onStart(ANativeActivity *activity);
  static void onStop(ANativeActivity *activity);
  static void onNativeWindowCreated(ANativeActivity *activity,ANativeWindow *window);
  static void onNativeWindowDestroyed(ANativeActivity *activity,ANativeWindow *window);
  static void onNativeWindowResized(ANativeActivity *activity,ANativeWindow *window);
  static void onInputQueueCreated(ANativeActivity *activity,AInputQueue *queue);
  static void onInputQueueDestroyed(ANativeActivity *activity,AInputQueue *queue);
  static void onConfigurationChanged(ANativeActivity* activity);
  static void onResume(ANativeActivity* activity);
  static void onPause(ANativeActivity* activity);
protected:
  void stepEventLoop();
  void windowCreated(ANativeWindow*);
  void windowDestroyed(ANativeWindow*);
  void windowResized(ANativeWindow*);
  void queueCreated(AInputQueue*);
  void queueDestroyed(AInputQueue*);
  void mousePressed(short, short, short, short);
  void mouseReleased(short, short, short, short);
  void mouseMoved(short, short, short, short);
public:
  void setNativeActivity(ANativeActivity*);
};

QQQ_C_API void QQQ_CALL hxCreateApplication(IApplication** ppapp) {
  *ppapp = new AndroidApplication;
}

}

#endif



#endif /* ANDROID_APPLICATION_H_ */
