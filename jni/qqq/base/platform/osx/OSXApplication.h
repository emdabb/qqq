/**
 * @file OSXApplication.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date May 19, 2015
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef JNI_QQQ_BASE_PLATFORM_OSX_OSXAPPLICATION_H_
#define JNI_QQQ_BASE_PLATFORM_OSX_OSXAPPLICATION_H_

#include <base/application_base.h>

namespace qqq {

class OSXApplication : public ApplicationBase {
    std::string mTitle;
    int mPositionX;
    int mPositionY;
    int mWidth;
    int mHeight;
    bool mIsFullscreen;
    bool mDifferenceMouse;
    int mLastMouseX;
    int mLastMouseY;

    bool mActive;
    bool mDestroyed;
    void* mWindow;
    void* mView;
    void* mPool;
public:
    OSXApplication();
    virtual ~OSXApplication();
    /**
     * Inherited from IApplication
     */
    //virtual bool create(const char*, const char*);
    virtual void destroy();
//    virtual IGraphicsDevice* getGraphicsDevice() const;
//    virtual IGraphicsContext* getGraphicsContext() const;
//    virtual IComputeContext* getComputeContext() const;
//    virtual IAudioDevice* getAudioDevice() const;
    virtual void resize(uint16_t w, uint16_t h);
    virtual void setPosition(uint16_t x, uint16_t y);
    virtual void setMouseRelative(bool);
    virtual void setFullscreen(bool);
    //virtual void* getDisplay() const;
    virtual void* getSurface() const;
    //virtual void setApplet(IApplet*);
    //virtual IApplet* getApplet() const ;
    virtual void start();
    virtual void stop();
    virtual void pause();
    virtual void resume();
    virtual void loadPlugin(const char*, const char*, int) {}
    virtual void getWidth(uint32_t*);
    virtual void getHeight(uint32_t*);
    /**
     * Inherited from ApplicationBase
     */
    virtual void preCreate();
    virtual void postCreate();
    /**
     * OSX specific
     */
    void setWindow(void *window);
    void internal_mousePressed(int x,int y,int button);
    void internal_mouseMoved(int x,int y);

    bool isActive() {
        return mActive != false;
    }

    void update(int);
    void render();
    void mouseReleased(int, int, int);
    int translateKey(int);
    /**
     * todo: move to IApplication interface
     */
    virtual void activate();
    virtual void deactivate();
    virtual void mousePressed(int, int, int);
    virtual void mouseMoved(int, int);
};

}


#endif /* JNI_QQQ_BASE_PLATFORM_OSX_OSXAPPLICATION_H_ */
