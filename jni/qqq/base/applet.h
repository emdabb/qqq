/**
 * @file applet.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 9, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef APPLET_H_
#define APPLET_H_

namespace qqq {
class AssetManager;
struct IApplet {
  virtual ~IApplet() {}
  virtual bool create(AssetManager*) = 0;
  virtual void destroy() = 0;
  virtual void update(int, int) = 0;
  virtual void draw() = 0;
};
}


#endif /* APPLET_H_ */
