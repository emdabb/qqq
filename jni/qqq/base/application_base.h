/**
 * @file application_base.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 5, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef APPLICATION_BASE_H_
#define APPLICATION_BASE_H_

#include <core/dictionary.h>
#include <base/application.h>
#include <base/applet.h>
#include <gfx/graphics_context.h>
#include <gfx/graphics_device.h>
#include <gfx/graphics_initializer.h>
#include <audio/audio_device.h>
#include <compute/compute_context.h>
#include <content/asset_manager.h>
#include <map>
#include <string>
#include <core/system.h>

namespace qqq {

class ApplicationBase : public IApplication {

  struct compute_plugin {
    PFNCREATECOMPUTECONTEXT create_context;
    int type;

    compute_plugin(PFNCREATECOMPUTECONTEXT arg0 = NULL, const int devtype = -1)
    : create_context(arg0)
    , type(devtype) {

    }
  };

  struct gfxplugin {
    PFNCREATEGRAPHICSCONTEXT new_GraphicsContext;
    PFNCREATEGRAPHICSDEVICE  new_GraphicsDevice;

    gfxplugin(PFNCREATEGRAPHICSCONTEXT create0 = NULL, PFNCREATEGRAPHICSDEVICE create1 = NULL)
    : new_GraphicsContext(create0)
    , new_GraphicsDevice(create1) {

    }
  };

  struct audio_plugin {
    PFNQQQCREATEAUDIODEVICE create_AudioDevice;

    audio_plugin(PFNQQQCREATEAUDIODEVICE arg0 = NULL)
    : create_AudioDevice(arg0) {

    }

  };

  Dictionary<gfxplugin*>::Type gfx_plugins;
  Dictionary<compute_plugin*>::Type compute_plugins;
  Dictionary<audio_plugin*>::Type audio;
protected:
  IGraphicsContext* m_graphics_context;
  IGraphicsDevice*  m_graphics_device;
  IAudioDevice* 	  m_audio_device;
  IApplet* m_applet;
  GraphicsInitializer mGraphicsInitializer;
  IComputeContext* _compute_context;
public:
  ApplicationBase();

  ~ApplicationBase();

  virtual void* getDisplay() const {
    return NULL;
  }
  virtual void* getSurface() const {
    return NULL;
  }

  IGraphicsContext* create_graphics_context(const char* renderDevice);

  IGraphicsDevice* create_graphics_device(const char* renderDevice);

  void create_context_and_graphics_device(const char* renderDevice);

  void create_compute_context(const char* api);

  void create_audio_device(const char* api);

  virtual bool create(const char* renderDevice, const char* audioDevice);

  virtual void destroy() {}

  virtual void preCreate() {

  }

  virtual void postCreate() {

  }

  IGraphicsContext* getGraphicsContext() const;

  IGraphicsDevice* getGraphicsDevice() const;

  IComputeContext* getComputeContext() const;

  IAudioDevice* getAudioDevice() const;

  void register_plugin(const std::string& api, PFNCREATEGRAPHICSCONTEXT arg0, PFNCREATEGRAPHICSDEVICE arg1);

  void register_plugin(const std::string& api, PFNCREATECOMPUTECONTEXT arg1, const int type);

  void register_plugin(const std::string& api, PFNQQQCREATEAUDIODEVICE);


  virtual void loadPlugin(const char*, const char*, int) ;

  virtual IApplet* getApplet() const {
    return m_applet;
  }

  virtual void setApplet(IApplet* a) {
    m_applet = a;
  }

  virtual void set_fullscreen(bool val) {
      mGraphicsInitializer.fullscreen(val);
  }

};
} // qqq


#endif /* APPLICATION_BASE_H_ */
