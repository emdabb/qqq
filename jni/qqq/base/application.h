/**
 * @file application.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 5, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef APPLICATION_H_
#define APPLICATION_H_

#include <qqq.h>
#include <core/event.h>
#include <core/runnable.h>



namespace qqq {
struct IGraphicsDevice;
struct IGraphicsContext;
struct IComputeContext;
class  AssetManager;
struct IAudioDevice;
struct IApplet;

enum {
  E_PLUGIN_GFX,
  E_PLUGIN_AUDIO,
  E_PLUGIN_COMPUTE
};

struct WindowResizeArgs {
    size_t width;
    size_t height;
};

struct MouseEventArgs {
	DISALLOW_COPY_AND_ASSIGN(MouseEventArgs);
    const short X, Y;
    const short PointerId;
    const short ButtonId;
};

struct KeyEventArgs {
	DISALLOW_COPY_AND_ASSIGN(KeyEventArgs);
    enum {
        Key_ENTER = 10,     //!< Key_ENTER
        Key_TAB = 8,        //!< Key_TAB
        Key_SPACE = 32,     //!< Key_SPACE

        Key_LEFT = 1024,    //!< Key_LEFT
        Key_RIGHT,          //!< Key_RIGHT
        Key_UP,             //!< Key_UP
        Key_DOWN,           //!< Key_DOWN

        // Keyboard keys
        Key_ESC = 2048,     //!< Key_ESC
        Key_PAUSE,          //!< Key_PAUSE
        Key_SHIFT,          //!< Key_SHIFT
        Key_CTRL,           //!< Key_CTRL
        Key_ALT,            //!< Key_ALT
        Key_SPECIAL,        //!< Key_SPECIAL
        Key_BACKSPACE,      //!< Key_BACKSPACE
        Key_DELETE,         //!< Key_DELETE

        // Cellphone keys
        Key_SOFTLEFT = 4096,//!< Key_SOFTLEFT
        Key_SOFTRIGHT,      //!< Key_SOFTRIGHT
        Key_ACTION,         //!< Key_ACTION
        Key_BACK,           //!< Key_BACK
        Key_MENU,           //!< Key_MENU
        Key_SEARCH,         //!< Key_SEARCH
    };

    const uint16_t code;
};

class IApplication : public IRunnable {
public:
  virtual ~IApplication() {}
  virtual bool create(const char*, const char*) = 0;
  virtual void destroy() = 0;
  virtual IGraphicsDevice* getGraphicsDevice() const = 0;
  virtual IGraphicsContext* getGraphicsContext() const = 0;
  virtual IComputeContext* getComputeContext() const = 0;
  virtual IAudioDevice* getAudioDevice() const = 0;
  virtual void resize(uint16_t w, uint16_t h) = 0;
  virtual void setPosition(uint16_t x, uint16_t y) = 0;
  virtual void setMouseRelative(bool) = 0;
  virtual void setFullscreen(bool) = 0;
  virtual void* getDisplay() const = 0;
  virtual void* getSurface() const = 0;
  virtual void setApplet(IApplet*) = 0;
  virtual IApplet* getApplet() const  = 0;
  virtual void start() = 0;
  virtual void stop() = 0;
  virtual void pause() = 0;
  virtual void resume() = 0;
  virtual void loadPlugin(const char*, const char*, int) = 0;
  virtual void getWidth(uint32_t*) = 0;
  virtual void getHeight(uint32_t*) = 0;

  EventHandler<WindowResizeArgs> OnResize;
  EventHandler<MouseEventArgs> OnMouseMove;
  EventHandler<MouseEventArgs> OnMousePressed;
  EventHandler<MouseEventArgs> OnMouseReleased;
  EventHandler<KeyEventArgs> OnKeyPressed;
  EventHandler<KeyEventArgs> OnKeyReleased;
};

typedef void(QQQ_CALL *PFNQQQCREATEAPPLICATION)(IApplication**);
QQQ_C_API void QQQ_CALL hxCreateApplication(IApplication**);

}


#endif /* APPLICATION_H_ */
