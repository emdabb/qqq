/**
 * @file application_base.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 24, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include "application_base.h"
#include <core/debug_log.h>

using namespace qqq;

ApplicationBase::ApplicationBase() :
		m_graphics_context(NULL), m_graphics_device(NULL), m_audio_device(NULL), m_applet(
				NULL), _compute_context(NULL) {
}

ApplicationBase::~ApplicationBase() {

}

void ApplicationBase::create_audio_device(const char* api) {
	DEBUG_METHOD();
	DEBUG_VALUE_AND_TYPE_OF(api);
	Dictionary<audio_plugin*>::Type::iterator it = audio.find(api);
	if (it != audio.end()) {
		(*it).second->create_AudioDevice(&m_audio_device);
		AudioInitializer ainit;
		ainit.Bitrate = 22050;
		m_audio_device->create(ainit);
	}
}

IGraphicsContext* ApplicationBase::create_graphics_context(
		const char* renderDevice) {
	DEBUG_METHOD();
	IGraphicsContext* ctx = NULL;
	Dictionary<gfxplugin*>::Type::iterator it = gfx_plugins.find(renderDevice);
	if (it != gfx_plugins.end()) {
		(*it).second->new_GraphicsContext(&ctx);
		if (true
				!= ctx->create(getDisplay(), getSurface(),
						&mGraphicsInitializer)) {
			throw std::runtime_error("error creating graphics context.");
		}
		OnResize += event(ctx, &IGraphicsContext::resize);
	}
	return ctx;
}

IGraphicsDevice* ApplicationBase::create_graphics_device(
		const char* renderDevice) {
	DEBUG_METHOD();
	IGraphicsDevice* dev = NULL;
	Dictionary<gfxplugin*>::Type::iterator it = gfx_plugins.find(renderDevice);
	if (it != gfx_plugins.end()) {
		(*it).second->new_GraphicsDevice(&dev);
		OnResize += event(dev, &IGraphicsDevice::onContextResize);
	}
	return dev;
}

void ApplicationBase::create_context_and_graphics_device(
		const char* renderDevice) {
	DEBUG_METHOD();
	try {
		m_graphics_context = create_graphics_context(renderDevice);
		m_graphics_device = create_graphics_device(renderDevice);
		m_graphics_device->create(m_graphics_context);
	} catch (std::runtime_error& e) {
		DEBUG_MESSAGE("runtime error >> \"%s\"", e.what());
	} catch (...) {
		DEBUG_MESSAGE("Unhandled exception caught.");
	}
}

void ApplicationBase::create_compute_context(const char* api) {
	DEBUG_METHOD();
	try {
		Dictionary<compute_plugin*>::Type::iterator it = compute_plugins.find(
				api);
		if (it != compute_plugins.end()) {
			(*it).second->create_context(&_compute_context);
			_compute_context->create((*it).second->type);
		}

	} catch (...) {
		DEBUG_MESSAGE("Unhandled exception caught.");
		_compute_context = NULL;
	}
}

bool ApplicationBase::create(const char* renderDevice,
		const char* audioDevice) {
	DEBUG_METHOD();
	preCreate();
	if (renderDevice && gfx_plugins.size() > 0) {
		create_context_and_graphics_device(renderDevice);
	}

	if (audioDevice && audio.size() > 0) {
		create_audio_device(audioDevice);
	}

	if (m_applet) {
		if (!m_applet->create(new AssetManager)) {
			throw std::runtime_error("error creating applet.");

		}
	}
	postCreate();
	return true;
}

void ApplicationBase::register_plugin(const std::string& api,
		PFNCREATEGRAPHICSCONTEXT arg0, PFNCREATEGRAPHICSDEVICE arg1) {
	DEBUG_METHOD();
	if (gfx_plugins.find(api) == gfx_plugins.end()) {
		gfx_plugins[api] = new gfxplugin(arg0, arg1);
	}
}

void ApplicationBase::register_plugin(const std::string& api,
		PFNCREATECOMPUTECONTEXT arg1, const int type) {
	DEBUG_METHOD();
	if (compute_plugins.find(api) == compute_plugins.end()) {
		compute_plugins[api] = new compute_plugin(arg1, type);
	}
}

void ApplicationBase::register_plugin(const std::string& api,
		PFNQQQCREATEAUDIODEVICE arg0) {
	DEBUG_METHOD();
	if (audio.find(api) == audio.end()) {
		audio[api] = new audio_plugin(arg0);
	}
}

void ApplicationBase::loadPlugin(const char* api, const char* fn, int type) {
	DEBUG_METHOD();
	DEBUG_VALUE_AND_TYPE_OF(api);
	DEBUG_VALUE_AND_TYPE_OF(fn);
	try {
		lib_t lib = system::load_library(fn);
		switch (type) {
		case E_PLUGIN_GFX: {
			PFNCREATEGRAPHICSDEVICE pfnCreateGraphicsDevice = NULL;
			PFNCREATEGRAPHICSCONTEXT pfnCreateGraphicsContext = NULL;
			//PFNQQQGETAPISTRINGPROC	pfnGetApiString = NULL;
			pfnCreateGraphicsDevice =
					(PFNCREATEGRAPHICSDEVICE) system::get_proc_address(lib,
							"hxCreateGraphicsDevice");
			pfnCreateGraphicsContext =
					(PFNCREATEGRAPHICSCONTEXT) system::get_proc_address(lib,
							"hxCreateGraphicsContext");
			register_plugin(api, pfnCreateGraphicsContext,
					pfnCreateGraphicsDevice);
			break;
		}
		case E_PLUGIN_AUDIO: {
			PFNQQQCREATEAUDIODEVICE pfnCreateAudioDevice = NULL;
			pfnCreateAudioDevice =
					(PFNQQQCREATEAUDIODEVICE) system::get_proc_address(lib,
							"hxCreateAudioDevice");
			register_plugin(api, pfnCreateAudioDevice);
		}
			break;
		case E_PLUGIN_COMPUTE:
			break;
		}
	} catch (UnsatisfiedLinkerException& err) {
		DEBUG_METHOD();
		DEBUG_MESSAGE(err.what());
		assert(0);
	} catch (...) {

		DEBUG_MESSAGE("unhandled exception");
		assert(0);
	}
}

IGraphicsContext* ApplicationBase::getGraphicsContext() const {
	return m_graphics_context;
}

IGraphicsDevice* ApplicationBase::getGraphicsDevice() const {
	return m_graphics_device;
}

IComputeContext* ApplicationBase::getComputeContext() const {
	return _compute_context;
}

IAudioDevice* ApplicationBase::getAudioDevice() const {
	return m_audio_device;
}
