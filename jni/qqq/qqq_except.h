/**
 * @file qqq_except.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jul 10, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef QQQ_EXCEPT_H_
#define QQQ_EXCEPT_H_

#include <stdexcept>
#include <string>
namespace qqq {

struct UnimplementedMethodException : public std::runtime_error {
    UnimplementedMethodException() : std::runtime_error("unimplemented method!") {

    }

    virtual ~UnimplementedMethodException() throw() {

    }
};

struct IOException : public std::runtime_error {
  IOException(const std::string& wut) : std::runtime_error("io exception: " + wut) {

  }

  virtual ~IOException() throw() {

  }
};

struct SocketException : public IOException {
	SocketException() :IOException("socket exception: ") 
	{

	}

	virtual ~SocketException() throw()
	{

	}
};

struct FileNotFoundException : public IOException {
  FileNotFoundException(const std::string& which) : IOException("file not found " + which) {

  }

  virtual ~FileNotFoundException() throw() {

  }
};

struct UnsatisfiedLinkerException : public std::runtime_error {
  UnsatisfiedLinkerException(std::string const& what) : std::runtime_error("Unsatisfied linker error: " + what) {
  }
  virtual ~UnsatisfiedLinkerException() throw() {}
};

}


#endif /* QQQ_EXCEPT_H_ */
