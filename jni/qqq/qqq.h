/*
 * qqq.h
 *
 *  Created on: Jun 11, 2014
 *      Author: miel
 */

#ifndef QQQ_H_
#define QQQ_H_

#include "qqq_types.h"
#include "qqq_capi.h"
#include "qqq_except.h"

#define PACK_2UINT16(a, b) ((uint32_t)(((a)) << 16)) | ((b))

#define QQQ_APISTRING_OPENGL	"qqq::OpenGL"
#define QQQ_APISTRING_OPENGLES	"qqq::OpenGL-ES"
#define QQQ_APISTRING_OPENGLES2	"qqq::OpenGL-ES2"
#define QQQ_APISTRING_OPENAL	"qqq::OpenAL"
#define QQQ_APISTRING_CRICKET	"qqq::Cricket"
#define QQQ_APISTRING_OPENCL	"qqq::OpenCL"
#define QQQ_APISTRING_D3D9		"qqq::d3d9"
#define QQQ_APISTRING_D3D10		"qqq::d3d10"
#define QQQ_APISTRING_D3D11		"qqq::d3d11"

typedef const char* (QQQ_CALL *PFNQQQGETAPISTRINGPROC)(void);

// Original Version
#define DISALLOW_COPY_AND_ASSIGN(TypeName) \
TypeName(const TypeName&) = delete;                 \
void operator=(const TypeName&) = delete

#ifdef WIN32
#if defined(_MSC_VER)
// disable warning about byte padding for aligned structs.
// see: https://msdn.microsoft.com/en-us/library/t7khkyth.aspx
#pragma warning(disable : 4820) 
// The optimizer removed an inline function that is not called.
// see: https://msdn.microsoft.com/en-us/library/cw9x3tcf.aspx
#pragma warning(disable : 4514)
//warning C4996: 'vsprintf': This function or variable may be unsafe. Consider using vsprintf_s instead. To disable deprecation, use _CRT_SECURE_NO_WARNINGS.
//see online help for details.
#pragma warning(disable : 4996)
// function not inlined
// see online help for details.
#pragma warning(disable : 4710)
// warning C4324: 'qqq::Vector3': structure was padded due to alignment specifier
// see online help for details.
#pragma warning(disable : 4324)

#endif
#define __INT32_MAX__ std::numeric_limits<int32_t>::infinity()
	#define NOMINMAX
    // definitions for MSVS and ICC on Windows
    #define __forceinline 	__forceinline
    #define __noinline 		__declspec(noinline)
#else
    // definitions for GCC
    #define __forceinline 	__attribute__((always_inline))
    #define __noinline 		__attribute__((noinline))
    #define __debugbreak()
#endif


#endif /* QQQ_H_ */
