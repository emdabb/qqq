/**
 * @file astream.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 19, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef ASTREAM_H_
#define ASTREAM_H_


namespace qqq {

}


#endif /* ASTREAM_H_ */
