/**
 * @file audio_device.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 24, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef AUDIO_DEVICE_H_
#define AUDIO_DEVICE_H_

#include <qqq.h>

namespace qqq {

struct AudioInitializer {
  int 	Bitrate;
  void* UserData;
};

struct IAudioDevice {
public:
  static const int MAX_BANKS				= 32;
  static const int MAX_STREAMS				= 8;
  static const int MAX_SOURCES				= 32;
  static const int MAX_SOUNDS				= 64;
  static const int MAX_BUFFERS_PER_SOURCE	= 2;
  static const int MAX_BUFFER_SIZE			= 1024;
  static const int MAX_BUFFERS				= MAX_SOURCES * MAX_BUFFERS_PER_SOURCE;
public:
  virtual ~IAudioDevice() {}

  virtual void create(AudioInitializer const&) = 0;
  virtual void play(int, real const&, real const&) = 0;
  virtual void stop(int) = 0;
  virtual void pause(int) = 0;
  virtual void update() = 0;
  virtual void resume(int) = 0;
  //virtual int register_stream(const char* fn) = 0;
  virtual int registerStream(std::istream*) = 0;
};

typedef void(QQQ_CALL *PFNQQQCREATEAUDIODEVICE)(IAudioDevice**);
QQQ_C_API void QQQ_CALL hxCreateAudioDevice(IAudioDevice**);

}



#endif /* AUDIO_DEVICE_H_ */
