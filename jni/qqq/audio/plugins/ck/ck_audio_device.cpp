/**
 * @file ck_audio_device.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jul 28, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include <audio/audio_device.h>
#include <ck/ck.h>
#include <ck/config.h>
#include <ck/bank.h>
#include <ck/sound.h>
#include <ck/customfile.h>
#include <ck/customstream.h>
#include <ck/mixer.h>
#include <ck/pathtype.h>
#if defined(__ANDROID__)
#include <jni.h>
#include <android/native_activity.h>
#endif
#include <cstring>
#include <cassert>
#include <core/debug_log.h>

namespace qqq {

class CkStlFile: public CkCustomFile {
    std::istream* is;
    typedef std::istream::char_type char_type;
public:
    CkStlFile(std::istream* ins) :
            is(ins) {
    }

    virtual ~CkStlFile() {
    }

    /** Returns true if the file was successfully opened. */
    virtual bool isValid() const {
        return is != NULL;
    }

    /** Read from the file.  Returns number of bytes actually read. */
    virtual int read(void* buf, int bytes) {
        is->read(static_cast<char_type*>(buf), bytes);
        return static_cast<int>(is->gcount());
    }

    /** Returns the size of the file. */
    virtual int getSize() const {
        int cur = is->tellg();
        is->seekg(0, is->beg);
        int a = is->tellg();
        is->seekg(0, is->end);
        int b = is->tellg();
        is->seekg(cur, is->beg);
        return b - a;
    }

    /** Sets the read position in the file. */
    virtual void setPos(int pos) {
        is->seekg(pos, is->beg);
    }

    /** Returns the read position in the file. */
    virtual int getPos() const {
        return is->tellg();
    }

};

class ck_audio_device: public IAudioDevice {
    CkConfig* mConfig;
    CkSound* mSoundId[MAX_SOUNDS];
    CkBank* mAudioBank;
#if defined(__ANDROID__)
#endif
public:
    ck_audio_device() :
            mAudioBank(NULL) {
        DEBUG_METHOD();

    }

    virtual ~ck_audio_device() {
        stop(-1);
    }

    virtual void create(AudioInitializer const& init) {
        DEBUG_METHOD();
        memset(mSoundId, 0, MAX_SOUNDS * sizeof(CkSound*));
#if defined(__ANDROID__)
        ANativeActivity* activity = (ANativeActivity*) init.UserData;
        JavaVM* jvm = activity->vm;
        JNIEnv* env = activity->env;
        jobject obj = activity->clazz;
        mConfig = new CkConfig(jvm, obj);
        mConfig->useJavaAudio = false;
#else
        mConfig = new CkConfig;
#endif

        mConfig->audioUpdateMs = 16;
        mConfig->streamBufferMs = 1500.0f;
        DEBUG_MESSAGE("calling CkInit()");
        if(CkInit(mConfig) == 0){
            DEBUG_MESSAGE("CkInit() failed!");
        }
        DEBUG_MESSAGE("CkInit() succeeded!");

    }

    virtual void play(int id, real const& gain, real const& pitch) {
//		CkMixer* mixer = sounds[id]->getMixer();
//		mixer->setVolume(gain);
//		mixer->setPaused(false);
        mSoundId[id]->setPitchShift(pitch);
        mSoundId[id]->setVolume(gain);
        mSoundId[id]->play();
    }

    virtual void stop(int val) {
        if (val < 0) {
            CkShutdown();
            return;
        }
    }

    virtual void pause(int val) {
        if (val < 0) {
            CkSuspend();
            return;
        }
        mSoundId[val]->getMixer()->setPaused(true);
    }

    virtual void resume(int val) {
        if (val < 0) {
            CkResume();
        }
    }

    virtual void update() {
        CkUpdate();
    }

    int isStreamAvailable() {
        for (int i = 0; i < MAX_SOUNDS; i++) {
            if (mSoundId[i] == NULL) {
                return i;
            }
        }
        return -1;
    }

    int registerStream(const char* fn) {
        DEBUG_METHOD();
        CkSetCustomFileHandler(NULL, NULL);
        int id;
        if ((id = isStreamAvailable()) < 0) {
            return -1;
        }
//		CkSetCustomFileHandler(&open_file, NULL);
//		CkSound::setCustomStreamHandler(&open_stream, NULL);
        mSoundId[id] = CkSound::newStreamSound(fn);
        assert(mSoundId[id]);
        return id;
    }

    virtual int registerStream(std::istream* ins) {
        DEBUG_METHOD();
        CkSetCustomFileHandler(open_file, static_cast<void*>(ins));
        int id;
        if ((id = isStreamAvailable()) < 0) {
            return -1;
        }
        mSoundId[id] = CkSound::newStreamSound(".ogg", kCkPathType_Default);

        return id;
    }
protected:
    static CkCustomFile* open_file(const char* fn, void* ins) {
        return new CkStlFile(static_cast<std::istream*>(ins));
    }
};

//QQQ_C_API void new_ck_audio_device QQQ_CALL (audio_device** ppdev, audio_initializer* pinit) {
//  *ppdev = new ck_audio_device(pinit);
//}

QQQ_C_API void QQQ_CALL hxCreateAudioDevice (IAudioDevice** ppdev) {
    *ppdev = new ck_audio_device();
}

}

