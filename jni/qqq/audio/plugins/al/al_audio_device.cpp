/**
 * @file al_audio_device.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 24, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include <audio/audio_device.h>
#include <core/debug_log.h>
#include <core/string_util.h>
#include <AL/al.h>
#include <AL/alc.h>

namespace qqq {
namespace impl {
class al_audio_device : public IAudioDevice {
  ALuint sources[MAX_SOURCES];
  ALuint buffers[MAX_BUFFERS];
  ALsizei   freq[MAX_SOURCES];
  ALenum  format[MAX_SOURCES];
  int  streamids[MAX_SOURCES];
  std::istream* streams[MAX_SOUNDS];
  std::streamsize offset[MAX_SOURCES];
  typedef std::istream::char_type char_t;
protected:
  int next_available_source() {
    for(int i=0; i < MAX_SOURCES; i++) {
      ALint state = 0;
      alGetSourcei(sources[i], AL_SOURCE_STATE, &state);
      if(state != AL_PLAYING) {
        return i;
      }
    }
    return MAX_SOURCES;
  }

  std::streamsize update_stream(ALuint bid, int si) {
    static char_t data[MAX_BUFFER_SIZE] = { 0 };
    int streamid = streamids[si];
    streams[streamid]->read(data, MAX_BUFFER_SIZE);
    std::streamsize gcount = streams[streamid]->gcount();
    alBufferData(bid, format[si], data, (ALsizei)gcount, freq[si]);
    return gcount;
  }
public:
  al_audio_device() {
  }

  virtual ~al_audio_device() {
    alDeleteSources(MAX_SOURCES, sources);
    alDeleteBuffers(MAX_BUFFERS, buffers);
  }

  virtual void create(AudioInitializer const& with) {
    DEBUG_METHOD();
    ALCdevice* pdev = alcOpenDevice(alcGetString(NULL, ALC_DEFAULT_DEVICE_SPECIFIER));
    const ALint attr[] = {
      ALC_FREQUENCY, 		22050,
      ALC_MONO_SOURCES, 	MAX_SOURCES,
      ALC_REFRESH, 		16,
      ALC_STEREO_SOURCES, 0,
      ALC_SYNC, 			AL_FALSE,
      AL_INVALID
    };
    std::vector<std::string> al_ext;
    string_util::split(alcGetString(pdev, ALC_EXTENSIONS), " ", &al_ext);
    DEBUG_VALUE_AND_TYPE_OF(alcGetString(pdev, ALC_DEVICE_SPECIFIER));
    for(size_t i=0; i < al_ext.size(); i++) {
      DEBUG_VALUE_AND_TYPE_OF(al_ext[i]);
    }


    ALCcontext* ctx = alcCreateContext(pdev, attr);
    alcMakeContextCurrent(ctx);
    if (alcGetError(pdev) != ALC_NO_ERROR) {
      //throw std::string(alGetErrorString(alGetError()));
    }
    alGetError();
    alDopplerVelocity(343.5f);

    alGenSources(MAX_SOURCES, sources);
    alGenBuffers(MAX_BUFFERS, buffers);
  }

  virtual void play(int streamid, real const& gain, real const& pitch) {
    if(!streams[streamid]) {
      return;
    }

    int si 		= next_available_source();
    ALuint sid  = sources[si];
    alSourcei(sid, AL_BUFFER, 0);

    for(int i=0; i < MAX_BUFFERS_PER_SOURCE; i++) {
      int bi 	   = si * MAX_BUFFERS_PER_SOURCE + i;
      ALuint bid = buffers[bi];
      offset[si] += update_stream(bid, si);
    }

    alSourcef(sid, AL_GAIN, gain);
    alSourcef(sid, AL_PITCH, pitch);
    alSourcePlay(sid);
  }

  virtual void stop(int) {

  }

  virtual void pause(int) {

  }

  virtual void update() {
    alcSuspendContext(NULL);
    ALint state, nprocessed;
    ALuint sid;

    for(int i=0; i < MAX_SOURCES; i++) {
      sid = sources[i];
      alGetSourcei(sid, AL_SOURCE_STATE, &state);
      switch(state) {
      case AL_PLAYING:
        alGetSourcei(sid, AL_BUFFERS_PROCESSED, &nprocessed);
        while(nprocessed--) {
          ALuint bid;
          alSourceUnqueueBuffers(sid, 1, &bid);
          offset[i] += update_stream(bid, i);
          alSourceQueueBuffers(sid, 1, &bid);
        }
        break;
      default:
        break;
      }
    }
    alcProcessContext(NULL);
  }

  void resume(int) {

  }

  int register_stream(const char* fn) {
    return -1;
  }

  virtual int registerStream(std::istream*) {
    return -1;
  }

};
}


//QQQ_C_API void QQQ_CALL new_al_audio_device(audio_device** ppDevice, audio_initializer* pInitializer) {
//  *ppDevice = new impl::al_audio_device;
//}

QQQ_C_API void QQQ_CALL hxCreateAudioDevice(IAudioDevice** ppDevice, AudioInitializer* pInitializer) {
    *ppDevice = new impl::al_audio_device;
}

}


