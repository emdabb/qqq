# All qqq libraries require the same includes
include_directories (${qqq_SOURCE_DIR}/jni/qqq ${qqq_BINARY_DIR}/jni/qqq)

# Grab the scripts for dealing with purely static libraries
#include (LocateStaticLibraries)
#include (MergeStaticLibraries)

# add the qqq source code subdirectory
add_subdirectory (vendor)
add_subdirectory (qqq)

