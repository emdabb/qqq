
README
------
* to update submodules do a git-pup
(git pull && git submodule init && git submodule update && git submodule status)

Creating a project (cmake)
------------------------------------------------
* create a sibling directory of qqq called qqq_build
* looks a bit like this:

 /path/to/qqq_src
 /path/to/qqq_build

* cd to qqq_build

* mkdir build.linux (or similar)

* for linux builds: cmake -G"Eclipse CDT4 - Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug ../../qqq_src

* for android build: cmake -G"Eclipse CDT4 - Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug -DCMAKE_TOOLCHAIN_FILE="../cmake/toolchain/android.cmake ../../qqq_src 

* for iOS build: cmake -G"Eclipse CDT4 - Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug -DCMAKE_TOOLCHAIN_FILE="../cmake/toolchain/iOS.cmake ../../qqq_src 
